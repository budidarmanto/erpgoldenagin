/* Form Adjustment Controller */
angular.module('DevplusApp').controller('FormInternalTransferController', ['$rootScope', '$scope', 'settings', 'response', '$http', '$location', '$uibModal', '$filter', function($rootScope, $scope, settings, response, $http, $location, $uibModal, $filter) {

    $scope.response = response;
    $scope.data = response.data.length == 0 ? {} : response.data;
    if(response.action == ''){
      window.location.hash = 'no-access';
    };
    $scope.dataProduct = response.dataProduct;
    $scope.dataComment = response.dataComment;
    $scope.optionTax = response.optionTax;
    $scope.optionWorkflow = response.optionWorkflow;

    // Popup Product
    $scope.showPopupProduct = function(product){

      if(typeof $scope.data.source == 'undefined' || $scope.data.source == null){
        toastr['info']("Pilih gudang asal!", "");
        return;
      }

    var el = $('.page-content');
    console.log(el.attr('style'));
    App.blockUI({
        target: el,
        animate: true,
        overlayColor: 'none'
    });
        
      var modalInstance = $uibModal.open({
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'template/datagrid-product-finder',
        controller: 'ProductFinderController',
        keyboard: false,
        size: 'lg',
        resolve: {
          params: function(){
            return {
              isEditing : (typeof product != 'undefined') ? true : false
            }
          },
          response: function () {
            return $http.get('/product-finder?warehouse_location='+$scope.data.source).then(function(response){
                
            App.unblockUI(el);
              return response.data;
            });
          },
          deps: ['$ocLazyLoad', function($ocLazyLoad) {
              return $ocLazyLoad.load({
                  name: 'DevplusApp',
                  insertBefore: '#ng_load_plugins_before',
                  files: [
                    'js/controllers/ProductFinderController.js'
                  ],
              });
          }]
        }
      });

      modalInstance.result.then(function (data) {
          console.log(data);
        var getformat = function(dt){
          return {
            id: '',
            product: dt.id,
            name: dt.name,
            code: dt.code,
            qty: 0,
            uom: dt.uom,
            status: 0,
            stock: dt.stock_akhir,
            stockUom: dt.uom,
            stockUomName: dt.uom_name,
            description: 'Deskripsi',
            optionUom: dt.optionUom,
          }
        }
        
        $.each(data, function(i, dt) {
          $.ajax({
            method: 'get',
            url: 'general/option-uom?product=' + dt.id,
            success: function (resp) {
              dt['optionUom'] = resp;
              if (typeof product != 'undefined') {
                var idx = $scope.dataProduct.indexOf(product);
                if (idx != -1) {
                  $scope.dataProduct[idx] = getformat(dt);
                }

              } else {
                $scope.dataProduct.push(getformat(dt));
                toastr['success']((data.length) + " Produk ditambahkan!", "Ditambahkan");
              }
              $scope.$apply();
              $scope.validateStock();
            }
          });
        });
      });
    }

    $scope.deleteProduct = function(product){
      $scope.dataProduct.splice($scope.dataProduct.indexOf(product), 1);
    }
    // End Popup Product


    $scope.save = function(data, $event, success){
      var form = $($event.currentTarget).parents('form');

      if(form.valid() && $scope.notEmptyProduct($scope.dataProduct)){

        var statusStock = true;
        for(var x in $scope.dataProduct){
          if(!$scope.dataProduct[x].valid){
            toastr['error']("Stok produk "+$scope.dataProduct[x].name+" tidak tersedia!", "Failed");
            statusStock = false;
          }
        }
        if(!statusStock){
          if(typeof success != 'undefined'){
            success(false);
          }

          return false;
        }

        data.dataProduct = $scope.dataProduct;

        var btn = Ladda.create($event.currentTarget);
        btn.start();
        $($event.currentTarget).parents('.actions').find('button').attr('disabled', true);

        $http.post(response.link, data).then(function(resp){
          // add a little bit delay
          setTimeout(function(){
            if(resp.data.status){
              if(response.action == 'create'){
                $scope.data = {};
                window.location.hash = response.back;
              }
              toastr['success']("Data berhasil disimpan!", "Saved");
              $scope.dataProduct = resp.data.dataProduct;
              $scope.refreshSidebarAlert();
              if(typeof success != 'undefined'){
                success();
              }
            }else{
              if(typeof resp.data.messages != 'undefined' && resp.data.messages != ''){
                toastr['error'](resp.data.messages, "Failed");
              }else{
                toastr['error']("Data gagal disimpan!", "Failed");
              }
            }
            btn.stop();
            $($event.currentTarget).parents('.actions').find('button').removeAttr('disabled');
          }, 1000); // delay 1 sec
        });
      }
    }

    $scope.saveAndReturn = function(data, $event){
      $scope.save(data, $event, function(){
        window.location.hash = response.back;
      });
    }

    $scope.onDelete = function(){
      $scope.data = {};
      $location.url(response.back);
    }

    $scope.getStatus = function(status){

      var workflow = $filter('filter')($scope.optionWorkflow, {
        id: status
      }, true);
      if(workflow.length > 0){
        return workflow[0];
      }

      return [];
    }

    $scope.changeStatus = function(status, $event){
      $scope.confirmChangeStatus(status, $scope.response.optionWorkflow, function(swl){
        $scope.save($scope.data, $event, function(){
          var btn = Ladda.create($event.currentTarget);
          btn.start();

          $http.get($scope.response.back+'/change-status/'+$scope.data.id+'/'+status).then(function(resp){
            setTimeout(function(){
              $scope.$apply(function(){
                  $scope.data.status = status;
                  $scope.dataComment = resp.data.dataComment;
                  $scope.initAccess();
              });
              $scope.refreshSidebarAlert();
              btn.stop();
              swl();
            }, 1000);
          });
        });
      });
    }

    $scope.addComment = function(){
      if($scope.comment.trim() == ''){
        $scope.comment = '';
        return;
      }
      $http.post('general/stockmovement-comment/'+$scope.data.id, {comment: $scope.comment}).then(function(resp){
        $scope.dataComment = resp.data;
        $scope.comment = '';
      });
    }

    $scope.validateQty = function() {

        setTimeout(function(){
            $scope.validateStock();
        }, 1);

        return true;
    };

    $scope.validateStock = function(apl){
      angular.forEach($scope.dataProduct, function(value){
        var status = false;
        if(typeof value.stock != 'undefined' && value.stock != null){
          var uom = $filter('filter')(value.optionUom, {id: value.uom})[0];
          var qty = value.qty * (uom.ratio * 1);
          if(qty <= value.stock){
            status = true;
          }

        }
        value.valid = status;
      });
      
      if(typeof apl === 'undefined' || apl === null || apl === true) {
         $scope.$apply();
      }

    }

    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        setTimeout(function(){
          App.initAjax();
          $scope.validateStock();
        }, 1)

        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = true;
    });

    $scope.changeSelect = function(){
      if($scope.data.source == $scope.data.destination){
        toastr['error']("Lokasi Asal dan tujuan harus berbeda!", "Failed");
      }
      $scope.validateStock(false);
    }

    $scope.initAccess = function(){

      if($scope.data.status != '1IP' && $scope.response.action != 'create'){
        $scope.response.action = 'view';
      }
    }

    $scope.initAccess();
}]);
