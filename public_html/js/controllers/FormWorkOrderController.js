/* Setup blank page controller */
angular.module('DevplusApp')
    .filter('to_trusted', ['$sce', function($sce){
        return function(text) {
            return $sce.trustAsHtml(text);
        };
    }]);

angular.module('DevplusApp').controller('FormWorkOrderController', ['$rootScope', '$scope', 'settings', 'response', '$http', '$location', '$uibModal', '$filter', function($rootScope, $scope, settings, response, $http, $location, $uibModal, $filter) {

    $scope.response = response;
    $scope.data = response.data.length == 0 ? {} : response.data;
    $scope.dataWoPending = response.hasOwnProperty('dataWoPending') ? response.dataWoPending : {};
    
    $scope.getWoDuration = function () {
        var durationUom = $filter('filter')(response.dataOptionDuration, {id: $scope.data.ro_duration_uom});
        return $scope.data.ro_duration + ' ' + durationUom[0].text;
    }
    
    $scope.getWoStatusLabel = function() {
        var status = $filter('filter')(response.dataWoStatus, {id: $scope.data.status});
        var template = '-';
        if(status.length > 0) {
            var statusData = $scope.getStatus(status[0].id);
            var workflowDropdown = '<ul class="dropdown-menu" role="menu" style="right: 0; left: auto;">';
            for(var i = 0; i < statusData.next.length; i++) {
                workflowDropdown += '<li>'+
                            '<a href="javascript:;" onclick="angular.element(this).scope().changeStatus(\'' + $scope.data.id + '\', \''+ statusData.next[i].id + '\');">' + statusData.next[i].text + '</a>';
                        '</li>';
            }
            workflowDropdown += '</ul>';
            template = '<div class="btn-group dropdown-status">'+
                            '<button id="btnStatus" class="btn-status" style="background-color: '+ status[0].color +'" type="button" data-toggle="dropdown">' + 
                                '<span>' + status[0].label +'</span>' + 
                                (statusData.next.length > 0 ? ' <i class="fa fa-angle-down"></i>' : '') +
                            '</button>' +
                            (statusData.next.length > 0 ? workflowDropdown : '') +
                        '</div>';
        }
        
        return template;
    }
    
    $scope.getStatus = function(status){
        var workflow = $filter('filter')(response.dataWoStatus, {
            id: status
        }, true);
        if(workflow.length > 0){
            return workflow[0];
        }

        return [];
    }
    
    $scope.changeStatus = function (status, $event, extra, needPopup) {
        if (typeof extra == 'undefined') {
            extra = '';
        }

        if (typeof needPopup == 'undefined') {
            needPopup = true;
        }

        if (status == '5WD' && $scope.data.next_sequence == null) {
            if(needPopup) {
                $scope.showPopupWODone($scope.data, status);
                return;
            }
        }

        $scope.confirmChangeStatus(status, $scope.response.dataWoStatus, function (swl) {
            $scope.loading = true;
            $http.get('/manufacture/work-order/change-status/' + $scope.data.id + '/' + status + extra).then(function (resp) {
                $scope.loading = false;
                if (resp.data.status === true) {
                    setTimeout(function () {
                        $scope.$apply(function () {
                            $scope.data.status = status;
                        });
                        if (status == '5WD') {
                            window.location = '#/manufacture/work-order/';
                        } else {
                            swl();
                        }
                    }, 0);
                }
                else {
                    if (typeof resp.data.messages != 'undefined' && resp.data.messages != '') {
                        swal({
                            title: 'Gagal',
                            text: resp.data.messages,
                            type: "error",
                            timer: 2000,
                            showConfirmButton: false
                        });
                    } else {
                        swal({
                            title: 'Gagal',
                            text: 'Gagal mengubah status!',
                            type: "error",
                            timer: 2000,
                            showConfirmButton: false
                        });
                    }
                }
                $scope.refreshSidebarAlert();
                btn.stop();
            });
        });
    }
    
    $scope.showPopupWODone = function (row, status) {
        var modalInstance = $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'modalWODone.html',
            controller: 'ModalWODoneCtrl',
            keyboard: false,
            size: 'md',
            resolve: {
                params: function () {
                    return {
                        id: row.id,
                        unit_name: row.uom,
                        status: status,
                        link: '/manufacture/work-order/',
                        data: {}
                    };
                }
            }
        });

        modalInstance.result.then(function (data) {
            var extra = '?';
            for (var i in data) {
                extra += i + '=' + data[i] + '&';
            }
            extra = extra.substring(0, extra.length - 1);
            $scope.changeStatus(status, null, extra, false)
        });
    }

    $scope.updateStatusButton = function(id, status) {
        var status = $filter('filter')(response.dataWoStatus, {id: status});
        var statusCurrent = $filter('filter')(response.dataWoStatus, {id: $scope.oldStatus});
        var dataDropdown = angular.copy($scope.getStatus(statusCurrent[0].id));
        for(var i = 0; i < dataDropdown.next.length; i++) {
            if(dataDropdown.next[i].id === status[0].id) {
                dataDropdown.next.splice(i, 1);
            }
        }
        if(status[0].id !== statusCurrent[0].id) {
            dataDropdown.next.unshift(statusCurrent[0]);
        }
        var itemDropdown = '';
        for(var i = 0; i < dataDropdown.next.length; i++) {
            itemDropdown += '<li>'+
                        '<a href="javascript:;" onclick="angular.element(this).scope().changeStatus(\'' + $scope.data.id + '\', \''+ dataDropdown.next[i].id + '\');">' + dataDropdown.next[i].text + '</a>';
                    '</li>';
        }

        $('#btnStatus span').text(status[0].label);
        $('#btnStatus').css({'background-color': status[0].color});
        $('#btnStatus').parent().find('ul').empty().append(itemDropdown);
        $scope.data.status = status[0].id;
    }
    
    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        setTimeout(function(){
          App.initAjax();
        }, 1)
        
        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;
    });
}]);

angular.module('DevplusApp').controller('ModalWODoneCtrl', ['$scope', '$uibModalInstance', '$filter', 'params', 'FileUploader', '$http', function ($scope, $uibModalInstance, $filter, params, FileUploader, $http) {
    $scope.response = {
        title: 'Work Order Done'
    };
    
    $scope.data = params.data;
    $scope.link = params.link;
    $scope.id = params.id;
    $scope.status = params.status;
    $scope.unit_name = params.unit_name;
    $scope.response.action = '';
    $scope.bomData = [];
    $scope.response.fields = [
        //{name: 'scrap', label: 'Scrap', type: 'number', attributes: {required: '', decimals: 0}},
    ];

    $http.get($scope.link + 'bom-data/' + $scope.id).then(function (resp) {
        console.log(resp);
        if (resp.data.boms.length > 0) {
            $scope.response.fields = [];
            if (resp.data.last_wo) {
                $scope.response.fields = [
                    { name: 'scrap_bb', label: 'Bahan Jadi', type: 'number', attributes: { required: '', decimals: 0 }, unit_name: $scope.unit_name },
                    { name: 'scrap_bj', label: 'Scrap', type: 'number', attributes: { required: '', decimals: 0 }, unit_name: $scope.unit_name },
                ];

            } else {
                $scope.response.fields = [
                    { name: 'scrap_item', label: 'Scrap', type: 'number', attributes: { required: '', decimals: 0 } }
                ];
            }
            for (var i = 0; i < resp.data.boms.length; i++) {
                $scope.bomData.push(resp.data.boms[i].id);
                $scope.response.fields.push({ name: 'qty_consumed_' + resp.data.boms[i].id, label: 'Waste (' + resp.data.boms[i].name + ')', type: 'number', unit_name: resp.data.boms[i].unit_name});
                $scope.data['qty_consumed_' + resp.data.boms[i].id] = 0;
            }
        } else {
            $scope.response.fields.push({ name: 'qty_consumed', label: 'Qty ', type: 'number' });
        }
    });

    $scope.save = function (data, $event) {
        var form = $($event.currentTarget).parents('form');
        for (var i = 0; i < $scope.bomData.length; i++) {
            if (typeof $scope.data['qty_consumed_' + $scope.bomData[i]] != 'undefined') {
                $scope.data['qty_consumed[' + $scope.bomData[i] + ']'] = $scope.data['qty_consumed_' + $scope.bomData[i]];
                delete $scope.data['qty_consumed_' + $scope.bomData[i]];
            }
        }
        if (form.valid()) {
            $uibModalInstance.close($scope.data);
        };

    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }

    setTimeout(function () {
        App.initAjax();
    }, 1);
}]);
