/* Form Adjustment Controller */
angular.module('DevplusApp').controller('FormCashierOrderController', ['$rootScope', '$scope', 'settings', 'response', '$http', '$location', '$uibModal', '$filter', function($rootScope, $scope, settings, response, $http, $location, $uibModal, $filter) {

    $scope.response = response;
    $scope.data = response.data.length == 0 ? {} : response.data;
    if(response.action == ''){
      window.location.hash = 'no-access';
    };
    $scope.dataProduct = response.dataProduct;
    $scope.dataComment = response.dataComment;
    $scope.optionTax = response.optionTax;
    $scope.optionWorkflow = response.optionWorkflow;

    // Popup Product
    $scope.showPopupProduct = function(product){

      var modalInstance = $uibModal.open({
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'template/datagrid-product-finder',
        controller: 'ProductFinderController',
        keyboard: false,
        size: 'lg',
        resolve: {
          params: function(){
            return {
              isEditing : (typeof product != 'undefined') ? true : false
            }
          },
          response: function () {

            return $http.get('/product-finder').then(function(response){
              return response.data;
            });
          },
          deps: ['$ocLazyLoad', function($ocLazyLoad) {
              return $ocLazyLoad.load({
                  name: 'DevplusApp',
                  insertBefore: '#ng_load_plugins_before',
                  files: [
                    'js/controllers/ProductFinderController.js'
                  ],
              });
          }]
        }
      });

      modalInstance.result.then(function (data) {

        var getformat = function(dt){
          return {
            id: '',
            product: dt.id,
            name: dt.name,
            qty: 0,
            uom: dt.uom,
            discount: [],
            price: (dt.optionSupplier.length > 0) ? dt.optionSupplier[0].price : 0,
            tax: '',
            tax_amount: 0,
            status: 0,
            optionUom: dt.optionUom
          }
        }

        if(typeof product != 'undefined'){

          var idx = $scope.dataProduct.indexOf(product);
          if(idx != -1){
            $scope.dataProduct[idx] = getformat(data[0]);
          }

        }else{
          for(var i in data){
            var dt = data[i];
            $scope.dataProduct.push(getformat(dt));
          }

          toastr['success']((data.length)+" Produk ditambahkan!", "Ditambahkan");
        }
      });
    }

    $scope.deleteProduct = function(product){
      $scope.dataProduct.splice($scope.dataProduct.indexOf(product), 1);
    }
    // End Popup Product

    $scope.save = function(data, $event, success){
      var form = $($event.currentTarget).parents('form');

      if(form.valid() && $scope.notEmptyProduct($scope.dataProduct)){
        data.dataProduct = $scope.dataProduct;

        var btn = Ladda.create($event.currentTarget);
        btn.start();
        $($event.currentTarget).parents('.actions').find('button').attr('disabled', true);

        $http.post(response.link, data).then(function(resp){
          // add a little bit delay
          setTimeout(function(){
            if(resp.data.status){
              if(response.action == 'create'){
                $scope.data = {};
                window.location.hash = response.back;
              }
              toastr['success']("Data berhasil disimpan!", "Saved");
              $scope.dataProduct = resp.data.dataProduct;
              $scope.refreshSidebarAlert();
              if(typeof success != 'undefined'){
                success();
              }
            }else{
              toastr['error']("Data gagal disimpan!", "Failed");
            }
            btn.stop();
            $($event.currentTarget).parents('.actions').find('button').removeAttr('disabled');
          }, 1000); // delay 1 sec
        });
      }
    }

    $scope.saveAndReturn = function(data, $event){
      $scope.save(data, $event, function(){
        window.location.hash = response.back;
      });
    }

    $scope.onDelete = function(){
      $scope.data = {};
      $location.url(response.back);
    }

    $scope.getStatus = function(status){

      var workflow = $filter('filter')($scope.optionWorkflow, {
        id: status
      }, true);
      if(workflow.length > 0){
        return workflow[0];
      }

      return [];
    }

    $scope.changeStatus = function(status, $event){
      $scope.confirmChangeStatus(status, $scope.response.optionWorkflow, function(swl){
        var btn = Ladda.create($event.currentTarget);
        btn.start();

        $http.get($scope.response.back+'/change-status/'+$scope.data.id+'/'+status).then(function(resp){
          setTimeout(function(){
            $scope.$apply(function(){
                $scope.data.status = status;
                $scope.dataComment = resp.data.dataComment;
                $scope.initAccess();
            });
            $scope.refreshSidebarAlert();
            btn.stop();
            swl();
          }, 1000);
        });
      });
    }

    $scope.countTotal = function(product){
      var price = 0;
      var discount = 0;
      var tax = 0;
      var subtotal = 0;

      if(product.status != 2){
        price = product.price * product.qty;

        var lastdisc = price;
        var discount = 0;
        angular.forEach(product.discount, function (value, key) {
          var curdisc = lastdisc * (parseFloat(value) / 100);
          discount += curdisc;
          lastdisc -= curdisc;
        });

        priceAfterDiscount = price - discount;
        tax = priceAfterDiscount * (product.tax_amount / 100);
        subtotal = priceAfterDiscount + tax;
      }

      return {
        price: price,
        discount: discount,
        tax: tax,
        subtotal: subtotal
      }
    }

    $scope.getSubTotal = function(product){
      return $scope.countTotal(product).subtotal;
    }

    $scope.getTotal = function(){

      var totalPrice = 0;
      var totalDiscount = 0;
      var totalTax = 0;
      var totalAll = 0;

      angular.forEach($scope.dataProduct, function(product, key){
        var total = $scope.countTotal(product);
        totalPrice += total.price;
        totalDiscount += total.discount;
        totalTax += total.tax;
        totalAll += total.subtotal;
      });

      return {
        price: totalPrice,
        discount: totalDiscount,
        tax: totalTax,
        total: totalAll
      }
    }

    $scope.addComment = function(){
      if($scope.comment.trim() == ''){
        $scope.comment = '';
        return;
      }
      $http.post('general/stockmovement-comment/'+$scope.data.id, {comment: $scope.comment}).then(function(resp){
        $scope.dataComment = resp.data;
        $scope.comment = '';
      });
    }

    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        setTimeout(function(){
          App.initAjax();
        }, 1)

        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = true;
    });

    $scope.initAccess = function(){

      if($scope.response.action != 'create'){
        $scope.response.action = 'view';
      }
    }

    $scope.initAccess();
}]);
