/* Setup blank page controller */
angular.module('DevplusApp').controller('DatagridPajakController', ['$rootScope', '$scope', 'settings', 'response', '$http', '$filter', function($rootScope, $scope, settings, response, $http, $filter) {

    $scope.loading = false;
    $scope.response = response;
    $scope.params = {
      sort: '',
      keyword: '',
      perpage: '',
      page: ''
    };
    $scope.currentLink = angular.copy($scope.response.link);
    $scope.dataWorkflow = $scope.response.dataWorkflow || {};

    $scope.selected = [];
    $scope.$watch('master', function(value) {
        angular.forEach($scope.selected, function(selected) {
            selected.isChecked = value;
        });
    });

    $scope.getCurrentStatus = function(code){

      var status = '';
      var dt = $filter('filter')($scope.dataWorkflow, {
        code: code
      }, true);
      if(dt.length > 0){
        status = dt[0].description;
      }
      return status;

    }

    $scope.getNextWorkflow = function(code){
      var data = [];
      var dt = $filter('filter')($scope.dataWorkflow, {
        code: code
      }, true);
      if(dt.length > 0){
        var next = dt[0].next;
        for(var i in next){
          var dtnext = $filter('filter')($scope.dataWorkflow, {
            code: next[i]
          }, true);
          if(dtnext.length > 0){
            data.push(dtnext[0]);
          }
        }
      }
      return data;
    }

    $scope.allChecked = function() {

      if($scope.response.data && $scope.response.data.length <= 0){
        return false;
      }
        var i;
        for (i = 0; i < $scope.selected.length; ++i) {
            if (!$scope.selected[i].isChecked) {
                return false;
            }
        }
        return true;
    };
    $scope.initSelect = function(data){
      $scope.selected.push({
        isChecked: false,
        value: data.id
      });
    }

    $scope.getSortingStatus = function(column){
      var status = '';
      if(column.sortable){
        status = 'sorting'
        if(column.sorting == 'asc'){
          status = 'sorting_asc';
        }else if(column.sorting == 'desc'){
          status = 'sorting_desc';
        }
      }

      return status;
    }

    $scope.perpageChanged = function(){
      $scope.setParams('perpage', $scope.response.perPage);
    }

    $scope.pageChanged = function(){
      $scope.setParams('page', $scope.response.currentPage);
    }

    $scope.changeStatus = function(data, status){
      $scope.loading = true;
      $http.get('pajak/pajak/changestatus/'+data.id+'/'+status).then(function(resp){
        $scope.loading = false;
        $scope.refresh();
      });
    }

    var keywordTimeout;
    $scope.keywordChanged = function(){
      clearTimeout(keywordTimeout);
      keywordTimeout = setTimeout(function(){
          $scope.setParams('keyword', $scope.keyword);
      }, 500);
    }

    $scope.setParams = function(params, value){
      var queryString = [];
      angular.forEach($scope.params, function(val, key){

        if(params == key){
          $scope.params[key] = value;
        }
        if($scope.params[key] != ''){
          queryString.push(key+'='+$scope.params[key]);
        }
      });
      if(queryString.length > 0){
        queryString = '?'+queryString.join('&');
      }

      $scope.currentLink = $scope.response.link+queryString;
      $scope.refresh();
    }

    $scope.refresh = function(){
      if($scope.loading){
        return;
      }

      $scope.loading = true;
      $scope.selected = [];
      $http.get($scope.currentLink).then(function(resp){
        $scope.loading = false;
        $scope.response = resp.data;
      });
    }

    $scope.onDelete = function(){
      $scope.selected = [];
      $scope.refresh();
    }

    $scope.$on('$viewContentLoaded', function() {

        // initialize core components
        setTimeout(function(){
          App.initAjax();
        }, 1);

        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = true;

    });
}]);
