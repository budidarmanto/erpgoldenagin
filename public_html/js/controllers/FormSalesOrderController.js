/* Setup blank page controller */
angular.module('DevplusApp').controller('FormSalesOrderController', ['$rootScope', '$scope', 'settings', 'response', '$http', '$location', '$uibModal', '$filter', function($rootScope, $scope, settings, response, $http, $location, $uibModal, $filter) {

    $scope.response = response;
    $scope.data = response.data.length == 0 ? {} : response.data;
    if(response.action == ''){
      window.location.hash = 'no-access';
    };
    $scope.dataProduct = response.dataProduct;
    $scope.dataComment = response.dataComment;
    $scope.optionTax = response.optionTax;
    $scope.optionWorkflow = response.optionWorkflow;

    // Popup Product
    $scope.showPopupProduct = function(product){

      var modalInstance = $uibModal.open({
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'template/datagrid-product-finder',
        controller: 'ProductFinderController',
        keyboard: false,
        size: 'lg',
        resolve: {
          params: function(){
            return {
              isEditing : (typeof product != 'undefined') ? true : false
            }
          },
          response: function () {

            return $http.get('/product-finder').then(function(response){
              return response.data;
            });
          },
          deps: ['$ocLazyLoad', function($ocLazyLoad) {
              return $ocLazyLoad.load({
                  name: 'DevplusApp',
                  insertBefore: '#ng_load_plugins_before',
                  files: [
                    'js/controllers/ProductFinderController.js'
                  ],
              });
          }]
        }
      });

      modalInstance.result.then(function (data) {

        var getformat = function(dt){
          return {
            id: '',
            product: dt.id,
            name: dt.name,
            qty: 0,
            uom: dt.uom,
            supplier: '',
            alternative: dt.alternative,
            expected_date: '',
            price: (dt.optionSupplier.length > 0) ? dt.optionSupplier[0].price : '',
            discount: [],
            status: 0,
            tax: '',
            tax_amount: 0,
            optionSupplier: dt.optionSupplier,
            optionUom: dt.optionUom
          }
        }

        if(typeof product != 'undefined'){

          var idx = $scope.dataProduct.indexOf(product);
          if(idx != -1){
            $scope.dataProduct[idx] = getformat(data[0]);
          }

        }else{
          for(var i in data){
            var dt = data[i];
            $scope.dataProduct.push(getformat(dt));
          }

          toastr['success']((data.length)+" Produk ditambahkan!", "Ditambahkan");
        }
      });
    }

    $scope.deleteProduct = function(product){
      $scope.dataProduct.splice($scope.dataProduct.indexOf(product), 1);
    }
    // End Popup Product

    // Popup Supplier
    $scope.showPopupSupplier = function(product){

      var idx = $scope.dataProduct.indexOf(product);
      var modalInstance = $uibModal.open({
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'modalRequestSupplier.html',
        controller: 'ModalRequestSupplierCtrl',
        keyboard: false,
        size: 'md',
        resolve: {
          params: function () {
            return {
              data: (product || {})
            };
          }
        }
      });

      modalInstance.result.then(function (data) {
          $scope.dataProduct[idx].supplier = data.id;
          $scope.dataProduct[idx].price = data.price;
      });
    }
    // End Popup Supplier

    // Popup Discount
    $scope.showPopupDiscount = function(product){

      var idx = $scope.dataProduct.indexOf(product);
      var modalInstance = $uibModal.open({
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'modalRequestDiscount.html',
        controller: 'ModalRequestDiscountCtrl',
        keyboard: false,
        size: 'sm',
        resolve: {
          params: function () {
            return {
              data: (product || {}),
            };
          }
        }
      });

      modalInstance.result.then(function (data) {
        $scope.dataProduct[idx].discount = data;
      });
    }
    // End Popup Discount

    $scope.save = function(data, $event, success){
      var form = $($event.currentTarget).parents('form');

      if(form.valid() && $scope.notEmptyProduct($scope.dataProduct)){
        data.dataProduct = $scope.dataProduct;

        var btn = Ladda.create($event.currentTarget);
        btn.start();
        $($event.currentTarget).parents('.actions').find('button').attr('disabled', true);

        $http.post(response.link, data).then(function(resp){
          // add a little bit delay
          setTimeout(function(){
            if(resp.data.status){
              if(response.action == 'create'){
                $scope.data = {};
                window.location.hash = response.back;
              }
              toastr['success']("Data berhasil disimpan!", "Saved");
              $scope.dataProduct = resp.data.dataProduct;
              $scope.refreshSidebarAlert();
              if(typeof success != 'undefined'){
                success();
              }
            }else{
              toastr['error']("Data gagal disimpan!", "Failed");
            }
            btn.stop();
            $($event.currentTarget).parents('.actions').find('button').removeAttr('disabled');
          }, 1000); // delay 1 sec
        });
      }
    }

    $scope.saveAndReturn = function(data, $event){
      $scope.save(data, $event, function(){
        window.location.hash = response.back;
      });
    }

    $scope.onDelete = function(){
      $scope.data = {};
      $location.url(response.back);
    }

    $scope.getStatus = function(status){

      var workflow = $filter('filter')($scope.optionWorkflow, {
        id: status
      }, true);
      if(workflow.length > 0){
        return workflow[0];
      }

      return [];
    }

    $scope.changeStatus = function(status, $event){
      $scope.confirmChangeStatus(status, $scope.response.optionWorkflow, function(swl){
        $scope.save($scope.data, $event, function(){
          var btn = Ladda.create($event.currentTarget);
          btn.start();

          $http.get($scope.response.back+'/change-status/'+$scope.data.id+'/'+status).then(function(resp){
            setTimeout(function(){
              $scope.$apply(function(){
                  $scope.data.status = status;
                  $scope.initAccess();
                  $scope.dataComment = resp.data.dataComment;
              });
              $scope.refreshSidebarAlert();
              btn.stop();
              swl();
            }, 1000);
          });
        });
      });
    }

    $scope.countTotal = function(product){
      var price = 0;
      var discount = 0;
      var tax = 0;
      var subtotal = 0;

      if(product.status != 2){
        price = product.price * product.qty;

        var lastdisc = price;
        var discount = 0;
        angular.forEach(product.discount, function (value, key) {
          var curdisc = lastdisc * (parseFloat(value) / 100);
          discount += curdisc;
          lastdisc -= curdisc;
        });

        priceAfterDiscount = price - discount;
        tax = priceAfterDiscount * (product.tax_amount / 100);
        subtotal = priceAfterDiscount + tax;
      }

      return {
        price: price,
        discount: discount,
        tax: tax,
        subtotal: subtotal
      }
    }

    $scope.getSubTotal = function(product){
      return $scope.countTotal(product).subtotal;
    }

    $scope.getTotal = function(){

      var totalPrice = 0;
      var totalDiscount = 0;
      var totalTax = 0;
      var totalAll = 0;

      angular.forEach($scope.dataProduct, function(product, key){
        var total = $scope.countTotal(product);
        totalPrice += total.price;
        totalDiscount += total.discount;
        totalTax += total.tax;
        totalAll += total.subtotal;
      });

      return {
        price: totalPrice,
        discount: totalDiscount,
        tax: totalTax,
        total: totalAll
      }
    }

    $scope.showDiscount = function(discount){
      var dtDiscount = [];
      for(var i in discount){
        if(isNaN(discount[i])){
          continue;
        }
        dtDiscount.push(discount[i]+'%');
      }
      return dtDiscount.join('+');
    }

    $scope.saveExpectedDate = function ($data, product) {
      var idx = $scope.dataProduct.indexOf(product);
      var date = moment($data, "DD/MM/YYYY").format('DD/MM/YYYY');
      $scope.dataProduct[idx].expected_date = date;
    }

    $scope.taxChanged = function($data, product){
      var idx = $scope.dataProduct.indexOf(product);
      if($data == null){
        $scope.dataProduct[idx].tax = '';
      }

      var tax = $filter('filter')($scope.optionTax, {
        id: $data
      }, true);
      if(tax.length > 0 && $data != null && $data != ''){
        $scope.dataProduct[idx].tax_amount = tax[0].amount;
      }else{
        $scope.dataProduct[idx].tax_amount = 0;
      }
    }

    $scope.addComment = function(){
      if($scope.comment.trim() == ''){
        $scope.comment = '';
        return;
      }
      $http.post('general/stockmovement-comment/'+$scope.data.id, {comment: $scope.comment}).then(function(resp){
        $scope.dataComment = resp.data;
        $scope.comment = '';
      });
    }

    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        setTimeout(function(){
          App.initAjax();
        }, 1)

        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = true;
    });

    $scope.initAccess = function(){

      if($scope.data.status != '1SR' && $scope.response.action != 'create'){
        $scope.response.action = 'view';
      }
    }

    $scope.initAccess();
}]);



// RequestSupplier Popup Controller
angular.module('DevplusApp').controller('ModalRequestSupplierCtrl', ['$scope', '$uibModalInstance', 'params', function($scope,  $uibModalInstance, params) {
  $scope.response = {
    title: 'Module'
  };
  $scope.data = params.data;
  $scope.optionSupplier = $scope.data.optionSupplier;



  $scope.save = function(data){
    $uibModalInstance.close(data);
  }

  $scope.cancel = function(){
    $uibModalInstance.dismiss('cancel');
  }

  setTimeout(function(){
    App.initAjax();
  }, 1);
}]);
// End RequestSupplier Popup Controller


// RequestSupplier Popup Controller
angular.module('DevplusApp').controller('ModalRequestDiscountCtrl', ['$scope', '$uibModalInstance', 'params', function($scope,  $uibModalInstance, params) {
  $scope.response = {
    title: 'Module'
  };
  $scope.data = params.data;
  $scope.dataDiscount = [];

  for(var i in $scope.data.discount){
    $scope.dataDiscount.push({
      value: $scope.data.discount[i]
    })
  }

  $scope.addDiscount = function(){
    $scope.dataDiscount.push({value : 0});
  }

  $scope.deleteDiscount = function($index){
    $scope.dataDiscount.splice($index, 1);
  }

  $scope.save = function(){
    var dtDiscount = [];
    for(var i in $scope.dataDiscount){
      if(isNaN($scope.dataDiscount[i])){
          dtDiscount.push($scope.dataDiscount[i].value);
      }
    }
    $uibModalInstance.close(dtDiscount);
  }

  $scope.cancel = function(){
    $uibModalInstance.dismiss('cancel');
  }

  setTimeout(function(){
    App.initAjax();
  }, 1);
}]);
// End RequestSupplier Popup Controller
