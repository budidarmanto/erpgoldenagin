/* Setup blank page controller */
angular.module('DevplusApp')
    .filter('to_trusted', ['$sce', function($sce){
        return function(text) {
            return $sce.trustAsHtml(text);
        };
    }]);

angular.module('DevplusApp').controller('FormManufactureOrderController', ['$rootScope', '$scope', 'settings', 'response', '$http', '$location', '$uibModal', '$filter', '$q', '$timeout', function ($rootScope, $scope, settings, response, $http, $location, $uibModal, $filter, $q, $timeout) {

    $scope.loading = false;
    $scope.response = response;
    $scope.data = response.data.length == 0 ? {} : response.data;
    $scope.picName = response.data.hasOwnProperty('picName') ? response.data.picName : '';
    $scope.productName = response.data.hasOwnProperty('productName') ? response.data.productName : '';
    $scope.dataBom = response.dataBom ? response.dataBom : [];
    $scope.dataBomLoading = false;
    $scope.data.status = response.data.status;
    $scope.oldStatus = response.data.status;
    $scope.data.qty = response.data.hasOwnProperty('qty') ? response.data.qty : 0;
    $scope.routingDisabled = response.hasOwnProperty('routingDisabled') ? response.routingDisabled : false;
    $scope.dataBomDataSubMO = response.dataBomDataSubMO ? response.dataBomDataSubMO : [];
    $scope.saveThenToProduct = false;

    if(response.action == ''){
        window.location.hash = 'no-access';
    }
    
    $scope.$watch('data.qty', function(){
        for(var i = 0; i < $scope.dataBom.length; i++) {
            $scope.dataBom[i]['required'] = parseInt($scope.data.qty || 0) * parseInt($scope.dataBom[i]['qty']);
            $scope.dataBom[i]['status'] = $scope.dataBom[i]['stock'] > 0 ? ($scope.dataBom[i]['stock'] >= $scope.dataBom[i]['required'] ? response.bomStatusAvailable : response.bomStatusPartialAvailable) : response.bomStatusNotAvailable;
        }
    });
    
    // Popup User
    $scope.showPopupUser = function(user){
        var modalInstance = $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'template/datagrid-user-finder',
            controller: 'UserFinderController',
            keyboard: false,
            size: 'lg',
            resolve: {
                params: function(){
                    return {
                        isEditing : (typeof user != 'undefined') ? true : false
                    }
                },
                response: function () {
                    return $http.get('/user-finder').then(function(response){
                        return response.data;
                    });
                },
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'DevplusApp',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'js/controllers/UserFinderController.js?v1'
                        ],
                    });
                }]
            }
        });

        modalInstance.result.then(function (data) {
            $scope.data.pic = data.id;
            $scope.picName = data.fullname;
        });
    }
    
    // Popup Product
    $scope.showPopupProduct = function(product, skipCountStock){
        var modalInstance = $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'template/datagrid-product-finder-single',
            controller: 'ProductFinderSingleController',
            keyboard: false,
            size: 'lg',
            resolve: {
                params: function(){
                    return {
                        isEditing : (typeof product != 'undefined') ? true : false
                    }
                },
                response: function () {
                    if (skipCountStock) {
                        skipCountStock = '&skip_count_stock=1';
                    } else {
                        skipCountStock = '';
                    }
                    return $http.get('/product-finder?routing=' + $scope.data.routing +'&master=1'+skipCountStock).then(function(response){
                        return response.data;
                    });
                },
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'DevplusApp',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'js/controllers/ProductFinderSingleController.js?v2'
                        ],
                    });
                }]
            }
        });

        modalInstance.result.then(function (data) {
            var routing = $filter('filter')(response.optionRouting, {id: data.routing});
            if(routing.length > 0) {
                //$scope.data.routing = data.routing;
                $scope.routingDisabled = true;
                setTimeout(function(){
                    $('.bs-select').selectpicker('refresh');
                }, 100);
            }
            $scope.data.product = data.id;
            $scope.productName = data.name
            $scope.getBomData();
        });
    }
    
    
    $scope.getBomData = function(){
        $scope.dataBom = [];
        $scope.dataBomLoading = true;
        if(typeof $scope.data.product != 'undefined'){
            $.get('/manufacture/manufacture-order/bom-data/'+$scope.data.product+'?routing='+$scope.data.routing).then(function(res){
                $scope.$apply(function(){
                    console.log(res);
                    $scope.dataBom = res;
                    for(var i = 0; i < $scope.dataBom.length; i++) {
                        $scope.dataBom[i]['required'] = parseInt($scope.data.qty) * parseInt($scope.dataBom[i]['qty']);
                        $scope.dataBom[i]['status'] = $scope.dataBom[i]['stock'] > 0 ? ($scope.dataBom[i]['stock'] >= $scope.dataBom[i]['required'] ? response.bomStatusAvailable : response.bomStatusPartialAvailable) : response.bomStatusNotAvailable;
                        $scope.dataBom[i]['mo-item'] = 0;
                        //$scope.dataBom[i]['templateSubMo'] = $scope.getStatusSubManufactureOrderItem();
                        //console.log($scope.dataBom[i]['category']);
                        /*if (($scope.dataBom[i]['category']).trim()  == 'Barang Jadi') {
                            $scope.dataSubMOChack = true;    
                        } else {
                            $scope.dataSubMOChack = false;
                        }*/
                        
                    }
                    
                    $scope.dataBomLoading = false;
                });
            });
        }
    }

    $scope.changeRoute = function () {
        $scope.getBomData();
    }

    $scope.createSubMO = function(product) {
        swal({
            title: "Konfirmasi",
            text: "Apa anda ingin menyimpan Manufacture Order ini sebagai draft?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Simpan Sebagai Draft",
            cancelButtonText: "Tidak",
            closeOnConfirm: true,
            closeOnCancel: true
        },
        function (isConfirm) {
            $scope.currentLink = 'manufacture/manufacture-order/create?product=' + product
            if (isConfirm) {
                setTimeout(function () {
                    $scope.saveThenToProduct = product;
                    document.getElementById('form_action_save').click();
                }, 0);
            } else {
                window.location.hash = $scope.currentLink;
                $scope.refresh();
            }
        });
    }

    $scope.refresh = function () {
        $http.get($scope.currentLink).then(function (response) {
            setTimeout(function () {
                $scope.$apply(function () {
                    $scope.loading = false;
                    $scope.response = response;
                    $scope.data = response.data.length == 0 ? {} : response.data;
                    $scope.picName = response.data.hasOwnProperty('picName') ? response.data.picName : '';
                    $scope.productName = response.data.hasOwnProperty('productName') ? response.data.productName : '';
                    $scope.dataBom = response.dataBom ? response.dataBom : [];
                    $scope.data.status = response.data.status;
                    $scope.oldStatus = response.data.status;
                    $scope.data.qty = response.data.hasOwnProperty('qty') ? response.data.qty : 0;
                    $scope.routingDisabled = response.hasOwnProperty('routingDisabled') ? response.routingDisabled : false;
                    $scope.dataBomDataSubMO = response.dataBomDataSubMO ? response.dataBomDataSubMO : [];
                });
                window.location.reload();
            }, 0);
        });
    }

    $scope.getBomDataSubMO = function(product){
        console.log("getBomDataSubMO"); 
        
        if(typeof product != 'undefined'){
            $.get('/manufacture/manufacture-order/bom-data/'+product).then(function(res){
                $scope.$apply(function(){
                    ($scope.dataBomDataSubMO).push(res);
                    console.log("sebelum for"); console.log($scope.dataBomDataSubMO);
                    for(var i = 0; i < $scope.dataBomDataSubMO.length; i++) {
                        for(var j = 0; j < $scope.dataBomDataSubMO[i].length; j++) {
                            console.log("for");
                            console.log($scope.dataBomDataSubMO[i][j]);
                            $scope.dataBomDataSubMO[i][j]['required'] = parseInt($scope.dataBomDataSubMO[i][j]['qty']);
                            $scope.dataBomDataSubMO[i][j]['status'] = 'AV';
                        }
                        
                    }
                });
            });
        }
    }
    
    $scope.getMoStatusLabel = function() {
        var status = $filter('filter')(response.dataMoStatus, {id: $scope.data.status});
        var template = '-';
        if(status.length > 0) {
            var statusData = $scope.getStatus(status[0].id);
            var workflowDropdown = '<ul class="dropdown-menu" role="menu" style="right: 0; left: auto;">';
            for(var i = 0; i < statusData.next.length; i++) {
                workflowDropdown += '<li>'+
                            '<a href="javascript:;" onclick="angular.element(this).scope().changeStatus(\'' + $scope.data.id + '\', \''+ statusData.next[i].id + '\');">' + statusData.next[i].text + '</a>';
                        '</li>';
            }
            workflowDropdown += '</ul>';
            template = '<div class="btn-group dropdown-status">'+
                            '<button id="btnStatus" class="btn-status" style="background-color: '+ status[0].color +'" type="button" data-toggle="dropdown">' + 
                                '<span>' + status[0].label +'</span>' + 
                                (statusData.next.length > 0 && response.isEditing === true === true ? ' <i class="fa fa-angle-down"></i>' : '') +
                            '</button>' +
                            (statusData.next.length > 0 && response.isEditing === true === true ? workflowDropdown : '') +
                        '</div>';
        }
        
        return template;
    }
    
    $scope.getStatus = function(status){
        var workflow = $filter('filter')(response.dataMoStatus, {
            id: status
        }, true);
        if(workflow.length > 0){
            return workflow[0];
        }

        return [];
    }
    
    $scope.changeStatus = function (status, $event) {
        $scope.confirmChangeStatus(status, $scope.response.dataMoStatus, function (swl) {
            var btn = Ladda.create($event.currentTarget);
            btn.start();

            $http.get($scope.response.back + '/change-status/' + $scope.data.id + '/' + status).then(function (resp) {
                if (resp.data.status === true) {
                    setTimeout(function () {
                        $scope.$apply(function () {
                            $scope.data.status = status;
                        });
                        swl();
                    }, 1000);
                }
                else {
                    if (typeof resp.data.messages != 'undefined' && resp.data.messages != '') {
                        swal({
                            title: 'Gagal',
                            text: resp.data.messages,
                            type: "error",
                            timer: 2000,
                            showConfirmButton: false
                        });
                    } else {
                        swal({
                            title: 'Gagal',
                            text: 'Gagal mengubah status!',
                            type: "error",
                            timer: 2000,
                            showConfirmButton: false
                        });
                    }
                }
                $scope.refreshSidebarAlert();
                btn.stop();
            });
        });
    }

    $scope.showPopupStock = function (dataStock) {
        var modalInstance = $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'modalStock.html',
            controller: 'ModalStockCtrl',
            keyboard: false,
            size: 'lg',
            resolve: {
                params: function () {
                    return {
                        data: (dataStock || {})
                    };
                }
            }
        });
    }

    $scope.changeStatusSubMo = function(elm){
        console.log("changeStatusSubMo");
        var tr = $(elm).closest('tr.ng-scope');
        var bomName = tr.find('td#bom-name').text(); console.log(bomName);
        var val = $(elm).text();
        $(elm).closest('div').find('#btn-sub-mo').text(val);
        for(var i = 0; i < $scope.dataBom.length; i++) {
            if (bomName.trim() == ($scope.dataBom[i]['name']).trim()) {
                $scope.dataBom[i]['mo-item'] = 1;
                $scope.getBomDataSubMO($scope.dataBom[i]['product']);
            }            
            
        }
    }

    $scope.tes = function () {
        var html = '<div ng-if="dataSubMOChack" ng-bind-html="getStatusSubManufactureOrderItem() | to_trusted"></div>';
        return html;
    }

    $scope.getStatusSubManufactureOrderItem = function () {
        console.log("getStatusSubManufactureOrderItem kkkk");
        var htmlButtom = '<div class="btn-group dropdown-status">'+
                            '<button id="btn-sub-mo" class="btn-status" type="button" data-toggle="dropdown" style="background-color: orange">Change'+
                                '<i class="fa fa-angle-down" ng-hide=""></i>'+
                            '</button>'+
                            '<ul class="dropdown-menu" role="menu" style="right: 0; left: auto;" ng-hide="">'+
                                '<li id="btn-create-sub-mo"><a href="javascript:;" onclick="angular.element(this).scope().changeStatusSubMo(this);">Create</a></li>'+
                            '</ul>'+
                        '</div>';
        return htmlButtom;
    }
    
    $scope.updateStatusButton = function(id, status) {
        var status = $filter('filter')(response.dataMoStatus, {id: status});
        var statusCurrent = $filter('filter')(response.dataMoStatus, {id: $scope.oldStatus});
        var dataDropdown = angular.copy($scope.getStatus(statusCurrent[0].id));
        for(var i = 0; i < dataDropdown.next.length; i++) {
            if(dataDropdown.next[i].id === status[0].id) {
                dataDropdown.next.splice(i, 1);
            }
        }
        if(status[0].id !== statusCurrent[0].id) {
            dataDropdown.next.unshift(statusCurrent[0]);
        }
        var itemDropdown = '';
        for(var i = 0; i < dataDropdown.next.length; i++) {
            itemDropdown += '<li>'+
                        '<a href="javascript:;" onclick="angular.element(this).scope().changeStatus(\'' + $scope.data.id + '\', \''+ dataDropdown.next[i].id + '\');">' + dataDropdown.next[i].text + '</a>';
                    '</li>';
        }

        $('#btnStatus span').text(status[0].label);
        $('#btnStatus').css({'background-color': status[0].color});
        $('#btnStatus').parent().find('ul').empty().append(itemDropdown);
        $scope.data.status = status[0].id;
    }

    $scope.save = function(data, $event, success){
        var form = $($event.currentTarget).parents('form');
        var dataMo = [];
        //$scope.checkAvailableStock(form);
        if(form.valid()){
            var btn = Ladda.create($event.currentTarget);
            btn.start();
            var loopMo = 0;
            var productMaster = [];
            var routing = [];
            var producProduct = [];
            myArray = [];

			myArray.push({
				code : data.code,
				location : data.location,
				pic : data.pic,
				product: data.product, 
				qty : data.qty,
				routing : data.routing,
				status : data.status,
				dataBom: $scope.dataBom
			});
            console.log(myArray);

            // Creating an empty initial promise that always resolves itself.
            var promise = $q.all([]);

            // Iterating list of items.
            angular.forEach(myArray, function (item) {
              promise = promise.then(function () {
                return $timeout(function () {
                  $http.post(response.link, item);
                }, 2000);
              });
            });

            promise.finally(function () {
                if(response.action == 'create'){
                    $scope.data = {};
                    if($scope.saveThenToProduct) {
                        window.location.hash = $scope.currentLink;
                        $scope.refresh();
                    } else {
                        window.location.hash = response.back;
                        $scope.refreshSidebarAlert();
                    }
                }
                toastr['success']("Data berhasil disimpan!", "Saved");
                if(typeof success != 'undefined'){
                    success();
                }
            });
            /*var i = 0;
            for (var i = 0; i < dataMo.length; ) {
                $($event.currentTarget).parents('.actions').find('button').attr('disabled', true);
                $http.post(response.link, dataMo[i]).then(function(resp){
                    // add a little bit delay
                    setTimeout(function(){
                        if(resp.data.status){
                            if(response.action == 'create'){
                                $scope.data = {};
                                window.location.hash = response.back;
                            }
                            toastr['success']("Data berhasil disimpan!", "Saved");
                            if(typeof success != 'undefined'){
                                success();
                            }
                        }
                        else{
                            if(typeof resp.data.messages != 'undefined' && resp.data.messages != ''){
                                toastr['error'](resp.data.messages, "Failed");
                            }
                            //else{
                                //toastr['error']("Data gagal disimpan!", "Failed");
                            //    toastr['success']("Data berhasil disimpan!", "Saved");
                            //    success();
                           // }
                        }
                        btn.stop();
                        $($event.currentTarget).parents('.actions').find('button').removeAttr('disabled');
                    }, 1000); // delay 1 sec
                }).finally(function(){
                    i++;
                });
            }*/
        }
    }

    $scope.saveAndReturn = function(data, $event){
        $scope.save(data, $event, function(){
            window.location.hash = response.back;
        });
    }

    $scope.onDelete = function(){
        console.log("onDelete");
        $scope.data = {};
        $location.url(response.back);
    }
    
    
    $scope.propertyName = '';
    $scope.reverse = true;

    $scope.sortBy = function(propertyName) {
        $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
        $scope.propertyName = propertyName;
    };

    $scope.getSortClass = function(propertyName){
        var cls = "sorting";
        if(propertyName == this.propertyName){
            if($scope.reverse){
                cls = "sorting_desc";
            }else{
                cls = "sorting_asc";
            }
        }
        return cls;
    };
    
    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        setTimeout(function(){
          App.initAjax();
        }, 1)
        
        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;
    });
}]);

angular.module('DevplusApp').controller('ModalStockCtrl', ['$scope', '$uibModalInstance', 'params', function ($scope, $uibModalInstance, params) {
    $scope.response = {
        title: 'Module'
    };
    $scope.data = params.data;
    $scope.sumStock = 0;
    for (var key in $scope.data) {
        $scope.sumStock += parseFloat($scope.data[key].qty);
    }

    console.log($scope.sumStock);

    console.log($scope.data);

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }

    setTimeout(function () {
        App.initAjax();
    }, 1);
}]);