angular.module('DevplusApp').controller('DatagridWorkOrderController', ['$rootScope', '$scope', 'settings', 'response', '$http', '$filter', '$uibModal', function($rootScope, $scope, settings, response, $http, $filter, $uibModal) {
    $scope.loading = false;
    $scope.response = response;
    $scope.checkToggle = false;
    $scope.optionWorkflow = $scope.response.optionWorkflow;
    $scope.woData = {};
    $scope.showConfirmationStatusChange = true;

    $scope.params = {
        sort: '',
        keyword: '',
        perpage: '',
        page: '',
        from: '',
        to: '',
        workflow: ''
    };
    $scope.queryString = '';
    
    $scope.initDateRangePicker = function () {
        if (!jQuery().daterangepicker) {
            return;
        }
        var from = moment();
        var to = moment();
        $scope.dateFrom = from.format('YYYY-MM-DD');
        $scope.dateTo = to.format('YYYY-MM-DD');
        $scope.chartDate = from.format('YYYY-MM-DD');

        $('#date-range').daterangepicker({
            "ranges": {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "MM/DD/YYYY",
                "separator": " - ",
                "applyLabel": "Apply",
                "cancelLabel": "Cancel",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Custom",
                "daysOfWeek": [
                    "Su",
                    "Mo",
                    "Tu",
                    "We",
                    "Th",
                    "Fr",
                    "Sa"
                ],
                "monthNames": [
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                ],
                "firstDay": 1
            },
            "startDate": from.format('MM/DD/YYYY'),
            "endDate": to.format('MM/DD/YYYY'),
            "opens": ('right'),
        }, function(start, end, label) {
            if ($('#date-range').attr('data-display-range') != '0') {
                $('#date-range span').html(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
            }
        });
        if ($('#date-range').attr('data-display-range') != '0') {
            $('#date-range span').html(from.format('MMM D, YYYY') + ' - ' + to.format('MMM D, YYYY'));
        }

        $('#date-range').show();

        $('#date-range').on('apply.daterangepicker', function(ev, picker) {
            $scope.params.from = picker.startDate.format('YYYY-MM-DD');
            $scope.setParams('to', picker.endDate.format('YYYY-MM-DD'));
        });
    };
    
    $scope.currentLink = angular.copy($scope.response.link);

    $scope.selected = [];
    $scope.allChecked = function() {
        $scope.checkToggle = !$scope.checkToggle;

        if($scope.response.data && $scope.response.data.length <= 0){
            return false;
        }

        var i;
        for (i = 0; i < $scope.selected.length; ++i) {
            $scope.selected[i].isChecked = $scope.checkToggle;
        }
    };

    $scope.initSelect = function(data){
        $scope.selected.push({
            isChecked: false,
            value: data.id
        });
    }

    $scope.getSortingStatus = function(column){
        var status = '';
        if(column.sortable){
            status = 'sorting'
            if(column.sorting == 'asc'){
                status = 'sorting_asc';
            }else if(column.sorting == 'desc'){
                status = 'sorting_desc';
            }
        }

        return status;
    }

    $scope.perpageChanged = function(){
        $scope.setParams('perpage', $scope.response.perPage);
    }

    $scope.pageChanged = function(){
        $scope.setParams('page', $scope.response.currentPage);
    }

    var keywordTimeout;
    $scope.keywordChanged = function(){
        clearTimeout(keywordTimeout);
        keywordTimeout = setTimeout(function(){
            $scope.setParams('keyword', $scope.keyword);
        }, 500);
    }

    $scope.setParams = function(params, value){
        var queryString = [];
        angular.forEach($scope.params, function(val, key){
            if(params == key){
                $scope.params[key] = value;
            }
            if(typeof $scope.params[key] != 'undefined' && $scope.params[key] != '' && $scope.params[key] != null){
                queryString.push(key+'='+$scope.params[key]);
            }
        });
        if(queryString.length > 0){
            queryString = '?'+queryString.join('&');
        }

        $scope.queryString = queryString;
        $scope.currentLink = $scope.response.link+queryString;
        $scope.refresh();
    }

    $scope.refresh = function(){
        if($scope.loading){
            return;
        }

        $scope.loading = true;
        $scope.selected = [];
        $scope.checkToggle = false;
        $http.get($scope.currentLink).then(function(resp){
            $scope.loading = false;
            $scope.response = resp.data;
        });
    }

    $scope.onDelete = function(){
        $scope.selected = [];
        $scope.refresh();
    }

    $scope.$on('$viewContentLoaded', function() {

        // initialize core components
        setTimeout(function(){
          App.initAjax();
        }, 1);

        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;

        $scope.initDateRangePicker();
    });

    $scope.handleDoubleClick = function(id){
        window.location = '#/'+$scope.response.link+'/detail/'+id;
    }

    $scope.workflowChanged = function(){
        $scope.setParams('workflow', $scope.workflow);
    }

    $scope.getStatus = function(status){
        var workflow = $filter('filter')($scope.optionWorkflow, {
            id: status
        }, true);
        if(workflow.length > 0){
            return workflow[0];
        }

        return [];
    }

    $scope.showPopupWODone = function (row, status){
        var modalInstance = $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'modalWODone.html',
            controller: 'ModalWODoneCtrl',
            keyboard: false,
            size: 'md',
            resolve: {
                params: function () {
                    return {
                        id: row.id,
                        unit_name: row.uom,
                        status: status,
                        link: $scope.response.link,
                        data: {}
                    };
                }
            }
        });

        modalInstance.result.then(function (data) {
            var extra = '?';
            for(var i in data) {
                extra += i + '=' + data[i] + '&';
            }
            extra = extra.substring(0, extra.length - 1);
            $scope.changeStatus(row, status, extra, false)
        });
    }

    $scope.changeStatus = function(row, status, extra, needPopup){
        if(typeof extra == 'undefined') {
            extra = '';
        }
        if(typeof needPopup == 'undefined') {
            needPopup = true;
        }
        
        if (status == '5WD' && row.next_sequence == null) {
            if(needPopup) {
                $scope.showPopupWODone(row, status);
                return;
            }
        }
        
        $scope.confirmChangeStatus(status, $scope.response.optionWorkflow, function(swl){
            $scope.loading = true;
            $http.get($scope.response.link + '/change-status/' +row.id+'/'+status + extra).then(function(resp){
                $scope.loading = false;
                if(resp.data.status === true) {
                    swl();
                    $scope.refresh();
                    $scope.refreshSidebarAlert();
                }
                else {
                    if(typeof resp.data.messages != 'undefined' && resp.data.messages != ''){
                        swal({
                            title: 'Gagal',
                            text: resp.data.messages,
                            type: "error",
                            timer: 2000,
                            showConfirmButton: false
                        });
                    }else{
                        swal({
                            title: 'Gagal',
                            text: 'Gagal mengubah status!',
                            type: "error",
                            timer: 2000,
                            showConfirmButton: false
                        });
                    }
                }
            });
        });
    }
}]);


angular.module('DevplusApp').controller('ModalWODoneCtrl', ['$scope', '$uibModalInstance', '$filter', 'params', 'FileUploader', '$http', function($scope,  $uibModalInstance, $filter, params, FileUploader, $http) {
    $scope.response = {
        title: 'Work Order Done'
    };
    
    $scope.data = params.data;
    $scope.link = params.link;
    $scope.id = params.id;
    $scope.status = params.status;
    $scope.unit_name = params.unit_name;
    $scope.response.action = '';
    $scope.bomData = [];
    $scope.response.fields = [
        //{name: 'scrap', label: 'Scrap', type: 'number', attributes: {required: '', decimals: 0}},
    ];
    
    $http.get($scope.link+'/bom-data/' + $scope.id).then(function(resp) {
        if(resp.data.boms.length > 0) {
            $scope.response.fields = [];
            if (resp.data.last_wo) {
                $scope.response.fields = [
                    { name: 'scrap_bb', label: 'Bahan Jadi', type: 'number', attributes: { required: '', decimals: 0 }, unit_name: $scope.unit_name},
                    { name: 'scrap_bj', label: 'Scrap', type: 'number', attributes: { required: '', decimals: 0 }, unit_name: $scope.unit_name},
                ];
                
            } else { 
                $scope.response.fields = [
                    {name: 'scrap_item', label: 'Scrap', type: 'number', attributes: {required: '', decimals: 0}}
                ];
            }
            for(var i = 0; i < resp.data.boms.length; i++) {
                $scope.bomData.push(resp.data.boms[i].id);
                $scope.response.fields.push({ name: 'qty_consumed_' + resp.data.boms[i].id, label: 'Waste (' + resp.data.boms[i].name + ')', type: 'number', unit_name: resp.data.boms[i].unit_name});
                $scope.data['qty_consumed_' + resp.data.boms[i].id] = 0;
            }
        } else {
            $scope.response.fields.push({name: 'qty_consumed', label: 'Qty ', type: 'number'});           
        }
    });

    $scope.save = function(data, $event){
        var form = $($event.currentTarget).parents('form');
        for(var i = 0; i < $scope.bomData.length; i++) {
            if(typeof $scope.data['qty_consumed_' + $scope.bomData[i]] != 'undefined') {
                $scope.data['qty_consumed['+ $scope.bomData[i] + ']'] = $scope.data['qty_consumed_' + $scope.bomData[i]];
                delete $scope.data['qty_consumed_' + $scope.bomData[i]];
            }
        }
        if(form.valid()){
            $uibModalInstance.close($scope.data);
        };

    }

    $scope.cancel = function(){
        $uibModalInstance.dismiss('cancel');
    }

    setTimeout(function(){
        App.initAjax();
    }, 1);
}]);

