/* Setup blank page controller */
angular.module('DevplusApp').controller('FormReturnOrderController', ['$rootScope', '$scope', 'settings', 'response', '$http', '$location', '$uibModal', '$filter', function($rootScope, $scope, settings, response, $http, $location, $uibModal, $filter) {

    $scope.response = response;
    $scope.data = response.data.length == 0 ? {} : response.data;
    if(response.action == ''){
      window.location.hash = 'no-access';
    };
    $scope.dataProduct = response.dataProduct;
    $scope.dataComment = response.dataComment;
    $scope.optionTax = response.optionTax;
    $scope.optionWorkflow = response.optionWorkflow;

    // Popup Product
    $scope.showPopupProduct = function(product){

      var modalInstance = $uibModal.open({
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'template/datagrid-product-finder',
        controller: 'ProductFinderController',
        keyboard: false,
        size: 'lg',
        resolve: {
          params: function(){
            return {
              isEditing : (typeof product != 'undefined') ? true : false
            }
          },
          response: function () {

            return $http.get('/product-finder?supplier='+$scope.data.contact).then(function(response){
              return response.data;
            });
          },
          deps: ['$ocLazyLoad', function($ocLazyLoad) {
              return $ocLazyLoad.load({
                  name: 'DevplusApp',
                  insertBefore: '#ng_load_plugins_before',
                  files: [
                    'js/controllers/ProductFinderController.js'
                  ],
              });
          }]
        }
      });

      modalInstance.result.then(function (data) {

        var getformat = function(dt){
          console.log(dt);
          return {
            id: '',
            product: dt.id,
            name: dt.name,
            qty: 0,
            maxQty: dt.stock,
            uom: dt.uom,
            status: 0,
            optionUom: dt.optionUom
          }
        }

        if(typeof product != 'undefined'){

          var idx = $scope.dataProduct.indexOf(product);
          if(idx != -1){
            $scope.dataProduct[idx] = getformat(data[0]);
          }

        }else{
          for(var i in data){
            var dt = data[i];
            $scope.dataProduct.push(getformat(dt));
          }

          toastr['success']((data.length)+" Produk ditambahkan!", "Ditambahkan");
        }
      });
    }

    $scope.deleteProduct = function(product){
      $scope.dataProduct.splice($scope.dataProduct.indexOf(product), 1);
    }
    // End Popup Product

    // Popup Supplier
    $scope.showPopupSupplier = function(product){

      var idx = $scope.dataProduct.indexOf(product);
      var modalInstance = $uibModal.open({
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'modalRequestSupplier.html',
        controller: 'ModalRequestSupplierCtrl',
        keyboard: false,
        size: 'md',
        resolve: {
          params: function () {
            return {
              data: (product || {})
            };
          }
        }
      });

      modalInstance.result.then(function (data) {
          $scope.dataProduct[idx].supplier = data.id;
          $scope.dataProduct[idx].price = data.price;
      });
    }
    // End Popup Supplier

    $scope.save = function(data, $event, success){
      var form = $($event.currentTarget).parents('form');

      if(form.valid() && $scope.notEmptyProduct($scope.dataProduct)){
        data.dataProduct = $scope.dataProduct;

        var btn = Ladda.create($event.currentTarget);
        btn.start();
        $($event.currentTarget).parents('.actions').find('button').attr('disabled', true);

        $http.post(response.link, data).then(function(resp){
          // add a little bit delay
          setTimeout(function(){
            if(resp.data.status){
              if(response.action == 'create'){
                $scope.data = {};
                window.location.hash = response.back;
              }
              toastr['success']("Data berhasil disimpan!", "Saved");
              $scope.dataProduct = resp.data.dataProduct;
              $scope.refreshSidebarAlert();
              if(typeof success != 'undefined'){
                success();
              }
            }else{
              toastr['error']("Data gagal disimpan!", "Failed");
            }
            btn.stop();
            $($event.currentTarget).parents('.actions').find('button').removeAttr('disabled');
          }, 1000); // delay 1 sec
        });
      }
    }

    $scope.saveAndReturn = function(data, $event){
      $scope.save(data, $event, function(){
        window.location.hash = response.back;
      });
    }

    $scope.onDelete = function(){
      $scope.data = {};
      $location.url(response.back);
    }

    $scope.getStatus = function(status){

      var workflow = $filter('filter')($scope.optionWorkflow, {
        id: status
      }, true);
      if(workflow.length > 0){
        return workflow[0];
      }

      return [];
    }

    $scope.changeStatus = function(status, $event){
      $scope.confirmChangeStatus(status, $scope.response.optionWorkflow, function(swl){
        $scope.save($scope.data, $event, function(){
          var btn = Ladda.create($event.currentTarget);
          btn.start();

          $http.get($scope.response.back+'/change-status/'+$scope.data.id+'/'+status).then(function(resp){
            setTimeout(function(){
              $scope.$apply(function(){
                  $scope.data.status = status;
                  $scope.initAccess();
                  $scope.dataComment = resp.data.dataComment;
              });
              $scope.refreshSidebarAlert();
              btn.stop();
              swl();
            }, 1000);
          });
        });
      });
    }

    $scope.validateQty = function(data, product) {

        if(isNaN(data)){
          return false;
        }

        data = parseFloat(data);

        var idx = $scope.dataProduct.indexOf(product);
        var max = $scope.dataProduct[idx].maxQty;
        max = parseFloat(max);

        if(data > max){
          $scope.dataProduct[idx].qty = max;
          return false;
        }
    };


    $scope.addComment = function(){
      if($scope.comment.trim() == ''){
        $scope.comment = '';
        return;
      }
      $http.post('general/stockmovement-comment/'+$scope.data.id, {comment: $scope.comment}).then(function(resp){
        $scope.dataComment = resp.data;
        $scope.comment = '';
      });
    }

    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        setTimeout(function(){
          App.initAjax();
        }, 1)

        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = true;
    });

    $scope.initAccess = function(){

      if($scope.data.status != '1RP' && $scope.response.action != 'create'){
        $scope.response.action = 'view';
      }
    }

    $scope.initAccess();
}]);



// RequestSupplier Popup Controller
angular.module('DevplusApp').controller('ModalRequestSupplierCtrl', ['$scope', '$uibModalInstance', 'params', function($scope,  $uibModalInstance, params) {
  $scope.response = {
    title: 'Module'
  };
  $scope.data = params.data;
  $scope.optionSupplier = $scope.data.optionSupplier;



  $scope.save = function(data){
    $uibModalInstance.close(data);
  }

  $scope.cancel = function(){
    $uibModalInstance.dismiss('cancel');
  }

  setTimeout(function(){
    App.initAjax();
  }, 1);
}]);
// End RequestSupplier Popup Controller
