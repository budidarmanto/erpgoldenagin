/* Setup blank page controller */
angular.module('DevplusApp').controller('DatagridStockController', ['$rootScope', '$scope', 'settings', 'response', '$http', '$uibModal', '$filter', function($rootScope, $scope, settings, response, $http, $uibModal, $filter) {

    $scope.loading = false;
    $scope.response = response;
    $scope.checkToggle = false;
    $scope.optionWarehouse = response.optionWarehouse;
    $scope.optionWarehouseLocation = [];
    $scope.export_format = 'excel';

    $scope.params = {
      sort: '',
      keyword: '',
      perpage: '',
      page: '',
      category: '',
      type: '',
      warehouse: '',
      warehouse_location: ''
    };
    $scope.currentLink = angular.copy($scope.response.link);
    $scope.queryString = '';

    $scope.selected = [];
    $scope.allChecked = function() {
      $scope.checkToggle = !$scope.checkToggle;

      if($scope.response.data && $scope.response.data.length <= 0){
        return false;
      }

      var i;
      for (i = 0; i < $scope.selected.length; ++i) {
          $scope.selected[i].isChecked = $scope.checkToggle;
      }
    };

    // Popup Product
    $scope.openStockCard = function(product){

      var modalInstance = $uibModal.open({
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'template/stock-card',
        controller: 'StockCardController',
        keyboard: false,
        size: 'lg',
        resolve: {
          params: function(){
            return {

            }
          },
          response: function () {
            var wh = '/';
            if(typeof $scope.warehouse != 'undefined' && $scope.warehouse != null){
              wh += $scope.warehouse;
            }else{
              wh += 'null';
            }

            if(typeof $scope.warehouse_location != 'undefined' && $scope.warehouse_location != null){
              wh += '/'+$scope.warehouse_location;
            }

            return $http.get('/stock-card/'+product+wh).then(function(response){
              return response.data;
            });
          },
          deps: ['$ocLazyLoad', function($ocLazyLoad) {
              return $ocLazyLoad.load({
                  name: 'DevplusApp',
                  insertBefore: '#ng_load_plugins_before',
                  files: [
                    'js/controllers/StockCardController.js?v=3'
                  ],
              });
          }]
        }
      });
    }

    $scope.initSelect = function(data){
      $scope.selected.push({
        isChecked: false,
        value: data.id
      });
    }

    $scope.getSortingStatus = function(column){
      var status = '';
      if(column.sortable){
        status = 'sorting'
        if(column.sorting == 'asc'){
          status = 'sorting_asc';
        }else if(column.sorting == 'desc'){
          status = 'sorting_desc';
        }
      }

      return status;
    }

    $scope.perpageChanged = function(){
      $scope.setParams('perpage', $scope.response.perPage);
    }

    $scope.pageChanged = function(){
      $scope.setParams('page', $scope.response.currentPage);
    }

    var keywordTimeout;
    $scope.keywordChanged = function(){
      clearTimeout(keywordTimeout);
      keywordTimeout = setTimeout(function(){
          $scope.setParams('keyword', $scope.keyword);
      }, 500);
    }

    $scope.setParams = function(params, value){
      var queryString = [];
      angular.forEach($scope.params, function(val, key){

        if(params == key){
          $scope.params[key] = value;
        }
        if(typeof $scope.params[key] != 'undefined' && $scope.params[key] != '' && $scope.params[key] != null){
          queryString.push(key+'='+$scope.params[key]);
        }
      });
      if(queryString.length > 0){
        $scope.queryString = queryString.join('&');
        queryString = '?'+$scope.queryString;

      }

      $scope.queryString = queryString;
      $scope.currentLink = $scope.response.link+queryString;
      $scope.refresh();
    }

    $scope.refresh = function(){
      if($scope.loading){
        return;
      }

      $scope.loading = true;
      $scope.selected = [];
      $scope.checkToggle = false;
      $http.get($scope.currentLink).then(function(resp){
        $scope.loading = false;
        $scope.response = resp.data;
      });
    }

    $scope.onDelete = function(){
      $scope.selected = [];
      $scope.refresh();
    }

    $scope.$on('$viewContentLoaded', function() {

        // initialize core components
        setTimeout(function(){
          App.initAjax();
        }, 1);

        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;

    });

    $scope.categoryChanged = function(){
      $scope.setParams('category', $scope.category);
    }

    $scope.typeChanged = function(){
      $scope.setParams('type', $scope.type);
    }

    $scope.warehouseLocationChanged = function(){
      $scope.setParams('warehouse_location', $scope.warehouse_location);
    }

    $scope.warehouseChanged = function(){

        $scope.params.warehouse_location = null;
        $scope.warehouse_location = null;
        $scope.setParams('warehouse', $scope.warehouse);
        var optWarehouse = $filter('filter')($scope.optionWarehouse, {id: $scope.warehouse});

        if(optWarehouse){
            $scope.optionWarehouseLocation = optWarehouse[0].warehouseLocation;
            if($scope.optionWarehouseLocation){

                $scope.warehouseLocationChanged();

            }

        }

    }
    // setTimeout(function(){
    //   $scope.warehouseChanged();
    // }, 100)




    $scope.handleDoubleClick = function(id){
      window.location = '#/'+$scope.response.modify+'/'+id;
    }

    $scope.showPopupForecast = function(dataForecast){
      var modalInstance = $uibModal.open({
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'modalForecast.html',
        controller: 'ModalForecastCtrl',
        keyboard: false,
        size: 'lg',
        resolve: {
          params: function () {
            return {
              data: (dataForecast || {})
            };
          }
        }
      });
    }

    $scope.showPopupStock = function(dataStock){
      var modalInstance = $uibModal.open({
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'modalStock.html',
        controller: 'ModalStockCtrl',
        keyboard: false,
        size: 'lg',
        resolve: {
          params: function () {
            return {
              data: (dataStock || {})
            };
          }
        }
      });
    }

    $scope.showPopupReserved = function(dataReserved){
      var modalInstance = $uibModal.open({
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'modalReserved.html',
        controller: 'ModalReservedCtrl',
        keyboard: false,
        size: 'lg',
        resolve: {
          params: function () {
            return {
              data: (dataReserved || {})
            };
          }
        }
      });
    }


}]);

angular.module('DevplusApp').controller('ModalStockCtrl', ['$scope', '$uibModalInstance', 'params', function($scope,  $uibModalInstance, params) {
  $scope.response = {
    title: 'Module'
  };
  $scope.data = params.data;
  $scope.sumStock = 0;
  for(var key in $scope.data) {
      $scope.sumStock += parseFloat($scope.data[key].qty);
  }
  
  console.log($scope.sumStock);

  console.log($scope.data);

  $scope.cancel = function(){
    $uibModalInstance.dismiss('cancel');
  }

  setTimeout(function(){
    App.initAjax();
  }, 1);
}]);

angular.module('DevplusApp').controller('ModalForecastCtrl', ['$scope', '$uibModalInstance', 'params', function($scope,  $uibModalInstance, params) {
  $scope.response = {
    title: 'Module'
  };
  $scope.data = params.data;

  console.log($scope.data);

  $scope.cancel = function(){
    $uibModalInstance.dismiss('cancel');
  }

  setTimeout(function(){
    App.initAjax();
  }, 1);
}]);

angular.module('DevplusApp').controller('ModalReservedCtrl', ['$scope', '$uibModalInstance', 'params', function($scope,  $uibModalInstance, params) {
  $scope.response = {
    title: 'Module'
  };
  $scope.data = params.data;

  console.log($scope.data);

  $scope.cancel = function(){
    $uibModalInstance.dismiss('cancel');
  }

  setTimeout(function(){
    App.initAjax();
  }, 1);
}]);
