/* Setup blank page controller */
angular.module('DevplusApp').controller('FormReceiptOrderController', ['$rootScope', '$scope', 'settings', 'response', '$http', '$location', '$uibModal', '$filter', function($rootScope, $scope, settings, response, $http, $location, $uibModal, $filter) {

    $scope.response = response;
    $scope.data = response.data.length == 0 ? {} : response.data;
    if(response.action == ''){
      window.location.hash = 'no-access';
    };
    $scope.dataProduct = response.dataProduct;
    $scope.dataComment = response.dataComment;
    $scope.optionTax = response.optionTax;
    $scope.optionWorkflow = response.optionWorkflow;
    $scope.withPo = response.withPo;

    // Popup Product
    $scope.showPopupProduct = function(product){

      var modalInstance = $uibModal.open({
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'template/datagrid-product-finder',
        controller: 'ProductFinderController',
        keyboard: false,
        size: 'lg',
        resolve: {
          params: function(){
            return {
              isEditing : (typeof product != 'undefined') ? true : false
            }
          },
          response: function () {

            return $http.get('/product-finder').then(function(response){
              return response.data;
            });
          },
          deps: ['$ocLazyLoad', function($ocLazyLoad) {
              return $ocLazyLoad.load({
                  name: 'DevplusApp',
                  insertBefore: '#ng_load_plugins_before',
                  files: [
                    'js/controllers/ProductFinderController.js'
                  ],
              });
          }]
        }
      });

      modalInstance.result.then(function (data) {

        var getformat = function(dt){
          return {
            id: '',
            reference: '',
            product: dt.id,
            name: dt.name,
            qty: 0,
            uom: dt.uom,
            optionUom: dt.optionUom
          }
        }

        if(typeof product != 'undefined'){

          var idx = $scope.dataProduct.indexOf(product);
          if(idx != -1){
            $scope.dataProduct[idx] = getformat(data[0]);
          }

        }else{
          for(var i in data){
            var dt = data[i];
            $scope.dataProduct.push(getformat(dt));
          }

          toastr['success']((data.length)+" Produk ditambahkan!", "Ditambahkan");
        }


      });
    }

    $scope.deleteProduct = function(product){
      $scope.dataProduct.splice($scope.dataProduct.indexOf(product), 1);
    }
    // End Popup Product

    // Popup Reference
    $scope.showPopupReference = function(){

      var modalInstance = $uibModal.open({
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'modalReceiptReference.html',
        controller: 'ModalReceiptReferenceCtrl',
        keyboard: false,
        size: 'lg',
        resolve: {
          response: function () {
            return $http.get('inventory/receipt-order/data-reference').then(function(response){
              return response.data;
            });
          }
        }
      });

      modalInstance.result.then(function (data) {
        $scope.data.reference = data.id;
        $scope.data.reference_code = data.code;

        $http.get('inventory/receipt-order/data-incoming/'+data.id).then(function(response){
          $scope.dataProduct = [];
          angular.forEach(response.data.data, function(value){
            $scope.dataProduct.push({
              id: '',
              reference: value.reference,
              product: value.product,
              name: value.name,
              qty: value.qty,
              maxQty: value.qty,
              uom: value.uom,
              optionUom: value.optionUom
            });
          })
        });

        setTimeout(function(){
          App.initAjax();
        }, 1);
      });
    }
    // End Popup Reference

    // Popup Incoming
    $scope.showPopupIncoming = function(){

      if($scope.data.reference == '' || typeof $scope.data.reference == 'undefined'){
        $scope.showPopupReference();
        return;
      }

      var modalInstance = $uibModal.open({
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'template/datagrid-incoming-popup',
        controller: 'DatagridIncomingPopupController',
        keyboard: false,
        size: 'lg',
        resolve: {
          params: function(){
            return {
              po: $scope.data.reference_code
            }
          },
          response: function () {

            return $http.get('/inventory/incoming/'+$scope.data.reference).then(function(response){
              return response.data;
            });
          },
          deps: ['$ocLazyLoad', function($ocLazyLoad) {
              return $ocLazyLoad.load({
                  name: 'DevplusApp',
                  insertBefore: '#ng_load_plugins_before',
                  files: [
                    'js/controllers/DatagridIncomingPopupController.js'
                  ],
              });
          }]
        }
      });

      modalInstance.result.then(function (data) {
        angular.forEach(data, function(value){

          var searchProd = $filter('filter')($scope.dataProduct, {
            reference: value.id
          }, true);

          if(searchProd.length > 0){
            var idx = $scope.dataProduct.indexOf(searchProd[0]);
            $scope.dataProduct[idx].qty = value.qty;
            $scope.dataProduct[idx].uom = value.uom;
            $scope.dataProduct[idx].optionUom = value.optionUom;
            $scope.dataProduct[idx].product_name = value.product_name;
          }else{
            $scope.dataProduct.push({
              id: '',
              reference: value.id,
              product: value.product,
              name: value.product_name,
              qty: value.qty,
              maxQty: value.qty,
              uom: value.uom,
              optionUom: value.optionUom
            });
          }
        });
      });
    }
    // End Popup Incoming

    $scope.save = function(data, $event, success){
      var form = $($event.currentTarget).parents('form');

      if(form.valid() && $scope.notEmptyProduct($scope.dataProduct)){
        data.dataProduct = $scope.dataProduct;

        var btn = Ladda.create($event.currentTarget);
        btn.start();
        $($event.currentTarget).parents('.actions').find('button').attr('disabled', true);

        $http.post(response.link, data).then(function(resp){
          // add a little bit delay
          setTimeout(function(){
            if(resp.data.status){
              if(response.action == 'create'){
                $scope.data = {};
                window.location.hash = response.back;
              }
              toastr['success']("Data berhasil disimpan!", "Saved");
              $scope.dataProduct = resp.data.dataProduct;
              $scope.refreshSidebarAlert();
              if(typeof success != 'undefined'){
                success();
              }
            }else{
              toastr['error']("Data gagal disimpan!", "Failed");
            }
            btn.stop();
            $($event.currentTarget).parents('.actions').find('button').removeAttr('disabled');
          }, 1000); // delay 1 sec
        });
      }
    }

    $scope.saveAndReturn = function(data, $event){
      $scope.save(data, $event, function(){
        window.location.hash = response.back;
      });
    }

    $scope.onDelete = function(){
      $scope.data = {};
      $location.url(response.back);
    }

    $scope.getStatus = function(status){

      var workflow = $filter('filter')($scope.optionWorkflow, {
        id: status
      }, true);
      if(workflow.length > 0){
        return workflow[0];
      }

      return [];
    }

    $scope.changeStatus = function(status, $event){
      $scope.confirmChangeStatus(status, $scope.response.optionWorkflow, function(swl){
        $scope.save($scope.data, $event, function(){
          var btn = Ladda.create($event.currentTarget);
          btn.start();

          $http.get($scope.response.back+'/change-status/'+$scope.data.id+'/'+status).then(function(resp){
            setTimeout(function(){
              $scope.$apply(function(){
                  $scope.data.status = status;
                  $scope.dataComment = resp.data.dataComment;
              });
              $scope.initAccess();
              $scope.refreshSidebarAlert();
              btn.stop();
              swl();
            }, 1000);
          });
        });
      });
    }

    $scope.validateQty = function(data, product) {

        if(isNaN(data)){
          return false;
        }

        if(!$scope.withPo){
          return true;
        }
        data = parseFloat(data);

        var idx = $scope.dataProduct.indexOf(product);
        var max = $scope.dataProduct[idx].maxQty;
        max = parseFloat(max);

        if(data > max){
          $scope.dataProduct[idx].qty = max;
          return false;
        }
    };

    $scope.addComment = function(){
      if($scope.comment.trim() == ''){
        $scope.comment = '';
        return;
      }
      $http.post('general/stockmovement-comment/'+$scope.data.id, {comment: $scope.comment}).then(function(resp){
        $scope.dataComment = resp.data;
        $scope.comment = '';
      });

    }

    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        setTimeout(function(){
          App.initAjax();
        }, 1)


        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = true;

    });

    $scope.initAccess = function(){

      if($scope.data.status != '1RR' && $scope.response.action != 'create'){
        $scope.response.action = 'view';
      }
    }

    $scope.initAccess();
}]);


// Modal Reference Controller
angular.module('DevplusApp').controller('ModalReceiptReferenceCtrl', ['$scope', '$uibModalInstance', 'response', '$http', function($scope, $uibModalInstance, response, $http) {

    $scope.loading = false;
    $scope.response = response;
    $scope.params = {
      sort: '',
      keyword: '',
      perpage: '',
      page: ''
    };
    $scope.currentLink = angular.copy($scope.response.link);


    $scope.getSortingStatus = function(column){
      var status = '';
      if(column.sortable){
        status = 'sorting'
        if(column.sorting == 'asc'){
          status = 'sorting_asc';
        }else if(column.sorting == 'desc'){
          status = 'sorting_desc';
        }
      }

      return status;
    }

    $scope.perpageChanged = function(){
      $scope.setParams('perpage', $scope.response.perPage);
    }

    $scope.pageChanged = function(){
      $scope.setParams('page', $scope.response.currentPage);
    }

    var keywordTimeout;
    $scope.keywordChanged = function(){
      clearTimeout(keywordTimeout);
      keywordTimeout = setTimeout(function(){
          $scope.setParams('keyword', $scope.keyword);
      }, 500);
    }

    $scope.setParams = function(params, value){
      var queryString = [];
      angular.forEach($scope.params, function(val, key){

        if(params == key){
          $scope.params[key] = value;
        }
        if($scope.params[key] != ''){
          queryString.push(key+'='+$scope.params[key]);
        }
      });
      if(queryString.length > 0){
        queryString = '?'+queryString.join('&');
      }

      $scope.currentLink = $scope.response.link+queryString;
      $scope.refresh();
    }

    $scope.refresh = function(){
      if($scope.loading){
        return;
      }

      $scope.loading = true;
      $http.get($scope.currentLink).then(function(resp){
        $scope.loading = false;
        $scope.response = resp.data;
      });
    }

    $scope.select = function(data){
      $uibModalInstance.close(data);
    }

    $scope.cancel = function(){
      $uibModalInstance.dismiss('cancel');
    }


    $scope.$on('$viewContentLoaded', function() {

        // initialize core components
        setTimeout(function(){
          App.initAjax();
        }, 1);

        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;

    });
}]);
// End Popup Reference
