/* Setup blank page controller */
angular.module('DevplusApp').controller('StockAbnormalController', ['$scope', '$uibModalInstance', 'response', 'params', '$http', '$filter', '$uibModal', function($scope, $uibModalInstance, response, params, $http, $filter, $uibModal) {
  $scope.loading = false;
  $scope.response = response;
  $scope.checkToggle = false;
  $scope.params = {
    sort: '',
    keyword: '',
    perpage: '',
    page: '',
  };
  $scope.currentLink = angular.copy($scope.response.link);
  $scope.queryString = '';

  $scope.selected = [];
  $scope.allChecked = function() {
    $scope.checkToggle = !$scope.checkToggle;

    if($scope.response.data && $scope.response.data.length <= 0){
      return false;
    }

    var i;
    for (i = 0; i < $scope.selected.length; ++i) {
        $scope.selected[i].isChecked = $scope.checkToggle;
    }
  };

  $scope.initSelect = function(data){
    $scope.selected.push({
      isChecked: false,
      value: data.id
    });
  }

  $scope.getSortingStatus = function(column){
    var status = '';
    if(column.sortable){
      status = 'sorting'
      if(column.sorting == 'asc'){
        status = 'sorting_asc';
      }else if(column.sorting == 'desc'){
        status = 'sorting_desc';
      }
    }

    return status;
  }

  $scope.perpageChanged = function(){
    $scope.setParams('perpage', $scope.response.perPage);
  }

  $scope.pageChanged = function(){
    $scope.setParams('page', $scope.response.currentPage);
  }

  $scope.setParams = function(params, value){
    var queryString = [];
    angular.forEach($scope.params, function(val, key){

      if(params == key){
        $scope.params[key] = value;
      }
      if(typeof $scope.params[key] != 'undefined' && $scope.params[key] != '' && $scope.params[key] != null){
        queryString.push(key+'='+$scope.params[key]);
      }
    });
    if(queryString.length > 0){
      $scope.queryString = queryString.join('&');
      queryString = '?'+$scope.queryString;
    }

    $scope.currentLink = $scope.response.link+queryString;
    $scope.refresh();
  }

  $scope.refresh = function(){
    if($scope.loading){
      return;
    }

    $scope.loading = true;
    $scope.selected = [];
    $scope.checkToggle = false;
    $http.get($scope.currentLink).then(function(resp){
      $scope.countStock = 0;
      $scope.loading = false;
      $scope.response = resp.data;
    });
  }

  $scope.onDelete = function(){
    $scope.selected = [];
    $scope.refresh();
  }


  setTimeout(function(){
    // $scope.datestart = '05/09/2017';
    // $scope.dateend = '05/09/2017';
    App.initAjax();

    // var dateNow = new Date();
    // $('#datestart, #dateend').datetimepicker({
    //     defaultDate:dateNow
    // });
  }, 100);

  $scope.handleDoubleClick = function(id){
    window.location = '#/'+$scope.response.modify+'/'+id;
  }
  //
  $scope.cancel = function(){
    $uibModalInstance.dismiss('cancel');
  }
}]);
