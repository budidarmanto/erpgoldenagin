angular.module('DevplusApp')
    .filter('to_trusted', ['$sce', function($sce){
        return function(text) {
            return $sce.trustAsHtml(text);
        };
    }]);

angular.module('DevplusApp').directive('customOnChange', function() {
    return {
        restrict: 'A',
            link: function (scope, element, attrs) {
            var onChangeHandler = scope.$eval(attrs.customOnChange);
            element.on('change', onChangeHandler);
            element.on('$destroy', function() {
                element.off();
            });

        }
    };
});
angular.module('DevplusApp').controller('FormRoutingController', ['$rootScope', '$scope', 'settings', 'response', '$http', '$location', '$uibModal', '$filter', function($rootScope, $scope, settings, response, $http, $location, $uibModal, $filter) {
    $scope.response = response;
    $scope.data = response.data.length == 0 ? {} : response.data;
    $scope.dataOperation = response.dataOperation;
    $scope.lastSequence = response.lastSequence ? response.lastSequence : 0;

    if(response.action == ''){
        window.location.hash = 'no-access';
    }

    $scope.showPopupOperation = function(dataOperation){
        var modalInstance = $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'modalOperation.html',
            controller: 'ModalOperationCtrl',
            keyboard: false,
            size: 'md',
            resolve: {
                params: function () {
                    return {
                        data: (dataOperation || {}),
                        optionWorkCenter: $scope.response.optionWorkCenter,
                        optionDurationUom: $scope.response.optionDurationUom,
                        optionOperationReource: $scope.response.optionOperationReource,
                        currentSequence: $scope.lastSequence + 1,
                        worksheetFile: $scope.getWorksheet(angular.copy(dataOperation)),
                    };
                }
            }
        });

        modalInstance.result.then(function (data) {
            if(dataOperation){
                $scope.dataOperation[$scope.dataOperation.indexOf(dataOperation)] = data;
            }else{
                console.log(data);
                $scope.dataOperation.push(data);
                $scope.lastSequence = data.sequence;
                if($scope.dataOperation.length > 1) {
                    $scope.dataOperation[$scope.dataOperation.length - 2].next_sequence = data.sequence;
                }
            }
        });
    }

    $scope.deleteOperation = function(operation){
        $scope.dataOperation.splice($scope.dataOperation.indexOf(operation), 1);
    }

    $scope.save = function(data, $event, success){
        var form = $($event.currentTarget).parents('form');
        if(form.valid()){

            var encoded = new FormData();
            var dataOperation = $filter('orderBy')($scope.dataOperation, 'sequence');
            for(var i = 0; i < dataOperation.length; i++) {
                dataOperation[i].sequence = i + 1;
                if(dataOperation[i].length >=  i + 2) {
                    dataOperation[i].next_sequence = i + 2;
                };
            }

            for(var i in data) {
                if(data[i] !== null) {
                    encoded.append(i, data[i]);
                }
            }
            for(var i = 0; i < dataOperation.length; i++) {
                if(dataOperation[i].hasOwnProperty('id')) {
                    encoded.append('dataOperation[' + i + '][id]', dataOperation[i].id);
                }
                encoded.append('dataOperation[' + i + '][name]', dataOperation[i].name);
                encoded.append('dataOperation[' + i + '][work_center]', dataOperation[i].work_center);
                encoded.append('dataOperation[' + i + '][description]', dataOperation[i].description);
                encoded.append('dataOperation[' + i + '][duration]', dataOperation[i].duration);
                encoded.append('dataOperation[' + i + '][duration_uom]', dataOperation[i].duration_uom);
                encoded.append('dataOperation[' + i + '][capacity]', dataOperation[i].capacity);
                encoded.append('dataOperation[' + i + '][resource]', dataOperation[i].resource);
                encoded.append('dataOperation[' + i + '][sequence]', dataOperation[i].sequence);
                encoded.append('dataOperation[' + i + '][next_sequence]', dataOperation[i].next_sequence);
                encoded.append('dataOperation[' + i + '][worksheet]', dataOperation[i].worksheet);
                if(dataOperation[i].hasOwnProperty('worksheetFile')) {
                    encoded.append('dataOperation' + i + 'worksheetFile', dataOperation[i].worksheetFile);
                }
            }

            var btn = Ladda.create($event.currentTarget);
            btn.start();
            $($event.currentTarget).parents('.actions').find('button').attr('disabled', true);

            $http.post(response.link, encoded, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).then(function(resp){
                // add a little bit delay
                setTimeout(function(){
                    if(resp.data.status){
                        if(response.action == 'create'){
                            $scope.data = {};
                            window.location.hash = response.back;
                        }
                        toastr['success']("Data berhasil disimpan!", "Saved");
                        if(typeof success != 'undefined'){
                            success();
                        }
                    }else{
                        toastr['error']("Data gagal disimpan!", "Failed");
                    }
                    btn.stop();
                    $($event.currentTarget).parents('.actions').find('button').removeAttr('disabled');
                }, 1000); // delay 1 sec
            });
        }
    }

    $scope.saveAndReturn = function(data, $event){
        $scope.save(data, $event, function(){
            window.location.hash = response.back;
        });
    }

    $scope.onDelete = function(){
        $scope.data = {};
        $location.url(response.back);
    }

    $scope.getWorksheet = function(operation){
        if(typeof operation === 'undefined') {
            return '';
        }
        
        if(typeof operation.link === 'undefined' || operation.link === null) {
            return operation.worksheet;
        }
        else {
            return '<a href="' + operation.link + '" target="_blank">' + operation.worksheet + '</a>';
        }
    }
   
    $scope.getDurationName = function(duration_uom){
        var optFilter = $filter('filter')(response.optionDurationUom, {id: duration_uom});
        if(typeof optFilter != 'undefined' && optFilter.length > 0){
            return optFilter[0].text;
        }
    }
    
    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        setTimeout(function(){
          App.initAjax();
        }, 1);

        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;
    });
}]);

angular.module('DevplusApp').controller('ModalOperationCtrl', ['$scope', '$uibModalInstance', '$filter', 'params', 'FileUploader', function($scope,  $uibModalInstance, $filter, params, FileUploader) {
    $scope.response = {
        title: 'Operasi'
    };
    $scope.data = params.data;
    $scope.optionWorkCenter = params.optionWorkCenter;
    $scope.optionDurationUom = params.optionDurationUom; console.log(params);
    $scope.optionOperationReource = params.optionOperationReource;
    $scope.currentSequence = params.currentSequence;
    if(typeof $scope.data.duration_uom == 'undefined') {
        $scope.data.duration_uom = $scope.optionDurationUom[0].id;
    }
    if(typeof $scope.data.resource == 'undefined') {
        $scope.data.resource = $scope.optionOperationReource[0].id;
    }

    $scope.worksheets = [];
    $scope.worksheetFile = params.worksheetFile;
    
    $scope.response.action = '';
    $scope.response.fields = [
        {name: 'name', label: 'Nama Operasi', type: 'text', attributes: {required: '', maxlength: 100}},
        {name: 'work_center', label: 'Work Center', type: 'select', attributes: {'ng-options': 'item.id as item.text for item in optionWorkCenter', required: ''}},
        {name: 'duration', label: 'Durasi', type: 'number', attributes: {required: ''}},
        {name: 'duration_uom', label: false, type: 'select', attributes: {required: '', 'ng-options': 'item.id as item.text for item in optionDurationUom',}},
        {name: 'description', label: 'Keterangan', type: 'textarea'},
        {name: 'capacity', label: 'Kapasitas', type: 'number', attributes: {required: '', decimals: 0}},
        {name: 'resource', label: false, type: 'select', attributes: {required: '', 'ng-options': 'item.id as item.text for item in optionOperationReource',}},
    ];

    $scope.prepareUpload = function (event) {
        var files = event.target.files;
        for(var i = 0; i < files.length; i++) {
            $scope.worksheets.push(files[i]);
        }
    }

    $scope.save = function(data, $event){
        var form = $($event.currentTarget).parents('form');

        if(form.valid()){
            $scope.data.workcenter_name = $filter('filter')($scope.optionWorkCenter, {
                id: $scope.data.work_center
            })[0].text;
            $scope.data.description = (typeof $scope.data.description == 'undefined') ? '' : $scope.data.description;
            if(!$scope.data.hasOwnProperty('sequence')) {
                $scope.data.sequence = $scope.currentSequence;
                $scope.data.next_sequence = null;
            }
            
            for(var i = 0; i < $scope.worksheets.length; i++) {
                $scope.data['worksheetFile'] = $scope.worksheets[i];
                $scope.data['worksheet'] = $scope.worksheets[i].name;
                break;
            }
            $uibModalInstance.close($scope.data);
        };

    }

    $scope.cancel = function(){
        $uibModalInstance.dismiss('cancel');
    }

    setTimeout(function(){
        App.initAjax();
    }, 1);
}]);
