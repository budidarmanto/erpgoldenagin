/* Setup blank page controller */
angular.module('DevplusApp').controller('ReportStockCardController', ['$rootScope', '$scope', 'settings', 'response', '$http', '$uibModal', '$filter', function ($rootScope, $scope, settings, response, $http, $uibModal, $filter) {

  $scope.loading = false;
  $scope.response = response;
  $scope.checkToggle = false;
  $scope.optionWarehouse = response.optionWarehouse;
  $scope.optionWarehouseLocation = [];
  $scope.export_format = 'excel';

  $scope.params = {
    sort: '',
    keyword: '',
    perpage: '',
    page: '',
    category: '',
    type: '',
    warehouse: '',
    warehouse_location: '',
    datestart: '',
    dateend: ''
  };
  $scope.currentLink = angular.copy($scope.response.link);
  $scope.queryString = '';

  $scope.selected = [];
  $scope.allChecked = function () {
    $scope.checkToggle = !$scope.checkToggle;

    if ($scope.response.data && $scope.response.data.length <= 0) {
      return false;
    }

    var i;
    for (i = 0; i < $scope.selected.length; ++i) {
      $scope.selected[i].isChecked = $scope.checkToggle;
    }
  };


  $scope.initSelect = function (data) {
    $scope.selected.push({
      isChecked: false,
      value: data.id
    });
  }

  $scope.getSortingStatus = function (column) {
    var status = '';
    if (column.sortable) {
      status = 'sorting'
      if (column.sorting == 'asc') {
        status = 'sorting_asc';
      } else if (column.sorting == 'desc') {
        status = 'sorting_desc';
      }
    }

    return status;
  }

  $scope.perpageChanged = function () {
    $scope.setParams('perpage', $scope.response.perPage);
  }

  $scope.pageChanged = function () {
    $scope.setParams('page', $scope.response.currentPage);
  }

  var keywordTimeout;
  $scope.keywordChanged = function () {
    clearTimeout(keywordTimeout);
    keywordTimeout = setTimeout(function () {
      $scope.setParams('keyword', $scope.keyword);
    }, 500);
  }

  $scope.setParams = function (params, value) {
    var queryString = [];
    angular.forEach($scope.params, function (val, key) {

      if (params == key) {
        $scope.params[key] = value;
      }
      if (typeof $scope.params[key] != 'undefined' && $scope.params[key] != '' && $scope.params[key] != null) {
        queryString.push(key + '=' + $scope.params[key]);
      }
    });
    if (queryString.length > 0) {
      $scope.queryString = queryString.join('&');
      queryString = '?' + $scope.queryString;

    }

    $scope.queryString = queryString;
    $scope.currentLink = $scope.response.link + queryString;
    $scope.refresh();
  }

  $scope.refresh = function () {
    if ($scope.loading) {
      return;
    }

    $scope.loading = true;
    $scope.selected = [];
    $scope.checkToggle = false;
    $http.get($scope.currentLink).then(function (resp) {
      $scope.loading = false;
      $scope.response = resp.data;
    });
  }

  $scope.onDelete = function () {
    $scope.selected = [];
    $scope.refresh();
  }

  $scope.$on('$viewContentLoaded', function () {

    // initialize core components
    setTimeout(function () {
      App.initAjax();
    }, 1);

    // set default layout mode
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;

  });

  $scope.categoryChanged = function () {
    $scope.setParams('category', $scope.category);
  }

  $scope.typeChanged = function () {
    $scope.setParams('type', $scope.type);
  }

  $scope.warehouseLocationChanged = function () {
    $scope.setParams('warehouse_location', $scope.warehouse_location);
  }

  $scope.warehouseChanged = function () {

    $scope.params.warehouse_location = null;
    $scope.warehouse_location = null;
    $scope.setParams('warehouse', $scope.warehouse);
    var optWarehouse = $filter('filter')($scope.optionWarehouse, { id: $scope.warehouse });

    if (optWarehouse) {
      $scope.optionWarehouseLocation = optWarehouse[0].warehouseLocation;
      if ($scope.optionWarehouseLocation) {

        $scope.warehouseLocationChanged();

      }

    }

  }

  setTimeout(function () {
    // $scope.datestart = '05/09/2017';
    // $scope.dateend = '05/09/2017';
    App.initAjax();

    // var dateNow = new Date();
    // $('#datestart, #dateend').datetimepicker({
    //     defaultDate:dateNow
    // });

    var date = moment().format("DD/MM/YYYY");

    $('#datestart, #dateend').val(date);
    $scope.datestart = date;
    $scope.dateend = date;
    $scope.setParams('datestart', moment(date, 'DD/MM/YYYY').format('YYYY-MM-DD'));
    $scope.setParams('dateend', moment(date, 'DD/MM/YYYY').format('YYYY-MM-DD'));
    console.log($scope.params);
  }, 0);

  $scope.dateStartChanged = function ($event) {

    var date = '';
    if ($scope.datestart != '') {
      date = moment($scope.datestart, 'DD/MM/YYYY').format('YYYY-MM-DD');
    }

    $scope.setParams('datestart', date);
  }

  $scope.dateEndChanged = function ($event) {
    var date = '';
    if ($scope.dateend != '') {
      date = moment($scope.dateend, 'DD/MM/YYYY').format('YYYY-MM-DD');
    }

    $scope.setParams('dateend', date);
  }

  $scope.handleDoubleClick = function (id) {
    window.location = '#/' + $scope.response.modify + '/' + id;
  }

}]);