/* Setup blank page controller */
angular.module('DevplusApp').controller('ProductFinderController', ['$scope', '$uibModalInstance', 'response', 'params', '$http', '$filter', function($scope, $uibModalInstance, response, params, $http, $filter) {
  $scope.loading = false;
  $scope.response = response;
  $scope.checkToggle = false;
  $scope.isEditing = params.isEditing;
  $scope.optionWarehouse = response.optionWarehouse;
  $scope.optionWarehouseLocation = [];

  $scope.params = {
    sort: '',
    keyword: '',
    perpage: '',
    page: '',
    category: '',
    type: '',
    warehouse_location: '',
    cat: response.defaultCategory
  };
  if(response.supplier != ''){
    $scope.params.supplier = response.supplier;
  }

  $scope.currentLink = angular.copy($scope.response.link);
  $scope.dataTmp = [];
  $scope.selectionOnly = false;

  $scope.selected = [];

  $scope.selectedHasItem = function(dt){
    var select = $filter('filter')($scope.selected, {
      id : dt.id
    }, true);
    if(select.length > 0){
      return true;
    }else{
      return false;
    }
  }

  $scope.clearSelection = function(){
    $scope.selected = [];
    if($scope.selectionOnly){
        $scope.selectionOnly = false;
        $scope.response.data = $scope.dataTmp;
    }
    for(var i in $scope.response.data){
      $scope.response.data[i].selected = false;
    }
  }

  $scope.initChecked = function(){

    for(var i in $scope.response.data){
      var dt = $scope.response.data[i];
      dt.selected = $scope.selectedHasItem(dt);
    }
  }
  $scope.initChecked();


  $scope.allChecked = function() {
    $scope.checkToggle = !$scope.checkToggle;

    if($scope.response.data && $scope.response.data.length <= 0){
      return false;
    }

    for(var i in $scope.response.data){
      var dt = $scope.response.data[i];
      if($scope.checkToggle){
        if(!$scope.selectedHasItem(dt)){
          $scope.selected.push(dt);
        }
        dt.selected = true;
      }else{
        var select = $filter('filter')($scope.selected, {
          id : dt.id
        }, true);
        if(select.length > 0){
          var idx = $scope.selected.indexOf(select[0]);
          $scope.selected.splice(idx, 1);
        }
        dt.selected = false;
      }
    }
  };

  $scope.addSelection = function(dt){

    if(!$scope.selectedHasItem(dt)){
      $scope.selected.push(dt);
    }else{
      var select = $filter('filter')($scope.selected, {
        id : dt.id
      }, true);
      if(select.length > 0){
        var idx = $scope.selected.indexOf(select[0]);
        $scope.selected.splice(idx, 1);
      }
    }
  }

  $scope.isChecked = function(dt){
    if($scope.selected.indexOf(dt) != -1){
      return true;
    }

    return false;
  }


  $scope.getSortingStatus = function(column){
    var status = '';
    if(column.sortable){
      status = 'sorting'
      if(column.sorting == 'asc'){
        status = 'sorting_asc';
      }else if(column.sorting == 'desc'){
        status = 'sorting_desc';
      }
    }

    return status;
  }

  $scope.perpageChanged = function(){
    $scope.setParams('perpage', $scope.response.perPage);
  }

  $scope.pageChanged = function(){
    $scope.setParams('page', $scope.response.currentPage);
  }

  $scope.warehouseLocationChanged = function(){
    $scope.setParams('warehouse_location', $scope.warehouse_location);
  }

  $scope.warehouseChanged = function(){
// console.log($scope.warehouse);
    if($scope.warehouse){
      var optWarehouse = $filter('filter')($scope.optionWarehouse, {id: $scope.warehouse});

      if(optWarehouse){
          $scope.optionWarehouseLocation = optWarehouse[0].warehouseLocation;
          if($scope.optionWarehouseLocation){
              $scope.warehouse_location = $scope.optionWarehouseLocation[0].id;
              $scope.warehouseLocationChanged();

          }

      }
    }

  }
  setTimeout(function(){
    $scope.warehouseChanged();
  }, 100);

  var keywordTimeout;
  $scope.keywordChanged = function(){
    clearTimeout(keywordTimeout);
    keywordTimeout = setTimeout(function(){
        $scope.setParams('keyword', $scope.keyword);
    }, 500);
  }

  $scope.setParams = function(params, value){
    var queryString = [];
    angular.forEach($scope.params, function(val, key){

      if(params == key){
        $scope.params[key] = value;
      }
      if(typeof $scope.params[key] != 'undefined' && $scope.params[key] != '' && $scope.params[key] != null){
        queryString.push(key+'='+$scope.params[key]);
      }
    });
    if(queryString.length > 0){
      queryString = '?'+queryString.join('&');
    }

    $scope.currentLink = $scope.response.link+queryString;
    $scope.refresh();
  }

  $scope.refresh = function(){
    if($scope.loading){
      return;
    }

    $scope.loading = true;
    $scope.checkToggle = false;
    $http.get($scope.currentLink).then(function(resp){
      $scope.loading = false;
      $scope.response = resp.data;
      $scope.initChecked();
    });
  }


  $scope.onlySelected = function(){
    $scope.selectionOnly = true;
    $scope.dataTmp = angular.copy($scope.response.data);
    $scope.response.data = $scope.selected;
  }

  $scope.allData = function(){
    $scope.selectionOnly = false;
    $scope.response.data = $scope.dataTmp;
  }

  $scope.$on('$viewContentLoaded', function() {

      // initialize core components
      setTimeout(function(){
        App.initAjax();

      }, 1);

      // set default layout mode
      $rootScope.settings.layout.pageContentWhite = true;
      $rootScope.settings.layout.pageBodySolid = false;
      $rootScope.settings.layout.pageSidebarClosed = false;

  });

  $scope.category = response.defaultCategory;

  $scope.categoryChanged = function(){
    $scope.setParams('category', $scope.category);
  }

  $scope.typeChanged = function(){
    $scope.setParams('type', $scope.type);
  }

  $scope.addProduct = function(data){
    $uibModalInstance.close([data]);
  }

  $scope.addSelectedProduct = function(){
    $uibModalInstance.close($scope.selected);
  }
  //
  $scope.cancel = function(){
    $uibModalInstance.dismiss('cancel');
  }
}]);
