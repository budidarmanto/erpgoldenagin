/* Setup blank page controller */
angular.module('DevplusApp').controller('StockCardController', ['$scope', '$uibModalInstance', 'response', 'params', '$http', '$filter', '$uibModal', function($scope, $uibModalInstance, response, params, $http, $filter, $uibModal) {
  $scope.loading = false;
  $scope.response = response;
  $scope.checkToggle = false;
  $scope.datestart = new Date().toJSON().slice(0,10).replace(/-/g,'-');
  $scope.dateend = new Date().toJSON().slice(0,10).replace(/-/g,'-');
  
    $scope.openStockAbnormal = function(){
      var modalInstance = $uibModal.open({
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'template/stock-abnormal',
        controller: 'StockAbnormalController',
        keyboard: false,
        size: 'lg',
        resolve: {
          params: function(){
            return {

            }
          },
          response: function () {
            var wh = '/';
            if(typeof $scope.response.params.warehouse != 'undefined' && $scope.response.params.warehouse != null){
              wh += $scope.response.params.warehouse;
            }else{
              wh += 'null';
            }

            if(typeof $scope.response.params.warehouse_location != 'undefined' && $scope.response.params.warehouse_location != null){
              wh += '/'+$scope.response.params.warehouse_location;
            }
            wh += '?datestart=' + $scope.params.datestart + '&dateend=' + $scope.params.dateend;

            return $http.get('/stock-abnormal/'+$scope.response.params.product+wh).then(function(response){
              return response.data;
            });
          },
          deps: ['$ocLazyLoad', function($ocLazyLoad) {
              return $ocLazyLoad.load({
                  name: 'DevplusApp',
                  insertBefore: '#ng_load_plugins_before',
                  files: [
                    'js/controllers/StockAbnormalController.js?v=2'
                  ],
              });
          }]
        }
      });
    }

  $scope.params = {
    sort: '',
    keyword: '',
    perpage: '',
    page: '',
    datestart: '',
    dateend: '',
  };
  $scope.currentLink = angular.copy($scope.response.link);
  $scope.queryString = '';

  $scope.selected = [];
  $scope.allChecked = function() {
    $scope.checkToggle = !$scope.checkToggle;

    if($scope.response.data && $scope.response.data.length <= 0){
      return false;
    }

    var i;
    for (i = 0; i < $scope.selected.length; ++i) {
        $scope.selected[i].isChecked = $scope.checkToggle;
    }
  };

  $scope.initSelect = function(data){
    $scope.selected.push({
      isChecked: false,
      value: data.id
    });
  }

  $scope.getSortingStatus = function(column){
    var status = '';
    if(column.sortable){
      status = 'sorting'
      if(column.sorting == 'asc'){
        status = 'sorting_asc';
      }else if(column.sorting == 'desc'){
        status = 'sorting_desc';
      }
    }

    return status;
  }

  $scope.perpageChanged = function(){
    $scope.setParams('perpage', $scope.response.perPage);
  }

  $scope.pageChanged = function(){
    $scope.setParams('page', $scope.response.currentPage);
  }

  var keywordTimeout;
  $scope.keywordChanged = function(){
    clearTimeout(keywordTimeout);
    keywordTimeout = setTimeout(function(){
        $scope.setParams('keyword', $scope.keyword);
    }, 500);
  }

  $scope.setParams = function(params, value){
    var queryString = [];
    angular.forEach($scope.params, function(val, key){

      if(params == key){
        $scope.params[key] = value;
      }
      if(typeof $scope.params[key] != 'undefined' && $scope.params[key] != '' && $scope.params[key] != null){
        queryString.push(key+'='+$scope.params[key]);
      }
    });
    if(queryString.length > 0){
      $scope.queryString = queryString.join('&');
      queryString = '?'+$scope.queryString;
    }

    $scope.currentLink = $scope.response.link+queryString;
    $scope.refresh();
  }

  $scope.refresh = function(){
    if($scope.loading){
      return;
    }

    $scope.loading = true;
    $scope.selected = [];
    $scope.checkToggle = false;
    $http.get($scope.currentLink).then(function(resp){
      $scope.countStock = 0;
      $scope.loading = false;
      $scope.response = resp.data;
    });
  }

  $scope.setParams('datestart', $scope.datestart);
  $scope.setParams('dateend', $scope.dateend);

  //console.log($scope.queryString);

  $scope.onDelete = function(){
    $scope.selected = [];
    $scope.refresh();
  }


  setTimeout(function(){
    // $scope.datestart = '05/09/2017';
    // $scope.dateend = '05/09/2017';
    App.initAjax();

    // var dateNow = new Date();
    // $('#datestart, #dateend').datetimepicker({
    //     defaultDate:dateNow
    // });

    $('#datestart, #dateend').val(moment().format("DD/MM/YYYY"));
  }, 100);

  $scope.dateStartChanged = function($event){

    var date = '';
    if($scope.datestart != ''){
      date = moment($scope.datestart, 'DD/MM/YYYY').format('YYYY-MM-DD');
    }

    $scope.setParams('datestart', date);
  }

  $scope.dateEndChanged = function($event){
    var date = '';
    if($scope.dateend != ''){
      date = moment($scope.dateend, 'DD/MM/YYYY').format('YYYY-MM-DD');
    }

    $scope.setParams('dateend', date);
  }

  $scope.handleDoubleClick = function(id){
    window.location = '#/'+$scope.response.modify+'/'+id;
  }
  //
  $scope.cancel = function(){
    $uibModalInstance.dismiss('cancel');
  }
}]);
