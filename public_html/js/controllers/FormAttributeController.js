/* Setup blank page controller */
angular.module('DevplusApp').controller('FormAttributeController', ['$rootScope', '$scope', 'settings', 'response', '$http', '$location', '$uibModal', function($rootScope, $scope, settings, response, $http, $location, $uibModal) {

    $scope.response = response;
    $scope.data = response.data.length == 0 ? {} : response.data;
    $scope.dataAttribute = response.dataAttribute;

    if(response.action == ''){
      window.location.hash = 'no-access';
    }

    $scope.showPopupAttribute = function(dataAttribute){

      var modalInstance = $uibModal.open({
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'modalAttribute.html',
        controller: 'ModalAttributeCtrl',
        keyboard: false,
        size: 'md',
        resolve: {
          params: function () {
            return {
              data: (dataAttribute || {})
            };
          }
        }
      });

      modalInstance.result.then(function (data) {
        if(dataAttribute){
          $scope.dataAttribute[$scope.dataAttribute.indexOf(dataAttribute)] = data;
        }else{
          $scope.dataAttribute.push(data);
        }

      });
    }

    $scope.deleteAttribute = function(attribute){
      $scope.dataAttribute.splice($scope.dataAttribute.indexOf(attribute), 1);
    }

    $scope.save = function(data, $event, success){
      var form = $($event.currentTarget).parents('form');

      if(form.valid()){
        data.dataAttribute = $scope.dataAttribute;

        var btn = Ladda.create($event.currentTarget);
        btn.start();
        $($event.currentTarget).parents('.actions').find('button').attr('disabled', true);

        $http.post(response.link, data).then(function(resp){
          // add a little bit delay
          setTimeout(function(){
            if(resp.data.status){
              if(response.action == 'create'){
                $scope.data = {};
                window.location.hash = response.back;
                return;
              }
              toastr['success']("Data berhasil disimpan!", "Saved");
              $scope.dataAttribute = resp.data.dataAttribute;

              if(typeof success != 'undefined'){
                success();
              }
            }else{
              toastr['error']("Data gagal disimpan!", "Failed");
            }
            btn.stop();
            $($event.currentTarget).parents('.actions').find('button').removeAttr('disabled');
          }, 1000); // delay 1 sec
        });
      }
    }

    $scope.saveAndReturn = function(data, $event){
      $scope.save(data, $event, function(){
        window.location.hash = response.back;
      });
    }

    $scope.onDelete = function(){
      $scope.data = {};
      $location.url(response.back);
    }

    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        setTimeout(function(){
          App.initAjax();
        }, 1);

        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;

    });
}]);

angular.module('DevplusApp').controller('ModalAttributeCtrl', ['$scope', '$uibModalInstance', 'params', function($scope,  $uibModalInstance, params) {
  $scope.response = {
    title: 'Module'
  };
  $scope.data = params.data;

  $scope.response.action = '';
  $scope.response.fields = [
    {name: 'name', label: 'Nilai Varian', type: 'text', attributes: {required: '', maxlength: 100}},
  ];


  $scope.save = function(){
    $uibModalInstance.close($scope.data);
  }

  $scope.cancel = function(){
    $uibModalInstance.dismiss('cancel');
  }

  setTimeout(function(){
    App.initAjax();
  }, 100);
}]);
