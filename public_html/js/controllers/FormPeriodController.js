/* Setup blank page controller */
angular.module('DevplusApp').controller('FormPeriodController', ['$rootScope', '$scope', 'settings', 'response', '$http', '$location', '$filter', function($rootScope, $scope, settings, response, $http, $location, $filter) {

    $scope.response = response;
    $scope.data = response.data.length == 0 ? {} : response.data;
    $scope.dataMonth = $scope.data.month || [];
    $scope.dataDetail = response.dataDetail;
    if(response.action == ''){
      window.location.hash = 'no-access';
    }

    if($scope.dataMonth.length <= 0){
          angular.forEach($scope.dataDetail, function(resp){
            $scope.dataMonth.push({
              month : resp.month,
              status : resp.status
          });
        });
    }


    $scope.refresh = function(){
      var statusLive = null;
      angular.forEach($scope.dataMonth, function(value, key){

        if(value.status == 0 && statusLive === null){
          statusLive = true;
        }else if(statusLive !== null){
          statusLive = false;
        }

  // console.log(statusLive);
        $scope.dataMonth[key].statusLive = statusLive;
      });
    }

    $scope.refresh();


    $scope.getMonth = function(data){
      return moment().month(data.month - 1).format('MMMM');
    }


    $scope.changeStatus = function(data){

      $http.get('setting/period/close/'+$scope.data.id+'/'+data.month).then(function(resp){
        var index = $scope.dataMonth.indexOf(data);
        $scope.dataMonth[index].status = 1;

        $scope.refresh();
      });
    };


    $scope.save = function(data, $event, success){
      var form = $($event.currentTarget).parents('form');
      // console.log(data);
      // return;
      if(form.valid()){

        var btn = Ladda.create($event.currentTarget);
        btn.start();
        $($event.currentTarget).parents('.actions').find('button').attr('disabled', true);

        $http.post(response.link, data).then(function(resp){
          // add a little bit delay
          setTimeout(function(){
            if(resp.data.status){
              if(response.action == 'create'){
                $scope.data = {};
                window.location.hash = response.back;
              }
              toastr['success']("Data berhasil disimpan!", "Saved");
              if(typeof success != 'undefined'){
                success();
              }
            }else{
              toastr['error']("Data gagal disimpan!", "Failed");
            }
            btn.stop();
            $($event.currentTarget).parents('.actions').find('button').removeAttr('disabled');
          }, 1000); // delay 1 sec
        });
      }
    }

    $scope.saveAndReturn = function(data, $event){
      $scope.save(data, $event, function(){
        window.location.hash = response.back;
      });
    }

    $scope.onDelete = function(){
      $scope.data = {};
      $location.url(response.back);
    }

    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        setTimeout(function(){
          App.initAjax();
        }, 1)


        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;

    });
}]);
