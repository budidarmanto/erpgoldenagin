/* Setup blank page controller */
angular.module('DevplusApp').controller('FormRequestOrderController', ['$rootScope', '$scope', 'settings', 'response', '$http', '$location', '$uibModal', '$filter', function($rootScope, $scope, settings, response, $http, $location, $uibModal, $filter) {

    $scope.response = response;
    $scope.data = response.data.length == 0 ? {} : response.data;
    if(response.action == ''){
      window.location.hash = 'no-access';
    };
    $scope.dataProduct = response.dataProduct;
    $scope.dataComment = response.dataComment;
    $scope.optionTax = response.optionTax;
    $scope.optionWorkflow = response.optionWorkflow;
    $scope.isPurchaser = $scope.response.isPurchaser;
    $scope.isRequester = $scope.response.isRequester;
    $scope.taxSupplier = response.taxSupplier;

    // Popup Product
    $scope.showPopupProduct = function(product){

      var modalInstance = $uibModal.open({
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'template/datagrid-product-finder',
        controller: 'ProductFinderController',
        keyboard: false,
        size: 'lg',
        resolve: {
          params: function(){
            return {
              isEditing : (typeof product != 'undefined') ? true : false
            }
          },
          response: function () {
            var alternative = '';
            if($scope.isPurchaser){
              product.alternative.push(product.product);
              if(typeof product != 'undefined' && product.alternative.length > 0){
                alternative = '/'+product.alternative.join('.');
              }
            }
            if($scope.data.category == null || typeof $scope.data.category == 'undefined'){
              toastr['warning']("Anda harus memilih Kategori Produk!", "Warning");
              return;
            }

            return $http.get('/product-finder'+alternative+'?cat='+$scope.data.category).then(function(response){

              return response.data;
            });
          },
          deps: ['$ocLazyLoad', function($ocLazyLoad) {
              return $ocLazyLoad.load({
                  name: 'DevplusApp',
                  insertBefore: '#ng_load_plugins_before',
                  files: [
                    'js/controllers/ProductFinderController.js'
                  ],
              });
          }]
        }
      });

      modalInstance.result.then(function (data) {

        var getformat = function(dt){

          var supplier = '';
          var tax = '';
          var taxAmount = 0;
          var discount = [];
          if((dt.optionSupplier.length > 0)){
            var dataSupplier = dt.optionSupplier[0];
            supplier = dataSupplier.id;
            if(dt.optionSupplier[0].pkp == true){
              tax = $scope.taxSupplier.tax;
              taxAmount = $scope.taxSupplier.tax_amount;
            }


            if(dataSupplier.discount != null){
              discount.push(dataSupplier.discount);
            }
            if(dataSupplier.tax != null){
              tax = dataSupplier.tax;
              taxAmount = $filter('filter')($scope.optionTax, {id: dataSupplier.tax})[0].amount;
            }
          }

          return {
            id: '',
            product: dt.id,
            name: dt.name,
            qty: 0,
            uom: dt.uom,
            supplier: supplier,
            alternative: dt.alternative,
            expected_date: moment().add(1, 'days').format('DD/MM/YYYY'),
            price: (dt.optionSupplier.length > 0) ? dt.optionSupplier[0].price : 0,
            discount: discount,
            status: 0,
            tax: tax,
            tax_amount: taxAmount,
            optionSupplier: dt.optionSupplier,
            optionUom: dt.optionUom
          }
        }

        if(typeof product != 'undefined'){

          var idx = $scope.dataProduct.indexOf(product);
          if(idx != -1){
            // = (currentUomRatio * currentQty) / newRatio

            var newProduct = getformat(data[0]);

            var newQty = 0;
            var currentUom = $filter('filter')($scope.dataProduct[idx].optionUom, {
              id: $scope.dataProduct[idx].uom
            }, true);
            if(currentUom.length > 0){
              var newUom = $filter('filter')(newProduct.optionUom, {
                id: newProduct.uom
              }, true);
              if(newUom.length > 0){
                  newQty = (currentUom[0].ratio * $scope.dataProduct[idx].qty) / newUom[0].ratio;
              }

            }

            $scope.dataProduct[idx].product = newProduct.product;
            $scope.dataProduct[idx].name = newProduct.name;
            $scope.dataProduct[idx].qty = newQty;
            $scope.dataProduct[idx].uom = newProduct.uom;
            $scope.dataProduct[idx].supplier = newProduct.supplier;
            $scope.dataProduct[idx].alternative = newProduct.alternative;
            $scope.dataProduct[idx].price = newProduct.price;
            $scope.dataProduct[idx].tax = newProduct.tax;
            $scope.dataProduct[idx].taxAmount = newProduct.taxAmount;
            $scope.dataProduct[idx].optionSupplier = newProduct.optionSupplier;
            $scope.dataProduct[idx].optionUom = newProduct.optionUom;
          }

        }else{
          for(var i in data){
            var dt = data[i];
            $scope.dataProduct.push(getformat(dt));
          }

          toastr['success']((data.length)+" Produk ditambahkan!", "Ditambahkan");
        }


      });
    }

    $scope.deleteProduct = function(product){
      $scope.dataProduct.splice($scope.dataProduct.indexOf(product), 1);
    }
    // End Popup Product

    // Popup Supplier
    $scope.showPopupSupplier = function(product){

      var idx = $scope.dataProduct.indexOf(product);
      var modalInstance = $uibModal.open({
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'modalRequestSupplier.html',
        controller: 'ModalRequestSupplierCtrl',
        keyboard: false,
        size: 'md',
        resolve: {
          params: function () {
            return {
              data: (product || {})
            };
          }
        }
      });

      modalInstance.result.then(function (data) {

          $scope.dataProduct[idx].supplier = data.id;
          $scope.dataProduct[idx].price = data.price;
          var tax = '';
          var taxAmount = 0;
          var discount = [];
          if(data.pkp == true){
            tax = $scope.taxSupplier.tax;
            taxAmount = $scope.taxSupplier.tax_amount;
          }

          if(data.discount != null){
            discount.push(data.discount);
          }
          if(discount.tax != null){
            tax = discount.tax;
            taxAmount = $filter('filter')($scope.optionTax, {id: data.tax})[0].amount;
          }


          $scope.dataProduct[idx].tax = tax;
          $scope.dataProduct[idx].tax_amount = taxAmount;
          $scope.dataProduct[idx].discount = discount;
      });
    }
    // End Popup Supplier

    // Popup Discount
    $scope.showPopupDiscount = function(product){

      var idx = $scope.dataProduct.indexOf(product);
      var modalInstance = $uibModal.open({
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'modalRequestDiscount.html',
        controller: 'ModalRequestDiscountCtrl',
        keyboard: false,
        size: 'sm',
        resolve: {
          params: function () {
            return {
              data: (product || {}),
            };
          }
        }
      });

      modalInstance.result.then(function (data) {
        $scope.dataProduct[idx].discount = data;
      });
    }
    // End Popup Discount


    $scope.save = function(data, $event, success){
      var form = $($event.currentTarget).parents('form');

      if(form.valid() && $scope.notEmptyProduct($scope.dataProduct)){
        data.dataProduct = $scope.dataProduct;

        var btn = Ladda.create($event.currentTarget);
        btn.start();
        $($event.currentTarget).parents('.actions').find('button').attr('disabled', true);

        $http.post(response.link, data).then(function(resp){
          // add a little bit delay
          setTimeout(function(){
            if(resp.data.status){
              if(response.action == 'create'){
                $scope.data = {};
                window.location.hash = response.back;
              }
              toastr['success']("Data berhasil disimpan!", "Saved");
              $scope.dataProduct = resp.data.dataProduct;
              $scope.refreshSidebarAlert();
              if(typeof success != 'undefined'){
                success();
              }
            }else{
              toastr['error']("Data gagal disimpan!", "Failed");
            }
            btn.stop();
            $($event.currentTarget).parents('.actions').find('button').removeAttr('disabled');
          }, 1000); // delay 1 sec
        });
      }
    }

    $scope.saveAndReturn = function(data, $event){
      $scope.save(data, $event, function(){
        window.location.hash = response.back;
      });
    }

    $scope.onDelete = function(){
      $scope.data = {};
      $location.url(response.back);
    }

    $scope.getStatus = function(status){

      var workflow = $filter('filter')($scope.optionWorkflow, {
        id: status
      }, true);
      if(workflow.length > 0){
        if($scope.isRequester && status != '1PC'){
          workflow[0].next = [];
        }
        if(!$scope.isRequester && (status != '2PS' && status != '3PC' && status != '4PQ')){
          workflow[0].next = [];
        }
        return workflow[0];
      }

      return [];
    }

    $scope.changeStatus = function(status, $event){
      $scope.confirmChangeStatus(status, $scope.response.optionWorkflow, function(swl){
        $scope.save($scope.data, $event, function(){
          var btn = Ladda.create($event.currentTarget);
          btn.start();

          $http.get($scope.response.back+'/change-status/'+$scope.data.id+'/'+status).then(function(resp){
            setTimeout(function(){
              $scope.$apply(function(){
                  $scope.data.status = status;
                  $scope.initAccess();
                  $scope.dataComment = resp.data.dataComment;
              });
              $scope.refreshSidebarAlert();
              toastr['success']("Status telah diubah!", "Sukses");
              btn.stop();
              swl();
            }, 1000);
          });
        });
      });
    }

    $scope.countTotal = function(product, isSubtotal){
      var price = 0;
      var discount = 0;
      var tax = 0;
      var subtotal = 0;

      if(product.status != 2 || isSubtotal == true){
        price = product.price * product.qty;

        var lastdisc = price;
        var discount = 0;
        angular.forEach(product.discount, function (value, key) {
          var curdisc = lastdisc * (parseFloat(value) / 100);
          discount += curdisc;
          lastdisc -= curdisc;
        });

        priceAfterDiscount = price - discount;
        tax = priceAfterDiscount * (product.tax_amount / 100);
        subtotal = priceAfterDiscount + tax;
      }

      return {
        price: price,
        discount: discount,
        tax: tax,
        subtotal: subtotal
      }
    }

    $scope.getSubTotal = function(product){
      return $scope.countTotal(product, true).subtotal;
    }

    $scope.getTotal = function(){

      var totalPrice = 0;
      var totalDiscount = 0;
      var totalTax = 0;
      var totalAll = 0;

      angular.forEach($scope.dataProduct, function(product, key){
        var total = $scope.countTotal(product, false);
        totalPrice += total.price;
        totalDiscount += total.discount;
        totalTax += total.tax;
        totalAll += total.subtotal;
      });

      return {
        price: totalPrice,
        discount: totalDiscount,
        tax: totalTax,
        total: totalAll
      }
    }

    $scope.showDiscount = function(discount){
      var dtDiscount = [];
      for(var i in discount){
        if(isNaN(discount[i])){
          continue;
        }
        dtDiscount.push(discount[i]+'%');
      }
      return dtDiscount.join('+');
    }

    $scope.showTotalDiscount = function(product){

      var price = product.price * product.qty;

      var lastdisc = price;
      var discount = 0;

      angular.forEach(product.discount, function (value, key) {
        if(isNaN(value)){
          return;
        }

        var curdisc = lastdisc * (parseFloat(value) / 100);
        discount += curdisc;
        lastdisc -= curdisc;
      });

      return discount;

    }

    $scope.saveExpectedDate = function ($data, product) {
      var idx = $scope.dataProduct.indexOf(product);
      var date = moment($data, "DD/MM/YYYY").format('DD/MM/YYYY');
      $scope.dataProduct[idx].expected_date = date;
    }

    $scope.taxChanged = function($data, product){
      var idx = $scope.dataProduct.indexOf(product);
      if($data == null){
        $scope.dataProduct[idx].tax = '';
      }

      var tax = $filter('filter')($scope.optionTax, {
        id: $data
      }, true);
      if(tax.length > 0 && $data != null && $data != ''){
        $scope.dataProduct[idx].tax_amount = tax[0].amount;
      }else{
        $scope.dataProduct[idx].tax_amount = 0;
      }
    }

    $scope.addComment = function(){
      if($scope.comment.trim() == ''){
        $scope.comment = '';
        return;
      }
      $http.post('general/stockmovement-comment/'+$scope.data.id, {comment: $scope.comment}).then(function(resp){
        $scope.dataComment = resp.data;
        $scope.comment = '';
      });

    }

    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        setTimeout(function(){
          App.initAjax();
        }, 1)


        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = true;

    });

    $scope.resetDataProduct = function(){
      $scope.dataProduct = [];
    }

    $scope.initAccess = function(){

      if($scope.data.status != '1PC' && $scope.data.status != '2PS' && response.action != 'create' && $scope.data.status != '4PQ'){
        $scope.response.action = 'view';
      }

      if($scope.isRequester && $scope.data.status != '1PC' && $scope.response.action != 'create'){
        $scope.response.action = 'view';
      }else if($scope.isPurchaser && ($scope.data.status != '2PS' && $scope.data.status != '4PQ')){
        $scope.response.action = 'view';
      }

    }

    $scope.initAccess();
}]);



// RequestSupplier Popup Controller
angular.module('DevplusApp').controller('ModalRequestSupplierCtrl', ['$scope', '$uibModalInstance', 'params', function($scope,  $uibModalInstance, params) {
  $scope.response = {
    title: 'Module'
  };
  $scope.data = params.data;
  $scope.optionSupplier = $scope.data.optionSupplier;



  $scope.save = function(data){

    $uibModalInstance.close(data);
  }

  $scope.cancel = function(){
    $uibModalInstance.dismiss('cancel');
  }

  setTimeout(function(){
    App.initAjax();
  }, 1);
}]);
// End RequestSupplier Popup Controller


// RequestDiscount Popup Controller
angular.module('DevplusApp').controller('ModalRequestDiscountCtrl', ['$scope', '$uibModalInstance', 'params', function($scope,  $uibModalInstance, params) {
  $scope.response = {
    title: 'Module'
  };
  $scope.data = params.data;
  $scope.dataDiscount = [];

  for(var i in $scope.data.discount){
    $scope.dataDiscount.push({
      value: $scope.data.discount[i]
    })
  }

  $scope.addDiscount = function(){
    $scope.dataDiscount.push({value : 0});
  }

  $scope.deleteDiscount = function($index){
    $scope.dataDiscount.splice($index, 1);
  }

  $scope.save = function(){
    var dtDiscount = [];
    for(var i in $scope.dataDiscount){
      if(isNaN($scope.dataDiscount[i])){
          dtDiscount.push($scope.dataDiscount[i].value);
      }
    }
    $uibModalInstance.close(dtDiscount);
  }

  $scope.cancel = function(){
    $uibModalInstance.dismiss('cancel');
  }

  setTimeout(function(){
    App.initAjax();
  }, 1);
}]);
// End RequestSupplier Popup Controller
