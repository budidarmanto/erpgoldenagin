/* Setup blank page controller */
angular.module('DevplusApp').controller('DatagridIncomingRequestController', ['$rootScope', '$scope', 'settings', 'response', '$http', '$filter', function($rootScope, $scope, settings, response, $http, $filter) {

    $scope.loading = false;
    $scope.response = response;
    $scope.checkToggle = false;

    $scope.params = {
      sort: '',
      keyword: '',
      perpage: '',
      page: '',
      date: '',
    };
    $scope.link = angular.copy($scope.response.link);
    $scope.originalLink = angular.copy($scope.response.link);
    $scope.currentLink = angular.copy($scope.response.link);
    $scope.queryString = '';
    $scope.dataTmp = [];
    $scope.selectionOnly = false;

    $scope.selected = [];

    $scope.selectedHasItem = function(dt){
      var select = $filter('filter')($scope.selected, {
        id : dt.id
      }, true);
      if(select.length > 0){
        return true;
      }else{
        return false;
      }
    }

    $scope.clearSelection = function(){
      $scope.selected = [];
      if($scope.selectionOnly){
          $scope.selectionOnly = false;
          $scope.response.data = $scope.dataTmp;
      }
      for(var i in $scope.response.data){
        $scope.response.data[i].selected = false;
      }
      $scope.setParamsRn();
    }

    $scope.initChecked = function(){

      for(var i in $scope.response.data){
        var dt = $scope.response.data[i];
        dt.selected = $scope.selectedHasItem(dt);
      }
    }
    $scope.initChecked();


    $scope.allChecked = function() {
      $scope.checkToggle = !$scope.checkToggle;

      if($scope.response.data && $scope.response.data.length <= 0){
        return false;
      }

      for(var i in $scope.response.data){
        var dt = $scope.response.data[i];
        if($scope.checkToggle){
          if(!$scope.selectedHasItem(dt)){
            $scope.selected.push(dt);
          }
          dt.selected = true;
        }else{
          var select = $filter('filter')($scope.selected, {
            id : dt.id
          }, true);
          if(select.length > 0){
            var idx = $scope.selected.indexOf(select[0]);
            $scope.selected.splice(idx, 1);
          }
          dt.selected = false;
        }
      }

      $scope.setParamsRn();
    };

    $scope.addSelection = function(dt){

      if(!$scope.selectedHasItem(dt)){
        $scope.selected.push(dt);
      }else{
        var select = $filter('filter')($scope.selected, {
          id : dt.id
        }, true);
        if(select.length > 0){
          var idx = $scope.selected.indexOf(select[0]);
          $scope.selected.splice(idx, 1);
        }
      }

      $scope.setParamsRn(dt);
    }

    $scope.isChecked = function(dt){
      if($scope.selected.indexOf(dt) != -1){
        return true;
      }

      return false;
    }

    $scope.getSortingStatus = function(column){
      var status = '';
      if(column.sortable){
        status = 'sorting'
        if(column.sorting == 'asc'){
          status = 'sorting_asc';
        }else if(column.sorting == 'desc'){
          status = 'sorting_desc';
        }
      }

      return status;
    }

    $scope.perpageChanged = function(){
      $scope.setParams('perpage', $scope.response.perPage);
    }

    $scope.pageChanged = function(){
      $scope.setParams('page', $scope.response.currentPage);
    }

    var keywordTimeout;
    $scope.keywordChanged = function(){
      clearTimeout(keywordTimeout);
      keywordTimeout = setTimeout(function(){
          $scope.setParams('keyword', $scope.keyword);
      }, 500);
    }

    $scope.dateChanged = function($event){
      var date = '';
      if($scope.date != ''){
        date = moment($scope.date, 'DD/MM/YYYY').format('YYYY-MM-DD');
      }

      $scope.setParams('date', date);
    }

    $scope.setParamsRn = function(dt){
      if($scope.selected.length == 1 && typeof dt != 'undefined'){
        $scope.link = $scope.originalLink+'/'+dt.rn;
        $scope.currentLink = $scope.originalLink+'/'+dt.rn+$scope.queryString;
        $scope.refresh();
      }else if($scope.selected.length <= 0){
        $scope.link = $scope.originalLink;
        $scope.currentLink = $scope.originalLink+$scope.queryString;
        $scope.refresh();
      }
    }

    $scope.setParams = function(params, value){
      var queryString = [];
      angular.forEach($scope.params, function(val, key){

        if(params == key){
          $scope.params[key] = value;
        }
        if(typeof $scope.params[key] != 'undefined' && $scope.params[key] != '' && $scope.params[key] != null){
          queryString.push(key+'='+$scope.params[key]);
        }
      });
      if(queryString.length > 0){
        queryString = '?'+queryString.join('&');
      }

      $scope.queryString = queryString;
      $scope.currentLink = $scope.link+queryString;
      $scope.refresh();
    }

    $scope.refresh = function(){
      if($scope.loading){
        return;
      }

      $scope.loading = true;
      $scope.checkToggle = false;
      $http.get($scope.currentLink).then(function(resp){
        $scope.loading = false;
        $scope.response = resp.data;
        $scope.initChecked();
      });
    }

    $scope.onlySelected = function(){
      $scope.selectionOnly = true;
      $scope.dataTmp = angular.copy($scope.response.data);
      $scope.response.data = $scope.selected;
    }

    $scope.allData = function(){
      $scope.selectionOnly = false;
      $scope.response.data = $scope.dataTmp;
    }

    $scope.addTransfer = function(data){
      var link = 'inventory/external-transfer/create/with-rn?q='+data.id;
      window.location.hash = link;
    }

    $scope.addSelectedTransfer = function(){
      var id = [];
      angular.forEach($scope.selected, function(value){
        id.push(value.id);
      });
      var link = 'inventory/external-transfer/create/with-rn?q='+id.join('.');
      window.location.hash = link;
    }

    $scope.$on('$viewContentLoaded', function() {

        // initialize core components
        setTimeout(function(){
          App.initAjax();
        }, 1);

        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;

    });

}]);
