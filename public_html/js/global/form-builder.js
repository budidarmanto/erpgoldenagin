/* Handle Form Builder
*

By default it will read scope.response.fields, if you want to make a custom fields,
you have to declare field attribute in your controller.. there is two way to make a fields
- First (-- Default --, Define fields from response)
you just have to define the name you've already describe in your Server Side Response
it will read scope.response.fields variable
<form-builder field="field_name"></form-builder>

- Second (Define an array data)
$scope.yourVariable = [
  {name: 'field_name', label: 'Your Label', type: 'text', attributes: {required: '', maxlength: 100}},
];
then call in your view write
<form-builder field="field_name" data="yourVariable"></form-builder>

-- Third (Define a single data)
$scope.yourVariable = {
  name: 'field_name',
  label: 'Your Label',
  type: 'text',
  attributes: {required: '', maxlength: 100}
};
then call in your view write
<form-builder data="yourVariable"></form-builder>


ATTRS
field, data, ng-model, form-roup
*/
DevplusApp.directive('formBuilder', function ($http, $compile, $filter, FileUploader, $parse) {
  var buildAttributes = function(attributes){
    if(typeof attributes == 'undefined'){
      return;
    }

    var attrString = [];
    for(var attr in attributes){

      var attrVal = attributes[attr];
      if(typeof attributes[attr] == 'string')
        attributes[attr].trim();
      if(attrVal == ''){
        attrString.push(attr);
      }else{
        attrString.push(attr+'="'+attributes[attr]+'"');
      }
    }
    return (attrString) ? ' '+attrString.join(' ') : '';
  }

  return {
    restrict: 'E',
    link: function (scope, element, attrs) {

      var field;

      if(typeof attrs.data != 'undefined'){

        if(typeof attrs.field != 'undefined'){

          var searchField = $filter('filter')(scope[attrs.data], {
            name : attrs.field
          }, true);
          if(searchField.length <= 0){
            return false;
          }

          field = searchField[0];

        }else{
          field = attrs.data;
        }
      }else {
        // console.log(attrs.field);
        var searchField = $filter('filter')(scope.response.fields, {
          name : attrs.field
        }, true);

        if(searchField.length <= 0){
          return false;
        }

        field = searchField[0];
      }

      if(typeof field == 'undefined'){
        return false;
      }

      if(!field.attributes){
        field.attributes = {};
      }

      var ngModel = 'data.'+field.name;

      if(typeof attrs.ngModel != 'undefined'){
        ngModel = attrs.ngModel;
      }

      var help = '';
      if(field.help){
        help = '<span class="help-block">'+field.help+'</span>';
      }

      var template = '';
      var formGroup = {
        open : '<div class="form-group form-md-line-input form-md-floating-label">',
        close : '</div>'
      };

      var formClass = "";
      if(field.attributes.class){
        formClass = field.attributes.class+' ';
      }

      if(field.type == 'multiple'){
        field.attributes.multiple = 'multiple';
      }

      var label = field.label ? '<label>'+field.label+'</label>' : '';

      // Handle File Upload
      scope.deleteFile = function(path, ngModel){

        var file = path+'/'+scope.$eval(ngModel);
        $http.post('remove-upload', {file: file}).then(function(res){
          var model = $parse(ngModel);
          model.assign(scope, '');
        })
      }

      var uploaderName = '';
      if(field.type == 'image'){
        uploaderName = 'uploader'+field.name;
        var moveTo = (typeof field.attributes.moveTo != 'undefined') ? field.attributes.moveTo : '';

        (function(){
          var uploader = scope[uploaderName] = new FileUploader(
          {
              url: 'upload',
          });

          uploader.onAfterAddingFile = function(fileItem)
          {
              if(moveTo != ''){
                fileItem.formData.push({moveTo: moveTo});
              }
              fileItem.upload();
          };

          uploader.onSuccessItem = function(fileItem, response, status, headers)
          {
            var model = $parse(ngModel);
            model.assign(scope, response.fileName);
            element.find('.fileinput-preview img').attr('src', moveTo+'/'+response.fileName)
          };
        })();

      }

      if(typeof attrs.formGroup != 'undefined' && attrs.formGroup == 'false'){
        formGroup.open = '';
        formGroup.close = '';
      }

      var disabled = (typeof field.attributes.disabled != 'undefined') ? field.attributes.disabled : 'ng-disabled="response.action==\'view\'"';

      var ngValue;

      if(typeof attrs.ngModel != 'undefined'){
        ngValue = scope[attrs.ngModel];
      }else{
        ngValue = scope.data[field.name];
      }

      var fieldAttr = '';
      switch (field.type){
        case 'text' :
        case 'password' :
        case 'email' :
        case 'password-strength':
                      fieldAttr = buildAttributes(field.attributes);

                      if(field.type == 'password-strength'){
                        formGroup.open = '<div class="form-group form-md-line-input form-md-floating-label last password-strength">';
                      }
                      template = formGroup.open+'<input type="'+field.type+'" name="'+field.name+'" ng-model="'+ngModel+'"'+fieldAttr+' class="input-sm form-control '+formClass+'" '+disabled+'/>'+label+help+formGroup.close;
                      break;
        case 'date' :
                      fieldAttr = buildAttributes(field.attributes);

                      template = formGroup.open+'<input type="text" name="'+field.name+'"'+fieldAttr+' class="input-sm form-control date-picker '+formClass+'" '+disabled+' ng-value="'+ngModel+' | date: \'dd/MM/yyyy\'" />'+label+help+formGroup.close;
                      break;
        case 'number' :
                      fieldAttr = buildAttributes(field.attributes);

                      template = formGroup.open+'<input type="text" name="'+field.name+'" ng-model="'+ngModel+'"'+fieldAttr+' class="input-sm form-control '+formClass+'" '+disabled+' number-format decimals="2"/>'+label+help+formGroup.close;
                      break;
        case 'switch' :
                      fieldAttr = buildAttributes(field.attributes);
                      // if create, default value = true
                      var checked = '';
                      if(scope.response.action == 'create'){
                        checked = ' ng-init="'+ngModel+'=true"';
                      }

                      formGroup.open = '<div class="form-group">';
                      template = formGroup.open+'<div class="togglebutton">'+label+' &nbsp;&nbsp<label><input type="checkbox" name="'+field.name+'" ng-model="'+ngModel+'"'+fieldAttr+' '+disabled+checked+'><span class="toggle"></span></label></div>'+formGroup.close;
                      break
        case 'select' :
        case 'multiple' :
        case 'select-ajax' :
        // Searchable add attribute {data-live-search : true}
                      var defaultOption;
                      if(typeof field.attributes['default-option'] != 'undefined'){
                        defaultOption = '<option value="">'+field.attributes['default-option']+'</option>';
                      }else{
                        defaultOption = '<option value="" style="display: none;"></option>';
                      }
                      fieldAttr = buildAttributes(field.attributes);

                      var classes = (field.type == 'select-ajax') ? 'selectpicker-ajax' : 'selectpicker';

                      formGroup.open = '<div class="form-group form-md-line-input">';
                      template = formGroup.open+'<select name="'+field.name+'" ng-model="'+ngModel+'"'+fieldAttr+' class="form-control '+formClass+' '+classes+' bs-select" '+disabled+'>'+defaultOption+'</select>'+label+help+formGroup.close;

                      break;
        case 'textarea' :

                      fieldAttr = buildAttributes(field.attributes);

                      template = formGroup.open+'<textarea name="'+field.name+'" ng-model="'+ngModel+'"'+fieldAttr+' class="input-sm form-control '+formClass+'" '+disabled+'></textarea>'+label+help+formGroup.close;
                      break;
        case 'image' :
                      fieldAttr = buildAttributes(field.attributes);

                      var thumbnail = '<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"><img src="https://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div><div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px;"><img src="'+moveTo+'/{{ '+ngModel+' }}" alt=""></div>';

                      template = formGroup.open+'<div class="fileinput" data-provides="fileinput" ng-class="('+ngModel+') ? \'fileinput-exists\' : \'fileinput-new\'">'+thumbnail+'<div ng-hide="response.action==\'view\'"><span class="btn default btn-file btn-sm"><span class="fileinput-new"> Select image </span><span class="fileinput-exists"> Change </span><input type="file" name="'+field.name+'" ng-model="'+ngModel+'"'+fieldAttr+' nv-file-select="" uploader="'+uploaderName+'" /> </span><a href="javascript:;" class="btn red fileinput-exists btn-sm" data-dismiss="fileinput" ng-click="deleteFile(\''+moveTo+'\', \''+ngModel+'\') "> Remove </a></div></div>'+formGroup.close;
                      break;
        case 'upload' :
                      fieldAttr = buildAttributes(field.attributes);

                      // var thumbnail = '<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"><img src="https://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div><div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px;"><img src="'+moveTo+'/{{ '+ngModel+' }}" alt=""></div>';

                      template = formGroup.open+'<div class="fileinput" data-provides="fileinput" ng-class="('+ngModel+') ? \'fileinput-exists\' : \'fileinput-new\'">{{ '+ngModel+' }}<div ng-hide="response.action==\'view\'"><span class="btn default btn-file btn-sm"><span class="fileinput-new"> Select File </span><span class="fileinput-exists"> Change </span><input type="file" name="'+field.name+'" ng-model="'+ngModel+'"'+fieldAttr+' nv-file-select="" uploader="'+uploaderName+'" /> </span><a href="javascript:;" class="btn red fileinput-exists btn-sm" data-dismiss="fileinput" ng-click="deleteFile(\''+moveTo+'\', \''+ngModel+'\') "> Remove </a></div></div>'+formGroup.close;
                      break;
          // case 'radio' :
          //             field.attributes = buildAttributes(field.attributes);
          //             template = '<div class="form-group form-md-radios">'+label+'<div class="md-radio-inline"></div>'+help+'</div>';
          //             break;
      }

      // console.log()
// $('.selectpicker-ajax').append('<option value="597f4042296ee1dc8b"  selected="selected">KITA INDONESIA RAYA, PT</option>').selectpicker('refresh');
      // element.html(template).show();
      var el = $compile(template)(scope);
      element.replaceWith(el);
      element = $(el[0]);

      // handle datepicker data
      // element.find
      if(field.type == 'date'){
          var dt = moment(scope.data[field.name]).format('DD/MM/YYYY');
          element.find('.date-picker').val(dt);
      }

      if(field.type == 'select-ajax'){
            ngValue = (typeof ngValue == 'undefined') ? '' : ngValue;
        // if(ngValue != '' || typeof ngValue != 'undefined'){
            var source = field.attributes['data-source'];
            $.get(source+'?q='+ngValue).then(function(response){
              if(response.length > 0){
                var opt = '';
                if(ngValue == ''){
                  opt += '<option value="" selected="selected">-- Pilihan --</option>';
                }
                for(var r in response){
                  var selected = (ngValue != '' && r == 0) ? ' selected="selected"' : '';
                  opt += '<option value="'+response[r].id+'"'+selected+'>'+response[r].text+'</option>';
                }
                element.find('.selectpicker-ajax').html(opt).selectpicker('refresh');
              }
            })
        // }
      }

      element.find('.date-picker').on('dp.change', function(e){
        scope.data[field.name] = moment(e.date.toDate()).format('YYYY-MM-DD')+' 00:00:00';
      });

    }
  }
});
