<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProductMasterTableAdjustManufacture extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_master', function (Blueprint $table) {
          $table->string('routing', 25)->nullable();
          
          $table->foreign('routing')
                ->references('id')->on('manufacture_routing')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_master', function (Blueprint $table) {
            $table->dropColumn(['routing']);
        });
    }
}
