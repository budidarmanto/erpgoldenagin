<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManufactureOrderBomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manufacture_order_bom', function (Blueprint $table) {
            
            $table->string('id', 25);
            $table->string('manufacture_order', 25);
            $table->string('product_bom', 25);
            $table->decimal('qty_total', 18, 2);
            $table->decimal('qty_consumed', 18, 2)->nullable();
            $table->string('status', 3)->nullable();
            $table->string('created_by', 25)->nullable();
            $table->string('updated_by', 25)->nullable();
            $table->timestamps();

            $table->primary('id');
            
            $table->foreign('manufacture_order')
                ->references('id')->on('manufacture_order')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            
            $table->foreign('product_bom')
                ->references('id')->on('product_bom')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manufacture_order_bom');
    }
}
