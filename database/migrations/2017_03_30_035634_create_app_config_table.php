<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_config', function (Blueprint $table) {
          $table->string('id', 25);
          $table->string('code', 255);
          $table->text('value');
          $table->text('description')->nullable();

          $table->string('created_by', 25)->nullable();
          $table->string('updated_by', 25)->nullable();
          $table->timestamps();

          $table->primary('id');
          $table->unique('code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_config');
    }
}
