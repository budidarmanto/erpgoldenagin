<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCRMContactTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_contact', function (Blueprint $table) {
            $table->string('id', 25);
            $table->string('company', 25)->nullable();
            $table->string('name', 50);
            $table->string('picture', 255)->nullable();
            $table->text('address')->nullable();
            $table->string('city', 50)->nullable();
            $table->string('postal_code', 5)->nullable();
            $table->string('state', 50)->nullable();
            $table->string('country', 50)->nullable();
            $table->string('website', 255)->nullable();
            $table->string('phone', 50)->nullable();
            $table->string('mobile', 50)->nullable();
            $table->string('fax', 50)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('tax_no', 50)->nullable();
            $table->string('type', 25); // type: individual, company
            $table->boolean('active')->default(true);
            $table->boolean('pkp')->default(false);

            $table->integer('revision')->default(1);
            $table->string('created_by', 25)->nullable();
            $table->string('updated_by', 25)->nullable();
            $table->timestamps();

            $table->primary('id');

            $table->foreign('company')
                  ->references('id')->on('setting_company')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_contact');
    }
}
