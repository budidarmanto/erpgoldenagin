<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_master', function (Blueprint $table) {
            $table->string('id', 25);
            $table->string('code', 50);
            $table->string('category', 25);
            $table->string('uom', 25);
            $table->string('name', 200);
            $table->string('type', 25);
            $table->string('barcode', 100)->nullable();
            $table->string('picture', 255)->nullable();
            $table->decimal('sale_price', 18, 2)->default(0);
            $table->decimal('cost_price', 18, 2)->default(0);
            $table->boolean('pos')->default(false);
            $table->string('tax', 25)->nullable();
            $table->decimal('discount',18,2)->default(0);
            $table->boolean('active')->default(true);
            $table->decimal('weight',18,2)->default(0);
            $table->text('description')->nullable();
            $table->integer('revision')->default(1);
            $table->string('created_by', 25)->nullable();
            $table->string('updated_by', 25)->nullable();
            $table->timestamps();

            $table->primary('id');
            $table->unique('code');

            $table->foreign('uom')
                    ->references('id')->on('product_uom')
                    ->onDelete('restrict')
                    ->onUpdate('cascade');

             $table->foreign('category')
                    ->references('id')->on('product_category')
                    ->onDelete('restrict')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_master');
    }
}
