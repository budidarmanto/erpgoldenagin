<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManufactureBomProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manufacture_bom_product', function (Blueprint $table) {
          $table->string('id', 25);
          $table->string('bom', 25);
          $table->string('product', 25);
          $table->string('routing_operation', 25);
          $table->decimal('qty', 18,2)->default(0);
          $table->string('uom', 25);


          $table->string('created_by', 25)->nullable();
          $table->string('updated_by', 25)->nullable();
          $table->timestamps();

          $table->primary('id');

          $table->foreign('bom')
                ->references('id')->on('manufacture_bom')
                ->onDelete('cascade')
                ->onUpdate('cascade');

          $table->foreign('product')
                ->references('id')->on('product_product')
                ->onDelete('restrict')
                ->onUpdate('cascade');

          $table->foreign('routing_operation')
                ->references('id')->on('manufacture_routing_operation')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manufacture_bom_product');
    }
}
