<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterManufactureRoutingOperationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('manufacture_routing_operation', function (Blueprint $table) {
            
            $table->integer('sequence')->nullable();
            $table->integer('next_sequence')->nullable();
            $table->string('resource', 1)->nullable();
            $table->integer('capacity')->nullable();
            $table->string('duration_uom', 5)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('manufacture_routing_operation', function (Blueprint $table) {
            $table->dropColumn(['sequence', 'next_sequence', 'resource', 'capacity', 'duration_uom']);
        });
    }
}
