<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManufactureRoutingOperationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manufacture_routing_operation', function (Blueprint $table) {
          $table->string('id', 25);
          $table->string('routing', 25);
          $table->string('work_center', 25);
          $table->string('name', 200);
          $table->integer('duration')->default(0);
          $table->string('worksheet', 200)->nullable();
          $table->text('description')->nullable();

          $table->string('created_by', 25)->nullable();
          $table->string('updated_by', 25)->nullable();
          $table->timestamps();

          $table->primary('id');

          $table->foreign('routing')
                ->references('id')->on('manufacture_routing')
                ->onDelete('cascade')
                ->onUpdate('cascade');

          $table->foreign('work_center')
                ->references('id')->on('manufacture_work_center')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manufacture_routing_operation');
    }
}
