<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting_company', function (Blueprint $table) {
          $table->string('id', 25);
          $table->string('code', 25);
          $table->string('name',50);
          $table->string('parent', 25)->nullable();
          $table->integer('left')->nullable();
          $table->integer('right')->nullable();
          $table->integer('depth')->nullable();
          $table->string('picture',100)->nullable();
          $table->mediumText('address')->nullable();
          $table->string('city', 50)->nullable();
          $table->string('postal_code', 5)->nullable();
          $table->string('state', 50)->nullable();
          $table->string('country', 50)->nullable();
          $table->string('website', 255)->nullable();
          $table->string('phone', 50)->nullable();
          $table->string('fax', 50)->nullable();
          $table->string('email', 255)->nullable();
          $table->boolean('active')->default(false);;
          $table->string('created_by', 25)->nullable();
          $table->string('updated_by', 25)->nullable();
          $table->timestamps();

          $table->primary('id');
          $table->unique('code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting_company');
    }
}
