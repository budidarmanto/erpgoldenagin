<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashierSessionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cashier_session', function (Blueprint $table) {
            $table->string('id', 25);
            $table->string('company', 25);
            $table->string('code', 25);
            $table->timestamp('start')->nullable();
            $table->timestamp('end')->nullable();
            $table->decimal('balance',18,2)->default(0);
            $table->integer('status')->default(0);

            $table->string('created_by', 25)->nullable();
            $table->string('updated_by', 25)->nullable();
            $table->timestamps();

            $table->primary('id');
            $table->unique('code');

            $table->foreign('company')
                    ->references('id')->on('setting_company')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cashier_session');
    }
}
