<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManufactureWorkOrderPendingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manufacture_work_order_pending', function (Blueprint $table) {
            
            $table->string('id', 25);
            $table->string('work_order', 25);
            $table->timestamp('pending_start');
            $table->timestamp('pending_end')->nullable();
            
            $table->primary('id');
            
            $table->foreign('work_order')
                ->references('id')->on('manufacture_work_order')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manufacture_work_order_pending');
    }
}
