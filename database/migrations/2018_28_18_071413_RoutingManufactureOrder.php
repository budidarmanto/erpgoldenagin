<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RoutingManufactureOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('manufacture_order', function (Blueprint $table) {
            $table->string('routing', 25)->nullable();
            $table->string('stock_movement_ref', 250)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('manufacture_order', function (Blueprint $table) {
            $table->dropColumn('routing');
            $table->string('stock_movement_ref', 25)->change();
        });
    }
}
