<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryStockMovementProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_stock_movement_product', function (Blueprint $table) {
            $table->string('id', 25);
            $table->string('stock_movement',25);
            $table->string('reference',25)->nullable();
            $table->string('product',50);
            $table->string('uom', 25);
            $table->decimal('qty',18,2)->default(0);
            $table->decimal('rcp_qty',18,2)->default(0);
            $table->decimal('do_qty',18,2)->default(0);
            $table->decimal('price',18,2)->default(0);
            $table->json('discount')->nullable();
            $table->string('tax', 25)->nullable();
            $table->decimal('tax_amount',18,2)->default(0);
            $table->decimal('total',18,2)->default(0);
            $table->string('contact', 25)->nullable();
            $table->string('status', 25)->nullable()->default(0);
            $table->text('description')->nullable();
            $table->timestamp('expected_date')->nullable();

            $table->string('created_by', 25)->nullable();
            $table->string('updated_by', 25)->nullable();
            $table->timestamps();

            $table->primary('id');

            $table->foreign('stock_movement')
                  ->references('id')->on('inventory_stock_movement')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('product')
                  ->references('id')->on('product_product')
                  ->onDelete('restrict')
                  ->onUpdate('cascade');

            $table->foreign('uom')
                  ->references('id')->on('product_uom')
                  ->onDelete('restrict')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_stock_movement_product');
    }
}
