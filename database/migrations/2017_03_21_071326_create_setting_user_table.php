<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting_user', function (Blueprint $table) {
            $table->string('id', 25);
            $table->string('username', 25);
            $table->string('password', 255);
            $table->string('remember_token', 255)->nullable();
            $table->string('fullname', 50);
            $table->string('nickname', 50)->nullable();
            $table->string('email', 255);
            $table->string('picture', 255)->nullable();
            $table->boolean('active')->default(false);
            $table->boolean('is_login')->default(false);
            $table->dateTime('last_activity')->nullable();
            $table->integer('count_wrong_password')->default(0);
            $table->text('group')->nullable();
            $table->text('company')->nullable();
            $table->json('warehouse_location')->nullable();

            $table->string('created_by', 25)->nullable();
            $table->string('updated_by', 25)->nullable();
            $table->timestamps();

            $table->primary('id');
            $table->unique('email', 'username');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting_user');
    }
}
