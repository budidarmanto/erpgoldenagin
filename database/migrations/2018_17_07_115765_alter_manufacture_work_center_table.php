<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterManufactureWorkCenterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('manufacture_work_center', function (Blueprint $table) {
            $table->string('company', 25)->nullable();
            $table->integer('capacity')->nullable();
            $table->string('warehouse_location', 25)->nullable();
          
            $table->foreign('company')
                ->references('id')->on('setting_company')
                ->onDelete('cascade')
                ->onUpdate('cascade');
          
            $table->foreign('warehouse_location')
                ->references('id')->on('inventory_warehouse_location')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('manufacture_work_center', function (Blueprint $table) {
            $table->dropColumn(['company', 'capacity', 'warehouse_location']);
        });
    }
}
