<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManufactureWorkOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manufacture_work_order', function (Blueprint $table) {
            
            $table->string('id', 25);
            $table->string('manufacture_order', 25);
            $table->string('routing_operation', 25);
            $table->timestamp('date_start')->nullable();
            $table->timestamp('date_end')->nullable();
            $table->string('status', 10)->nullable();
            $table->decimal('scrap', 18, 2)->nullable();

            $table->string('created_by', 25)->nullable();
            $table->string('updated_by', 25)->nullable();
            $table->timestamps();

            $table->primary('id');
            
            $table->foreign('manufacture_order')
                ->references('id')->on('manufacture_order')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            
            $table->foreign('routing_operation')
                ->references('id')->on('manufacture_routing_operation')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manufacture_work_order');
    }
}
