<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UndoAlterProductBomTableAdjustManufacture extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_bom', function (Blueprint $table) {
            $table->dropColumn(['qty_total', 'qty_consumed', 'status']);
            $table->string('bom_type', 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_bom', function (Blueprint $table) {
            $table->decimal('qty_total', 18,2)->default(0)->nullable();
            $table->decimal('qty_consumed', 18,2)->default(0)->nullable();
            $table->integer('status')->nullable();
            $table->dropColumn(['bom_type']);
        });
    }
}
