<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductFavoriteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_favorite', function (Blueprint $table) {
          $table->string('id', 25);
          $table->string('company', 25);
          $table->string('product', 25);
          $table->timestamp('date')->nullable();
          $table->integer('sold')->default(0);
          $table->boolean('active')->default(true);

          $table->string('created_by', 25)->nullable();
          $table->string('updated_by', 25)->nullable();
          $table->timestamps();

          $table->primary('id');

          $table->foreign('company')
                  ->references('id')->on('setting_company')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

          $table->foreign('product')
                  ->references('id')->on('product_product')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_favorite');
    }
}
