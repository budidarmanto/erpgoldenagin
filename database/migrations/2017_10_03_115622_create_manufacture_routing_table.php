<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManufactureRoutingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manufacture_routing', function (Blueprint $table) {
          $table->string('id', 25);
          $table->string('code', 25);
          $table->string('name', 200);
          $table->text('description')->nullable();

          $table->string('created_by', 25)->nullable();
          $table->string('updated_by', 25)->nullable();
          $table->timestamps();

          $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manufacture_routing');
    }
}
