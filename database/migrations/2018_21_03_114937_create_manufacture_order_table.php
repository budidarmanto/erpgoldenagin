<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManufactureOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manufacture_order', function (Blueprint $table) {
            
            $table->string('id', 25);
            $table->string('code', 25);
            $table->string('company', 25);
            $table->string('product', 25);
            $table->decimal('qty', 18, 2);
            $table->decimal('scrap_total', 18, 2)->nullable();
            $table->timestamp('date_start')->nullable();
            $table->timestamp('date_end')->nullable();
            $table->string('pic', 25);
            $table->string('status', 10)->nullable();
            $table->string('stock_movement_ref', 25)->nullable();
            $table->string('stock_movement_ref_bom', 25)->nullable();

            $table->string('created_by', 25)->nullable();
            $table->string('updated_by', 25)->nullable();
            $table->timestamps();

            $table->primary('id');
            $table->unique('code');
            
            $table->foreign('company')
                ->references('id')->on('setting_company')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            
            $table->foreign('product')
                ->references('id')->on('product_master')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manufacture_order');
    }
}
