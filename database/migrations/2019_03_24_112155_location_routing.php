<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LocationRouting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('manufacture_routing', function (Blueprint $table) {
            $table->string('location_raw', 25)->nullable();
            $table->string('location_result', 25)->nullable();
            $table->string('location_scrap', 25)->nullable();
            $table->string('location_waste', 25)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('manufacture_routing', function (Blueprint $table) {
            $table->dropColumn('location_raw');
            $table->dropColumn('location_result');
            $table->dropColumn('location_scrap');
            $table->dropColumn('location_waste');
        });
    }
}
