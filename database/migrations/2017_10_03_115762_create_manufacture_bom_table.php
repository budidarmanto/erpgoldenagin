<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManufactureBomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manufacture_bom', function (Blueprint $table) {
          $table->string('id', 25);
          $table->string('product', 25);
          $table->string('routing', 25);
          $table->string('name', 200);
          $table->decimal('qty', 18,2)->default(0);
          $table->string('uom', 25);
          $table->string('status', 10)->nullable();
          $table->text('description')->nullable();

          $table->string('created_by', 25)->nullable();
          $table->string('updated_by', 25)->nullable();
          $table->timestamps();

          $table->primary('id');

          $table->foreign('product')
                ->references('id')->on('product_product')
                ->onDelete('restrict')
                ->onUpdate('cascade');

          $table->foreign('routing')
                ->references('id')->on('manufacture_routing')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manufacture_bom');
    }
}
