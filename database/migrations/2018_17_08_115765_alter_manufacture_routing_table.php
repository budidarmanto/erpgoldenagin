<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterManufactureRoutingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('manufacture_routing', function (Blueprint $table) {
            $table->string('company', 25)->nullable();
          
            $table->foreign('company')
                ->references('id')->on('setting_company')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('manufacture_routing', function (Blueprint $table) {
            $table->dropColumn(['company']);
        });
    }
}
