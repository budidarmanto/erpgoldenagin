<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryWarehouseLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_warehouse_location', function (Blueprint $table) {
            $table->string('id', 25);
            $table->string('code', 25)->nullable();
            $table->string('warehouse',25);
            $table->string('name',50);
            $table->boolean('active')->default(true);
            $table->boolean('pos')->default(false);

            $table->string('created_by', 25)->nullable();
            $table->string('updated_by', 25)->nullable();
            $table->timestamps();

            $table->primary('id');

            $table->foreign('warehouse')
                  ->references('id')->on('inventory_warehouse')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_warehouse_location');
    }
}
