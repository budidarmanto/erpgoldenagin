<?php echo $__env->make('auth.login-header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!-- BEGIN FORGOT PASSWORD FORM -->
<h1>Lupa Password?</h1>
<p> Silahkan masukan alamat email Anda. Kami akan mengirimkan tautan untuk memperbaharui kata sandi Anda. </p>
<form action="forgot" method="post">
    <?php echo e(csrf_field()); ?>

    <?php echo $__env->make('alerts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="form-group">
        <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Email" name="email" /> </div>
    <div class="form-actions forgot">
        <button type="submit" class="btn green-haze btn-block uppercase" >Submit</button>
    </div>
    <div class="form-actions">
      <div class="pull-right forget-password-block">
          <a href="login" id="forget-password" class="forget-password"><i class="fa fa-angle-left"></i> Kembali</a>
      </div>
    </div>
</form>
<!-- END FORGOT PASSWORD FORM -->
<?php echo $__env->make('auth.login-footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
