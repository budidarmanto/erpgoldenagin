<div class="row">
    <div class="col-md-12 page-404">
      <div class="number font-warning"> <i class="icon-lock"></i> </div>
        <div class="details">
            <h3><?php echo app('translator')->getFromJson('system.error.no_access.oops'); ?></h3>
            <p> <?php echo app('translator')->getFromJson('system.error.no_access.permission'); ?>
                <br/>
                <a href="#"> <?php echo app('translator')->getFromJson('system.error.no_access.return'); ?> </a> . </p>
        </div>
    </div>
</div>
