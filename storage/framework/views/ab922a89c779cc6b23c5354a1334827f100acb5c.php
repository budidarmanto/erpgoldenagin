<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  </head>
  <body>
    <table>
      <tr>
        <td colspan="4">
          <h3 style="margin: 0; padding: 0;">Stock Card</h3>
        </td>
        <td colspan="3"><span>Barang: <?php echo e($productName); ?></span></td>
      </tr>
      <tr>
        <td colspan="4"></td>
        <td colspan="3"><span>Tanggal: <?php echo e(date('d/m/Y', strtotime($fromDate))); ?> - <?php echo e(date('d/m/Y', strtotime($excelToDate))); ?></span></td>
      </tr>
    </table>
    <table border="1">
      <thead>
        <tr>
          <th>Tanggal</th>
          <th>Kode</th>
          <th>Tipe</th>
          <th>Masuk</th>
          <th>Keluar</th>
          <th>Stok</th>
          <th>Satuan Ukuran</th>
        </tr>
      </thead>
      <tbody>
        <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
          <td><?php echo e($val['date']); ?></td>
          <td><?php echo e($val['code']); ?></td>
          <td><?php echo e($val['type_name']); ?></td>
          <td align="center"><?php echo e($val['qty_in']); ?></td>
          <td align="center"><?php echo e($val['qty_out']); ?></td>
          <td align="center"><?php echo e($val['count_stock']); ?></td>
          <td align="center"><?php echo e($val['uom_name']); ?></td>
        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </tbody>
    </table>
  </body>
</html>
