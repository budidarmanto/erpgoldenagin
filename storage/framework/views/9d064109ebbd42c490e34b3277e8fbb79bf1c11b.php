<html><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style>
    .page-break {
        page-break-after: always;
    }
    body, table {
      font-size: 12px;
    }
    table h1 {
      font-size: 14px;
    }
    table th {
      font-weight: normal;
    }
    @media  print {
      table {page-break-inside: avoid;}
    }
    @page  {
      margin: 1.5cm;    
    }
    </style>
</head><body>
    <table style="width:100%">
      <tr>
        <td style="text-align:center">
          <span style="float:right">F-0308010017</span>
          <h1 style="margin: 0; padding: 0;">Laporan Stock Card</h1>
        </td>
      </tr>
      <tr>
        <td><span>Gudang: <?php echo e($warehouseName); ?> / <?php echo e($warehouseLocationName); ?></span></td>
      </tr>
      <tr>
        <td><span>Tanggal: <?php echo e(date('d/m/Y', strtotime($fromDate))); ?> - <?php echo e(date('d/m/Y', strtotime($toDate))); ?></span></td>
      </tr>
      <?php if($category): ?>
        <tr>
            <td><span>Kategori: <?php echo e($category->name); ?></span></td>
        </tr>
      <?php endif; ?>
    </table>
    <br>
    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <table class="table-detail" style="width:100%">
      <tbody>
        <tr>
            <td colspan="9">Kode: <?php echo e($val['code']); ?></td>
        </tr>
        <tr>
            <td colspan="9">Nama Barang: <?php echo e($val['name']); ?></td>
        </tr>
        <tr>
            <th>Tanggal Transaksi</th>
            <th>Nomor Penyesuaian</th>
            <th>Keterangan</th>
            <th>Gudang Asal</th>
            <th>Gudang Tujuan</th>
            <th>Masuk</th>
            <th>Keluar</th>
            <th>Saldo Akhir</th>
        </tr>
        <?php $__currentLoopData = $val['stock_card']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo e($dt['date']); ?></td>
            <td><?php echo e($dt['code']); ?></td>
            <td><?php echo e($dt['description']); ?></td>
            <td><?php echo e($dt['source_name']); ?></td>
            <td><?php echo e($dt['destination_name']); ?></td>
            <td><?php echo e($dt['qty_in']); ?></td>
            <td><?php echo e($dt['qty_out']); ?></td>
            <td><?php echo e($dt['stock_akhir']); ?></td>
        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td colspan="9"><br></td>
        </tr>
      </tbody>
    </table>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</body></html>