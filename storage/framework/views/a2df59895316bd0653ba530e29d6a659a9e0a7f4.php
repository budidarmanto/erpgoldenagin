<?php echo $__env->make('auth.login-header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<form action="login" class="login-form" method="post">
  <div class="form-title">
      <span class="form-title">Selamat datang,</span><br/>
      <span class="form-subtitle" style="padding-left: 0; font-size: 14px;">silahkan masuk menggunakan akun Anda.</span>
  </div>
  <?php echo e(csrf_field()); ?>

  <?php echo $__env->make('alerts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="form-group">
    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
    <label class="control-label visible-ie8 visible-ie9">Username</label>
    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username" />
  </div>
  <div class="form-group">
    <label class="control-label visible-ie8 visible-ie9">Password</label>
    <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" />
  </div>
  <div class="form-actions">
      <button type="submit" class="btn green-haze btn-block uppercase btn-login">Login</button>
  </div>
  <div class="form-actions">
      <!-- <div class="pull-left">
          <label class="rememberme mt-checkbox mt-checkbox-outline">
              <input type="checkbox" name="remember" value="1" /> Remember me
              <span></span>
          </label>
      </div> -->
      <div class="pull-right forget-password-block">
          <a href="forgot" id="forget-password" class="forget-password">Forgot Password?</a>
      </div>
  </div>
</form>
<?php echo $__env->make('auth.login-footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
