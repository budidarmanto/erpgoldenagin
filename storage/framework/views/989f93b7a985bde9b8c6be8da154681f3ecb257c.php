<!-- BEGIN MAIN CONTENT -->
<div class="row">
  <div class="col-md-12">
      <!-- BEGIN: ACCORDION DEMO -->
    <form role="form" id="{{ response.id }}">
      <div class="portlet">
        <div class="portlet-title">
          <?php echo $__env->make('pages.partials.form.form-title', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          <?php echo $__env->make('pages.partials.form.form-action', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>
        <div class="portlet-body">
          <div class="row margin-bottom-5 margin-top-20">
            <div class="col-md-8 col-md-offset-2">
              <div class="row">
                <div class="col-md-6">
                  <form-builder field="code"></form-builder>
                </div>
                <div class="col-md-6">
                  <form-builder field="name"></form-builder>
                </div>
              </div>
              <form-builder field="address"></form-builder>
              <div class="row">
                <div class="col-md-8">
                  <form-builder field="city"></form-builder>
                </div>
                <div class="col-md-4">
                  <form-builder field="postal_code"></form-builder>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <form-builder field="state"></form-builder>
                </div>
                <div class="col-md-6">
                  <form-builder field="country"></form-builder>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <form-builder field="phone"></form-builder>
                </div>
                <div class="col-md-6">
                  <form-builder field="fax"></form-builder>
                </div>
              </div>
              <form-builder field="company"></form-builder>
            </div>
          </div>


          <div class="portlet">
            <div class="portlet-title">
              <div class="caption">Lokasi Penyimpanan</div>
              <div class="actions">
                <a href="#" class="btn btn-sm btn-success" ng-click="showPopupLocation()"  ng-if="response.action!='view'"> Tambah <i class="fa fa-plus"></i></a>
              </div>
            </div>
            <div class="portlet-body">
              <div class="table-responsive">
                <table class="table table-hover table-light dataTable" border="0" cellspacing="0" cellpadding="0">
                  <thead>
                    <tr>
                      <th>Kode</th>
                      <th>Lokasi</th>
                      <th>Aktif</th>
                      <th style="width: 120px;" class="text-center" ng-if="response.action!='view'">Tindakan</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr ng-repeat="loc in dataLocation" ng-dblclick="showPopupLocation(loc)">
                      <td>{{ loc.code }}</td>
                      <td>{{ loc.name }}</td>
                      <td>{{ (loc.active) ? 'Aktif' : 'Tidak Aktif' }}</td>
                      <td class="text-center" ng-if="response.action!='view'">
                        <a href="javascript:;" class="btn btn-icon-only tooltips" data-original-title="Modify" data-placement="left" ng-click="showPopupLocation(loc)">
                          <i class="fa fa-edit"></i>
                        </a>
                        <a href="javascript:;" class="btn btn-icon-only tooltips" data-original-title="Delete" data-placement="left" delete-confirm on-delete="deleteLocation(loc)">
                          <i class="fa fa-trash"></i>
                        </a>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
<!-- END MAIN CONTENT -->

<!-- EDIT UOM POPUP TEMPLATE -->
<script type="text/ng-template" id="modalLocation.html">
  <form>
  <div class="modal-header">
      <h3 class="modal-title" id="modal-title">Lokasi Penyimpanan</h3>
  </div>
  <div class="modal-body" id="modal-body">
    <div ng-repeat="field in response.fields">
      <form-builder field="{{ field.name }}"></form-builder>
    </div>
  </div>
  <div class="modal-footer">
      <button class="btn btn-sm btn-default" type="button" ng-click="cancel()">Batal&nbsp;&nbsp;<i class="fa fa-times"></i></button>
      <button class="btn btn-sm green-haze" type="submit" ng-click="save(data, $event)">Simpan&nbsp;&nbsp;<i class="fa fa-floppy-o"></i></button>
  </div>
  </form>
</script>
<!-- END EDIT UOM POPUP TEMPLATE -->
