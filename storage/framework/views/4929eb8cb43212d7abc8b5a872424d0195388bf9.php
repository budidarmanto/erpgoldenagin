<div class="row margin-bottom-5 margin-top-20">
  <div class="col-md-8 col-md-offset-2">
    <div ng-repeat="field in response.fields">
      <form-builder field="{{ field.name }}"></form-builder>
    </div>
  </div>
</div>
