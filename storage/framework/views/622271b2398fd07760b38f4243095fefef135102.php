<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style>
    .page-break {
        page-break-after: always;
    }
    body, table {
      font-size: 12px;
    }
    table h1 {
      font-size: 14px;
    }
    table th {
      font-weight: normal;
    }
    @media  print {
      table {page-break-inside: avoid;}
    }
    @page  {
      margin: 1.5cm;    
    }
    </style>
  </head>
  <body>
    <table style="width:100%">
      <tr>
        <td style="text-align:center">
          <h1 style="margin: 0; padding: 0;">Laporan Persediaan</h1>
        </td>
      </tr>
      <tr>
        <td><span>Gudang: <?php echo e($warehouseName); ?> / <?php echo e($warehouseLocationName); ?></span></td>
      </tr>
      <tr>
        <td><span>Tanggal: <?php echo e(date('d/m/Y', strtotime($today))); ?></span></td>
      </tr>
      <?php if($category): ?>
        <tr>
            <td><span>Kategori: <?php echo e($category->name); ?></span></td>
        </tr>
      <?php endif; ?>
      <?php if($type): ?>
        <tr>
            <td><span>Tipe: <?php echo e($type); ?></span></td>
        </tr>
      <?php endif; ?>
    </table>
    <br>
    <table class="table-detail" style="width:100%">
      <tbody>
        <tr>
            <th>No</th>
            <th>Kode</th>
            <th>Nama</th>
            <th>Kategori</th>
            <th>Satuan Ukuran</th>
            <th>Stok</th>
        </tr>
        <?php 
        $no = 1;
        ?>
        <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo e($no++); ?></td>
            <td><?php echo e($dt['code']); ?></td>
            <td><?php echo e($dt['name']); ?></td>
            <td><?php echo e($dt['category_name']); ?></td>
            <td><?php echo e($dt['uom_name']); ?></td>
            <td><?php echo e(number_format($dt['stock_akhir'],2)); ?></td>
        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        
      </tbody>
    </table>
  </body>
</html>