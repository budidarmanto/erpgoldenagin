<div class="portlet light">
  <div class="portlet-title">
    <div class="caption">Komentar</div>
  </div>
  <div class="portlet-body">
    <div class="chat-form">
        <div class="input-cont">
            <textarea name="comment" ng-model="comment" class="form-control" type="text" placeholder="Type a message here..." ng-disabled="response.action == 'create'"></textarea>
        </div>
        <div class="btn-cont">

            <a href="" class="btn blue icn-only" ng-click="addComment()">
                <i class="fa fa-check icon-white"></i>
            </a>
        </div>
    </div>
    <div style="padding: 20px; 10px;">

      <div class="timeline timeline-log">
        <div class="timeline-item" ng-repeat="comment in dataComment | orderBy:'-date'">
          <div class="timeline-badge">
            <div class="timeline-badge-userpic">
              <img src="{{ (comment.picture != '' && comment.picture != null) ? 'img/user/'+comment.picture : 'img/user-placeholder.jpg' }}"/>
            </div>
          </div>
          <div class="timeline-body" style="border-left:2px solid {{ comment.color }}; background: #f9f9f9;">
            <div class="timeline-body-arrow" style="
  border-color: transparent {{ comment.color }} transparent transparent;
  "></div>
            <div class="timeline-body-head">
              <div class="timeline-body-head-caption">
                <a href="javascript:;" class="timeline-body-title" style="color: {{ log.color }}">{{ comment.name }}</a>
                <span class="timeline-body-time font-grey-cascade">{{ comment.created_at }}</span>
              </div>
            </div>
            <div class="timeline-body-content">
              <span class="font-grey-cascade" compile-template ng-bind-html="comment.comment"></span>
            </div>
          </div>
        </div>
      </div>
      <!-- <ul class="chats">

        <li ng-repeat="comment in dataComment" class="{{ comment.type }}">
          <div class="avatar-circle avatar">
            <img src="{{ (comment.picture != '' && comment.picture != null) ? 'img/user/'+comment.picture : 'img/user-placeholder.jpg' }}"/>
          </div>
          <div class="message">
            <span class="arrow"></span>
            <a href="javascript:;" class="name"> {{ comment.name }} </a>
            <span class="datetime"> at {{ comment.created_at }}</span>
            <span class="body"> {{ comment.comment }} </span>
          </div>
        </li> -->
      </ul>
    </div>
  </div>
</div>
