<div class="page-footer-inner"> 2017 &copy; Squirrel by DEV+. </div>
<div class="scroll-to-top">
    <i class="icon-arrow-up"></i>
</div>
<div class="modal fade" id="modalError" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <div class="row">
                  <div class="col-md-12 page-404">
                    <div class="number font-red"> 500 </div>
                    <div class="font-red" style="font-size: 18px;"> Internal Server Error </div>
                      <div class="">
                          <h3>Oops! Suatu kesalahan telah terjadi</h3>
                          <p> Kirimkan tautan dibawah ini dan segera laporkan kesalahan ini kepada tim kami! </p>
                          <pre id="linkError">

                          </pre>
                      </div>
                  </div>
              </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade bs-modal-sm" id="modalLockScreen" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
              <div class="row">
                  <div class="col-md-12" style="margin: 20px 0 10px 0;">
                    <div class="text-center">
                      <div class="image-circle-wrapper">
                        <?php if(Auth::user()->picture == ''): ?>
                          <img src="img/user-placeholder.jpg" alt="">
                        <?php else: ?>
                          <img src="img/user/<?php echo e(Auth::user()->picture); ?>" alt="">
                        <?php endif; ?>
                      </div>
                      <div class="row">
                        <div class="col-md-12 margin-top-10">
                        <span class="username username-hide-on-mobile" style="font-size: 14px;"> <?php echo e((Auth::user()->nickname != '') ? Auth::user()->nickname : Auth::user()->fullname); ?> </span>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12 margin-top-10">
                          <div class="form-group form-md-line-input form-md-floating-label ng-scope">
                            <input class="form-control input-sm" type="password" name="password" placeholder="Masukan Password" />
                          </div>
                        </div>
                        <div class="col-md-12">
                          <button class="btn btn-sm green-haze mt-ladda-btn ladda-button" type="submit" ng-click="save(data, $event)" data-style="zoom-out"><span class="ladda-label">Login</span></button>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<iframe id="printFrame" style="visibility: hidden;height: 1px;width: 1px;"></iframe>
