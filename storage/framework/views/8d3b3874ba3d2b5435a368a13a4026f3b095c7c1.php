<div class="table-filter">
  <div class="input-icon input-icon-sm right" style="width: 200px;">
    <i class="fa fa-search"></i>
    <input type="text" ng-model="keyword" class="form-control input-sm" placeholder="<?php echo app('translator')->getFromJson('system.datagrid.filter.search'); ?>" ng-change="keywordChanged()">
  </div>
</div>
