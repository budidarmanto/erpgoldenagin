<!-- BEGIN MAIN CONTENT -->
<div class="row">
  <div class="col-md-12">
      <!-- BEGIN: ACCORDION DEMO -->
    <form role="form" id="{{ response.id }}">
      <div class="portlet">
        <div class="portlet-title">
          <?php echo $__env->make('pages.partials.form.form-title', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          <?php echo $__env->make('pages.partials.form.form-action', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>
        <div class="portlet-body">
          <?php echo $__env->make('pages.partials.form.form-content', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>
      </div>
    </form>
  </div>
</div>
<!-- END MAIN CONTENT -->
