<!-- BEGIN MAIN CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN: ACCORDION DEMO -->
        <div class="portlet">
            <?php echo $__env->make('pages.partials.datagrid.datagrid-header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <div class="portlet-body">
              <div class="row margin-bottom-10">
                <div class="col-md-6">
                  <?php echo $__env->make('pages.partials.datagrid.datagrid-filter', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
                <div class="col-md-6 text-right">
                  <?php echo $__env->make('pages.partials.datagrid.datagrid-status', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
              </div>
              <?php echo $__env->make('pages.partials.datagrid.datagrid-table', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
              <div class="row margin-top-10">
                <div class="col-md-6">
                  <div ng-if="response.massAction.length > 0">
                    <?php echo $__env->make('pages.partials.datagrid.datagrid-mass-button', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                  </div>
                </div>
                <div class="col-md-6">
                  <?php echo $__env->make('pages.partials.datagrid.datagrid-pagination', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
<!-- END MAIN CONTENT -->
