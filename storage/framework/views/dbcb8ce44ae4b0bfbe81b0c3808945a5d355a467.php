<div class="page-sidebar navbar-collapse collapse">
    <div class="profile-sidebar">
      <div class="profile-userpic">
        <?php if(Auth::user()->authCompany()->picture == ''): ?>
          <img src="img/company-placeholder.png" class="img-responsive" alt="">
        <?php else: ?>
          <img src="img/company/<?php echo e(Auth::user()->authCompany()->picture); ?>" class="img-responsive" alt="">
        <?php endif; ?>
      </div>
      <div class="profile-sidebar-title"><?php echo e(Auth::user()->authCompany()->name); ?></div>
    </div>
    <!-- BEGIN SIDEBAR MENU -->
    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" ng-class="{'page-sidebar-menu-closed': settings.layout.pageSidebarClosed}">
        <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
        <li class="sidebar-search-wrapper">
            <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
            <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
            <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
            <!-- <form class="sidebar-search sidebar-search-bordered" action="#" method="POST">
                <a href="javascript:;" class="remove">
                    <i class="icon-close"></i>
                </a>
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                        <a href="javascript:;" class="btn submit">
                            <i class="icon-magnifier"></i>
                        </a>
                    </span>
                </div>
            </form> -->
            <!-- END RESPONSIVE QUICK SEARCH FORM -->
        </li>
        <!-- App -->
        <?php if(Auth::user()->hasApplication('app')): ?>
        <li class="nav-item">
            <a href="#" class="nav-link nav-toggle">
                <i class="icon-settings"></i>
                <span class="title"><?php echo app('translator')->getFromJson('system.sidebar.app.name'); ?></span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <?php if(Auth::user()->hasModule('AppGroup')): ?>
                <li>
                    <a href="#/app/group">
                        <span><?php echo app('translator')->getFromJson('system.sidebar.app.group'); ?></span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if(Auth::user()->hasModule('AppApplication')): ?>
                <li>
                    <a href="#/app/application">
                        <span><?php echo app('translator')->getFromJson('system.sidebar.app.application'); ?></span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if(Auth::user()->hasModule('AppRole')): ?>
                <li>
                    <a href="#/app/role">
                        <span><?php echo app('translator')->getFromJson('system.sidebar.app.role'); ?></span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if(Auth::user()->hasModule('AppConfig')): ?>
                <li>
                    <a href="#/app/config">
                        <span><?php echo app('translator')->getFromJson('system.sidebar.app.config'); ?></span>
                    </a>
                </li>
                <?php endif; ?>
            </ul>
        </li>
        <?php endif; ?>
        <!-- End App -->

        <!-- Setting -->
        <?php if(Auth::user()->hasApplication('setting')): ?>
        <li class="nav-item">
            <a href="#" class="nav-link nav-toggle">
                <i class="icon-wrench"></i>
                <span class="title"><?php echo app('translator')->getFromJson('system.sidebar.setting.name'); ?></span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <?php if(Auth::user()->hasModule('SettingUser')): ?>
                <li>
                    <a href="#/setting/user">
                        <span><?php echo app('translator')->getFromJson('system.sidebar.setting.user'); ?></span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if(Auth::user()->hasModule('SettingCompany')): ?>
                <li>
                    <a href="#/setting/company">
                        <span><?php echo app('translator')->getFromJson('system.sidebar.setting.company'); ?></span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if(Auth::user()->hasModule('SettingWorkflow')): ?>
                <li>
                    <a href="#/setting/workflow">
                        <span><?php echo app('translator')->getFromJson('system.sidebar.setting.workflow'); ?></span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if(Auth::user()->hasModule('SettingCurrency')): ?>
                <li>
                    <a href="#/setting/currency">
                        <span>Mata Uang</span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if(Auth::user()->hasModule('SettingTax')): ?>
                <li>
                    <a href="#/setting/tax">
                        <span>Pajak</span>
                    </a>
                </li>
                <?php endif; ?>
            </ul>
        </li>
        <?php endif; ?>
        <!-- End Setting -->

        <!-- CRM -->
        <?php if(Auth::user()->hasApplication('crm')): ?>
          <?php if(Auth::user()->hasModule('CrmContact')): ?>
          <li class="nav-item">
              <a href="#/crm/contact" class="nav-link">
                  <i class="icon-notebook"></i>
                  <span class="title"><?php echo app('translator')->getFromJson('system.sidebar.crm.contact'); ?></span>
              </a>
          </li>
          <?php endif; ?>
        <?php endif; ?>
        <!-- End CRM -->

        <!-- Product -->
        <?php if(Auth::user()->hasApplication('product')): ?>
        <li class="nav-item">
            <a href="#" class="nav-link nav-toggle">
                <i class="fa fa-dropbox"></i>
                <span class="title">Barang/Jasa</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <?php if(Auth::user()->hasModule('ProductUom')): ?>
                <li>
                    <a href="#/product/uom">
                        <span>Satuan Ukuran</span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if(Auth::user()->hasModule('ProductAttribute') && Params::get('PRODUCT_VARIANT') == 1): ?>
                <li>
                    <a href="#/product/attribute">
                        <span>Varian</span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if(Auth::user()->hasModule('ProductCategory')): ?>
                <li>
                    <a href="#/product/category">
                        <span>Kategori Produk</span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if(Auth::user()->hasModule('ProductMaster')): ?>
                <li>
                    <a href="#/product/master">
                        <span>Barang/Jasa</span>
                    </a>
                </li>
                <?php endif; ?>
            </ul>
        </li>
        <?php endif; ?>
        <!-- End Product -->

        <!-- Purchase -->
        <?php if(Auth::user()->hasApplication('purchase')): ?>
        <li class="nav-item">
            <a href="#" class="nav-link nav-toggle">
                <i class="fa fa-shopping-cart"></i>
                <span class="title">Pembelian</span>
                <span class="badge badge-danger" ng-hide="sidebarAlert.totalAllPurchase <= 0">{{ sidebarAlert.totalAllPurchase }}</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <?php if(Auth::user()->hasModule('PurchaseSupplier')): ?>
                <li>
                    <a href="#/purchase/supplier">
                        <span>Pemasok</span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if(Auth::user()->hasModule('PurchaseRequestorder')): ?>
                <li>
                    <a href="#/purchase/request-order">
                        <span>Pengajuan</span>
                        <span class="badge badge-danger" ng-hide="sidebarAlert.totalRequest <= 0">{{ sidebarAlert.totalRequest }}</span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if(Auth::user()->hasModule('PurchaseApprovalorder')): ?>
                <li>
                    <a href="#/purchase/approval-order">
                        <span>Persetujuan</span>
                        <span class="badge badge-danger" ng-hide="sidebarAlert.totalApproval <= 0">{{ sidebarAlert.totalApproval }}</span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if(Auth::user()->hasModule('PurchasePurchaseorder')): ?>
                <li>
                    <a href="#/purchase/purchase-order">
                        <span>Pembelian</span>
                        <span class="badge badge-danger" ng-hide="sidebarAlert.totalPurchase <= 0">{{ sidebarAlert.totalPurchase }}</span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if(Auth::user()->hasModule('PurchasePurchaselbs')): ?>
                <li>
                    <a href="#/purchase/purchase-lbs">
                        <span>Pembelian Sangkuriang</span>
                        <span class="badge badge-danger" ng-hide="sidebarAlert.totalPurchaseLbs <= 0">{{ sidebarAlert.totalPurchaseLbs }}</span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if(Auth::user()->hasModule('PurchaseRequestnote')): ?>
                <li>
                    <a href="#/purchase/request-note">
                        <span>Pengajuan Sangkuriang</span>
                        <span class="badge badge-danger" ng-hide="sidebarAlert.totalRequestNote <= 0">{{ sidebarAlert.totalRequestNote }}</span>
                    </a>
                </li>
                <?php endif; ?>
            </ul>
        </li>
        <?php endif; ?>
        <!-- End Purchase -->

        <!-- Inventory -->
        <?php if(Auth::user()->hasApplication('inventory')): ?>
        <li class="nav-item">
            <a href="#" class="nav-link nav-toggle">
                <i class="fa fa-cube"></i>
                <span class="title">Inventori</span>
                <span class="badge badge-danger" ng-hide="sidebarAlert.totalAllInventory <= 0">{{ sidebarAlert.totalAllInventory }}</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <?php if(Auth::user()->hasModule('InventoryWarehouse')): ?>
                <li>
                    <a href="#/inventory/warehouse">
                        <span>Gudang</span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if(Auth::user()->hasModule('InventoryStock')): ?>
                <li>
                    <a href="#/inventory/stock">
                        <span>Persediaan</span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if(Auth::user()->hasModule('InventoryIncoming')): ?>
                <li>
                    <a href="#/inventory/incoming">
                        <span>Barang Masuk</span>
                        <span class="badge badge-danger" ng-hide="sidebarAlert.totalIncoming <= 0">{{ sidebarAlert.totalIncoming }}</span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if(Auth::user()->hasModule('InventoryReceiptorder')): ?>
                <li>
                    <a href="#/inventory/receipt-order">
                        <span>Penerimaan Barang</span>
                        <span class="badge badge-danger" ng-hide="sidebarAlert.totalReceipt <= 0">{{ sidebarAlert.totalReceipt }}</span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if(Auth::user()->hasModule('InventoryOutgoing')): ?>
                <li>
                    <a href="#/inventory/outgoing">
                        <span>Barang Keluar</span>
                        <span class="badge badge-danger" ng-hide="sidebarAlert.totalOutgoing <= 0">{{ sidebarAlert.totalOutgoing }}</span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if(Auth::user()->hasModule('InventoryDeliveryorder')): ?>
                <li>
                    <a href="#/inventory/delivery-order">
                        <span>Pengiriman Barang</span>
                        <span class="badge badge-danger" ng-hide="sidebarAlert.totalDelivery <= 0">{{ sidebarAlert.totalDelivery }}</span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if(Auth::user()->hasModule('InventoryIncomingrequest')): ?>
                <li>
                    <a href="#/inventory/incoming-request">
                        <span>Pengajuan Sangkuriang</span>
                        <span class="badge badge-danger" ng-hide="sidebarAlert.totalIncomingRequest <= 0">{{ sidebarAlert.totalIncomingRequest }}</span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if(Auth::user()->hasModule('InventoryInternaltransfer') || Auth::user()->hasModule('InventoryExternaltransfer')): ?>

                  <!-- <li class="nav-item">
                      <a href="#" class="nav-link nav-toggle">
                          <span>Mutasi Barang</span>
                          <span class="arrow"></span>
                      </a>
                      <ul class="sub-menu">
                        <?php if(Auth::user()->hasModule('InventoryExternaltransfer')): ?>
                        <li>
                            <a href="#/inventory/external-transfer">
                                <span>Mutasi Eksternal</span>
                                <span class="badge badge-danger" ng-hide="sidebarAlert.totalExternalTransfer <= 0">{{ sidebarAlert.totalExternalTransfer }}</span>
                            </a>
                        </li>
                        <?php endif; ?>
                        <?php if(Auth::user()->hasModule('InventoryInternaltransfer')): ?>
                        <li>
                            <a href="#/inventory/internal-transfer">
                                <span>Mutasi Internal</span>
                                <span class="badge badge-danger" ng-hide="sidebarAlert.totalInternalTransfer <= 0">{{ sidebarAlert.totalInternalTransfer }}</span>
                            </a>
                        </li>
                        <?php endif; ?>
                      </ul>
                  </li> -->
                <?php endif; ?>

                <?php if(Auth::user()->hasModule('InventoryInternaltransfer')): ?>
                <li>
                    <a href="#/inventory/internal-transfer">
                        <span>Mutasi Internal</span>
                        <span class="badge badge-danger" ng-hide="sidebarAlert.totalInternalTransfer <= 0">{{ sidebarAlert.totalInternalTransfer }}</span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if(Auth::user()->hasModule('InventoryAdjustment')): ?>
                <li>
                    <a href="#/inventory/adjustment">
                        <span>Penyesuaian Persediaan</span>
                        <span class="badge badge-danger" ng-hide="sidebarAlert.totalAdjustment <= 0">{{ sidebarAlert.totalAdjustment }}</span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if(Auth::user()->hasModule('InventoryReturnorder')): ?>
                <li>
                    <a href="#/inventory/return-order">
                        <span>Retur</span>
                        <span class="badge badge-danger" ng-hide="sidebarAlert.totalReturn <= 0">{{ sidebarAlert.totalReturn }}</span>
                    </a>
                </li>
                <?php endif; ?>
            </ul>
        </li>
        <?php endif; ?>
        <!-- End Inventory -->

        <!-- Sales -->
        <?php if(Auth::user()->hasApplication('sales')): ?>
        <li class="nav-item">
            <a href="#" class="nav-link nav-toggle">
                <i class="fa fa-truck"></i>
                <span class="title">Penjualan</span>
                <span class="badge badge-danger" ng-hide="sidebarAlert.totalAllSales <= 0">{{ sidebarAlert.totalAllSales }}</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <?php if(Auth::user()->hasModule('SalesCustomer')): ?>
                <li>
                    <a href="#/sales/customer">
                        <span>Pelanggan</span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if(Auth::user()->hasModule('SalesSalesorder')): ?>
                <li>
                    <a href="#/sales/sales-order">
                        <span>Penjualan</span>
                        <span class="badge badge-danger" ng-hide="sidebarAlert.totalSales <= 0">{{ sidebarAlert.totalSales }}</span>
                    </a>
                </li>
                <?php endif; ?>
            </ul>
        </li>
        <?php endif; ?>
        <!-- End Sales -->

        <!-- Cashier -->
        <?php if(Auth::user()->hasApplication('cashier')): ?>
        <li class="nav-item">
            <a href="#" class="nav-link nav-toggle">
                <i class="fa fa-calculator"></i>
                <span class="title">Kasir</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <?php if(Auth::user()->hasModule('CashierBank')): ?>
                <li>
                    <a href="#/cashier/bank">
                        <span>Bank</span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if(Auth::user()->hasModule('CashierEdc')): ?>
                <li>
                    <a href="#/cashier/edc">
                        <span>EDC</span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if(Auth::user()->hasModule('CashierSurcharge')): ?>
                <li>
                    <a href="#/cashier/surcharge">
                        <span>Surcharge</span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if(Auth::user()->hasModule('CashierSession')): ?>
                <li>
                    <a href="#/cashier/session">
                        <span>Sesi</span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if(Auth::user()->hasModule('CashierCashierorder')): ?>
                <li>
                    <a href="#/cashier/cashier-order">
                        <span>Transaksi Penjualan</span>
                    </a>
                </li>
                <?php endif; ?>
            </ul>
        </li>
        <?php endif; ?>
        <!-- End Cashier -->

        <?php if(Auth::user()->hasApplication('manufacture')): ?>
        <li class="nav-item">
            <a href="#" class="nav-link nav-toggle">
                <i class="fa fa-industry"></i>
                <span class="title">Manufaktur</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <?php if(Auth::user()->hasModule('ManufactureWorkcenter')): ?>
                <li>
                    <a href="#/manufacture/work-center">
                        <span>Work Center</span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if(Auth::user()->hasModule('ManufactureRouting')): ?>
                <li>
                    <a href="#/manufacture/routing">
                        <span>Routing</span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if(Auth::user()->hasModule('ManufactureManufactureorder')): ?>
                <li>
                    <a href="#/manufacture/manufacture-order">
                        <span>Manufacture Order</span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if(Auth::user()->hasModule('ManufactureWorkorder')): ?>
                <li>
                    <a href="#/manufacture/work-order">
                        <span>Work Order</span>
                    </a>
                </li>
                <?php endif; ?>
            </ul>
        </li>
        <?php endif; ?>

        <!-- Report -->
        <?php if(Auth::user()->hasApplication('report')): ?>
        <li class="nav-item">
            <a href="#" class="nav-link nav-toggle">
                <i class="fa fa-file-text-o"></i>
                <span class="title">Laporan</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <?php if(Auth::user()->hasModule('ReportStockdaily')): ?>
                <li>
                    <a href="#/report/stock-daily">
                        <span>Laporan Perubahan Stok</span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if(Auth::user()->hasModule('ReportStockcard')): ?>
                <li>
                    <a href="#/report/stock-card">
                        <span>Stock Card</span>
                    </a>
                </li>
                <?php endif; ?>
            </ul>
        </li>
        <?php endif; ?>
        <!-- End Report -->
    </ul>
    <!-- END SIDEBAR MENU -->
</div>
