<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js" data-ng-app="DevplusApp"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js" data-ng-app="DevplusApp"> <![endif]-->
<!--[if !IE]>-->
<html lang="en" data-ng-app="DevplusApp">
<!--<![endif]-->
  <!-- BEGIN HEAD -->
  <head>
      <title>Squirrel Cloud ERP</title>
      <meta charset="utf-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="width=device-width, initial-scale=1" name="viewport" />
      <meta content="" name="description" />
      <meta content="" name="author" />
      <!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" /> -->
      <!-- BEGIN GLOBAL MANDATORY STYLES -->
      <!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" /> -->
      <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
      <link href="plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
      <link href="plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
      <link href="plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
      <link href="plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
      <link href="plugins/bootstrap-sweetalert/sweetalert.css" rel="stylesheet" type="text/css" />
      <link href="plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />
      <link href="plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
      <link href="plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
      <link href="plugins/angularjs/plugins/angular-xeditable/css/xeditable.min.css" rel="stylesheet" type="text/css" />
      <link href="plugins/ladda/ladda-themeless.min.css" rel="stylesheet" type="text/css" />
      <!-- END GLOBAL MANDATORY STYLES -->
      <!-- BEGIN DYMANICLY LOADED CSS FILES(all plugin and page related styles must be loaded between GLOBAL and THEME css files ) -->
      <link id="ng_load_plugins_before" />
      <!-- END DYMANICLY LOADED CSS FILES -->
      <link href="pages/css/profile.min.css" rel="stylesheet" type="text/css" />
      <!-- BEGIN PAGE LEVEL STYLES -->
      <!-- END PAGE LEVEL STYLES -->
      <!-- BEGIN THEME STYLES -->
      <link href="css/global/components-md.min.css" id="style_components" rel="stylesheet" type="text/css" />
      <link href="css/global/plugins.min.css" rel="stylesheet" type="text/css" />
      <link href="css/layout/layout.min.css" rel="stylesheet" type="text/css" />
      <link href="css/layout/themes/<?php echo e($theme); ?>.css" rel="stylesheet" type="text/css" id="style_color" />
      <link href="css/layout/custom.css?v=2" rel="stylesheet" type="text/css" />
      <!-- END THEME STYLES -->
      <link rel="shortcut icon" href="favicon.ico" />
  </head>
  <!-- END HEAD -->
  <!-- BEGIN BODY -->
  <body ng-controller="AppController" class="page-header-fixed page-sidebar-closed-hide-logo page-on-load" ng-class="{'page-content-white': settings.layout.pageContentWhite,'page-container-bg-solid': settings.layout.pageBodySolid, 'page-sidebar-closed': settings.layout.pageSidebarClosed}">
    <!-- BEGIN PAGE SPINNER -->
    <div ng-spinner-bar class="page-spinner-bar">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
    <!-- END PAGE SPINNER -->
    <!-- BEGIN HEADER -->
    <div data-ng-include="'template/header'" data-ng-controller="HeaderController" class="page-header navbar navbar-fixed-top"> </div>
    <!-- END HEADER -->
    <div class="clearfix"> </div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <div data-ng-include="'template/sidebar'" data-ng-controller="SidebarController" class="page-sidebar-wrapper"> </div>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!-- BEGIN ACTUAL CONTENT -->
                <div ui-view class="fade-in-up"> </div>
                <!-- END ACTUAL CONTENT -->
            </div>
        </div>
        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->
        <!-- 
<a href="javascript:;" class="page-quick-sidebar-toggler">
            <i class="icon-login"></i>
        </a>
        <div data-ng-include="'template/quick-sidebar'" data-ng-controller="QuickSidebarController" class="page-quick-sidebar-wrapper"></div>
 -->
        <!-- END QUICK SIDEBAR -->
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <div data-ng-include="'template/footer'" data-ng-controller="FooterController" class="page-footer"> </div>
    <!-- END FOOTER -->
  </body>
  <!-- END BODY -->
  <!-- BEGIN JAVASCRIPTS -->
  <!--[if lt IE 9]>
  <script src="plugins/respond.min.js"></script>
  <script src="plugins/excanvas.min.js"></script>
  <![endif]-->
  <script src="plugins/jquery.min.js" type="text/javascript"></script>
  <script src="plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
  <script src="plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
  <script src="plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
  <script src="plugins/jquery.blockui.min.js" type="text/javascript"></script>
  <script src="plugins/js.cookie.min.js" type="text/javascript"></script>
  <script src="plugins/moment.min.js" type="text/javascript"></script>
  <script src="plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
  <script src="plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
  <script src="plugins/bootstrap-select/js/ajax-bootstrap-select.js" type="text/javascript"></script>
  <script src="plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
  <script src="plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" type="text/javascript"></script>
  <script src="plugins/bootstrap-sweetalert/sweetalert.min.js" type="text/javascript"></script>
  <script src="plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
  <script src="plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
  <script src="plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
  <script src="plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
  <script src="plugins/ladda/spin.min.js" type="text/javascript"></script>
  <script src="plugins/ladda/ladda.min.js" type="text/javascript"></script>
  <!-- END CORE JQUERY PLUGINS -->
  <!-- BEGIN CORE ANGULARJS PLUGINS -->
  <script src="plugins/angularjs/angular.min.js" type="text/javascript"></script>
  <script src="plugins/angularjs/angular-sanitize.min.js" type="text/javascript"></script>
  <script src="plugins/angularjs/angular-touch.min.js" type="text/javascript"></script>
  <script src="plugins/angularjs/angular-moment.min.js" type="text/javascript"></script>
  <script src="plugins/angularjs/plugins/angular-ui-router.min.js" type="text/javascript"></script>
  <script src="plugins/angularjs/plugins/ocLazyLoad.min.js" type="text/javascript"></script>
  <script src="plugins/angularjs/plugins/ui-bootstrap-tpls.min.js" type="text/javascript"></script>
  <script src="plugins/angularjs/plugins/angular-xeditable/js/xeditable.min.js" type="text/javascript"></script>
  <script src="plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js" type="text/javascript"></script>

  <!-- END CORE ANGULARJS PLUGINS -->
  <!-- BEGIN APP LEVEL ANGULARJS SCRIPTS -->
  <script src="js/main.js" type="text/javascript"></script>
  <script src="js/directives.js" type="text/javascript"></script>
  <script src="js/global/form-builder.js" type="text/javascript"></script>
  <!-- END APP LEVEL ANGULARJS SCRIPTS -->
  <!-- BEGIN APP LEVEL JQUERY SCRIPTS -->
  <script src="js/global/app.js" type="text/javascript"></script>
  <script src="js/layout/layout.min.js" type="text/javascript"></script>
  <script src="js/layout/quick-sidebar.min.js" type="text/javascript"></script>
  <script src="js/layout/quick-nav.min.js" type="text/javascript"></script>
  <script src="js/layout/demo.min.js" type="text/javascript"></script>
  <!-- END APP LEVEL JQUERY SCRIPTS -->
  <!-- BEGIN APP LEVEL GLOBAL CONTROLLER -->

  <!-- END APP LEVEL GLOBAL CONTROLLER -->

  <!-- END JAVASCRIPTS -->
</html>
