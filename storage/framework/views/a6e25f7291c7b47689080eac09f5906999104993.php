<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  </head>
  <body>
    <table>
      <tr>
        <td colspan="3">
          <h3 style="margin: 0; padding: 0;">Barang/Jasa</h3>
        </td>
      </tr>
      <tr>
        <td colspan="3"></td>
        <td><span>Tanggal: <?php echo e(date('d/m/Y')); ?></span></td>
      </tr>
    </table>
    <table border="1">
      <thead>
        <tr>
          <th>Kode Barang/Jasa</th>
          <th>Nama</th>
          <th>Satuan Ukuran</th>
          <th>Kategori</th>
          <th>Status</th>
          <th>Barcode</th>
          <th>Harga Jual</th>
        </tr>
      </thead>
      <tbody>
        <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
          <td><?php echo e($val['code']); ?></td>
          <td><?php echo e($val['name']); ?></td>
          <td><?php echo e($val['uom_name']); ?></td>
          <td><?php echo e($val['category_name']); ?></td>
          <td><?php echo e($val['active']); ?></td>
          <td><?php echo e($val['barcode']); ?></td>
          <td><?php echo e($val['sale_price']); ?></td>
        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </tbody>
    </table>
  </body>
</html>
