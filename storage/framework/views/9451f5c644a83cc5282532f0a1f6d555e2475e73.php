<div class="modal-header">
  <button type="button" class="close" aria-hidden="true" ng-click="cancel()"></button>
    <h3 class="modal-title" id="modal-title">{{ response.title }}</h3>

    <div class="actions">
      <a href="{{ response.link+'?'+( (queryString == '') ? 'export=1' : queryString+'&export=1' ) }}" class="btn btn-sm default pull-right"><i class="fa fa-download"></i> Export</a> &nbsp;
      <a href="javascript:void(0)" ng-click="openStockAbnormal()" class="btn btn-sm default pull-right">Identify Abnormal</a>&nbsp;
    </div>
</div>
<div class="modal-body" id="modal-body">
  <div class="row margin-bottom-10">
    <div class="col-md-12 text-right">
      <?php echo $__env->make('pages.partials.datagrid.datagrid-status', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </div>
    <div class="col-md-8">
      <div class="table-filter row">
        <div class="col-md-4" style="padding-right: 0;">
          <div class="input-icon input-icon-sm right">
            <i class="fa fa-search"></i>
            <input type="text" ng-model="keyword" class="form-control input-sm" placeholder="<?php echo app('translator')->getFromJson('system.datagrid.filter.search'); ?>" ng-change="keywordChanged()">
          </div>
        </div>
        <div class="col-md-3" style="padding-right: 0; padding-left: 2;">
          <div class="input-icon input-icon-sm left">
            <i class="fa fa-calendar right"></i>
            <input type="text" ng-model="datestart" class="form-control input-sm date-picker" placeholder="Tanggal Mulai" ng-change="dateStartChanged($event)" id="datestart">
          </div>
        </div>
        <div class="col-md-3" style="padding-right: 0; padding-left: 2;">
          <div class="input-icon input-icon-sm left">
            <i class="fa fa-calendar right"></i>
            <input type="text" ng-model="dateend" class="form-control input-sm date-picker" placeholder="Tanggal Selesai" ng-change="dateEndChanged($event)" id="dateend">
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="table-responsive">
    <table class="table table-hover table-light dataTable" border="0" cellspacing="0" cellpadding="0">
      <thead>
        <tr>
          <th class="table-select" ng-if="response.massAction.length > 0">
            <label class="mt-checkbox mt-checkbox-outline">
                <input type="checkbox" ng-model="toggle" ng-change="allChecked()" />
                <span></span>
            </label>
          </th>
          <th ng-repeat="column in response.columns">
            <div ng-click="setParams('sort', (column.sorting == 'asc' ? '-' : '')+column.source)" ng-class="getSortingStatus(column)" class="th-inner">
            {{ column.label }}
            </div>
          </th>
        </tr>
      </thead>
      <tbody>
        <tr ng-if="response.currentPage == 1">
          <td colspan="7" style="font-weight: bold;">Stok Awal</td>
          <td style="font-weight: bold;">{{ response.beginQty | number:2 }}</td>
          <td></td>
        </tr>
        <tr ng-repeat="dt in response.data">
          <td class="table-select" ng-if="response.massAction.length > 0">
            <label class="mt-checkbox mt-checkbox-outline" ng-init="initSelect(dt)">
                <input type="checkbox" name="check" ng-model="selected[$index].isChecked"/>
                <span></span>
            </label>
          </td>
          <td compile-template ng-bind-html="dt['date']"></td>
          <td compile-template ng-bind-html="dt['code']"></td>
          <td compile-template ng-bind-html="dt['description']"></td>
          <td compile-template ng-bind-html="dt['source_name']"></td>
          <td compile-template ng-bind-html="dt['destination_name']"></td>
          <td compile-template ng-bind-html="dt['qty_in']"></td>
          <td compile-template ng-bind-html="dt['qty_out']"></td>
          <td>{{ dt.count_stock | number:2 }}</td>

          <!-- <td ng-repeat="column in response.columns" compile-template ng-bind-html="dt[column.source]"></td> -->
        </tr>
        <tr ng-if="response.currentPage == response.lastPage">
          <td colspan="7" style="font-weight: bold;">Stok Akhir</td>
          <td style="font-weight: bold;">{{ response.endQty | number:2 }}</td>
          <td></td>
        </tr>
      </tbody>
    </table>
  </div>
  <p class="text-center margin-top-10 margin-bottom-10 caption10" ng-if="response.data.length <= 0"><?php echo app('translator')->getFromJson('system.datagrid.table.no_record'); ?></p>

  <div class="row margin-top-10">
    <div class="col-md-6">
      <div ng-if="response.massAction.length > 0">
        <?php echo $__env->make('pages.partials.datagrid.datagrid-mass-button', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
    </div>
    <div class="col-md-6">
      <?php echo $__env->make('pages.partials.datagrid.datagrid-pagination', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </div>
  </div>
</div>
