<?php echo $__env->make('auth.login-header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!-- BEGIN FORGOT PASSWORD FORM -->
    <h3 class="font-green"><i class="icon-check"></i>&nbsp; Berhasil</h3>
    <p> Periksa email anda!, kami telah mengirimkan tautan untuk merubah password. </p>
    <a href="login" class="btn green btn-outline">Kembali</a>
<!-- END FORGOT PASSWORD FORM -->
<?php echo $__env->make('auth.login-footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
