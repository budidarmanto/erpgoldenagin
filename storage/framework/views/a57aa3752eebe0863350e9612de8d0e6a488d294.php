<!-- BEGIN MAIN CONTENT -->
<div class="row">
    <div class="col-md-12">
      <!-- BEGIN: ACCORDION DEMO -->
        <form role="form" id="{{ response.id }}">
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption font-green-sharp">
                        <span class="caption-subject bold uppercase ng-binding">Manufacture Order</span> 
                        &nbsp;&nbsp; 
                        <span class="btn-status" style="background: {{ getStatus(data.status).color }}; color: #fff; padding: 3px 6px;" ng-hide="response.action == 'create'">{{ getStatus(data.status).label }}</span>
                    </div>
                    <?php echo $__env->make('pages.partials.form.form-action', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <div class="actions" ng-hide="getStatus(data.status).text.length <= 0 || response.action == 'create' || response.role.indexOf('change') == -1" style="margin-left: 40px;float: left;">
                        <a href="#" onclick="return false;" class="btn blue btn-outline mt-ladda-btn ladda-button" ng-repeat="next in getStatus(data.status).next" data-style="zoom-out"  ng-click="changeStatus(next.id, $event)" style="margin-right: 5px;" >{{ next.text }}</a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row margin-bottom-5 margin-top-20">
                        <div class="col-md-8 col-md-offset-2">
                            <form-builder field="code"></form-builder>
                            <table style="width: 100%;">
                                <tbody>
                                    <tr>
                                        <td style="width: 75%; white-space: nowrap; padding-right: 15px;">
                                            <div class="form-group form-md-line-input form-md-floating-label" style="padding: 0 0 2px !important;">
                                                <label>Product</label><br />
                                                <input type="hidden" required name="product" ng-value="data.product" />
                                                <a 
                                                    href="#" 
                                                    class="btn btn-sm btn-success" 
                                                    ng-click="showPopupProduct(null, true)"
                                                    ng-if="response.action!='view'"> Pilih <i class="fa fa-search"></i>
                                                </a>
                                                <input type="text" style="display: inline-block; width: 100%; border: 0; margin-left: 15px;" name="productName" required readonly class="input-sm form-control" ng-disabled="response.action=='view'" aria-required="true" ng-value="productName">
                                            </div>
                                        </td>
                                        <td style="width: 25%; padding-left: 15px;">
                                            <form-builder field="qty"></form-builder>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <form-builder field="routing"></form-builder>
                            <div class="form-group form-md-line-input form-md-floating-label" style="padding: 0 0 2px !important;">
                                <label>Responsible</label><br />
                                <input type="hidden" required name="pic" ng-value="data.pic" />
                                <a 
                                    href="#" 
                                    class="btn btn-sm btn-success" 
                                    ng-click="showPopupUser()"
                                    ng-if="response.action!='view'"> Pilih <i class="fa fa-search"></i>
                                </a>
                                <input type="text" style="display: inline-block; width: auto; border: 0; margin-left: 15px;" required readonly class="input-sm form-control" ng-disabled="response.action=='view'" aria-required="true" name="picName" ng-value="picName">
                            </div>
                        </div>
                    </div>
                </div>
                
                
            </div>
            <div class="portlet" style="margin-bottom: 100px;">
                <div class="portlet-title">
                    <div class="caption">Bahan Baku</div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-light dataTable" border="0" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Bahan Baku</th>
                                    <th>Jumlah Dibutuhkan</th>
                                    <th>Stok</th>
                                    <th>Selisih</th>
                                    <th>Total Stok</th>
                                    <th>Satuan</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="bom in dataBom">
                                    <td>{{ $index + 1 }}.</td>
                                    <td id="bom-name">{{ bom.name }}</td>
                                    <td>{{ bom.required | number:2 }}</td>
                                    <td>{{ bom.stock | number:2 }}</td>
                                    <td>{{ (bom.stock - bom.required) | number:2 }}</td>
                                    <td ng-click="showPopupStock(bom.dataStock)" style="cursor: pointer;">{{ bom.total_stock | number:2 }}</td>
                                    <td>{{ bom.unit_name }}</td>
                                    <td class="text-center">
                                        <span class="label {{bom.stock > 0 ? (bom.stock >= bom.required ? 'label-success' : 'label-warning') : 'label-danger'}}">
                                            {{bom.stock > 0 ? (bom.stock >= bom.required ? 'Available' : 'Partial Available') : 'Not Available'}}
                                        </span>
                                    </td>
                                    <td class="text-center">
                                        <!--span class="label label-warning" ng-hide="{{bom.category == 'Barang Jadi' ? (bom.stock > 0 || bom.stock < bom.required  ? 'true' : 'false') : 'true'}}">
                                            Will created
                                        </span-->
                                        <!-- <span ng-if="bom.mo_created == 1 && bom.create_mo == 0" class="label {{bom.mo_created > 0 ? 'label-warning' : ''}}">
                                            {{bom.mo_created > 0 ? 'MO Will Created' : ''}}
                                        </span> -->
                                        <span style="cursor:pointer" ng-click="createSubMO(bom.product)" ng-if="bom.create_mo" class="label label-success">
                                            Make to Order
                                        </span>
                                    </td>                                    
                                </tr>
                                <tr ng-if="dataBomLoading">
                                    <td colspan="5">Loading...</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- END MAIN CONTENT -->


<!-- POPUP STOCK TEMPLATE -->
<script type="text/ng-template" id="modalStock.html">
  <div class="modal-header">
      <h3 class="modal-title" id="modal-title">Persediaan</h3>
  </div>
  <div class="modal-body" id="modal-body">
    <table class="table table-hover table-light dataTable" border="0" cellspacing="0" cellpadding="0">
      <thead>
        <tr>
          <th>Gudang</th>
          <th class="text-right">Jumlah</th>
          <th>Satuan Ukuran</th>
        </tr>
      </thead>
      <tbody>
        <tr ng-repeat="dt in data">
          <td>{{ dt.warehouse_location_name }}</td>
          <td class="text-right">{{ dt.qty | number:2 }}</td>
          <td>{{ dt.uom_name }}</td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <td style="text-align:right">Jumlah</td>
          <td class="text-right">{{ sumStock | number:2 }}</td>
          <td></td>
        </tr>
      </tfoot>
    </table>
  </div>
</script>
<!-- END POPUP STOCK TEMPLATE -->