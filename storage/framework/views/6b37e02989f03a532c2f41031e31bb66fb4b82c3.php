<!-- BEGIN MAIN CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN: ACCORDION DEMO -->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption font-red">
                    <!-- <i class="icon-settings font-green-sharp"></i> -->
                    <span class="caption-subject bold uppercase"> {{ response.title }} </span>
                    <span class="caption-helper hide"></span>
                </div>

                <div class="actions">
                    <a target="_blank" href="{{ response.link+( (queryString == '') ? '?export=1' : queryString+'&export=1' ) }}" class="btn btn-sm default"><i class="fa fa-download"></i> Export</a>
                    <a href="#/{{ response.create }}" class="btn btn-sm btn-primary" ng-if="response.role.indexOf('create') != -1"> <?php echo app('translator')->getFromJson('system.datagrid.header.create'); ?> <i class="fa fa-plus"></i></a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-6 col-md-offset-6 text-right">
                        <?php echo $__env->make('pages.partials.datagrid.datagrid-status', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    </div>
                </div>
                <div class="row margin-bottom-10">
                    <div class="col-md-12">
                        <div class="table-filter row">
                            <div class="col-md-3" style="padding-right: 0;">
                                <div class="input-icon input-icon-sm right">
                                    <i class="fa fa-search"></i>
                                    <input type="text" ng-model="keyword" class="form-control input-sm" placeholder="<?php echo app('translator')->getFromJson('system.datagrid.filter.search'); ?>" ng-change="keywordChanged()">
                                </div>
                            </div>
                            <div class="col-md-9" style="padding-right: 0; padding-left: 2;">
                                <select style="display: inline-block; width: auto;" ng-model="workflow" class="form-control input-sm" ng-options="item.id as item.label for item in response.optionWorkflow" ng-change="workflowChanged()">
                                    <option value="">-- Semua Status --</option>
                                </select>
                                <div id="date-range" class="tooltips btn btn-fit-height green" >
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Start Table -->
                <div class="table-responsive">
                    <table class="table table-hover table-light dataTable" border="0" cellspacing="0" cellpadding="0">
                        <thead>
                            <tr>
                                <th ng-repeat="column in response.columns">
                                    <div ng-click="setParams('sort', (column.sorting == 'asc' ? '-' : '')+column.source)" ng-class="getSortingStatus(column)" class="th-inner">
                                    {{ column.label }}
                                    </div>
                                </th>
                                <th style="width: 120px;" class="text-center"><?php echo app('translator')->getFromJson('system.datagrid.table.action'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="dt in response.data" ng-dblclick="handleDoubleClick(dt['id'])">
                                <td>{{ dt.code }}</td>
                                <td>{{ dt.product_name }}</td>
                                <td>{{ dt.qty }}</td>
                                <td>{{ dt.date_start }}</td>
                                <td>{{ dt.date_end }}</td>
                                <td>{{ dt.pic_name }}</td>
                                <td>
                                    <div class="btn-group dropdown-status">
                                        <button class="btn-status" type="button" data-toggle="dropdown" style="background-color: {{ getStatus(dt.status).color }}">{{ getStatus(dt.status).label }}
                                            <i class="fa fa-angle-down" ng-hide="getStatus(dt.status).next.length <= 0 || response.role.indexOf('change') == -1"></i>
                                        </button>
                                        <ul class="dropdown-menu" role="menu" style="right: 0; left: auto;" ng-hide="response.role.indexOf('change') == -1">
                                            <li ng-repeat="next in getStatus(dt.status).next">
                                                <a href="javascript:;" ng-click="changeStatus(dt.id, next.id)"> {{ next.text }} </a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <a href="#/{{ response.modify+'/'+dt['id'] }}" class="btn btn-icon-only tooltips" data-original-title="<?php echo app('translator')->getFromJson('system.datagrid.table.modify'); ?>" data-placement="left" ng-if="response.role.indexOf('modify') != -1 && dt['status'] == '1MD'">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <a href="#/{{ response.modify+'/'+dt['id'] }}" class="btn btn-icon-only tooltips" data-original-title="View" data-placement="left" ng-if="response.role.indexOf('modify') == -1 || dt['status'] != '1MD'">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    <a href="javascript:;" class="btn btn-icon-only tooltips" data-original-title="<?php echo app('translator')->getFromJson('system.datagrid.table.delete'); ?>" data-placement="left" delete-confirm-ajax="{{ dt['id'] }}" ng-if="response.role.indexOf('delete') != -1 && dt['status'] == '1MD'">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <p class="text-center margin-top-10 margin-bottom-10 caption10" ng-if="response.data.length <= 0"><?php echo app('translator')->getFromJson('system.datagrid.table.no_record'); ?></p>
                <!-- End Table -->
                <div class="row margin-top-10">
                    <div class="col-md-6">
                        <div ng-if="response.massAction.length > 0">
                            <!-- <?php echo $__env->make('pages.partials.datagrid.datagrid-mass-button', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> -->
                        </div>
                    </div>
                    <div class="col-md-6">
                        <?php echo $__env->make('pages.partials.datagrid.datagrid-pagination', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MAIN CONTENT -->
