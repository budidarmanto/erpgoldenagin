<!-- BEGIN MAIN CONTENT -->
<div class="row">
    <div class="col-md-12">
      <!-- BEGIN: ACCORDION DEMO -->
        <form role="form" id="{{ response.id }}">
            <div class="portlet">
                <div class="portlet-title">
                    <?php echo $__env->make('pages.partials.form.form-title', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php echo $__env->make('pages.partials.form.form-action', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
                <div class="portlet-body">
                    <?php echo $__env->make('pages.partials.form.form-content', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption">Operasi</div>
                            <div class="actions">
                                <a href="#" class="btn btn-sm btn-success" ng-click="showPopupOperation()"  ng-if="response.action!='view'"> Tambah <i class="fa fa-plus"></i></a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-responsive">
                                <table class="table table-hover table-light dataTable" border="0" cellspacing="0" cellpadding="0">
                                    <thead>
                                        <tr>
                                            <th>Sequence</th>
                                            <th>Nama Operasi</th>
                                            <th>Work Center</th>
                                            <th>Durasi</th>
                                            <th style="width: 120px;" class="text-center" ng-if="response.action!='view'">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="operation in dataOperation | orderBy: 'sequence'" ng-dblclick="showPopupOperation(operation)">
                                            <td>{{ $index + 1 }}</td>
                                            <td>{{ operation.name }}</td>
                                            <td>{{ operation.workcenter_name }}</td>
                                            <td>{{ operation.duration }} {{ getDurationName(operation.duration_uom) }}</td>
                                            <td class="text-center" ng-if="response.action!='view'">
                                                <a href="javascript:;" class="btn btn-icon-only tooltips" data-original-title="Modify" data-placement="left" ng-click="showPopupOperation(operation)">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                <a href="javascript:;" class="btn btn-icon-only tooltips" data-original-title="Delete" data-placement="left" delete-confirm on-delete="deleteOperation(operation)">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- END MAIN CONTENT -->

<!-- EDIT OPERATION POPUP TEMPLATE -->
<script type="text/ng-template" id="modalOperation.html">
    <form>
        <div class="modal-header">
            <h3 class="modal-title" id="modal-title">Operasi</h3>
        </div>
        <div class="modal-body" id="modal-body">
            <form-builder field="name"></form-builder>
            <form-builder field="work_center"></form-builder>
            <form-builder field="description"></form-builder>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group form-md-line-input">
                        <form-builder field="duration" form-group="false"></form-builder>
                        <label>Durasi</label>
                    </div>
                </div>
                <div class="col-md-6">
                    <form-builder field="duration_uom" label="false"></form-builder>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group form-md-line-input">
                        <form-builder field="capacity" form-group="false"></form-builder>
                        <label>Kapasitas</label>
                    </div>
                </div>
                <div class="col-md-6">
                    <form-builder field="resource" label="false"></form-builder>
                </div>
            </div>
            <div class="form-group form-md-line-input ng-scope">
                <label>Worksheet</label>
                <input type="file" name="worksheet" ng-model="worksheet" custom-on-change="prepareUpload" /><br />
                <span ng-bind-html="worksheetFile | to_trusted"></span>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-sm btn-default" type="button" ng-click="cancel()">Batal&nbsp;&nbsp;<i class="fa fa-times"></i></button>
            <button class="btn btn-sm green-haze" type="submit" ng-click="save(data, $event)">Simpan&nbsp;&nbsp;<i class="fa fa-floppy-o"></i></button>
        </div>
    </form>
</script>
<!-- END EDIT OPERATION POPUP TEMPLATE -->
