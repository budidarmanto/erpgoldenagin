<!-- BEGIN MAIN CONTENT -->
<div class="row">
  <div class="col-md-12">
      <!-- BEGIN: ACCORDION DEMO -->
    <form role="form" id="{{ response.id }}">
      <div class="portlet">
        <div class="portlet-title">
          <?php echo $__env->make('pages.partials.form.form-title', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          <?php echo $__env->make('pages.partials.form.form-action', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>
        <div class="portlet-body">
          <?php echo $__env->make('pages.partials.form.form-content', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

          <div class="portlet">
            <div class="portlet-title">
              <div class="caption">Varian</div>
              <div class="actions">
                <a href="#" class="btn btn-sm btn-success" ng-click="showPopupAttribute()"  ng-if="response.action!='view'"> Tambah <i class="fa fa-plus"></i></a>
              </div>
            </div>
            <div class="portlet-body">
              <div class="table-responsive">
                <table class="table table-hover table-light dataTable" border="0" cellspacing="0" cellpadding="0">
                  <thead>
                    <tr>
                      <th>Nilai</th>
                      <th style="width: 120px;" class="text-center" ng-if="response.action!='view'">Tindakan</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr ng-repeat="attribute in dataAttribute" ng-dblclick="showPopupAttribute(attribute)">
                      <td>{{ attribute.name }}</td>
                      <td class="text-center" ng-if="response.action!='view'">
                        <a href="javascript:;" class="btn btn-icon-only tooltips" data-original-title="Modify" data-placement="left" ng-click="showPopupAttribute(attribute)">
                          <i class="fa fa-edit"></i>
                        </a>
                        <a href="javascript:;" class="btn btn-icon-only tooltips" data-original-title="Delete" data-placement="left" delete-confirm on-delete="deleteAttribute(attribute)">
                          <i class="fa fa-trash"></i>
                        </a>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
<!-- END MAIN CONTENT -->

<!-- EDIT MODULE POPUP TEMPLATE -->
<script type="text/ng-template" id="modalAttribute.html">
  <div class="modal-header">
      <h3 class="modal-title" id="modal-title">Nilai Varian</h3>
  </div>
  <div class="modal-body" id="modal-body">
    <div ng-repeat="field in response.fields">
      <form-builder field="{{ field.name }}"></form-builder>
    </div>
  </div>
  <div class="modal-footer">
      <button class="btn btn-sm btn-default" type="button" ng-click="cancel()">Batal&nbsp;&nbsp;<i class="fa fa-times"></i></button>
      <button class="btn btn-sm green-haze" type="submit" ng-click="save(data, $event)">Simpan&nbsp;&nbsp;<i class="fa fa-floppy-o"></i></button>
  </div>
</script>
<!-- END EDIT MODULE POPUP TEMPLATE -->
