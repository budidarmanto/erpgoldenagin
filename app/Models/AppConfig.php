<?php

namespace App\Models;

use Devplus\Model\DevplusModel;

class AppConfig extends DevplusModel
{
  protected $table = 'app_config';
  protected $fillable = ['id', 'code', 'value', 'description'];
}
