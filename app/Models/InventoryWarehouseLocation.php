<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;
use Auth;

class InventoryWarehouseLocation extends DevplusModel
{
  protected $table = 'inventory_warehouse_location';
  protected $fillable = ['id', 'code', 'warehouse', 'name', 'active'];

  public static function getAuthOption($warehouse = null, $model = null){
    $listWarehouseLocation = Auth::user()->listWarehouseLocation();

    $query = null;
    if($model == null){
      $query = InventoryWarehouseLocation::select([
        'inventory_warehouse_location.id',
        'inventory_warehouse_location.name',
        'inventory_warehouse.id AS warehouse',
        'inventory_warehouse.name AS warehouse_name',
        'inventory_warehouse.company',
      ])
      ->join('inventory_warehouse', 'inventory_warehouse.id', '=', 'inventory_warehouse_location.warehouse');
      if(!empty($warehouse)){
        $query->where('inventory_warehouse.id', $warehouse);
      }
    }else{
      $query = $model;
    }

    $query->whereIn('inventory_warehouse_location.id', $listWarehouseLocation);

    return InventoryWarehouseLocation::getOption(null, $query);
  }

  public static function getOption($warehouse = null, $model = null){
    $company = Helper::currentCompany();
    $dataWarehouse = [];
    if($model == null){
        $query = InventoryWarehouseLocation::select([
          'inventory_warehouse_location.id',
          'inventory_warehouse_location.name',
          'inventory_warehouse.id AS warehouse',
          'inventory_warehouse.name AS warehouse_name',
          'inventory_warehouse.company',
        ])
        ->join('inventory_warehouse', 'inventory_warehouse.id', '=', 'inventory_warehouse_location.warehouse');
        if(!empty($warehouse)){
          $query->where('inventory_warehouse.id', $warehouse);
        }
        $data = $query->orderBy('inventory_warehouse.name', 'ASC')->orderBy('inventory_warehouse_location.name', 'ASC')->get();
    }else{
      $data = $model->orderBy('inventory_warehouse.name', 'ASC')->orderBy('inventory_warehouse_location.name', 'ASC')->get();
    }

    foreach($data as $val){
      $listCompany = json_decode($val->company, true);
      if(!empty($listCompany) && in_array($company, $listCompany)){
        $val['name'] = $val['warehouse_name'].'/'.$val['name'];
        $dataWarehouse[] = $val;
      }
    }

    $optionWarehouseLoc = [];
    if(count($dataWarehouse) > 0){
      foreach($dataWarehouse as $val){
        $optionWarehouseLoc[] = [
          'id' => $val['id'],
          'text' => $val['name'],
          'warehouse' => $val['warehouse']
        ];
      }
    }

    return $optionWarehouseLoc;
  }

}
