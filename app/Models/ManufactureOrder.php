<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Devplus\Helper\Helper;
use Devplus\Params\Params;
use Illuminate\Support\Facades\Input;

class ManufactureOrder extends DevplusModel
{
    public $updateFirstWo = true;
    protected $table = 'manufacture_order';
    protected $fillable = [
        'id',
        'code',
        'company',
        'product',
        'qty',
        'scrap_total',
        'date_start',
        'date_end',
        'pic',
        'status',
        'stock_movement_ref',
        'stock_movement_ref_bom',
        'routing',
        'location',
    ];
    
    public static function boot() {
        parent::boot();

        //while creating/inserting item into db  
        static::creating(function ($table) {
            $productId = $table->product;
            $productMaster = ProductMaster::select([
                        'product_master.*',
                    ])
                    ->join('product_product', 'product_product.product', '=', 'product_master.id')
                    ->where('product_master.id', $productId)
                    ->orWhere('product_product.id', $productId)
                    ->first();
            if(!empty($productMaster)) {
                $table->product = $productMaster->id;
            }
            
            if(empty($table->routing) && !empty($productMaster)) {
                $table->routing = $productMaster->routing;
            }
        });
        
        static::created(function($table) {
            // $productMaster = ProductMaster::where('id', $table->product)
            //         ->first();
            // if(!empty($productMaster)) {
            //     $routingOperations = ManufactureRoutingOperation::query()
            //     ->where('routing', empty($table->routing) ? $productMaster->routing : $table->routing)
            //     ->get();
            //     $optionWorkflow = SettingWorkflow::getOption('WO');
            //     foreach($routingOperations as $ro) {
            //         $wo = new ManufactureWorkOrder();
            //         $wo->manufacture_order = $table->id;
            //         $wo->routing_operation = $ro->id;
            //         if($ro->sequence === 1) {
            //             $wo->status = $optionWorkflow[0]['id'];
            //         }
            //         $wo->save();
            //     }
            // }
        });
        
        static::updating(function ($table) {
            $old_value = $table->getOriginal();

            $description = Input::get('description');
            $scrap_bb = Input::get('scrap_bb');
            $scrap_bj = Input::get('scrap_bj');
            $qtyConsumed = Input::get('qty_consumed'); 

            //dd($table->toArray());

            $productId = $table->product;
            $productMaster = ProductMaster::select([
                        'product_master.*',
                    ])
                    ->join('product_product', 'product_product.product', '=', 'product_master.id')
                    ->where('product_master.id', $productId)
                    ->orWhere('product_product.id', $productId)
                    ->first();
            $productProduct = ProductProduct::select([
                        'product_product.*',
                    ])
                    ->join('product_master', 'product_product.product', '=', 'product_master.id')
                    ->where('product_master.id', $productMaster->id)
                    ->first();
            if(!empty($productMaster)) {
                $table->product = $productMaster->id;
            }
            if(empty($table->routing) && !empty($productMaster)) {
                $table->routing = $productMaster->routing;
            }
            
            if($table->status == '2MS') {
                $table->status = '3MW';
                $table->date_start = date('Y-m-d H:i:s');
            }

            if($old_value['status'] != '4MD' && $table->status == '4MD') {
               
                $boms = ManufactureOrderBom::with('get_product_bom.get_uom')->where('manufacture_order', $table->id)->get();
                
                //dd($boms->toArray());

                $table->date_end = date('Y-m-d');

                // Create stock movement product bom wasted
                $stockMovementMoWasted = new InventoryStockMovement();
                $isCurrency = (Params::get('MULTI_CURRENCY') == 1) ? true : false;
                $defaultCurrency = SettingCurrency::getDefaultCurrency();
                if(!$isCurrency){
                    $stockMovementMoWasted->currency = $defaultCurrency;
                }
                //MO
                $stockMovementMoWasted->code = InventoryStockMovement::generateId('AD', Helper::currentCompany(), $table->get_routing->location_waste);
                $stockMovementMoWasted->date = date('Y-m-d');
                $stockMovementMoWasted->company = Helper::currentCompany();
                $stockMovementMoWasted->description = 'Penyesuaian untuk limbah bahan baku produk MO nomor ' . $table->code . '.';
                $stockMovementMoWasted->type = 'AD';
                $stockMovementMoWasted->source = $table->get_routing->location_waste;

                $stockMovementMoWasted->status = '1AC';
                $stockMovementMoWasted->save();
                
                foreach($boms as $bom) {
                    if(isset($qtyConsumed[$bom->id])) {
                        $stockMovementProduct = new InventoryStockMovementProduct();
                        $stockMovementProduct->stock_movement = $stockMovementMoWasted->id;
                        $stockMovementProduct->product = $bom->get_product_bom->product;
                        $stockMovementProduct->uom = $bom->get_product_bom->uom;
                        $stockMovementProduct->qty = $qtyConsumed[$bom->id] * $bom->get_product_bom->get_uom->ratio;
                        $stockMovementProduct->status = 0;
                        $stockMovementProduct->description = $description;
                        $stockMovementProduct->save();
                    }
                }

                // Create stock movement product master
                $stockMovementMo = new InventoryStockMovement();
                $isCurrency = (Params::get('MULTI_CURRENCY') == 1) ? true : false;
                $defaultCurrency = SettingCurrency::getDefaultCurrency();
                if(!$isCurrency){
                    $stockMovementMo->currency = $defaultCurrency;
                }
                //MO
                $stockMovementMo->code = InventoryStockMovement::generateId('AD', Helper::currentCompany(), $table->get_routing->location_result);
                $stockMovementMo->date = date('Y-m-d');
                $stockMovementMo->company = Helper::currentCompany();
                $stockMovementMo->description = 'Penyesuaian untuk produk jadi MO nomor ' . $table->code . '.';
                $stockMovementMo->type = 'AD';
                $stockMovementMo->source = $table->get_routing->location_result;

                $stockMovementMo->status = '1AC';
                $stockMovementMo->save();
                
                $stockMovementProductMo = new InventoryStockMovementProduct();
                $stockMovementProductMo->stock_movement = $stockMovementMo->id;
                $stockMovementProductMo->product = $productProduct->id;
                $stockMovementProductMo->uom = $productMaster->uom;
                $stockMovementProductMo->qty = $scrap_bb;
                $stockMovementProductMo->status = 0;
                $stockMovementProductMo->description = $description;
                $stockMovementProductMo->save();
                
                
                // Create stock movement product scrap
                $stockMovementMoScrap = new InventoryStockMovement();
                $isCurrency = (Params::get('MULTI_CURRENCY') == 1) ? true : false;
                $defaultCurrency = SettingCurrency::getDefaultCurrency();
                if(!$isCurrency){
                    $stockMovementMoScrap->currency = $defaultCurrency;
                }
                //MOS
                $stockMovementMoScrap->code = InventoryStockMovement::generateId('AD', Helper::currentCompany(), $table->get_routing->location_scrap);
                $stockMovementMoScrap->date = date('Y-m-d');
                $stockMovementMoScrap->company = Helper::currentCompany();
                $stockMovementMoScrap->description = 'Penyesuaian untuk produk scrap MO nomor ' . $table->code . '.';
                $stockMovementMoScrap->type = 'AD';
                $stockMovementMoScrap->source = $table->get_routing->location_scrap;

                $stockMovementMoScrap->status = '1AC';
                $stockMovementMoScrap->save();
                
                $stockMovementProductMoScrap = new InventoryStockMovementProduct();
                $stockMovementProductMoScrap->stock_movement = $stockMovementMoScrap->id;
                $stockMovementProductMoScrap->product = $productProduct->id;
                $stockMovementProductMoScrap->uom = $productMaster->uom;
                $stockMovementProductMoScrap->qty = $scrap_bj;
                $stockMovementProductMoScrap->status = 0;
                $stockMovementProductMoScrap->description = $description;
                $stockMovementProductMoScrap->save();
                
                $table->stock_movement_ref = $stockMovementMo->id . '|' . $stockMovementMoScrap->id . '|' . $stockMovementMoWasted->id;
            }
            elseif ($table->status == '2MC') {
                $stockMovement = InventoryStockMovement::where('id', $table->stock_movement_ref)->first();
                if(!empty($stockMovement)) {
                    $stockMovementProducts = InventoryStockMovementProduct::where('stock_movement', $stockMovement)->get();
                    foreach($stockMovementProducts as $stockMovementProduct) {
                        $stockMovementProduct->delete();
                    }
                    $stockMovement->delete();
                }
                $table->stock_movement_ref = null;
                $table->stock_movement_ref_bom = null;
            }
        });
        
        static::updated(function($table) {
            $old_value = $table->getOriginal();


            //dd($old_value, $table->status);

            $productMaster = ProductMaster::where('id', $table->product)
                    ->first();
            if(!empty($productMaster)) {
                $oldRouting = ManufactureWorkOrder::select([
                    'manufacture_routing_operation.routing',
                ])
                ->join('manufacture_routing_operation', 'manufacture_routing_operation.id', '=', 'manufacture_work_order.routing_operation')
                ->where('manufacture_work_order.manufacture_order', $table->id)
                ->first();
            }

            if($old_value['status'] != '3MW' && $table->status == '3MW') {
                $routingOperations = ManufactureRoutingOperation::query()
                ->where('routing', $table->routing)
                ->get();
                $optionWorkflow = SettingWorkflow::getOption('WO');

                $check_exists = ManufactureWorkOrder::where('manufacture_order', $table->id)->get();

                if(!count($check_exists)) {
                    foreach($routingOperations as $ro) {
                        $wo = new ManufactureWorkOrder();
                        $wo->manufacture_order = $table->id;
                        $wo->routing_operation = $ro->id;
                        if($ro->sequence === 1) {
                            $wo->status = $optionWorkflow[0]['id'];
                        }
                        $wo->save();
                    }
                }

                $boms = ManufactureOrderBom::with('get_product_bom.get_uom')->where('manufacture_order', $table->id)->get();
                    
                // Create stock movement product bom
                $stockMovementMoBom = new InventoryStockMovement();
                $isCurrency = (Params::get('MULTI_CURRENCY') == 1) ? true : false;
                $defaultCurrency = SettingCurrency::getDefaultCurrency();
                if(!$isCurrency){
                    $stockMovementMoBom->currency = $defaultCurrency;
                }
                //MO
                $stockMovementMoBom->code = InventoryStockMovement::generateId('AD', Helper::currentCompany(), $table->get_routing->location_raw);
                $stockMovementMoBom->date = date('Y-m-d');
                $stockMovementMoBom->company = Helper::currentCompany();
                $stockMovementMoBom->description = 'Penyesuaian untuk bahan baku produk MO nomor ' . $table->code . '.';
                $stockMovementMoBom->type = 'AD';
                $stockMovementMoBom->source = $table->get_routing->location_raw;

                $stockMovementMoBom->status = '1AC';
                $stockMovementMoBom->save();
                
                foreach($boms as $bom) {
                    $stockMovementProduct = new InventoryStockMovementProduct();
                    $stockMovementProduct->stock_movement = $stockMovementMoBom->id;
                    $stockMovementProduct->product = $bom->get_product_bom->product;
                    $stockMovementProduct->uom = $bom->get_product_bom->uom;
                    $stockMovementProduct->qty = $bom->qty_total * $bom->get_product_bom->get_uom->ratio * -1;
                    $stockMovementProduct->status = 0;
                    $stockMovementProduct->save();
                }
            }

            if(($table->status == '2MS' || $table->status == '3MW') && $table->updateFirstWo) {
                $firstWorkOrder = ManufactureWorkOrder::select([
                    'manufacture_work_order.*',
                ])
                ->join('manufacture_routing_operation', 'manufacture_routing_operation.id', '=', 'manufacture_work_order.routing_operation')
                ->where([
                    'manufacture_order' => $table->id,
                    'manufacture_routing_operation.sequence' => 1,
                ])
                ->first();
                if(!empty($firstWorkOrder)) {
                    $firstWorkOrder->status = '2WR';
                    $firstWorkOrder->save();
                }
            }
        });
    }
    
    public function get_routing() {
        return $this->belongsTo('App\Models\ManufactureRouting', 'routing');
    }

    public static function generateId(){
        $query = ManufactureOrder::orderBy('code', 'DESC');
        $prefix = 'MO.'.date('ym');

        return Helper::generateId($query, 'code', $prefix);
    }
}
