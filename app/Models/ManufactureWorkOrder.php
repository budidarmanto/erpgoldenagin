<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;

class ManufactureWorkOrder extends DevplusModel
{
    protected $table = 'manufacture_work_order';
    protected $fillable = [
        'id',
        'manufacture_order',
        'routing_operation',
        'date_start',
        'date_end',
        'scrap',
        'status',
    ];
    
    public static function boot() {
        parent::boot();
        
        static::updating(function($table) {
            if($table->status == '3WS') {
                $table->status = '4WP';
                $table->date_start = date('Y-m-d H:m:s');
            }
            if($table->status == '5WD') {
                $table->date_end = date('Y-m-d H:m:s');
            }
            
            if($table->status == '4WH') {
                $pending = new ManufactureWorkOrderPending();
                $pending->work_order = $table->id;
                $pending->pending_start = date('Y-m-d H:m:s');
            }
            if($table->status == '4WP') {
                $oldWo = ManufactureWorkOrder::where('id', $table->id)->first();
                if($oldWo->status == '4WH') {
                    $pending = ManufactureWorkOrderPending::where('work_order', $table->id)->orderBy('pending_start', 'desc')->first();
                    if(!empty($pending)) {
                        $pending->pending_end = date('Y-m-d H:m:s');
                    }
                }
            }
        });
        
        static::updated(function($table) {
            if($table->status == '5WD') {
                $currentWo = ManufactureWorkOrder::select([
                    'manufacture_work_order.*',
                    'manufacture_routing_operation.sequence',
                    'manufacture_routing_operation.next_sequence',
                ])
                ->join('manufacture_routing_operation', 'manufacture_routing_operation.id', '=', 'manufacture_work_order.routing_operation')
                ->where('manufacture_work_order.id', $table->id)
                ->first();
                
                $mo = ManufactureOrder::where('id', $table->manufacture_order)->first();
                if(!empty($mo)) {
                    $mo->scrap_total += $table->scrap;
                }
                
                $nextWo = ManufactureWorkOrder::select([
                    'manufacture_work_order.*',
                    'manufacture_routing_operation.sequence',
                ])
                ->join('manufacture_routing_operation', 'manufacture_routing_operation.id', '=', 'manufacture_work_order.routing_operation')
                ->where('manufacture_routing_operation.sequence', $currentWo->next_sequence)
                ->where('manufacture_work_order.manufacture_order', '=', $mo->id)
                ->first();
                
                if(empty($nextWo)) {
                    if(!empty($mo)) {
                        $mo->status = '4MD';
                        $mo->date_end = date('Y-m-d H:m:s');
                    }
                    $currentWo->status = '6FF';
                    $currentWo->save();
                }
                else {
                    $nextWo->status = '2WR';
                    $nextWo->save();
                }
                $mo->updateFirstWo = false;
                $mo->save();
            }
        });
    }
}
