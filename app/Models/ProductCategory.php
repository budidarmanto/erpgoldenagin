<?php

namespace App\Models;

use Devplus\Model\DevplusModelTree;
use Helper;
use Auth;

class ProductCategory extends DevplusModelTree
{
  protected $table = 'product_category';
  protected $fillable = ['id', 'name','parent', 'revision', 'reference'];

  public static function getOption($model = null){
    $listCategory = Auth::user()->authCategory();
    $data = ProductCategory::orderBy('left')->get();

    return Helper::toOption('id', 'name', $data, 'id', 'text', function($data){
      return str_repeat(' ', $data['depth']).$data['name'];
    });
  }
}
