<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;

class ManufactureRoutingOperation extends DevplusModel
{
    protected $table = 'manufacture_routing_operation';
    protected $fillable = [
        'id',
        'routing',
        'sequence',
        'next_sequence',
        'work_center',
        'name',
        'duration',
        'duration_uom',
        'worksheet',
        'description',
        'resource',
        'capacity',
    ];
    
    protected static function boot() {
        parent::boot();

        static::creating(function($table) {
            if(empty($table->sequence)) {
                $lastSequence = static::where('routing', $table->routing)->orderBy('sequence', 'DESC')->first();
                if(!empty($lastSequence)) {
                    $table->sequence = $lastSequence->sequence + 1;
                    $lastSequence->next_sequence = $table->sequence;
                    $lastSequence->save();
                }
            }
        });
    }
}
