<?php

namespace App\Models;

use Devplus\Model\DevplusModel;

class SettingPeriod extends DevplusModel
{
  protected $table = 'setting_period';
  protected $fillable  = ['year','month','status'];
}
