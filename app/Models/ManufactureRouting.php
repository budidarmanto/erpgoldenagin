<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;

class ManufactureRouting extends DevplusModel
{
    protected $table = 'manufacture_routing';
    protected $fillable = ['id', 'company', 'code', 'name', 'description', 'location_raw',  'location_result',  'location_scrap',  'location_waste'];

    public static function generateId(){
        $query = ManufactureRouting::orderBy('code', 'DESC');
        $prefix = 'RO.'.date('ym');

        return Helper::generateId($query, 'code', $prefix);
    }
  
    public static function getOption($model = null){
        if($model == null){
            $data = ManufactureRouting::where('company', \Devplus\Helper\Helper::currentCompany())->get();
        }else{
            $data = $model->get();
        }

        return Helper::toOption('id','name',$data);
    }
}
