<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;

class ProductUom extends DevplusModel
{
  protected $table = 'product_uom';
  protected $fillable = ['id', 'uom_category', 'code', 'name', 'type', 'ratio', 'active'];

  public static function getOption($model = null){
    if($model == null){
        $data = ProductUom::orderBy('name')->get();
    }else{
      $data = $model->orderBy('name')->get();
    }

    $opt = [];
    if(count($data) > 0){
      foreach($data as $val){
        $opt[] = [
          'id' => $val['id'],
          'text' => $val['name'],
          'uom_category' => $val['uom_category']
        ];
      }
    }

    return $opt;
  }

}
