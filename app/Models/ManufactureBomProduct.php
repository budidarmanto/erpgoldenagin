<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;

class ManufactureBomProduct extends DevplusModel
{
    protected $table = 'manufacture_bom_product';
    protected $fillable = ['id', 'bom', 'product', 'routing_operation', 'qty', 'uom'];
}
