<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;

class CashierEdc extends DevplusModel
{
  protected $table = 'cashier_edc';
  protected $fillable = ['id', 'name'];

  public static function getOption(){
    $data = CashierEdc::all();
    return Helper::toOption('id','name',$data);
  }
}
