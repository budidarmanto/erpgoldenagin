<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;

class CashierSurcharge extends DevplusModel
{
  protected $table = 'cashier_surcharge';
  protected $fillable = ['id', 'edc', 'bank', 'surcharge', 'payment_type'];
}
