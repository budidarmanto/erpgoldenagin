<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;

class ManufactureWorkCenter extends DevplusModel
{
    protected $table = 'manufacture_work_center';
    protected $fillable = [
        'id',
        'code',
        'name',
        'description',
        'company',
        'capacity',
        'warehouse_location',
    ];

    public static function getOption(){
        $data = ManufactureWorkCenter::where('company', \Devplus\Helper\Helper::currentCompany())->orderBy('name', 'ASC')->get();
        return Helper::toOption('id', 'name', $data, 'id', 'text');
    }
}
