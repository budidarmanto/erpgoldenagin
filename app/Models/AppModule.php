<?php

namespace App\Models;

use Devplus\Model\DevplusModel;

class AppModule extends DevplusModel
{
    protected $table = 'app_module';
    protected $fillable = ['id', 'application', 'code', 'name', 'role'];

    public function application()
    {
      return $this->belongsTo('App\Models\AppApplication', 'application');
    }
}
