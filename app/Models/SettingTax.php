<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;

class SettingTax extends DevplusModel
{
    protected $table = 'setting_tax';
    protected $fillable = ['id','name','amount','active'];

    public static function getOption($model = null){
      if($model == null){
          $data = SettingTax::orderBy('name')->get();
      }else{
        $data = $model->orderBy('name')->get();
      }

      $opt = [];
      if(count($data) > 0){
        foreach($data as $val){
          $opt[] = [
            'id' => $val['id'],
            'text' => $val['name'],
            'amount' => $val['amount']
          ];
        }
      }

      return $opt;
    }
}
