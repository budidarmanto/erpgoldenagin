<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;

class ProductVariant extends DevplusModel
{
  protected $table = 'product_variant';
  protected $fillable = ['id', 'product', 'attribute', 'attribute_value'];
}
