<?php

namespace App\Models;

use Devplus\Model\DevplusModelTree;
use Helper;

class SettingCompany extends DevplusModelTree
{
    protected $table = 'setting_company';
    protected $fillable = ['id', 'code', 'name','parent', 'picture',
        'address', 'city', 'postal_code', 'state',
        'country', 'website', 'phone', 'fax', 'email',
        'active'];

    public static function getOption($model = null){
      if($model == null){
          $data = SettingCompany::orderBy('left')->get();
      }else{
        $data = $model->orderBy('left')->get();
      }
      return Helper::toOption('id', 'name', $data, 'id', 'text', function($data){
        return str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $data['depth']).$data['name'];
      });
    }
}
