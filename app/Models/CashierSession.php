<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;
use Auth;

class CashierSession extends DevplusModel
{
    protected $table = 'cashier_session';
    protected $fillable = ['id', 'code', 'company', 'start', 'end', 'balance', 'status'];

    public static function generateId(){
      $companyId = Auth::user()->authCompany()->id;
      $companyCode = Auth::user()->authCompany()->code;

      $prefix = $companyCode.'.SS'.date('ymd');
      $query = CashierSession::where('company', $companyId)->orderBy('code', 'DESC');
      return Helper::generateId($query, 'code', $prefix);

    }
}
