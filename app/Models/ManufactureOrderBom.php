<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;

class ManufactureOrderBom extends DevplusModel
{
    protected $table = 'manufacture_order_bom';
    protected $fillable = [
        'id',
        'manufacture_order',
        'product_bom',
        'qty_total',
        'qty_consumed',
        'status',
    ];

    public function get_product_bom() {
        return $this->belongsTo('App\Models\ProductBom', 'product_bom');
    }
}
