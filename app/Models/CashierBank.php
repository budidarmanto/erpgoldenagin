<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;

class CashierBank extends DevplusModel
{
  protected $table = 'cashier_bank';
  protected $fillable = ['id', 'code', 'name'];

  public static function getOption(){
    $data = CashierBank::all();
    return Helper::toOption('id','name',$data);
  }
}
