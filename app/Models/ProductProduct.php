<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;

use App\Models\ProductMaster;
use App\Models\ProductSupplier;
use App\Models\ProductAlternative;
use App\Models\ProductUom;
use App\Models\ProductAttributeValue;
use App\Models\InventoryStock;
use App\Models\InventoryStockMovement;
use App\Models\InventoryStockMovementProduct;
use App\Models\InventoryWarehouse;
use App\Models\InventoryWarehouseLocation;
use Carbon\Carbon;
use DB;

class ProductProduct extends DevplusModel
{
  protected $table = 'product_product';
  protected $fillable = ['id', 'product', 'attribute', 'price', 'active'];

  public function product()
  {
      return ProductMaster::find($this->product);
  }

  public static function getQuery($modifierFn = null, $isMaster = false){

    $product = ProductProduct::select([
      'product_master.name AS name',
      'product_master.code AS code',
      'product_master.category AS category',
      'product_master.uom AS uom',
      'product_master.revision AS revision',
      'product_uom.uom_category AS uom_category',
      'product_master.type AS type',
      'product_master.barcode AS barcode',
      'product_master.picture AS picture',
      'product_master.sale_price AS sale_price',
      'product_master.cost_price AS cost_price',
      'product_master.pos AS pos',
      'product_master.tax AS tax',
      'product_master.routing AS routing',
      'manufacture_routing.name AS routing_name',
      'product_master.discount AS discount',
      'product_master.active AS active',
      'product_product.id',
      'product_master.id AS product_id',
      'product_product.attribute',
    ])
    ->join('product_master', 'product_master.id', '=', 'product_product.product')
    ->Join('product_uom', 'product_uom.id', '=', 'product_master.uom')
    ->leftJoin('manufacture_routing', 'manufacture_routing.id', '=', 'product_master.routing');
    /*if($isMaster) {
        $product->whereNotIn('product_product.id',\Illuminate\Support\Facades\DB::table('product_bom')->pluck('product'));
    }*/
    

    if (is_a($modifierFn, '\Closure')) {
      $product = call_user_func_array($modifierFn, [$product]);
    }
    $product->where([
      'product_product.active' => true,
    ]);

    return $product;
  }

  public static function renderProductAttribute($dataProduct){
    $attributeValue = ProductAttributeValue::all()->toArray();
    $attributeValue = collect($attributeValue)->pluck('name', 'id')->toArray();

    if(!empty($dataProduct)){
      foreach($dataProduct as $key => $val){
        $attrJson = json_decode($val['attribute'], true);
        $attr = collect($attrJson)->pluck('attribute_value')->map(function($value, $key) use ($attributeValue){
          return (isset($attributeValue[$value])) ? $attributeValue[$value] : '';
        })->implode(', ');
        $attr = (!empty($attr)) ? ' ('.$attr.')' : '';

        $dataProduct[$key]['attribute'] = $attrJson;
        $dataProduct[$key]['name'] = $val['name'].$attr;
      }
    }
    return $dataProduct;
  }

  public static function getData($modifierFn = null){
    $product = self::getQuery($modifierFn);
    $dataProduct = $product->get()->toArray();
    return self::renderProductAttribute($dataProduct);
  }

  public static function getRelatedData($dataProduct, $warehouse = null, $warehouseLocation = null){

    // Get Data ALL UOM Group BY UOM Category
    $dataUom = [];
    $uom = ProductUom::select(['id', 'code', 'name', 'uom_category', 'ratio', 'type'])->get();
    if(count($uom) > 0){
      foreach($uom as $val){
        $dataUom[$val['uom_category']][] = $val->toArray();
      }
    }

    if(!empty($dataProduct)){
      foreach($dataProduct as $key => $val){
        // Basic Unit

        // Get Option Supplier
        $optSupplier = [];
        $supplier = ProductSupplier::select([
          'product_supplier.supplier',
          'product_supplier.price',
          'product_supplier.discount',
          'product_supplier.tax',
          'crm_contact.name',
          'crm_contact.pkp',
        ])
        ->join('crm_contact', 'crm_contact.id', '=', 'product_supplier.supplier')
        ->where('product', $val['product_id'])->orderBy('product_supplier.price', 'ASC')->get();
        if(count($supplier) > 0){
          foreach($supplier as $vsupplier){
            $optSupplier[] = [
              'id' => $vsupplier['supplier'],
              'text' => $vsupplier['name'],
              'price' => $vsupplier['price'],
              'pkp' => $vsupplier['pkp'],
              'discount' => $vsupplier['discount'],
              'tax' => $vsupplier['tax'],
            ];
          }
        }
        // End Get Option Supplier

        // Get Option Related Product
        $alternative = [];
        $productAlternative = ProductAlternative::where('product_master', $val['product_id'])->get();
        if(count($productAlternative) > 0){
          $alternative = $productAlternative->pluck('product')->toArray();
        }
        // End Get Option Related Product

        $filterUom = (isset($dataUom[$val['uom_category']])) ? $dataUom[$val['uom_category']] : [];
        $optionUom = [];
        if(!empty($filterUom)){
          foreach($filterUom as $vuom){
            $optionUom[] = [
              'id' => $vuom['id'],
              'text' => $vuom['name'],
              'code' => $vuom['code'],
              'ratio' => $vuom['ratio'],
              'type' => $vuom['type'],
            ];
          }
        }
        $dataProduct[$key]['optionUom'] = $optionUom;
        $dataProduct[$key]['optionSupplier'] = $optSupplier;
        $dataProduct[$key]['alternative'] = $alternative;

        // Get Data Stock
        if(!empty($warehouse)){
          $dataWarehouseLocation = InventoryWarehouseLocation::getOption();
          $stock = ProductProduct::getDataStock($val['id'], $warehouse, $dataWarehouseLocation, $uom);
          $dataProduct[$key]['forecast'] = $stock['forecast'];
          $dataProduct[$key]['stock'] = $stock['stock'];
          $dataProduct[$key]['reserved'] = $stock['reserved'];
          $dataProduct[$key]['dataForecast'] = $stock['dataForecast'];
          $dataProduct[$key]['dataStock'] = $stock['dataStock'];
          $dataProduct[$key]['dataReserved'] = $stock['dataReserved'];

          $basicUnit = collect($dataUom[$val['uom_category']])->where('type', 'd')->first();
          if(!empty($basicUnit)){
            $dataProduct[$key]['basic_unit'] = $basicUnit['name'];
          }
        }
      }
    }

    return $dataProduct;
  }

  public static function getInventoryDataStock($products = [], $date = null, $warehouseLocations = []) {

    $data = [];

    $product = [];

    $listWarehouseLocation = array_map(function ($warehouse) {
      return '\''. $warehouse . '\'';
    }, $warehouseLocations);
    $strListWarehouseLocation = implode(',', $listWarehouseLocation);

    $inventoryStock = InventoryStock::select([
      'inventory_stock.product',
      'inventory_stock.begin_qty',
      'product_uom_default.id as uom',
      'product_uom_default.name as uom_name',
      'inventory_warehouse.name as inventory_warehouse_name',
      'inventory_warehouse_location.name as inventory_warehouse_location_name',
      'inventory_warehouse_location.id as inventory_warehouse_location_id'
    ])
    ->join('inventory_warehouse_location', 'inventory_warehouse_location.id', '=', 'inventory_stock.warehouse_location')
    ->leftJoin('inventory_warehouse', 'inventory_warehouse.id', '=', 'inventory_warehouse_location.warehouse')
    ->leftJoin('product_product','product_product.id', '=' ,'inventory_stock.product')
    ->leftJoin('product_master','product_master.id', '=' ,'product_product.product')
    ->leftJoin('product_uom',  'product_uom.id', '=', 'product_master.uom')
    ->leftJoin(DB::raw('(select * from product_uom where id in (select max(id) from product_uom where type =\'d\' and active = true group by uom_category)) as product_uom_default'), function ($join) {
        $join->on('product_uom_default.uom_category', '=', 'product_uom.uom_category');
    })
    ->whereIn('inventory_stock.product', $products);
    if($date) {
      $inventoryStock->whereDate('inventory_stock.date', $date);
    } else {
      $inventoryStock->whereDate('inventory_stock.date', "now()");
      //$inventoryStock->where('inventory_stock.date', DB::raw('(select max(date) from inventory_stock istock where istock.product = inventory_stock.product)'));
    }
    if($warehouseLocations) {
      $inventoryStock = $inventoryStock->whereIn('inventory_stock.warehouse_location', $warehouseLocations);
    }

    $inventoryStock = $inventoryStock->get();

    foreach($inventoryStock as $stock) {

      //dd($stock->toArray());
     
      $product[$stock->product] = $stock;

      if(!isset($data[$stock->product])) {
        $data[$stock->product] = [];
      }
      if(!isset($data[$stock->product][$stock->inventory_warehouse_location_id])) {
        $data[$stock->product][$stock->inventory_warehouse_location_id] = [
          'qty' => $stock->begin_qty,
          'uom' => $stock->uom,
          'uom_name' => $stock->uom_name,
          'warehouse_location' => $stock->inventory_warehouse_location_id,
          'warehouse_location_name' => $stock->inventory_warehouse_name.'/'.$stock->inventory_warehouse_location_name
        ];
      }
    }

    DB::enableQueryLog();

    $stockMovement = InventoryStockMovementProduct::select('inventory_stock_movement_product.product','inventory_stock_movement.source',
    'inventory_stock_movement.destination',
    'inventory_stock_movement.company',
    'inventory_stock_movement.type',
    'inventory_stock_movement.status',
    'inventory_stock_movement_product.qty',
    'product_uom.ratio',
    'product_uom.id as uom',
    'product_uom.name as uom_name',

    'source_warehouse.name as source_warehouse_name',
    'source_warehouse_location.name as source_warehouse_location_name',
    
    'destination_warehouse.name as destination_warehouse_name',
    'destination_warehouse_location.name as destination_warehouse_location_name'
    )
    ->join('inventory_stock_movement', 'inventory_stock_movement.id', '=', 'inventory_stock_movement_product.stock_movement')
    
    ->leftJoin('inventory_warehouse_location as source_warehouse_location', 'source_warehouse_location.id', '=', 'inventory_stock_movement.source')
    ->leftJoin('inventory_warehouse as source_warehouse', 'source_warehouse.id', '=', 'source_warehouse_location.warehouse')
    
    ->leftJoin('inventory_warehouse_location as destination_warehouse_location', 'destination_warehouse_location.id', '=', 'inventory_stock_movement.destination')
    ->leftJoin('inventory_warehouse as destination_warehouse', 'destination_warehouse.id', '=', 'destination_warehouse_location.warehouse')

    ->leftJoin('crm_contact','crm_contact.id','=','inventory_stock_movement.contact')
    ->leftJoin('setting_company','setting_company.id','=','inventory_stock_movement.company_destination')
    ->leftJoin('product_uom', 'inventory_stock_movement_product.uom', '=', 'product_uom.id')
    ->join('inventory_stock_movement_log', function($join){
      $join->on('inventory_stock_movement_log.stock_movement', '=', 'inventory_stock_movement.id')
      ->whereIn('inventory_stock_movement_log.status', ['3AA', '3IT']);
    });
    if($date) {
      $stockMovement->whereDate('inventory_stock_movement_log.created_at', $date);
    } else {
      $stockMovement->whereDate('inventory_stock_movement_log.created_at', date('Y-m-d'));
    }
    if($warehouseLocations) {
      $stockMovement->where(function($query) use($warehouseLocations) {
        $query->whereIn('inventory_stock_movement.destination', $warehouseLocations)
        ->orWhereIn('inventory_stock_movement.source', $warehouseLocations);
      });
    }
    $stockMovement = $stockMovement->whereIn('inventory_stock_movement_product.product', $products)
    ->get();
    
    //dd(str_replace_array('?', DB::getQueryLog()[0]['bindings'], DB::getQueryLog()[0]['query']));

    foreach($stockMovement as $move) {

      $update_qty = 0;
      $location = null;
      $location_name = null;
      
      switch($move->type) {
        case 'AD':
          $update_qty = $move->qty;
          $location = $move->source;
          $location_name = $move->source_warehouse_name.'/'.$move->source_warehouse_location_name;

          $update_qty *= $move->ratio;

          if($location) {
            if(!isset($data[$move->product])) {
              $data[$move->product] = [];
            }
            if(!isset($data[$move->product][$location])) {
              if($update_qty) {
                $data[$move->product][$location] = [
                  'qty' => $update_qty,
                  'uom' => $move->uom,
                  'uom_name' => $move->uom_name,
                  'warehouse_location' => $location,
                  'warehouse_location_name' => $location_name
                ];
              }
            } else {
              $data[$move->product][$location]['qty'] += $update_qty;
            }
          }
        break;
        case 'IT':
          $update_qty = $move->qty * -1;
          $location = $move->source;  
          $location_name = $move->source_warehouse_name.'/'.$move->source_warehouse_location_name;

          $update_qty *= $move->ratio;

          if($location) {
            if(!isset($data[$move->product])) {
              $data[$move->product] = [];
            }
            if(!isset($data[$move->product][$location])) {
              if($update_qty) {
                $data[$move->product][$location] = [
                  'qty' => $update_qty,
                  'uom' => $move->uom,
                  'uom_name' => $move->uom_name,		
                  'warehouse_location' => $location,
                  'warehouse_location_name' => $location_name
                ]; 
              }
            } else {
              $data[$move->product][$location]['qty'] += $update_qty;
            }
          }

          $update_qty = $move->qty;
          $location = $move->destination;
          $location_name = $move->destination_warehouse_name.'/'.$move->destination_warehouse_location_name;

          $update_qty *= $move->ratio;

          if($location) {
            if(!isset($data[$move->product])) {
              $data[$move->product] = [];
            }
            if(!isset($data[$move->product][$location])) {
              if($update_qty) {
                $data[$move->product][$location] = [
                  'qty' => $update_qty,
                  'uom' => $move->uom,
                  'uom_name' => $move->uom_name,		
                  'warehouse_location' => $location,
                  'warehouse_location_name' => $location_name
                ]; 
              }
            } else {
              $data[$move->product][$location]['qty'] += $update_qty;
            }
          }
        break;
      }
    }

    foreach($data as $key => $dt) {
      $data[$key] = array_filter($dt, function($v) use($warehouseLocations) {
        if($warehouseLocations && !in_array($v['warehouse_location'], $warehouseLocations)) {
          return false;
        }
        if($v['qty'] != 0) {
          return true;
        } else {
          return false;
        }
      });
    }

    return $data;
  }

  public static function getDataStock($productId, $warehouse = null, $dataWarehouseLocation = null, $dataUom = null){
    $warehouseId = (!empty($warehouse)) ? $warehouse->id : null;
    if(empty($dataWarehouseLocation)){
        $dataWarehouseLocation = InventoryWarehouseLocation::getOption();
    }

    if(empty($dataUom)){
        $dataUom = ProductUom::select(['id', 'code', 'name', 'uom_category', 'ratio', 'type'])->get();
    }

    $basicUnit = collect($dataUom)->where('type', 'd')->first()->toArray();


    $purchase = 0;
    $purchaseReceipt = 0;
    $sales = 0;
    $salesDelivered = 0;
    $outstandingReceipt = 0;
    $outstandingDelivery = 0;
    $receipt = 0;
    $delivered = 0;
    $stock = 0;
    $externalTransfer = 0;
    $chasier = 0;
    $adjustment = 0;

    $dataForecast = [];
    $dataStock = [];
    $dataReserved = [];

    $today = date('Y-m-d');
     /* $yesterday = date('Y-m-d', mktime(0, 0, 0, date('n'), date('j') - 1, date('Y')));
     $today = $yesterday; */

    $stockMovement = ProductProduct::getDataStockMovement($productId, ['PO', 'RC', 'SO', 'DO', 'AD', 'ET', 'IT', 'TF', 'CH', 'RT']);
    /* dd($stockMovement->toArray()); */

    $inventoryStock = InventoryStock::select([
      'inventory_stock.product',
      'inventory_stock.uom',
      'inventory_stock.warehouse_location',
      'inventory_stock.begin_qty',
      'inventory_stock.plus_qty',
      'inventory_stock.min_qty',
      'inventory_stock.end_qty'
    ])
    ->join('inventory_warehouse_location', 'inventory_warehouse_location.id', '=', 'inventory_stock.warehouse_location')
    ->where('inventory_stock.product', $productId)
    ->whereDate('inventory_stock.date', $today);
    if(!empty($warehouseId)){
      $inventoryStock->where('inventory_warehouse_location.warehouse', $warehouseId);
    }
    $inventoryStock = $inventoryStock->get();
     /* dd($dataUom);
     dd($basicUnit);
     dd($dataWarehouseLocation); */

    foreach($dataWarehouseLocation as $val){
      $dataStock[$val['id']] = [
          'qty' => 0,
          'uom' => $basicUnit['id'],
          'uom_name' => $basicUnit['name'],
          'warehouse_location' => $val['id'],
          'warehouse_location_name' => $val['text']
      ];
    }

    if(count($inventoryStock) > 0){
      foreach($inventoryStock as $val){
        $stock += $val->begin_qty;

        if(isset($dataStock[$val->warehouse_location])){
          $dataStock[$val->warehouse_location]['qty'] += $val->begin_qty;
        }else{
          $dataStock[$val->warehouse_location]['qty'] = $val->begin_qty;
        }

      }
    }

    if(!empty($stockMovement)){

      foreach($stockMovement as $val){

        if(!empty($warehouseId)){
          $wh_id = InventoryStockMovement::getWarehouseTransaction($val, $dataWarehouseLocation);
          if(empty($wh_id) || $wh_id != $warehouseId){
            continue;
          }
        }

        $type = $val['type'];
        $status = $val['status'];
        /* Convert Quantity to Basic Unit */
        $currentUom = collect($dataUom)->where('id', $val['uom'])->first();
        $qty = $val['qty'] * $currentUom['ratio'];
        $rcp_qty = $val['rcp_qty'] * $currentUom['ratio'];
        $do_qty = $val['do_qty'] * $currentUom['ratio'];
        /* End Convert Quantity */

        $statusIncoming = ['3PO', '5PR'];
        $statusPurchase = ['3PO'];
        $statusReceipt = ['3RA'];
        $statusCancelReceipt = ['1RR'];

        /* // Barang Dibeli yang masih outstanding */
        $isPurchase = ($type == 'PO' && in_array($status, $statusIncoming)) ? true : false;
        /* // Barang Dijual yang masih outstanding */
        $isSales = ($type == 'SO' && $status == '3SO') ? true : false;
        /* // Barang yang sudah diterima */
        $isReceipt = ($type == 'RC' && in_array($status, $statusReceipt)) ? true : false;
        /* // Barang yang sudah dikirim */
        $isDelivered = ($type == 'DO' && $status == '4DD') ? true : false;
        /* // Barang Dieterima yang masih outstanding */
        $isOutstandingReceipt = ($type == 'RC' && in_array($status, ['1RR'])) ? true : false;
        /* // Barang Dikirim yang masih outstanding */
        $isOutstandingDelivery = ($type == 'DO' && in_array($status, ['1DR', '3DO'])) ? true : false;
        /* // Barang Penyesuaian */
        $isAdjustment = ($type == 'AD' && $status == '3AA') ?  true : false;
        /* // Barang Mutasi Eksternal */
        $isExternalTransfer = ($type == 'ET' && $status == '3ED') ?  true : false;
        /* // Barang Mutasi Internal */
        $isInternalTransfer = ($type == 'IT' && $status == '3IT') ?  true : false;
        /* // Barang Transfer */
        $isTransfer = ($type == 'TF' && in_array($status, ['1TO', '2TR'])) ? true : false;

        $isChasier = ($type == 'CH' && $status == '1CS') ? true : false;

        $purchaseQty = $val['qty'] - $val['rcp_qty'];

        /* // Purchase */
        if(($isPurchase || $isTransfer) && $purchaseQty > 0){
          $purchase += $qty;
          $purchaseReceipt += $rcp_qty;
          $dataForecast[] = [
            'expected_date' => Carbon::parse($val['product_expected_date'])->format('d/m/Y'),
            'stock_movement' => $val['code'],
            'date' => Carbon::parse($val['date'])->format('d/m/Y'),
            'contact' => ($isTransfer) ? $val['company_source'] : $val['contact'],
            'qty' => $purchaseQty,
            'uom' => $currentUom['name'],
          ];
        }

        $salesQty = $val['qty'] - $val['do_qty'];

        /* // Sales */
        if(($isSales || $isExternalTransfer) && $salesQty > 0){
          $sales += $qty;
          $salesDelivered += $do_qty;
          $dataReserved[] = [
            'expected_date' => Carbon::parse($val['expected_date'])->format('d/m/Y'),
            'stock_movement' => $val['code'],
            'date' => Carbon::parse($val['date'])->format('d/m/Y'),
            'contact' => $val['contact'],
            'qty' => $salesQty,
            'uom' => $currentUom['name'],
          ];
        }

        /* // Outstanding Receipt */
        if($isOutstandingReceipt){
          $outstandingReceipt += $qty;
          $dataRef = collect($stockMovement)->where('stock_movement', $val['reference'])->first();

          if(empty($dataRef)){
            $dataRef = InventoryStockMovementProduct::select([
              'inventory_stock_movement.id AS stock_movement',
              'inventory_stock_movement.code',
              'inventory_stock_movement_product.expected_date AS product_expected_date',
              'crm_contact.name AS contact',
            ])
            ->join('inventory_stock_movement', 'inventory_stock_movement.id', '=', 'inventory_stock_movement_product.stock_movement')
            ->leftJoin('crm_contact', 'crm_contact.id', '=', 'inventory_stock_movement.contact')
            ->where('inventory_stock_movement.id', $val['reference'])->first();
          }

          $dataForecast[] = [
            'expected_date' => (!empty($dataRef)) ? Carbon::parse($dataRef->product_expected_date)->format('d/m/Y') : '-',
            'stock_movement' => (!empty($dataRef)) ? $dataRef->code : '-',
            'receipt_code' => $val['code'],
            'date' => Carbon::parse($val['date'])->format('d/m/Y'),
            'contact' => (!empty($dataRef)) ? $dataRef->contact : '-',
            'qty' => $qty,
            'uom' => $currentUom['name'],
          ];
        }

        /* // Outstanding Delivery */
        if($isOutstandingDelivery){
          $outstandingDelivery += $qty;
          $dataRef = collect($stockMovement)->where('stock_movement', $val['reference'])->first();
          if(empty($dataRef)){
            InventoryStockMovement::select([
              'inventory_stock_movement.expected_date',
              'inventory_stock_movement.code',
              'crm_contact.name AS contact',
            ])->leftJoin('crm_contact', 'crm_contact.id', '=', 'inventory_stock_movement.contact')
            ->where('inventory_stock_movement.id', $val['reference'])->first();
          }

          $dataReserved[] = [
            'expected_date' => (!empty($dataRef)) ? Carbon::parse($dataRef->expected_date)->format('d/m/Y') : '-',
            'stock_movement' => (!empty($dataRef)) ? $dataRef->code : '-',
            'delivery_code' => $val['code'],
            'date' => Carbon::parse($val['date'])->format('d/m/Y'),
            'contact' => (!empty($dataRef)) ? $dataRef->contact : '-',
            'qty' => $qty,
            'uom' => $currentUom['name'],
          ];
        }

        $currentDate = Carbon::parse($val['date'])->format('Y-m-d');
        if($currentDate == $today){
          if($isReceipt){
            $receipt += $qty;
            if(isset($dataStock[$val['destination']]))
              $dataStock[$val['destination']]['qty'] += $qty;
          }

          if($isDelivered){
            $delivered += $qty;
            if(isset($dataStock[$val['source']]))
              $dataStock[$val['source']]['qty'] -= $qty;
          }

          if($isAdjustment){
            $adjustment += $qty;
            $dataStock[$val['source']]['qty'] += $qty;
          }

          if($isInternalTransfer){
            $dataStock[$val['source']]['qty'] -= $qty;
            $dataStock[$val['destination']]['qty'] += $qty;
          }

          if($isChasier){
            $chasier += $qty;
            $dataStock[$val['source']]['qty'] -= $qty;
          }
        }
      }
    }


    $forecast = $purchase - $purchaseReceipt + $outstandingReceipt;

    $reserved = $sales - $salesDelivered + $outstandingDelivery;

    $inventory = $stock + $receipt - $delivered + $adjustment - $chasier;// - $chasier;

    $data = [
      'forecast' => $forecast,
      'stock' => $inventory,
      'reserved' => $reserved,
      'dataForecast' => $dataForecast,
      'dataReserved' => $dataReserved,
      'dataStock' => $dataStock
    ];

    return $data;
  }

  public static function getWarehouseLocationStock($listProductId = [], $warehouseLocation = null, $warehouse = null){

    $listWarehouseLocation = [];
    if(!empty($warehouseLocation)){
      $listWarehouseLocation[] = $warehouseLocation;
    }
    else if(!empty($warehouse)){
      $dtWarehouseLocation = InventoryWarehouseLocation::where('warehouse', $warehouse)->where('active', true)->get();
      foreach($dtWarehouseLocation as $val){
        $listWarehouseLocation[] = $val->id;
      }
    }else{
      $dtWarehouseLocation = InventoryWarehouseLocation::where('active', true)->get();
      foreach($dtWarehouseLocation as $val){
        $listWarehouseLocation[] = $val->id;
      }

    }


    $dataUom = ProductUom::select(['id', 'code', 'name', 'uom_category', 'ratio', 'type'])->get();
    $today = date('Y-m-d');
    $data = [];

    $products = ProductProduct::getQuery(function($q) use ($listProductId){
      return $q->whereIn('product_product.id', $listProductId);
    })->get();

    if(count($products) > 0){

      foreach($products as $product){
        $productId = $product->id;
        $stock = 0;
        $receipt = 0;
        $delivered = 0;
        $adjustment = 0;
        $chasier = 0;
        $minInternalTransfer = 0;
        $plusInternalTransfer = 0;

        $inventoryStock = InventoryStock::select([
          'inventory_stock.product',
          'inventory_stock.uom',
          'inventory_stock.warehouse_location',
          'inventory_stock.begin_qty',
          'inventory_stock.plus_qty',
          'inventory_stock.min_qty',
          'inventory_stock.end_qty'
        ])
        ->join('inventory_warehouse_location', 'inventory_warehouse_location.id', '=', 'inventory_stock.warehouse_location')
        ->where('inventory_stock.product', $productId)
        ->whereIn('inventory_stock.warehouse_location', $listWarehouseLocation)
        ->whereDate('inventory_stock.date', $today);

        $inventoryStock = $inventoryStock->get();
        if(count($inventoryStock) > 0){
          foreach($inventoryStock as $val){
            $stock += $val->begin_qty;
          }
        }

        $stockMovement = ProductProduct::getDataStockMovement($productId, ['RC', 'DO', 'AD', 'IT', 'CH'], function($q) use($today){
          return $q->whereDate('inventory_stock_movement_log.created_at', $today);
        });
        //dd(Helper::currentCompany());
        if(!empty($stockMovement)){
          foreach($stockMovement as $val){
            $type = $val['type'];
            $status = $val['status'];
            /* // Convert Quantity to Basic Unit */
            $currentUom   = collect($dataUom)->where('id', $val['uom'])->first();
            $qty          = $val['qty'] * $currentUom['ratio'];
            $rcp_qty      = $val['rcp_qty'] * $currentUom['ratio'];
            $do_qty       = $val['do_qty'] * $currentUom['ratio'];
            $isReceipt    = ($type == 'RC' && in_array($status, ['3RA'])) ? true : false;
            $isDelivered  = ($type == 'DO' && $status == '4DD') ? true : false;
            $isAdjustment = ($type == 'AD' && $status == '3AA') ?  true : false;
            $isInternalTransfer = ($type == 'IT' && $status == '3IT') ?  true : false;
            $isChasier    = ($type == 'CH' && $status == '1CS') ? true : false;
			
            if($isReceipt && in_array($val['destination'], $listWarehouseLocation)){
              $receipt += $qty;
            }

            if($isDelivered &&  in_array($val['source'], $listWarehouseLocation)){
              $delivered += $qty;
            }

            if($isAdjustment && in_array($val['source'], $listWarehouseLocation)){
              $adjustment += $qty;
            }

            if($isInternalTransfer){
              if(in_array($val['source'], $listWarehouseLocation)){
                $minInternalTransfer += $qty;
              }
              if(in_array($val['destination'], $listWarehouseLocation)){
                $plusInternalTransfer += $qty;
              }
            }
            if($isChasier && in_array($val['source'], $listWarehouseLocation)){
              $chasier += $qty;
            }
          }
        }

        $stock = $stock + $receipt - $delivered + $adjustment - $chasier - $minInternalTransfer + $plusInternalTransfer;
        $currentUom = collect($dataUom)->where('id', $product->uom)->first();
        $defaultUom = collect($dataUom)->where('uom_category', $currentUom['uom_category'])->where('type', 'd')->first();

        $data[] = [
          'product' => $productId,
          'stock' => $stock,
          'uom' => $defaultUom['id'],
          'uom_name' => $defaultUom['name']
        ];
      }
    }

    return $data;

  }


  public static function getDataStockMovement($productId, $type = [], $query = null){

    $stockMovement = InventoryStockMovementProduct::select([
      'inventory_stock_movement.id AS stock_movement',
      'inventory_stock_movement.code',
      'inventory_stock_movement.company',
      'inventory_stock_movement.source',
      'inventory_stock_movement.destination',
      'inventory_stock_movement.date',
      'inventory_stock_movement.type',
      'inventory_stock_movement.status',
      'inventory_stock_movement.expected_date',
      'inventory_stock_movement.reference',
      'crm_contact.name AS contact',
      'inventory_stock_movement_product.expected_date AS product_expected_date',
      'inventory_stock_movement_product.id',
      'inventory_stock_movement_product.product',
      'inventory_stock_movement_product.uom',
      'inventory_stock_movement_product.qty',
      'inventory_stock_movement_product.rcp_qty',
      'inventory_stock_movement_product.do_qty',
      'inventory_stock_movement_product.status AS statusprod',
      'setting_company.name AS company_source'
    ])->join('inventory_stock_movement', 'inventory_stock_movement.id', '=', 'inventory_stock_movement_product.stock_movement')
    ->leftJoin('crm_contact', 'crm_contact.id', '=', 'inventory_stock_movement.contact')
    ->leftJoin('setting_company', 'setting_company.id', '=', 'inventory_stock_movement.company_destination')
    ->join('inventory_stock_movement_log', function($join){
      $join->on('inventory_stock_movement_log.stock_movement', '=', 'inventory_stock_movement.id')
      ->where('inventory_stock_movement_log.status', '=', 'inventory_stock_movement.status');
    })
    ->where([
      'inventory_stock_movement_product.product' => $productId,
      'inventory_stock_movement.company' => Helper::currentCompany(),
    ])
    // ->whereDate('inventory_stock_movement.date', $today)
    ->whereIn('inventory_stock_movement.type', $type)
    ->orderBy('inventory_stock_movement.created_at', 'ASC');
    if (is_a($query, '\Closure')) {
      $stockMovement = call_user_func_array($query, [$stockMovement]);
    }
    $stockMovement = $stockMovement->get();

    return $stockMovement;
  }



}
