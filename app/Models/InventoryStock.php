<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;
use Illuminate\Support\Facades\DB;

class InventoryStock extends DevplusModel
{
    protected $table = 'inventory_stock';
    protected $fillable = ['id', 'product', 'uom', 'warehouse_location', 'date', 'begin_qty', 'plus_qty', 'min_qty', 'end_qty'];
    
    public static function getProductStock($productId, $warehouse = null, $warehouseLocation = null) {
	$listWarehouseLocation = [];
        if(!empty($warehouseLocation)){
            $listWarehouseLocation[] =  $warehouseLocation;
        }
        else if(!empty($warehouse)){
            $dtWarehouseLocation = InventoryWarehouseLocation::where('warehouse', $warehouse)->where('active', true)->get();
            foreach($dtWarehouseLocation as $val){
                $listWarehouseLocation[] =  $val->id;
            }
        }
        else{
            $dtWarehouse = InventoryWarehouse::getWarehouseCompany(\Devplus\Helper\Helper::currentCompany());
            foreach($dtWarehouse as $warehouse) {
                $dtWarehouseLocation = InventoryWarehouseLocation::where([
                    'warehouse' => $warehouse->id,
                    'active' => true
                ])->get();
                foreach($dtWarehouseLocation as $val){
                    $listWarehouseLocation[] =  $val->id;
                }
            }
        }

	$listWarehouseLocation = array_map(function ($warehouse) {
		return '\''. $warehouse . '\'';
	}, $listWarehouseLocation);
	$strListWarehouseLocation = implode(',', $listWarehouseLocation);
        
	$query = ProductProduct::select(DB::raw(''
                    .'product_master.name AS name, '
                    .'product_master.code AS code,'
                    .'product_master.category AS category,'
                    .'product_category.name AS category_name,'
                    .'product_master.uom AS uom,'
                    .'product_master.revision AS revision,'
                    .'product_uom.uom_category AS uom_category,'
                    .'product_master.type AS type,'
                    .'product_master.barcode AS barcode,'
                    .'product_master.picture AS picture,'
                    .'product_master.sale_price AS sale_price,'
                    .'product_master.cost_price AS cost_price,'
                    .'product_master.pos AS pos,'
                    .'product_master.tax AS tax,'
                    .'product_master.discount AS discount,'
                    .'product_master.active AS active,'
                    .'product_product.id,'
                    .'product_master.id AS product_id,'
                    .'product_product.attribute,'
                    .'product_uom_default.id as uom,'
                    .'product_uom_default.name as uom_name,'
                    .'COALESCE(stock.stock,0) as stock,'
                    .'COALESCE(adjustment.qty_ad, 0) as adjustment,'
                    .'COALESCE(aa.qty_mit, 0) as minIternal,'
                    .'COALESCE(bb.qty_pit, 0) as plusInternal,'
                    .'COALESCE(stock.stock, 0) + COALESCE(adjustment.qty_ad, 0) - COALESCE(aa.qty_mit, 0) + COALESCE(bb.qty_pit, 0) as stock_akhir'
                ))
                ->leftJoin('product_master','product_master.id', '=' ,'product_product.product')
                ->leftJoin('product_category','product_category.id', '=' ,'product_master.category')
                ->leftJoin('product_uom',  'product_uom.id', '=', 'product_master.uom')
                ->leftJoin(DB::raw('(select * from product_uom where id in (select max(id) from product_uom where type =\'d\' and active = true group by uom_category)) as product_uom_default'), function ($join) {
                    $join->on('product_uom_default.uom_category', '=', 'product_uom.uom_category');
                })
                ->leftJoin(DB::raw('(select product,  COALESCE(sum(begin_qty), 0) as stock from inventory_stock where date = (select max(date) from inventory_stock)  and warehouse_location in (' . $strListWarehouseLocation . ')  group by product) as stock'), function ($join) {
                    $join->on('stock.product', '=', 'product_product.id');
                })
                ->leftJoin(DB::raw('(select inventory_stock_movement_product.product,inventory_stock_movement.company,
                    inventory_stock_movement.type,inventory_stock_movement.status,
                    COALESCE(sum(inventory_stock_movement_product.qty), 0) as sum_ad, COALESCE(sum(product_uom.ratio*inventory_stock_movement_product.qty), 0) as qty_ad
                    from inventory_stock_movement_product 
                    join inventory_stock_movement on inventory_stock_movement.id = inventory_stock_movement_product.stock_movement 
                    left join crm_contact on crm_contact.id = inventory_stock_movement.contact 
                    left join setting_company on setting_company.id = inventory_stock_movement.company_destination 
                    left join product_uom on inventory_stock_movement_product.uom = product_uom.id
                    join inventory_stock_movement_log on inventory_stock_movement_log.stock_movement = inventory_stock_movement.id and inventory_stock_movement_log.status = \'3AA\'
                    where inventory_stock_movement_log.created_at::date = (select max(date) from inventory_stock)
                    and inventory_stock_movement.source in (' . $strListWarehouseLocation . ')
                    group by inventory_stock_movement_product.product, inventory_stock_movement.company, inventory_stock_movement.type, inventory_stock_movement.status) as adjustment'), function ($join) {
                    $join->on('adjustment.product', '=', 'product_product.id');
                })
                ->leftJoin(DB::raw('(select inventory_stock_movement_product.product,inventory_stock_movement.company,
                    inventory_stock_movement.type,inventory_stock_movement.status,
                    COALESCE(sum(inventory_stock_movement_product.qty), 0) as sum_mit, COALESCE(sum(product_uom.ratio*inventory_stock_movement_product.qty), 0) as qty_mit 
                    from inventory_stock_movement_product 
                    join inventory_stock_movement on inventory_stock_movement.id = inventory_stock_movement_product.stock_movement 
                    left join crm_contact on crm_contact.id = inventory_stock_movement.contact 
                    left join setting_company on setting_company.id = inventory_stock_movement.company_destination 
                    left join product_uom on inventory_stock_movement_product.uom = product_uom.id
                    join inventory_stock_movement_log on inventory_stock_movement_log.stock_movement = inventory_stock_movement.id and inventory_stock_movement_log.status = \'3IT\'
                    where inventory_stock_movement_log.created_at::date = (select max(date) from inventory_stock)
                    and inventory_stock_movement.source in (' . $strListWarehouseLocation . ') and inventory_stock_movement.destination not in (' . $strListWarehouseLocation . ')
                    group by inventory_stock_movement_product.product, inventory_stock_movement.company, inventory_stock_movement.type, inventory_stock_movement.status) as aa'), function ($join) {
                    $join->on('aa.product', '=', 'product_product.id');
                })
                ->leftJoin(DB::raw('(select inventory_stock_movement_product.product,inventory_stock_movement.company,
                    inventory_stock_movement.type,inventory_stock_movement.status,
                    COALESCE(sum(inventory_stock_movement_product.qty), 0) as sum_pit, COALESCE(sum(product_uom.ratio*inventory_stock_movement_product.qty), 0) as qty_pit 
                    from inventory_stock_movement_product 
                    join inventory_stock_movement on inventory_stock_movement.id = inventory_stock_movement_product.stock_movement 
                    left join crm_contact on crm_contact.id = inventory_stock_movement.contact 
                    left join setting_company on setting_company.id = inventory_stock_movement.company_destination 
                    left join product_uom on inventory_stock_movement_product.uom = product_uom.id
                    join inventory_stock_movement_log on inventory_stock_movement_log.stock_movement = inventory_stock_movement.id and inventory_stock_movement_log.status = \'3IT\'
                    where inventory_stock_movement_log.created_at::date = (select max(date) from inventory_stock)
                    and inventory_stock_movement.destination in (' . $strListWarehouseLocation . ') and inventory_stock_movement.source not in (' . $strListWarehouseLocation . ')
                    group by inventory_stock_movement_product.product, inventory_stock_movement.company, inventory_stock_movement.type, inventory_stock_movement.status) as bb'), function ($join) {
                    $join->on('bb.product', '=', 'product_product.id');
                });
                
		
        if(!empty($productId)){
          $query->whereIn('product_product.id', explode(';', $productId));  
	//$query->whereIn('product_product.id', explode('.', $productId));
        }
        
        $result = $query->get()->toArray();
        return $result;
    }
}
