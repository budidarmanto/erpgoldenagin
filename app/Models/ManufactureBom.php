<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;

class ManufactureBom extends DevplusModel
{
    protected $table = 'manufacture_bom';
    protected $fillable = ['id', 'product', 'routing', 'name', 'qty', 'uom', 'status', 'description'];
}
