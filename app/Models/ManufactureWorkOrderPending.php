<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;

class ManufactureWorkOrderPending extends DevplusModel
{
    protected $table = 'manufacture_work_order_pending';
    protected $fillable = [
        'id',
        'work_order',
        'pending_start',
        'pending_end',
    ];
}
