<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;

class ProductBom extends DevplusModel
{
    protected $table = 'product_bom';
    protected $fillable = [
        'id',
        'product_master',
        'product',
        'uom',
        'qty',
        'bom_type',
    ];

    public function get_uom() {
        return $this->belongsTo('App\Models\ProductUom', 'uom');
    }
}
