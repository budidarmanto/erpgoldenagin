<?php

namespace App\Models;

use Illuminate\Support\Facades\Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\AppGroup;
use App\Models\AppApplication;
use App\Models\AppModule;
use App\Models\SettingUser;
use App\Models\SettingCompany;
use App\Models\AppRole;
use App\Models\InventoryWarehouse;
use App\Models\InventoryWarehouseLocation;
use Illuminate\Support\Facades\Auth;
use Helper;
use Request;

class SettingUser extends Authenticatable
{
  use Notifiable;

  public $incrementing = false;
  protected $table = 'setting_user';
  protected $fillable = ['id', 'username', 'fullname', 'nickname', 'email', 'picture', 'active', 'is_login', 'last_activity', 'count_wrong_password', 'remember_token', 'group', 'company', 'password', 'warehouse_location', 'workcenter'];

  protected $authApplication;
  protected $authRole;
  protected $authCategory;
  protected $authCompany = null;

  public static function boot(){

      parent::boot();

      static::creating(function($table) {
          $table->id = uniqid().substr(md5(mt_rand()), 0, 5);
          $table->created_by = 'SYSTEM'; //(Auth::check()) ? Auth::user()->username : 'SYSTEM';
      });

      static::updating(function($table) {
          $table->updated_by = 'SYSTEM'; //(Auth::check()) ? Auth::user()->username : 'SYSTEM';
      });

  }


  public function buildAccess(){
    // Setup default routes
    $currentRoute = Request::route()->getName();
    if(!empty($currentRoute)){
      $arrRoute = preg_split('/(?=[A-Z])/',$currentRoute);
      if(count($arrRoute) > 3){
        unset($arrRoute[count($arrRoute) - 1]);
      }
      $this->currentRoute = implode('', $arrRoute);;
    }

    $this->authAppModule();
  }

  public function setAccess($route){
    $this->currentRoute = $route;
  }

  public function getAccess($route = null){
    if($route == null){
      $route = $this->currentRoute;
    }

    return (isset($this->authRole[$route])) ? $this->authRole[$route] : [];
  }

  public function hasAccess($accesName = '', $route = null){
    if($route == null){
      $route = $this->currentRoute;
    }
    if(empty($accesName) || empty($route) || !isset($this->authRole[$route])){
      return false;
    }

    if(in_array($accesName, $this->authRole[$route])){
      return true;
    }

    return false;
  }

  public function getAuthApplication(){
    return $this->authApplication;
  }

  public function hasApplication($application = ''){
    if(in_array($application, $this->authApplication)){
      return true;
    }

    return false;
  }

  public function hasModule($route){
    if(is_array($route)){
      $status = false;

      foreach($route as $val){
        if(isset($this->authRole[$val]) && in_array('view', $this->authRole[$val])){
          $status = true;
        }
      }
      return $status;

    }else{
      if(!isset($this->authRole[$route])){
        return false;
      }
      if(in_array('view', $this->authRole[$route])){
        return true;
      }
    }

    return false;
  }

  protected function authAppModule(){

      $listGroup = json_decode($this->group);
      $listApplication = Helper::toFlatOption('code', 'code', AppApplication::all());
      $listModule = Helper::toFlatOption('code', 'code', AppModule::all());

      $access = [];
      $accessApp = [];
      if(count($listGroup) > 0){
        foreach($listGroup as $vGroup){
          $group = AppGroup::find($vGroup);
          $arrAccess = json_decode($group->access);

          foreach($arrAccess as $vAccess){
            if(isset($listApplication[$vAccess->application]) && isset($listModule[$vAccess->module])){
              $routeName = ucfirst($listApplication[$vAccess->application]).ucfirst($listModule[$vAccess->module]);
              if(isset($access[$routeName])){
                $before = collect($access[$routeName]);
                $access[$routeName] = $before->merge($vAccess->role)->all();
              }else{
                  $access[$routeName] = $vAccess->role;
              }

              if(in_array('view', $vAccess->role) && !in_array($listApplication[$vAccess->application], $accessApp)){
                $accessApp[] = $listApplication[$vAccess->application];
              }
            }
          }
        }
      }

      $this->authRole = $access;
      $this->authApplication = $accessApp;
    }

    public function listCompany()
    {
        $company = json_decode($this->company);
        $dataCompany = SettingCompany::whereIn('id', $company)->where('active', true)->get();
        $company = collect($dataCompany)->pluck('id')->toArray();

        return $company;
    }

    public function listWorkcenter()
    {
        $workcenter = json_decode($this->workcenter);
        if(!empty($workcenter)){
          $whLoc = ManufactureWorkCenter::select(['id'])->whereIn('id', $workcenter)->pluck('id')->toArray();
        }else{
          $whLoc = ['9999999'];
        }

        return $whLoc;
    }

    public function listWarehouseLocation()
    {
        $warehouseLocation = json_decode($this->warehouse_location);
        if(!empty($warehouseLocation)){
          $dataWarehouseLocation = InventoryWarehouseLocation::select(['id'])->whereIn('id', $warehouseLocation)->where('active', true)->get();

          $whLoc = collect($dataWarehouseLocation)->pluck('id')->toArray();
        }else{
          $whLoc = ['9999999'];
        }

        return $whLoc;
    }

    public function listWarehouse(){
      $warehouseLocation = json_decode($this->warehouse_location);
      $dataWarehouseLocation = InventoryWarehouseLocation::select('warehouse')->whereIn('id', $warehouseLocation)->where('active', true)->get();
      $wh = [];
      if(count($dataWarehouseLocation) > 0){
        foreach($dataWarehouseLocation as $val){
          if(!in_array($val['warehouse'], $wh)){
            $wh[] = $val['warehouse'];
          }
        }
      }

      return $wh;

    }

    public function authCompany(){
        $sessionCompany = session('company');
        $listCompany = Auth::user()->listCompany();

        if(empty($sessionCompany) || !in_array($sessionCompany, $listCompany)){
          if(!empty($listCompany)){
              $sessionCompany = $listCompany[0];
          }
        }

        if($this->authCompany == null){
            $company = SettingCompany::find($sessionCompany);
            $this->authCompany = $company;
        }


        return $this->authCompany;
    }


    public function authCategory(){
      $groupId = $this->groups();
      $groups = AppGroup::whereIn('id', $groupId)->get()->toArray();

      $listCategory = [];

      if(count($groups) > 0){
        foreach($groups as $val){
          $category = (!empty($val['category'])) ? json_decode($val['category'], true) : [];
          if(count($category) > 0){
            foreach($category as $vcat){
              $listCategory[] = $vcat;
            }
          }
        }
      }
      $listCategory = collect($listCategory)->unique();
      return $listCategory->all();
    }

    public function groups(){
      return json_decode($this->group, true);
    }

    public function updateActivity(){

      SettingUser::where('id', $this->id)->update([
        'last_activity' => date('Y-m-d H:i:s')
      ]);

    }

}
