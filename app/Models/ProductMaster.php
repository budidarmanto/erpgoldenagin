<?php

namespace App\Models;

use Devplus\Model\DevplusModel;
use Helper;
use App\Models\ProductMaster;

class ProductMaster extends DevplusModel
{
    protected $table = 'product_master';
    protected $fillable = [
        'id',
        'code',
        'category',
        'uom',
        'name',
        'type',
        'barcode',
        'picture',
        'sale_price',
        'cost_price',
        'pos',
        'active',
        'revision',
        'tax',
        'discount',
        'weight',
        'description',
        'routing',
    ];

    public static function productCodeExists($productCode = null, $id = null){
        $query = ProductMaster::where('code', $productCode);
        if(!empty($id)){
            $query->where('id', '!=', $id);
        }
        $product = $query->first();
        if(!empty($product)){
            return true;
        }else{
            return false;
        }
    }
}
