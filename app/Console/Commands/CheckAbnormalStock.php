<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\ProductProduct;
use App\Models\ProductUom;
use App\Models\InventoryStockMovement;
use App\Models\InventoryStockMovementProduct;
use App\Models\InventoryStockMovementLog;
use App\Models\InventoryWarehouseLocation;
use App\Models\InventoryStock;
use App\Models\SettingCompany;
use Input;
use Params;
use Helper;
use DB;

class CheckAbnormalStock extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stock:check {last_date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Inventory Stock';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $warehouse = null;
			$whLoc = null;
      $product = '';
      $last_date = date('Y-m-d');

      if($this->argument('last_date')) {
        $last_date = $this->argument('last_date');
        if(!$this->validateDate($last_date)) {
          echo "Date format must be YYYY-MM-DD\nexample: stock:check 2019-02-18\n";
          return;
        }
      }

      $warehouseLocation = [];
      if(!empty($whLoc)) {
        $listIdWarehouseLocation = [$whLoc];
      } else {
        if(!empty($warehouse)){
          $warehouseLocation = InventoryWarehouseLocation::where('warehouse', $warehouse)->get();
        }else{
          $warehouseLocation = InventoryWarehouseLocation::all();
        }
        $listIdWarehouseLocation = $warehouseLocation->pluck('id')->toArray();
      }
      //dd($listIdWarehouseLocation);
			
      $get_transaction = InventoryStockMovementProduct::select([
        'inventory_stock_movement_product.product',
        'inventory_stock_movement_product.qty',
        'inventory_stock_movement_product.uom',
        'inventory_stock_movement.type',
        'inventory_stock_movement.type AS type_name',
        'inventory_stock_movement.status',
        'inventory_stock_movement.source',
        'inventory_stock_movement.destination',
        'inventory_stock_movement.description',
        'inventory_stock_movement.code',
        'product_uom.ratio',
        'product_uom.name as uom_name',
        'inventory_stock_movement_log.created_at as date',
      ])
      ->join('inventory_stock_movement', 'inventory_stock_movement.id', '=', 'inventory_stock_movement_product.stock_movement')
      ->join('inventory_stock_movement_log', function($join){
        $join->on('inventory_stock_movement_log.stock_movement', '=', 'inventory_stock_movement.id')
        ->whereIn('inventory_stock_movement_log.status', ['3AA', '3IT']);
      })
      ->leftJoin('product_uom', 'inventory_stock_movement_product.uom', '=', 'product_uom.id')
      ->whereDate('inventory_stock_movement_log.created_at', '<=', $last_date);

			if($product)
				$get_transaction->where('inventory_stock_movement_product.product', $product);

      if($listIdWarehouseLocation) {
        $get_transaction->where(function($q) use($listIdWarehouseLocation) {
          $q->whereIn('inventory_stock_movement.source', $listIdWarehouseLocation)
          ->orWhereIn('inventory_stock_movement.destination', $listIdWarehouseLocation);
        });  
      }
      $get_transaction->orderBy('inventory_stock_movement_log.created_at', 'ASC');

      echo "Get all transactions...\n";

      $get_transaction = $get_transaction->get()->toArray();

			echo "Calculate all transactions...\n";

      $data_transaction = [];

      $last_stock = [];
      foreach($get_transaction as $value) {
        
        $value['qty'] = $value['qty'] * $value['ratio'];

        $increment = 0;
        if($value['type'] == 'AD') {
          $increment = $value['qty'];

          $location = $value['source'];

          if(!isset($last_stock[$value['product']][$location])) {
            $last_stock[$value['product']][$location] = 0;					
          }

          $last_stock[$value['product']][$location] += $increment;
        }
        elseif($value['type'] == 'IT') {
          $increment = $value['qty'] * -1;

          $location = $value['source'];

          if (!isset($last_stock[$value['product']][$location])) {
              $last_stock[$value['product']][$location] = 0;
          }

          $last_stock[$value['product']][$location] += $increment;

          $increment = $value['qty'];

          $location = $value['destination'];

          if (!isset($last_stock[$value['product']][$location])) {
              $last_stock[$value['product']][$location] = 0;
          }

          $last_stock[$value['product']][$location] += $increment;
        }
      }
      
      $get_inventory_stock = InventoryStock::select([
        DB::raw('inventory_stock.end_qty as end_qty'),
        'inventory_stock.product',
        'inventory_stock.date',
        'inventory_stock.warehouse_location',
        'product_master.name as product_name',
        DB::raw("CONCAT(whs.name,'/',wls.name) AS warehouse_location_name")
      ])
      ->leftJoin('product_product','product_product.id', '=' ,'inventory_stock.product')
      ->leftJoin('product_master','product_master.id', '=' ,'product_product.product')
      ->leftJoin('inventory_warehouse_location as wls', 'inventory_stock.warehouse_location', '=', 'wls.id')
      ->leftJoin('inventory_warehouse as whs', 'wls.warehouse', '=', 'whs.id');

			if($product)
				$get_inventory_stock->where('inventory_stock.product', $product);

			$get_inventory_stock->whereDate('inventory_stock.date', $last_date);

      if($listIdWarehouseLocation) {
        $get_inventory_stock->where(function($q) use($listIdWarehouseLocation) {
          $q->whereIn('inventory_stock.warehouse_location', $listIdWarehouseLocation);
        });  
      }
      $get_inventory_stock = $get_inventory_stock->get();

			$count_failure = 0;

			$total_progress = count($get_inventory_stock);

      $failed = [];
      $failed_txt = null;

			$i = 1;
      foreach($get_inventory_stock as $stock) {

        if(!isset($last_stock[$stock->product][$stock->warehouse_location])) {
          continue;
        }

        if( number_format($last_stock[$stock->product][$stock->warehouse_location],2) != number_format($stock->end_qty,2) ) {
					$count_failure++;
					$failed[] = [
            'product' => $stock->product_name,
            'warehouse_location' => $stock->warehouse_location_name,
            'date' => date('Y-m-d', strtotime($stock->date)),
						'actual_end_qty' => number_format($last_stock[$stock->product][$stock->warehouse_location],2),
						'end_qty' => number_format($stock->end_qty,2)
          ];
          $failed_txt .= "product: $stock->product_name\r\nwarehouse: $stock->warehouse_location_name\r\ndate: ".date('Y-m-d', strtotime($stock->date))."\r\nactual end quantity: ".number_format($last_stock[$stock->product][$stock->warehouse_location],2)."\r\nend quantity: ".number_format($stock->end_qty,2)."\r\n";
				}
				$this->show_status($i++, $total_progress);
      }

      echo "\nDone. ".$count_failure." failure.";
      if($failed) {
        echo "failed: ";
        print_r($failed);
        file_put_contents('/home/erpgoldenagin/public_html/stock_error.txt', $failed_txt.PHP_EOL , FILE_APPEND | LOCK_EX);        
      }
		}
		
		private function show_status($done, $total, $size=30) {

			static $start_time;

			// if we go over our bound, just ignore it
			if($done > $total) return;

			if(empty($start_time)) $start_time=time();
			$now = time();

			$perc=(double)($done/$total);

			$bar=floor($perc*$size);

			$status_bar="\r[";
			$status_bar.=str_repeat("=", $bar);
			if($bar<$size){
					$status_bar.=">";
					$status_bar.=str_repeat(" ", $size-$bar);
			} else {
					$status_bar.="=";
			}

			$disp=number_format($perc*100, 0);

			$status_bar.="] $disp%  $done/$total";

			$rate = ($now-$start_time)/$done;
			$left = $total - $done;
			$eta = round($rate * $left, 2);

			$elapsed = $now - $start_time;

			$status_bar.= " remaining: ".number_format($eta)." sec.  elapsed: ".number_format($elapsed)." sec.";

			echo "$status_bar  ";

			flush();

			// when done, send a newline
			if($done == $total) {
					echo "\n";
			}
  }
  
  private function validateDate($date, $format = 'Y-m-d')
  {
      $d = \DateTime::createFromFormat($format, $date);
      return $d && $d->format($format) === $date;
  }
}
