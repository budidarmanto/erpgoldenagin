<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\ProductProduct;
use App\Models\ProductUom;
use App\Models\InventoryStockMovement;
use App\Models\InventoryStockMovementProduct;
use App\Models\InventoryStockMovementLog;
use App\Models\InventoryWarehouseLocation;
use App\Models\InventoryStock;
use App\Models\SettingCompany;
use Input;
use Params;
use Helper;
use DB;

class FixInventoryStock extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stock:fix {date}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fix Inventory Stock';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
			$check_date = date('Y-m-d');
			$product = "";

			if($this->argument('date')) {
        $check_date = $this->argument('date');
        if(!$this->validateDate($check_date)) {
          echo "Date format must be YYYY-MM-DD\nexample: stock:fix 2019-02-18\n";
          return;
        }
			}
			
			$yesterday = date('Y-m-d', strtotime('-1 day', strtotime($check_date)));

			$dataUom = ProductUom::select(['id', 'code', 'name', 'uom_category', 'ratio', 'type'])->get()->toArray();

			$all_products = ProductProduct::select(['product_product.id','product_uom_default.id as uom'])
			->join('product_master', 'product_master.id', '=', 'product_product.product')
			->leftJoin('product_uom',  'product_uom.id', '=', 'product_master.uom')
			->leftJoin(DB::raw('(select * from product_uom where id in (select max(id) from product_uom where type =\'d\' and active = true group by uom_category)) as product_uom_default'), function ($join) {
					$join->on('product_uom_default.uom_category', '=', 'product_uom.uom_category');
			})->get();

			$products_uom = [];
			foreach($all_products as $prod) {
				$products_uom[$prod->id] = $prod->uom; 
			}

      $get_transaction = InventoryStockMovementProduct::select([
        'inventory_stock_movement_product.product',
        'inventory_stock_movement_product.qty',
        DB::raw("0 AS qty_in"),
        DB::raw("0 AS qty_out"),
        'inventory_stock_movement_product.uom',
        'inventory_stock_movement.type',
        'inventory_stock_movement.status',
        'inventory_stock_movement.source',
        'inventory_stock_movement.destination',
        'inventory_stock_movement.description',
        'inventory_stock_movement.code',
        'inventory_stock_movement_log.created_at as date',
      ])
			->join('inventory_stock_movement', 'inventory_stock_movement.id', '=', 'inventory_stock_movement_product.stock_movement')
			->join('inventory_stock_movement_log', function($join){
        $join->on('inventory_stock_movement_log.stock_movement', '=', 'inventory_stock_movement.id')
        ->whereIn('inventory_stock_movement_log.status', ['3AA', '3IT']);
			});

			if($product)
				$get_transaction->where('inventory_stock_movement_product.product', $product);
			
			$get_transaction = $get_transaction->orderBy('inventory_stock_movement_log.created_at', 'ASC')
      ->orderBy('inventory_stock_movement_product.id', 'ASC');

			echo "Get all transactions...\n";

      $get_transaction = $get_transaction->get()->toArray();

			echo "Calculate all transactions...\n";

			//dd(count($get_transaction));

			$data_transaction = [];
			
			$dataUom = collect($dataUom);

			$last_stock = [];
			
			foreach($get_transaction as $value) {
				
				$currentUom = $dataUom->where('id', $value['uom'])->first();
				$basicUnit = $dataUom->where('type', 'd')->where('uom_category', $currentUom['uom_category'])->first();

				if(!empty($basicUnit)){
					$basicUnit = $basicUnit['id'];
				}

				$location = null;

				$date = strtotime(date('Y-m-d', strtotime($value['date'])));

				$currentUom = collect($dataUom)->where('id', $value['uom'])->first();

				$value['qty'] = $value['qty'] * $currentUom['ratio'];

				$increment = 0;
				if($value['type'] == 'AD') {
					if($value['qty'] > 0) {
						$value['qty_in'] = $value['qty'];
					} else {
						$value['qty_out'] = $value['qty'] * -1;
					}
					$increment = $value['qty'];

					$location = $value['source'];

					if(!isset($last_stock[$value['product']][$location])) {
						$last_stock[$value['product']][$location] = 0;
					}

					if(!isset($data_transaction[$value['product']][$location][$date])) {
						$data_transaction[$value['product']][$location][$date] = [
							'date' => date('Y-m-d', strtotime($value['date'])),
							'begin_qty' => $last_stock[$value['product']][$location],
							'plus_qty' => 0,
							'min_qty' => 0,
							'end_qty' => 0,
							'uom' => $basicUnit
						];
					}

					$last_stock[$value['product']][$location] += $increment;

					$data_transaction[$value['product']][$location][$date]['plus_qty'] += $value['qty_in'];
					$data_transaction[$value['product']][$location][$date]['min_qty'] += $value['qty_out'];
					$data_transaction[$value['product']][$location][$date]['end_qty'] = $last_stock[$value['product']][$location];
				}
				elseif($value['type'] == 'IT') {

					$location = $value['source'];
					$increment = $value['qty'] * -1;

					if(!isset($last_stock[$value['product']][$location])) {
						$last_stock[$value['product']][$location] = 0;
					}

					if(!isset($data_transaction[$value['product']][$location][$date])) {
						$data_transaction[$value['product']][$location][$date] = [
							'date' => date('Y-m-d', strtotime($value['date'])),
							'begin_qty' => $last_stock[$value['product']][$location],
							'plus_qty' => 0,
							'min_qty' => 0,
							'end_qty' => 0,
							'uom' => $basicUnit
						];
					}

					$last_stock[$value['product']][$location] += $increment;

					$data_transaction[$value['product']][$location][$date]['min_qty'] += $value['qty'];
					$data_transaction[$value['product']][$location][$date]['end_qty'] = $last_stock[$value['product']][$location];

					$location = $value['destination'];
					$increment = $value['qty'];

					if(!isset($last_stock[$value['product']][$location])) {
						$last_stock[$value['product']][$location] = 0;
					}

					if(!isset($data_transaction[$value['product']][$location][$date])) {
						$data_transaction[$value['product']][$location][$date] = [
							'date' => date('Y-m-d', strtotime($value['date'])),
							'begin_qty' => $last_stock[$value['product']][$location],
							'plus_qty' => 0,
							'min_qty' => 0,
							'end_qty' => 0,
							'uom' => $basicUnit
						];
					}
					
					$last_stock[$value['product']][$location] += $increment;

					$data_transaction[$value['product']][$location][$date]['plus_qty'] += $value['qty'];
					$data_transaction[$value['product']][$location][$date]['end_qty'] = $last_stock[$value['product']][$location];
				}
			}

			//dd($data_transaction);

			echo "Get current stock...\n";

			$get_inventory_stock = InventoryStock::select([
        'inventory_stock.begin_qty',
        'inventory_stock.plus_qty',
        'inventory_stock.min_qty',
        'inventory_stock.end_qty',
        'inventory_stock.product',
        'inventory_stock.date',
        'inventory_stock.warehouse_location',
			]);
			if($product)
				$get_inventory_stock->where('product', $product);

			$get_inventory_stock->whereDate('date', '>=', $yesterday);
			$get_inventory_stock = $get_inventory_stock->get();

			echo "Calculate current stock...\n";
			
			$data_stock = [];
			foreach($get_inventory_stock as $stock) {
				$date = strtotime(date('Y-m-d', strtotime($stock->date)));
				$data_stock[$stock->product][$stock->warehouse_location][$date] = [
					'begin_qty' => $stock->begin_qty,
					'plus_qty' => $stock->plus_qty,
					'min_qty' => $stock->min_qty,
					'end_qty' => $stock->end_qty
				];
			}

			//dd($data_stock);

			$count_inserted = 0;
			$count_updated = 0;
			$failed = [];
			$test = [];

			$created_at = date('Y-m-d H:i:s');

			$total_progress = count($data_transaction);

			$i = 1;
			foreach($data_transaction as $product => $locations) {
				$inserts = [];

				foreach($locations as $location => $transactions) {
					$begin = new \DateTime( $check_date );
					$end   = new \DateTime( date('Y-m-d') );

					$last_uom = $products_uom[$product];

					for ($day = $begin; $day <= $end; $day->modify('+1 day')) {

						$date = $day->format('Y-m-d');
						$timestamp = strtotime($date);
						
						$timestamp_yesterday = strtotime(date('Y-m-d', strtotime('-1 day', strtotime($date)))); 

						$trans = [
							'begin_qty' => 0,
							'plus_qty' => 0,
							'min_qty' => 0,								
							'end_qty' => 0,								
						];

						if(isset($transactions[$timestamp])) {
							$trans['begin_qty'] = $transactions[$timestamp]['begin_qty'];
							$trans['plus_qty'] = $transactions[$timestamp]['plus_qty'];
							$trans['min_qty'] = $transactions[$timestamp]['min_qty'];
							$trans['end_qty'] = $transactions[$timestamp]['end_qty'];
							$last_uom = $transactions[$timestamp]['uom'];
						} else if(isset($data_stock[$product][$location][$timestamp_yesterday])) {
							$trans['begin_qty'] = $data_stock[$product][$location][$timestamp_yesterday]['end_qty'];
							$trans['end_qty'] = $data_stock[$product][$location][$timestamp_yesterday]['end_qty'];
						}

						//$test[] = $date;

						if(isset($data_stock[$product][$location][$timestamp])) {

							$current_stock = $data_stock[$product][$location][$timestamp];

							// $test[] = [
							// 	'actual' => number_format($trans['end_qty'],2),
							// 	'current' => number_format($current_stock['end_qty'],2),
							// 	'date' => $date,
							// 	'warehouse_location' => $location
							// ]; 

							if(
								(number_format($trans['begin_qty'],2) != number_format($current_stock['begin_qty'],2)) ||
								(number_format($trans['plus_qty'],2) != number_format($current_stock['plus_qty'],2)) ||
								(number_format($trans['min_qty'],2) != number_format($current_stock['min_qty'],2)) ||
								(number_format($trans['end_qty'],2) != number_format($current_stock['end_qty'],2))								
							) {
								$updating = InventoryStock::where([
									'product' => $product,
									'warehouse_location' => $location,
								])->whereDate('date', $date)->update([
									'begin_qty' => $trans['begin_qty'],
									'plus_qty' => $trans['plus_qty'],
									'min_qty' => $trans['min_qty'],
									'end_qty' => $trans['end_qty'],
									'updated_by' => 'SYSTEM',
									'updated_at' => $created_at,
								]);
								if($updating) {
									//$test[] = ['trans' => $trans, 'current_stock' => $current_stock];
									$data_stock[$product][$location][$timestamp] = $trans;
									$count_updated++;
								}
								else {
									$failed[] = [
										'product' => $product,
										'location' => $location,
										'date' => $date
									];
								}
							}
						} else {
							$inserts[] = [
								'id' => uniqid().substr(md5(mt_rand()), 0, 5),
								'date' => $date,
								'product' => $product,
								'uom' => $last_uom,
								'warehouse_location' => $location,
								'begin_qty' => $trans['begin_qty'],
								'plus_qty' => $trans['plus_qty'],
								'min_qty' => $trans['min_qty'],
								'end_qty' => $trans['end_qty'],
								'created_by' => 'SYSTEM',
								'created_at' => $created_at
							];
						}
					}
				}

				//dd($test);

				if(InventoryStock::insert($inserts))
					$count_inserted += count($inserts);
				else {
					$failed[] = [
						'product' => $product,
						'location' => $location,
						'count' => count($inserts)
					];
				}
				$this->show_status($i++, $total_progress);
			}

			//dd($inserts);

			echo "Done. ".$count_inserted." inserted. ".$count_updated." updated";
			if($failed) {
        echo "failed: ";
  			var_dump($failed);
      }
		}
		
		private function show_status($done, $total, $size=30) {

			static $start_time;

			// if we go over our bound, just ignore it
			if($done > $total) return;

			if(empty($start_time)) $start_time=time();
			$now = time();

			$perc=(double)($done/$total);

			$bar=floor($perc*$size);

			$status_bar="\r[";
			$status_bar.=str_repeat("=", $bar);
			if($bar<$size){
					$status_bar.=">";
					$status_bar.=str_repeat(" ", $size-$bar);
			} else {
					$status_bar.="=";
			}

			$disp=number_format($perc*100, 0);

			$status_bar.="] $disp%  $done/$total";

			$rate = ($now-$start_time)/$done;
			$left = $total - $done;
			$eta = round($rate * $left, 2);

			$elapsed = $now - $start_time;

			$status_bar.= " remaining: ".number_format($eta)." sec.  elapsed: ".number_format($elapsed)." sec.";

			echo "$status_bar  ";

			flush();

			// when done, send a newline
			if($done == $total) {
					echo "\n";
			}
		}

	private function validateDate($date, $format = 'Y-m-d')
  {
      $d = \DateTime::createFromFormat($format, $date);
      return $d && $d->format($format) === $date;
  }
}
