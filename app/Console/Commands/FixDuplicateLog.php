<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class FixDuplicateLog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'log:fix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fix Duplicate Inventory Stock Movement Log';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $delete = DB::delete("delete from inventory_stock_movement_log where (stock_movement,created_at) not in (
        select * from (
          select stock_movement,min(created_at) from inventory_stock_movement_log group by stock_movement, status
        ) x
      ) and status in ('3IT','3AA')");

      echo "Done\n";
		}
}
