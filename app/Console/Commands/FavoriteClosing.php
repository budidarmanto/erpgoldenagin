<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\ProductProduct;
use App\Models\ProductUom;
use App\Models\ProductFavorite;
use App\Models\InventoryStockMovement;
use App\Models\InventoryStockMovementProduct;
use App\Models\InventoryStockMovementLog;
use App\Models\InventoryWarehouseLocation;
use App\Models\InventoryStock;
use App\Models\SettingCompany;
use Input;
use Params;
use Helper;
use Carbon\Carbon;

class FavoriteClosing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'favorite:closing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Closing Product Favorite';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $dataUom = ProductUom::select(['id', 'code', 'name', 'uom_category', 'ratio', 'type'])->get()->toArray();

      $today = date('Y-m-d');
      $lastWeek = date('Y-m-d', mktime(0, 0, 0, date('n'), date('j') - 7, date('Y')));


      $fromDate = Carbon::parse($lastWeek);
      $toDate = Carbon::parse($today);

      $fromDate = $fromDate->toDateTimeString();
      $toDate = $toDate->toDateTimeString();

      ProductFavorite::query()->update(['active' => false]);

      $query = InventoryStockMovementProduct::select([
        'inventory_stock_movement_product.product',
        'inventory_stock_movement_product.qty',
        'inventory_stock_movement_product.uom',
        'inventory_stock_movement.company'
      ])
      ->join('inventory_stock_movement', 'inventory_stock_movement.id', '=', 'inventory_stock_movement_product.stock_movement')
      ->where('inventory_stock_movement.type', 'CH')
      ->where('inventory_stock_movement.status', '1CS')
      ->whereBetween('inventory_stock_movement.date', [$fromDate, $toDate]);
      $dataStockMovement = $query->get()->toArray();

      // dd($dataStockMovement->toArray());
      $dataProduct = [];
      if(count($dataStockMovement) > 0){
        foreach($dataStockMovement as $val){
          $currentUom = collect($dataUom)->where('id', $val['uom'])->first();
          $qty = $val['qty'] * $currentUom['ratio'];

          if(!isset($dataProduct[$val['product']])){
            $dataProduct[$val['product']] = [
              'company' => $val['company'],
              'product' => $val['product'],
              'date' => $today,
              'sold' => $qty,
            ];
          }else{
            $dataProduct[$val['product']]['sold'] += $qty;
          }
        }
      }

      if(count($dataProduct) > 0){
        foreach($dataProduct as $val){
          ProductFavorite::create([
            'company' => $val['company'],
            'product' => $val['product'],
            'date' => $val['date'],
            'sold' => $val['sold'],
            'active' => true
          ]);
        }
      }
    }
}
