<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\ProductProduct;
use App\Models\ProductUom;
use App\Models\InventoryStockMovement;
use App\Models\InventoryStockMovementProduct;
use App\Models\InventoryStockMovementLog;
use App\Models\InventoryWarehouseLocation;
use App\Models\InventoryStock;
use App\Models\SettingCompany;
use Input;
use Params;
use Helper;

class StockClosing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stock:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Stock Closing';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $dataUom = ProductUom::select(['id', 'code', 'name', 'uom_category', 'ratio', 'type'])->get()->toArray();

      $now = date('Y-m-d');
      $yesterday = date('Y-m-d', mktime(0, 0, 0, date('n'), date('j') - 1, date('Y')));
//       $now = '2020-07-15';
//       $yesterday = '2020-07-14';

//  for($dd = 26; $dd <= 31; $dd++){
//       $now = date('Y-m-d', mktime(0, 0, 0, 3, $dd, date('Y')));
//       $yesterday = date('Y-m-d', mktime(0, 0, 0, 3, $dd - 1, date('Y')));


// dd($now);
      $stockMovementProduct = InventoryStockMovementProduct::select([
        'inventory_stock_movement_product.product',
        'inventory_stock_movement_product.qty',
        'inventory_stock_movement_product.uom',
        'inventory_stock_movement.code',
        'inventory_stock_movement.date',
        'inventory_stock_movement.type',
        'inventory_stock_movement.status',
        'inventory_stock_movement.source',
        'inventory_stock_movement.destination',
      ])
      ->join('inventory_stock_movement', 'inventory_stock_movement.id', '=', 'inventory_stock_movement_product.stock_movement')
      ->join('inventory_stock_movement_log', function($join){
        $join->on('inventory_stock_movement_log.stock_movement', '=', 'inventory_stock_movement.id')
        ->whereIn('inventory_stock_movement_log.status', ['3AA', '3IT']);
			})
      ->whereDate('inventory_stock_movement_log.created_at', $yesterday)
      //->where('inventory_stock_movement_product.product', '59ae5a331ec5018c6a')
      ->get()->toArray();

// dd($stockMovementProduct);
      $dataProduct = [];
      if(count($stockMovementProduct) > 0){
        foreach($stockMovementProduct as $key => $val){
          $type = $val['type'];
          $currentUom = collect($dataUom)->where('id', $val['uom'])->first();
          $qty = $val['qty'] * $currentUom['ratio'];

          $basicUnit = collect($dataUom)->where('type', 'd')->where('uom_category', $currentUom['uom_category'])->first();
          if(!empty($basicUnit)){
            $basicUnit = $basicUnit['id'];
          }

          $warehouseLocation = '';
          if(in_array($type, ['RC'])){
            $warehouseLocation = $val['destination'];
          }
          else if(in_array($type, ['DO', 'IT', 'CH', 'AD'])){
            $warehouseLocation = $val['source'];
          }

          $keyProduct = $val['product'].'.'.$warehouseLocation;

          if(!isset($dataProduct[$keyProduct])){
            $dataProduct[$keyProduct] = [
              'product' => $val['product'],
              'warehouse_location' => $warehouseLocation,
              'uom' => $basicUnit,
              'plus' => 0,
              'min' => 0
            ];
          }

          if(in_array($type, ['RC', 'AD'])){
            if($type === 'AD' && $qty < 0) {
                $dataProduct[$keyProduct]['min'] += -$qty;
            }
            else {
                $dataProduct[$keyProduct]['plus'] += $qty;
            }
          }else if(in_array($type, ['DO', 'CH'])){
            $dataProduct[$keyProduct]['min'] += $qty;
          }else if($type == 'IT'){
            $dataProduct[$keyProduct]['min'] += $qty;
            $targetKeyProduct = $val['product'].'.'.$val['destination'];
            if(!isset($dataProduct[$targetKeyProduct])){
              $dataProduct[$targetKeyProduct] = [
                'product' => $val['product'],
                'warehouse_location' => $val['destination'],
                'uom' => $basicUnit,
                'plus' => $qty,
                'min' => 0
              ];
            }else{
              $dataProduct[$targetKeyProduct]['plus'] += $qty;
            }
          }
//          echo var_dump($dataProduct);
        }
      }
// dd($dataProduct);
      if(count($dataProduct) > 0){
        foreach($dataProduct as $key => $val){
          // if($key != '59ae5a331ec5018c6a.5991834ef15505d491'){
          //   continue;
          // }
// $whloc = InventoryWarehouseLocation::find($val['warehouse_location']);
// dd($whloc);
          // Prev Stock
          $stock = InventoryStock::where([
            'product' => $val['product'],
            'warehouse_location' => $val['warehouse_location']
          ])->whereDate('date', $yesterday)->first();
// dd($stock);
          if(!empty($stock)){
            InventoryStock::where([
              'product' => $val['product'],
              'warehouse_location' => $val['warehouse_location']
            ])->whereDate('date', $yesterday)->update([
              'plus_qty' => $val['plus'],
              'min_qty' => $val['min'],
              'end_qty' => $stock->begin_qty + $val['plus'] - $val['min'],
            ]);
          }else{
            InventoryStock::create([
              'date' => $yesterday,
              'product' => $val['product'],
              'warehouse_location' => $val['warehouse_location'],
              'uom' => $basicUnit,
              'begin_qty' => 0,
              'plus_qty' => $val['plus'],
              'min_qty' => $val['min'],
              'end_qty' => $val['plus'] - $val['min']
            ]);
          }

        }
      }

      $allStock = InventoryStock::whereDate('date', $yesterday)->get();
      if(count($allStock) > 0){
        foreach($allStock as $val){
          InventoryStock::create([
              'date' => $now,
              'product' => $val->product,
              'uom' => $val->uom,
              'warehouse_location' => $val->warehouse_location,
              'begin_qty' => $val->end_qty,
              'plus_qty' => 0,
              'min_qty' => 0,
              'end_qty' => $val->end_qty,
          ]);
        }
      }
// }
      echo "OK";

    }
}
