<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\ManufactureRouting;
use App\Models\ManufactureRoutingOperation;
use App\Models\ManufactureRoutingWorkCenter;
use DB;
use Excel;
use PHPExcel_IOFactory;

class ImportRouting extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:routing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $get_routing = DB::table('manufacture_routing')->get();
        $db_routing = [];
        foreach($get_routing as $row) {
            $db_routing[strtolower(str_replace(' ','', $row->name))] = $row;
        }

        $get_work_center = DB::table('manufacture_work_center')->get();
        $db_work_center = [];
        foreach($get_work_center as $row) {
            $db_work_center[strtolower(str_replace(' ','', $row->name))] = $row;
        }

        $get_location = DB::table('inventory_warehouse_location')->get();
        $db_location = [];
        foreach($get_location as $row) {
            $db_location[strtolower(str_replace(' ','', $row->name))] = $row;
        }

        $company = '59f95fb921df6d4838';

        $file = base_path().'/public/imports/import_routing.xlsx';

        echo "loading file...\n";

        $objPHPExcel = PHPExcel_IOFactory::load($file);

        $data = [];

        $i = 0;

        $worksheet = $objPHPExcel->getSheet(0);

        $skipto = 0;

        $cell_collection = $worksheet->getCellCollection();

        $this->line('Mapping...');
        $total_progress = $worksheet->getHighestRow();
        
        $code = 0;
        
        $last_row = 0;
        foreach ($cell_collection as $cell) {
            $get_cell = $worksheet->getCell($cell);
            $column = $get_cell->getColumn();
            $row = $get_cell->getRow();
            $data_value = trim($get_cell->getValue());
            
            if($row == 1)
                continue;

            if($skipto && $row < $skipto)
                continue;
            
            if(!$data_value || $data_value == 'NULL')
                continue;
                
            switch ($column) {
                case 'B':
                    $code = strtolower(str_replace(' ','', $data_value));
                    $data[$code]['name']['value'] = $data_value;
                break;
                case 'C':
                    $data[$code]['description']['value'] = $data_value;
                break;
                case 'D':
                    $data_value = strtolower(str_replace(' ','', $data_value));
                    $raw_value = $data_value;
                    $data_value = @$db_location[$data_value];
                    if($data_value) {
                        $data[$code]['location_raw']['value'] = $data_value->id;
                    }
                    $data[$code]['location_raw']['raw_value'] = $raw_value;
                break;
                case 'E':
                    $data_value = strtolower(str_replace(' ','', $data_value));
                    $raw_value = $data_value;
                    $data_value = @$db_location[$data_value];
                    if($data_value) {
                        $data[$code]['location_result']['value'] = $data_value->id;
                    }
                    $data[$code]['location_result']['raw_value'] = $raw_value;
                break;
                case 'F':
                    $data_value = strtolower(str_replace(' ','', $data_value));
                    $raw_value = $data_value;
                    $data_value = @$db_location[$data_value];
                    if($data_value) {
                        $data[$code]['location_scrap']['value'] = $data_value->id;
                    }
                    $data[$code]['location_scrap']['raw_value'] = $raw_value;
                break;
                case 'G':
                    $data_value = strtolower(str_replace(' ','', $data_value));
                    $raw_value = $data_value;
                    $data_value = @$db_location[$data_value];
                    if($data_value) {
                        $data[$code]['location_waste']['value'] = $data_value->id;
                    }
                    $data[$code]['location_waste']['raw_value'] = $raw_value;
                break;
                case 'H':
                    $data[$code]['routing'][$row]['name']['value'] = $data_value;
                break;
                case 'I':
                    $data_value = strtolower(str_replace(' ','', $data_value));
                    $raw_value = $data_value;
                    $data_value = @$db_work_center[$data_value];
                    if($data_value) {
                        $data[$code]['routing'][$row]['work_center']['value'] = $data_value->id;
                    }
                    $data[$code]['routing'][$row]['work_center']['raw_value'] = $raw_value;
                break;
                case 'J':
                    $data[$code]['routing'][$row]['duration']['value'] = $data_value;
                break;
                case 'K':
                    $data[$code]['routing'][$row]['capacity']['value'] = $data_value;
                break;
                case 'L':
                    $data[$code]['routing'][$row]['description']['value'] = $data_value;
                break;
            }
            if($last_row != $row) {
                $this->show_status($row, $total_progress);
            }
            $last_row = $row;
        }

        $this->line('');
        $this->line('');
        $this->line('Inserting...');

        //dd($data);

        $datetime = date('Y-m-d H:i:s');
        $n = 1;
        $total_progress = count($data);

        $count_new_routing = 0;

        $skipto = false;

        foreach ($data as $code => $row) {
            $n++;

            if($skipto && $n < $skipto)
                continue;

            if(isset($db_routing[$code])) {
                continue;
            }

            if(!$code) {
                continue;
            }

            $new_routing = new ManufactureRouting;
            $new_routing->code = ManufactureRouting::generateId();
            $new_routing->company = $company;
            $new_routing->created_by = 'SYSTEM';
            $new_routing->updated_by = '';
            $new_routing->created_at = $datetime;
            $new_routing->updated_at = $datetime;
            foreach ($row as $key => $value) {
                if(in_array($key, ['routing']))
                    continue;
                $new_routing->{$key} = @$value['value'];
            }

            if(isset($row['routing']) && $new_routing->save()) {
                $count_new_routing++;
                $is_failed = false;
                foreach($row['routing'] as $route_n => $route) {
                    
                    if(!@$route['name']['value']) {
                        $is_failed = true;
                        break;
                    }

                    if(!@$route['work_center']['value']) {
                        $is_failed = true;
                        break;
                    }
                    
                    $new_route = new ManufactureRoutingOperation;
                    $new_route->routing = $new_routing->id;
                    $new_route->duration_uom = 's';
                    $new_route->resource = 'M';
                    $new_route->sequence = $route_n+1;
                    $new_route->next_sequence = isset($row['routing'][$route_n+1]) ? $route_n+2 : null;
                    $new_route->worksheet = null;
                    $new_route->created_by = 'SYSTEM';
                    $new_route->updated_by = '';
                    $new_route->created_at = $datetime;
                    $new_route->updated_at = $datetime;

                    foreach ($route as $key => $value) {
                        $new_route->{$key} = @$value['value'];
                    }
                    $new_route->save();
                }

                if($is_failed) {
                    $new_routing->delete();
                }
            }
            $this->show_status($n, $total_progress);
        }

        echo "\n\nNew routing: ".$count_new_routing."\n";
    }

    private function show_status($done, $total, $size=30) {

        static $start_time;

        // if we go over our bound, just ignore it
        if($done > $total) return;

        if(empty($start_time)) $start_time=time();
            $now = time();

        $perc=(double)($done/$total);

        $bar=floor($perc*$size);

        $status_bar="\r[";
        $status_bar.=str_repeat("=", $bar);
        if($bar<$size){
            $status_bar.=">";
            $status_bar.=str_repeat(" ", $size-$bar);
        } else {
            $status_bar.="=";
        }

        $disp=number_format($perc*100, 0);

        $status_bar.="] $disp%  $done/$total";

        $rate = ($now-$start_time)/$done;
        $left = $total - $done;
        $eta = round($rate * $left, 2);

        $elapsed = $now - $start_time;

        $status_bar.= " remaining: ".number_format($eta)." sec.  elapsed: ".number_format($elapsed)." sec.";

        echo "$status_bar  ";

        flush();

        // when done, send a newline
        if($done == $total) {
            echo "\n";
        }
    }
}
