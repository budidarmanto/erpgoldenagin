<?php

namespace App\Http\Controllers\Pajak;

use App\Models\PajakPegawai;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Params;

class Pegawai extends Controller
{

  public function index() {

    $dg = Datagrid::source(PajakPegawai::query());
    $dg->title('Pegawai');
    $dg->filter('keyword', function($query, $value){
      if($value != '')
          return $query->where('nip', 'ilike', '%'.$value.'%')->orWhere('npwp', 'ilike', '%'.$value.'%')->orWhere('nama', 'ilike', '%'.$value.'%');

      return $query;
    });
    $dg->add('npwp', 'NPWP');
    $dg->add('nip', 'No. Induk Pegawai');
    $dg->add('nama', 'Nama Pegawai');
    $datagrid = $dg->build();
    return response()->json($datagrid);
  }

  public function create(){
    $form = $this->anyForm(new PajakPegawai());
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }

    $dataForm = $this->getDataResponse($dataForm);
    return response()->json($dataForm);
  }

  public function getDataResponse($dataForm){
    $dataForm['optionStatusPtkp'] = Params::getOption('OPTION_MARITAL_STATUS');
    $dataForm['optionNegara'] = Params::getOption('OPTION_COUNTRY');
    $dataForm['optionGender'] = Params::getOption('OPTION_GENDER');
    return $dataForm;
  }


  public function modify($id = null) {

    $form = $this->anyForm(PajakPegawai::find($id));
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }
    $dataForm = $this->getDataResponse($dataForm);
    return response()->json($dataForm);
  }

  public function delete(){

    $id = Input::get('id');
    PajakPegawai::whereIn('id', $id)->delete();

    return response()->json([
      'status' => true
    ]);
  }

  public function anyForm($source){
    $form = FormBuilder::source($source);
    $form->title('Pegawai');
    $form->add('npwp', 'NPWP', 'text')->rule('required|max:25');
    $form->add('nip', 'No. Induk Pegawai', 'text')->rule('required|max:25');
    $form->add('nama', 'Nama Pegawai', 'text')->rule('required|max:100');
    $form->add('alamat', 'Alamat', 'textarea')->rule('required|max:255');
    $form->add('jenis_kelamin', 'Jenis Kelamin', 'radio')->rule('required');
    $form->add('status_ptkp', 'Status PTKP', 'select')->rule('required')->attributes([
      'ng-options' => 'item.id as item.text for item in response.optionStatusPtkp',
    ]);
    $form->add('tanggungan', 'Jumlah Tanggungan', 'number')->rule('required');
    $form->add('pangkat', 'Pangkat', 'text');
    $form->add('golongan', 'Golongan', 'text');
    $form->add('jabatan', 'Jabatan', 'text');
    $form->add('luar_negeri', 'Wajib Pajak Luar Negeri', 'switch')->attributes([
      'ng-change' => 'changeCountry()'
    ]);
    $form->add('negara', 'Negara', 'select')->attributes([
      'ng-options' => 'item.id as item.text for item in response.optionNegara',
      'data-live-search' => true
    ]);
    return $form;
  }

}
