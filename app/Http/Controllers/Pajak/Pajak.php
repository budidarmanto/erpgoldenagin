<?php

namespace App\Http\Controllers\Pajak;

use App\Models\PajakObjek;
use App\Models\PajakPajak;
use App\Models\PajakPegawai;
use App\Models\SettingPeriod;
use App\Models\SettingWorkflow;
use App\Http\Controllers\Controller;
use League\Csv\Writer;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Auth;
use DateTime;
use DB;
use Response;

class Pajak extends Controller
{

  public function index() {
    $company = Auth::user()->authCompany()->id;

    $optionMonth = [];
    $month = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    foreach($month as $val){
      $dateObj   = DateTime::createFromFormat('!m', $val);
      $monthName = $dateObj->format('F');

      $optionMonth[$val] = $monthName;
    }

    $source = PajakPajak::select('pajak_pegawai.npwp', 'pajak_pegawai.nama', 'pajak_pajak.period_year', 'pajak_pajak.period_month', 'pajak_pajak.objek', 'pajak_pajak.id', DB::raw('pajak_objek.name as objek_name'), 'pajak_pajak.bruto', 'pajak_pajak.pph', 'pajak_pajak.status')
    ->join('pajak_pegawai', 'pajak_pegawai.id', '=', 'pajak_pajak.pegawai')
    ->leftJoin('pajak_objek', 'pajak_objek.code', '=', 'pajak_pajak.objek')
    ->where('company', $company);

    $dg = Datagrid::source($source);
    $dg->title('PPH 21');
    $dg->filter('keyword', function($query, $value){
      if($value != '')
          return $query->where('pajak_pegawai.npwp', 'ilike', '%'.$value.'%')->orWhere('pajak_pegawai.nama', 'ilike', '%'.$value.'%');

      return $query;
    });
    $dg->add('npwp', 'NPWP');
    $dg->add('nama', 'Nama Pegawai');
    $dg->add('period_month', 'Masa')->options($optionMonth);
    $dg->add('period_year', 'Tahun');
    $dg->add('objek_name', 'Objek Pajak');
    $dg->add('bruto', 'Jumlah Bruto')->number();
    $dg->add('pph', 'Jumlah PPh 21')->number();
    $datagrid = $dg->build();

    $dataWorkflow = SettingWorkflow::where('code', 'TAX')->first();
    $workflow = [];
    if(!empty($dataWorkflow)){
      $workflow = json_decode($dataWorkflow->workflow, 1);
      // dd($dataWorkflow->getWorkflowDefault());
    }

    $datagrid['dataWorkflow'] = $workflow;
    return response()->json($datagrid);
  }

  public function create(){
    $form = $this->anyForm(new PajakPajak());
    $form->pre(function($input){
      $input['company'] = Auth::user()->authCompany()->id;
      $dataWorkflow = SettingWorkflow::where('code', 'TAX')->first();
      $defaultWorkflowCode = $dataWorkflow->getWorkflowDefault();
      $input['status'] = $defaultWorkflowCode['code'];
      return $input;
    });
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }

    $dataForm = $this->getDataResponse($dataForm);
    return response()->json($dataForm);
  }

  public function modify($id = null) {

    $pajak = PajakPajak::find($id);
    $form = $this->anyForm($pajak);
    $form->pre(function($input){
      $input['company'] = Auth::user()->authCompany()->id;
      return $input;
    });
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }

    $dataPegawai = PajakPegawai::find($pajak->pegawai);
    if(!empty($dataPegawai)){
      $dataForm['data']['npwp'] = $dataPegawai->npwp;
      $dataForm['data']['nip'] = $dataPegawai->nip;
      $dataForm['data']['nama'] = $dataPegawai->nama;
    }

    $dataForm = $this->getDataResponse($dataForm);
    return response()->json($dataForm);
  }

  public function getDataResponse($dataForm){
    $dataPeriod = SettingPeriod::select(['year', 'month'])->get();

    $year = [];
    $month = [];
    foreach($dataPeriod as $val){
      if(!in_array($val->year, $year))
        $year[] = $val->year;

        $month = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    }

    $optionYear = [];
    foreach($year as $val){
      $optionYear[] = [
        'id' => $val,
        'text' => $val
      ];
    }

    $optionMonth = [];
    foreach($month as $val){
      $dateObj   = DateTime::createFromFormat('!m', $val);
      $monthName = $dateObj->format('F');

      $optionMonth[] = [
        'id' => $val,
        'text' => $monthName
      ];
    }

    $dataForm['optionYear'] = $optionYear;
    $dataForm['optionMonth'] = $optionMonth;
    $dataForm['optionObjek'] = PajakObjek::getOption();

    return $dataForm;
  }

  public function delete(){

    $id = Input::get('id');
    PajakPajak::whereIn('id', $id)->delete();

    return response()->json([
      'status' => true
    ]);
  }

  public function changeStatus($id = null, $status = null){
    $pajak = PajakPajak::find($id);
    if(!empty($pajak)){
      $pajak->status = $status;
      $pajak->save();
    }

    return response()->json([
      'status' => true
    ]);
  }

  public function export()
  {
    $company = Auth::user()->authCompany()->id;

    $header = array('NPWP', 'Nama Pegawai', 'Masa', 'Tahun', 'Objek Pajak', 'Jumlah Bruto', 'Jumlah PPh');
    $pajaks = PajakPajak::select('pajak_pegawai.npwp', 'pajak_pegawai.nama', 'pajak_pajak.period_year', 'pajak_pajak.period_month', 'pajak_pajak.objek', 'pajak_pajak.id', DB::raw('pajak_objek.name as objek_name'), 'pajak_pajak.bruto', 'pajak_pajak.pph', 'pajak_pajak.status')
      ->join('pajak_pegawai', 'pajak_pegawai.id', '=', 'pajak_pajak.pegawai')
      ->leftJoin('pajak_objek', 'pajak_objek.code', '=', 'pajak_pajak.objek')
      ->where('company', $company)->get();

    $records = [];
    foreach($pajaks as $pajak) {
      $records[] = [$pajak->npwp, $pajak->nama, $pajak->period_year, $pajak->period_month, $pajak->objek_name, $pajak->bruto, $pajak->pph];
    }

    $csv = Writer::createFromString('');

    $csv->insertOne($header);

    $csv->insertAll($records);
    $csv->output('PPh 21-'.date('d-m-Y').'.csv');

  }

  public function anyForm($source){
    $form = FormBuilder::source($source);
    $form->title('PPH 21');
    $form->add('period_month', 'Masa', 'select')->rule('required')->attributes([
      'ng-options' => 'item.id as item.text for item in response.optionMonth'
    ]);
    $form->add('period_year', 'Tahun', 'select')->rule('required')->attributes([
      'ng-options' => 'item.id as item.text for item in response.optionYear'
    ]);
    $form->add('pembetulan', 'Pembetulan', 'number')->rule('required');
    $form->add('npwp', 'NPWP', 'text')->rule('required')->attributes(['readonly' => true]);
    $form->add('pegawai', 'Pegawai', 'hidden')->rule('required');
    $form->add('nama', 'Nama Pegawai', 'text')->rule('required')->attributes(['readonly' => true]);
    $form->add('nip', 'NIP', 'text')->rule('required')->attributes(['readonly' => true]);
    $form->add('objek', 'Kode Pajak', 'select')->rule('required')->attributes([
      'ng-options' => 'item.id as item.text for item in response.optionObjek'
    ]);
    $form->add('bruto', 'Jumlah Bruto', 'number')->rule('required');
    $form->add('pph', 'Jumlah PPH', 'number')->rule('required');
    return $form;
  }

  public function dataPegawai(){
    $dg = Datagrid::source(PajakPegawai::query());
    $dg->title('Pegawai');
    $dg->filter('keyword', function($query, $value){
      if($value != '')
          return $query->where('nip', 'ilike', '%'.$value.'%')->orWhere('npwp', 'ilike', '%'.$value.'%')->orWhere('nama', 'ilike', '%'.$value.'%');

      return $query;
    });
    $dg->add('npwp', 'NPWP');
    $dg->add('nip', 'No. Induk Pegawai');
    $dg->add('nama', 'Nama Pegawai');
    $datagrid = $dg->build();
    return response()->json($datagrid);
  }

}
