<?php

namespace App\Http\Controllers\Pajak;

use App\Models\PajakObjek;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;

class Objek extends Controller
{

  public function index() {

    $dg = Datagrid::source(PajakObjek::query());
    $dg->title('Objek Pajak');
    $dg->filter('keyword', function($query, $value){
      if($value != '')
          return $query->where('code', 'ilike', '%'.$value.'%')->orWhere('name', 'ilike', '%'.$value.'%');

      return $query;
    });
    $dg->add('code', 'Kode Objek Pajak');
    $dg->add('name', 'Objek Pajak');
    $datagrid = $dg->build();
    return response()->json($datagrid);
  }

  public function create(){
    $form = $this->anyForm(new PajakObjek());
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }
    return response()->json($dataForm);
  }

  public function modify($id = null) {

    $form = $this->anyForm(PajakObjek::find($id));
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }
    return response()->json($dataForm);
  }

  public function delete(){

    $id = Input::get('id');
    PajakObjek::whereIn('id', $id)->delete();

    return response()->json([
      'status' => true
    ]);
  }

  public function anyForm($source){
    $form = FormBuilder::source($source);
    $form->title('Objek Pajak');
    $form->add('code', 'Kode Objek Pajak', 'text')->rule('required|max:60');
    $form->add('name', 'Objek Pajak', 'text')->rule('required|max:100');
    return $form;
  }

}
