<?php

namespace App\Http\Controllers;

use App\Models\SettingUser;
use App\Models\SettingCompany;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\View;
use Session;
use App\Models\ManufactureOrder;
use App\Models\ManufactureWorkOrder;

use App\Models\ProductProduct;
use App\Models\ProductUom;
use App\Models\InventoryStockMovement;
use App\Models\InventoryStockMovementProduct;
use App\Models\InventoryStockMovementLog;
use App\Models\InventoryWarehouseLocation;
use App\Models\InventoryStock;

use DB;
use Excel;

class Home extends Controller {

    public function index() {
      $theme = config('app.theme');
      return view('layout', compact('theme'));
    }

    public function login() {
      $theme = config('app.theme');
        return view('auth.login', compact('theme'));
    }

    public function dologin() {
        $input = Input::all();

        $rules = [
            'username' => 'required',
            'password' => 'required|min:6',
        ];

        $validator = Validator::make($input, $rules);
        // If Validation Fails
        if ($validator->fails()) {

            return redirect()->route('AuthLogin')->withErrors("Username atau password salah!")->withInput(Input::except('password'));
        } else {
            $user = SettingUser::where('username', $input['username'])->orWhere('email', $input['username'])->first();

            if ($user) {

              if($user->active != 1){
                return redirect()->route('AuthLogin')->withErrors("User tidak aktif");
              }

                if (Hash::check($input['password'], $user->password)) {

                    $session = [
                        'username' => $user->username,
                        'password' => $input['password']
                    ];

                    $remember = true;
                    if (Auth::attempt($session, true)) {

                        return redirect()->intended('/');
                    } else {
                        // no action
                        //dd("failed attempt");
                    }
                } else {
                    // no action
                }
            }

            return redirect()->route('AuthLogin')->withErrors("Username atau password salah!");
        }
    }

    public function forgotPassword() {
        $theme = env('APP_THEME', 'squirrel');
        return view('auth.forgot-password', compact('theme'));
    }

    public function doForgotPassword() {
        $email = Input::get('email');

        $user = SettingUser::where('email', $email)->first();

        if ($user) {

            $resetPasswordLink = route('AuthReset') . '?token=' . Crypt::encrypt($user->username . '|' . time());
            $content = view('mail.forgot-password', ['link' => $resetPasswordLink, 'user' => $user])->render();
            // return response($content);
            Mail::send('mail.forgot-password', ['link' => $resetPasswordLink, 'user' => $user], function($message) use ($user) {
                $message->to($user->email, $user->username)->subject('Konfirmasi perubahan password');
            });

            return redirect()->route('AuthForgotSuccess');
        } else {
            return redirect()->route('AuthForgot')->withErrors("Email yang anda masukan tidak terdaftar!");
        }
    }

    public function forgotPasswordSuccess() {
      $theme = env('APP_THEME', 'squirrel');
        return view('auth.forgot-success', compact('theme'));
    }

    public function resetPassword() {

        $theme = env('APP_THEME', 'squirrel');
        $token = Input::get('token');

        try {
          $decrypt = decrypt($token);
        } catch (\Exception $e) {
          return response("Invalid Token");
        }

        list($username, $time) = explode('|', $decrypt);
        $nexttime = mktime(date('H', $time), date('i', $time) + 30, date('s', $time), date('n', $time), date('j', $time), date('Y', $time));

        if ($nexttime >= time()) {
            $view = View::make('auth.reset-password', compact('theme'))->render();
            return $view;
        } else {
            return response("Invalid Token");
        }

    }

    public function doResetPassword() {
        $token = Input::get('token');
        try {
          $decrypt = decrypt($token);
        } catch (\Exception $e) {
          return response("Invalid Token");
        }
        list($username, $time) = explode('|', $decrypt);

        $input = Input::all();
        $rules = [
            'new_password' => 'required|min:6',
            'new_password_confirm' => 'required|min:6|same:new_password',
        ];
        $validator = Validator::make($input, $rules);
        $password = Input::get('new_password');
        // If Validation Fails
        if ($validator->fails()) {
            if(strlen($password) < 6){
              $msg = 'Password minimal 6 karakter!';
            }else{
              $msg = 'Password tidak sama!';
            }
            return redirect()->back()->withErrors($msg);
        } else {
            if (!empty($password)) {
                $encrypt_password = \Illuminate\Support\Facades\Hash::make($password);
                $user = SettingUser::where('username', $username)->update([
                    'password' => $encrypt_password,
                ]);
                if($user){
                    return redirect()->route('AuthLogin')->with('success', 'Password anda berhasil diubah.');
                }
            } else {

            }
        }
    }

    public function logout() {

        Auth::logout();
        Session::forget('company');
        return redirect()->route('AuthLogin');
    }

    public function clearWOMO() {
        foreach(ManufactureOrder::all() as $data) {
            $data->delete();
        }

        foreach(ManufactureWorkOrder::all() as $data) {
            $data->delete();
        }

        // ManufactureOrder::truncate();
        // ManufactureWorkOrder::truncate();
    }

    public function test_stock_closing() {
        set_time_limit(3600);

        $dataUom = ProductUom::select(['id', 'code', 'name', 'uom_category', 'ratio', 'type'])->get()->toArray();

        $begin = new \DateTime( "2019-01-24" );
        $end   = new \DateTime( "2019-01-31" );

        for($i = $begin; $i <= $end; $i->modify('+1 day')){
            
            $now = $i->format("Y-m-d");
            $yesterday =  date('Y-m-d', strtotime($now.' -1 day'));

            $stockMovementProduct = InventoryStockMovementProduct::select([
            'inventory_stock_movement_product.product',
            'inventory_stock_movement_product.qty',
            'inventory_stock_movement_product.uom',
            'inventory_stock_movement.code',
            'inventory_stock_movement.date',
            'inventory_stock_movement.type',
            'inventory_stock_movement.status',
            'inventory_stock_movement.source',
            'inventory_stock_movement.destination',
            ])
            ->join('inventory_stock_movement', 'inventory_stock_movement.id', '=', 'inventory_stock_movement_product.stock_movement')
            ->join('inventory_stock_movement_log', function($join){
							$join->on('inventory_stock_movement_log.stock_movement', '=', 'inventory_stock_movement.id')
							->whereIn('inventory_stock_movement_log.status', ['3AA', '3IT']);
						})
            ->whereDate('inventory_stock_movement_log.created_at', $yesterday)
            ->where('inventory_stock_movement_product.product', '5a4ba6a512c6298178')
            ->get()->toArray();

            $dataProduct = [];

            if(count($stockMovementProduct) > 0){
                foreach($stockMovementProduct as $key => $val){
                    $type = $val['type'];
                    $currentUom = collect($dataUom)->where('id', $val['uom'])->first();
                    $qty = $val['qty'] * $currentUom['ratio'];

                    $basicUnit = collect($dataUom)->where('type', 'd')->where('uom_category', $currentUom['uom_category'])->first();
                    if(!empty($basicUnit)){
                    $basicUnit = $basicUnit['id'];
                    }

                    $warehouseLocation = '';
                    if(in_array($type, ['RC'])){
                        $warehouseLocation = $val['destination'];
                    }
                    else if(in_array($type, ['DO', 'IT', 'CH', 'AD'])){
                        $warehouseLocation = $val['source'];
                    }

                    $keyProduct = $val['product'].'.'.$warehouseLocation;

                    if(!isset($dataProduct[$keyProduct])){
                        $dataProduct[$keyProduct] = [
                            'product' => $val['product'],
                            'warehouse_location' => $warehouseLocation,
                            'uom' => $basicUnit,
                            'plus' => 0,
                            'min' => 0
                        ];
                    }

                    if(in_array($type, ['RC', 'AD'])){
                        if($type === 'AD' && $qty < 0) {
                            $dataProduct[$keyProduct]['min'] += -$qty;
                        }
                    else {
                        $dataProduct[$keyProduct]['plus'] += $qty;
                    }
                    }else if(in_array($type, ['DO', 'CH'])){
                        $dataProduct[$keyProduct]['min'] += $qty;
                    }else if($type == 'IT'){
                        $dataProduct[$keyProduct]['min'] += $qty;
                        $targetKeyProduct = $val['product'].'.'.$val['destination'];
                        if(!isset($dataProduct[$targetKeyProduct])){
                            $dataProduct[$targetKeyProduct] = [
                            'product' => $val['product'],
                            'warehouse_location' => $val['destination'],
                            'uom' => $basicUnit,
                            'plus' => $qty,
                            'min' => 0
                            ];
                        }else{
                            $dataProduct[$targetKeyProduct]['plus'] += $qty;
                        }
                    }
                }
            }

            if(count($dataProduct) > 0){
                foreach($dataProduct as $key => $val){

                    $stock = InventoryStock::where([
                        'product' => $val['product'],
                        'warehouse_location' => $val['warehouse_location']
                    ])->whereDate('date', $yesterday)->first();

                    if(!empty($stock)){
                        InventoryStock::where([
                            'product' => $val['product'],
                            'warehouse_location' => $val['warehouse_location']
                        ])->whereDate('date', $yesterday)->update([
                            'plus_qty' => $val['plus'],
                            'min_qty' => $val['min'],
                            'end_qty' => $stock->begin_qty + $val['plus'] - $val['min'],
                        ]);
                    }else{
                        InventoryStock::create([
                            'date' => $yesterday,
                            'product' => $val['product'],
                            'warehouse_location' => $val['warehouse_location'],
                            'uom' => $basicUnit,
                            'begin_qty' => 0,
                            'plus_qty' => $val['plus'],
                            'min_qty' => $val['min'],
                            'end_qty' => $val['plus'] - $val['min']
                        ]);
                    }

                }
            }

            $allStock = InventoryStock::whereDate('date', $yesterday)->where('product', '5a4ba6a512c6298178');

            $allStock = $allStock->get();

            if(count($allStock) > 0){
                foreach($allStock as $val){
                    InventoryStock::create([
                        'date' => $now,
                        'product' => $val->product,
                        'uom' => $val->uom,
                        'warehouse_location' => $val->warehouse_location,
                        'begin_qty' => $val->end_qty,
                        'plus_qty' => 0,
                        'min_qty' => 0,
                        'end_qty' => $val->end_qty,
                    ]);
                }
            }
        }

    }

    public function check_stock_closing($startdate = null, $code_item = null) {
        set_time_limit(3600);

        $dataUom = ProductUom::select(['id', 'code', 'name', 'uom_category', 'ratio', 'type'])->get()->toArray();

        $begin = new \DateTime( "2019-01-24" );
        $end   = new \DateTime( date('Y-m-d') );

        for($i = $begin; $i <= $end; $i->modify('+1 day')){
            
            $now = $i->format("Y-m-d");
            $yesterday =  date('Y-m-d', strtotime($now.' -1 day'));

            $stockMovementProduct = InventoryStockMovementProduct::select([
            'inventory_stock_movement_product.product',
            'inventory_stock_movement_product.qty',
            'inventory_stock_movement_product.uom',
            'inventory_stock_movement.code',
            'inventory_stock_movement.date',
            'inventory_stock_movement.type',
            'inventory_stock_movement.status',
            'inventory_stock_movement.source',
            'inventory_stock_movement.destination',
            ])
            ->join('inventory_stock_movement', 'inventory_stock_movement.id', '=', 'inventory_stock_movement_product.stock_movement')
            ->join('inventory_stock_movement_log', function($join){
							$join->on('inventory_stock_movement_log.stock_movement', '=', 'inventory_stock_movement.id')
							->whereIn('inventory_stock_movement_log.status', ['3AA', '3IT']);
						})
            ->whereDate('inventory_stock_movement_log.created_at', $yesterday);
            if($code_item) {
                $stockMovementProduct->join('product_product', 'product_product.id', '=', 'inventory_stock_movement_product.product')
                ->join('product_master', 'product_master.id', '=', 'product_product.product')
                ->where('product_master.code', $code_item);
            }
            $stockMovementProduct = $stockMovementProduct->get()->toArray();

            $dataProduct = [];

            if(count($stockMovementProduct) > 0){
                foreach($stockMovementProduct as $key => $val){
                    $type = $val['type'];
                    $currentUom = collect($dataUom)->where('id', $val['uom'])->first();
                    $qty = $val['qty'] * $currentUom['ratio'];

                    $basicUnit = collect($dataUom)->where('type', 'd')->where('uom_category', $currentUom['uom_category'])->first();
                    if(!empty($basicUnit)){
                    $basicUnit = $basicUnit['id'];
                    }

                    $warehouseLocation = '';
                    if(in_array($type, ['RC'])){
                        $warehouseLocation = $val['destination'];
                    }
                    else if(in_array($type, ['DO', 'IT', 'CH', 'AD'])){
                        $warehouseLocation = $val['source'];
                    }

                    $keyProduct = $val['product'].'.'.$warehouseLocation;

                    if(!isset($dataProduct[$keyProduct])){
                        $dataProduct[$keyProduct] = [
                            'product' => $val['product'],
                            'warehouse_location' => $warehouseLocation,
                            'uom' => $basicUnit,
                            'plus' => 0,
                            'min' => 0
                        ];
                    }

                    if(in_array($type, ['RC', 'AD'])){
                        if($type === 'AD' && $qty < 0) {
                            $dataProduct[$keyProduct]['min'] += -$qty;
                        }
                    else {
                        $dataProduct[$keyProduct]['plus'] += $qty;
                    }
                    }else if(in_array($type, ['DO', 'CH'])){
                        $dataProduct[$keyProduct]['min'] += $qty;
                    }else if($type == 'IT'){
                        $dataProduct[$keyProduct]['min'] += $qty;
                        $targetKeyProduct = $val['product'].'.'.$val['destination'];
                        if(!isset($dataProduct[$targetKeyProduct])){
                            $dataProduct[$targetKeyProduct] = [
                            'product' => $val['product'],
                            'warehouse_location' => $val['destination'],
                            'uom' => $basicUnit,
                            'plus' => $qty,
                            'min' => 0
                            ];
                        }else{
                            $dataProduct[$targetKeyProduct]['plus'] += $qty;
                        }
                    }
                }
            }

            if(count($dataProduct) > 0){
                foreach($dataProduct as $key => $val){

                    $stock = InventoryStock::where([
                        'product' => $val['product'],
                        'warehouse_location' => $val['warehouse_location']
                    ])->whereDate('date', $yesterday)->first();

                    if(!empty($stock)){
                        InventoryStock::where([
                            'product' => $val['product'],
                            'warehouse_location' => $val['warehouse_location']
                        ])->whereDate('date', $yesterday)->update([
                            'plus_qty' => $val['plus'],
                            'min_qty' => $val['min'],
                            'end_qty' => $stock->begin_qty + $val['plus'] - $val['min'],
                        ]);
                    }else{
                        InventoryStock::create([
                            'date' => $yesterday,
                            'product' => $val['product'],
                            'warehouse_location' => $val['warehouse_location'],
                            'uom' => $basicUnit,
                            'begin_qty' => 0,
                            'plus_qty' => $val['plus'],
                            'min_qty' => $val['min'],
                            'end_qty' => $val['plus'] - $val['min']
                        ]);
                    }

                }
            }

            $allStock = InventoryStock::whereDate('date', $yesterday);
            
            if($code_item) {
                $allStock->join('product_product', 'product_product.id', '=', 'inventory_stock_movement_product.product')
                ->join('product_master', 'product_master.id', '=', 'product_product.product')
                ->where('product_master.code', $code_item);
            }

            $allStock = $allStock->get();

            if(count($allStock) > 0){
                foreach($allStock as $val){
                    InventoryStock::create([
                        'date' => $now,
                        'product' => $val->product,
                        'uom' => $val->uom,
                        'warehouse_location' => $val->warehouse_location,
                        'begin_qty' => $val->end_qty,
                        'plus_qty' => 0,
                        'min_qty' => 0,
                        'end_qty' => $val->end_qty,
                    ]);
                }
            }
        }

    }

    public function compare_stock() 
    {
				ini_set('max_execution_time', 18000);
				
        $warehouse = null;
				$whLoc = null;
				$product = '17R1.236450052018';
				$last_date = date('Y-m-d');
				$last_date = '2019-02-13';

				$dataUom = ProductUom::select(['id', 'code', 'name', 'uom_category', 'ratio', 'type'])->get()->toArray();

				$warehouseLocation = [];
				if(!empty($whLoc)) {
					$listIdWarehouseLocation = [$whLoc];
				} else {
					if(!empty($warehouse)){
						$warehouseLocation = InventoryWarehouseLocation::where('warehouse', $warehouse)->get();
					}else{
						$warehouseLocation = InventoryWarehouseLocation::all();
					}
					$listIdWarehouseLocation = $warehouseLocation->pluck('id')->toArray();
				}
				//dd($listIdWarehouseLocation);
				
				$get_transaction = InventoryStockMovementProduct::select([
					'inventory_stock_movement_product.product',
					'inventory_stock_movement_product.qty',
					DB::raw("0 AS qty_in"),
					DB::raw("0 AS qty_out"),
					'inventory_stock_movement_product.uom',
					'inventory_stock_movement.type',
					'inventory_stock_movement.type AS type_name',
					'inventory_stock_movement.status',
					'inventory_stock_movement.source',
					'inventory_stock_movement.destination',
					'inventory_stock_movement.description',
					'inventory_stock_movement.code',
					'inventory_stock_movement_log.created_at as date',
				])
				->join('inventory_stock_movement', 'inventory_stock_movement.id', '=', 'inventory_stock_movement_product.stock_movement')
				->join('inventory_stock_movement_log', function($join){
        	$join->on('inventory_stock_movement_log.stock_movement', '=', 'inventory_stock_movement.id')
					->whereIn('inventory_stock_movement_log.status', ['3AA', '3IT']);
				})
				->whereDate('inventory_stock_movement_log.created_at', '<=', $last_date);

				if($product)
					$get_transaction->where('inventory_stock_movement_product.product', $product);

				if($listIdWarehouseLocation) {
					$get_transaction->where(function($q) use($listIdWarehouseLocation) {
						$q->whereIn('inventory_stock_movement.source', $listIdWarehouseLocation)
						->orWhereIn('inventory_stock_movement.destination', $listIdWarehouseLocation);
					});  
				}

				$get_transaction->orderBy('inventory_stock_movement_log.created_at', 'ASC')
				->orderBy('inventory_stock_movement_product.id', 'ASC');

				//echo "Calculate all transaction...\n";

				$get_transaction = $get_transaction->get()->toArray();

				$data_transaction = [];

				$last_stock = [];
				foreach($get_transaction as $value) {
					
					$date = strtotime(date('Y-m-d', strtotime($value['date'])));

					$currentUom = collect($dataUom)->where('id', $value['uom'])->first();

					$value['qty'] = $value['qty'] * $currentUom['ratio'];

					$increment = 0;
					if($value['type'] == 'AD') {
						if($value['qty'] > 0) {
							$value['qty_in'] = $value['qty'];
						} else {
							$value['qty_out'] = $value['qty'] * -1;
						}
						$increment = $value['qty'];
						
						$location = $value['source'];

						if(!isset($last_stock[$value['product']][$location])) {
							$last_stock[$value['product']][$location] = 0;					
						}

						$last_stock[$value['product']][$location] += $increment;
						
					}
					elseif($value['type'] == 'IT') {
						$location = $value['source'];
						$increment = $value['qty'] * -1;

						if(!isset($last_stock[$value['product']][$location])) {
							$last_stock[$value['product']][$location] = 0;					
						}

						$last_stock[$value['product']][$location] += $increment;

						$location = $value['destination'];
						$increment = $value['qty'];

						if(!isset($last_stock[$value['product']][$location])) {
							$last_stock[$value['product']][$location] = 0;					
						}

						$last_stock[$value['product']][$location] += $increment;
					}
				}

				$get_inventory_stock = InventoryStock::select([
					'inventory_stock.end_qty',
					'inventory_stock.product',
					'pm.name as product_name',
					'inventory_stock.date',
					'inventory_stock.warehouse_location',
					DB::raw("CONCAT(wh.name,'/',whl.name) AS location_name"),				
				])
				->leftJoin('product_product as pp', 'pp.id', '=', 'inventory_stock.product')
				->leftJoin('product_master as pm', 'pm.id', '=', 'pp.product')
				->leftJoin('inventory_warehouse_location as whl', 'inventory_stock.warehouse_location', '=', 'whl.id')
				->leftJoin('inventory_warehouse as wh', 'whl.warehouse', '=', 'wh.id');

				if($product)
					$get_inventory_stock->where('inventory_stock.product', $product);

				$get_inventory_stock->where(DB::raw('date(inventory_stock.date)'), $last_date);
				$get_inventory_stock->orderBy('inventory_stock.date', 'asc');

				if($listIdWarehouseLocation) {
					$get_inventory_stock->where(function($q) use($listIdWarehouseLocation) {
						$q->whereIn('inventory_stock.warehouse_location', $listIdWarehouseLocation);
					});  
				}
				$get_inventory_stock = $get_inventory_stock->get();

				$count_failure = 0;

				$total_progress = count($get_inventory_stock);

				$data = [];
				$data[] = ['Stock Compared '.$last_date];
				$data[] = ['Product','Location','Actual End Qty','Stock End Qty'];
				
				$failed = [];
				
				$i = 1;
				foreach($get_inventory_stock as $stock) {
					$date = strtotime(date('Y-m-d', strtotime($stock->date)));

					if(isset($last_stock[$stock->product][$stock->warehouse_location])) {
						$actual_end_qty = number_format($last_stock[$stock->product][$stock->warehouse_location],2);
					} else {
						$actual_end_qty = 'undefined';
						$failed[] = $stock->product;
					}

					$data[] = [$stock->product_name,$stock->location_name,$actual_end_qty,number_format($stock->end_qty,2)];

					//$this->show_status($i++, $total_progress);				
				}
				
				Excel::create('Stock Compared '.$last_date, function($excel) use ($data) {

					$excel->sheet('Stock', function($sheet) use ($data) {
							
							$sheet->fromArray($data, null, 'A1', true, false);

							$sheet->getStyle('A1')->applyFromArray(array(
								'font' => array(
										'bold' => true,
										'size' => 14
								)
							));

							$sheet->mergeCells('A1:D1');
					});
				})->export('xls');

				// echo "Done. ".count($failed)." failure.\n";
				// if($failed) {
				// 	echo "failed: ";
				// 	var_dump($failed);
				// } 
    }
}
