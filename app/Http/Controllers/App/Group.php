<?php

namespace App\Http\Controllers\App;

use App\Models\AppGroup;
use App\Models\AppRole;
use App\Models\AppApplication;
use App\Models\AppModule;
use App\Models\SettingUser;
use App\Models\ProductCategory;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Auth;

class Group extends Controller
{

  public function index() {

    $user = Auth::user();
    $user->authCategory();

    $dg = Datagrid::source(AppGroup::query());
    $dg->title(__('app.app.group.title'));
    $dg->filter('keyword', function($query, $value){
      if($value != '')
          return $query->where('name', 'ilike', '%'.$value.'%');

      return $query;
    });
    $dg->add('name', __('app.app.group.name'));
    $datagrid = $dg->build();
    return response()->json($datagrid);
  }

  public function create(){
    $form = $this->anyForm(new AppGroup());
    $form->pre(function($input){
      $input['access'] = json_encode($input['access']);
      // $input['category'] = json_encode($input['category']);
      return $input;
    });
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }
    $dataForm['dataDetail'] = $this->getSource();
    $dataForm['optionProductCategory'] = ProductCategory::getOption(null, true);

    return response()->json($dataForm);
  }

  public function modify($id = null) {

    $form = $this->anyForm(AppGroup::find($id));
    $form->pre(function($input){
      $input['access'] = json_encode($input['access']);
      // $input['category'] = json_encode($input['category']);
      return $input;
    });
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }
    $dataForm['data']['access'] = json_decode($dataForm['data']['access']);
    // $dataForm['data']['category'] = json_decode($dataForm['data']['category']);
    $dataForm['dataDetail'] = $this->getSource();
    $dataForm['optionProductCategory'] = ProductCategory::getOption(null, true);
    return response()->json($dataForm);
  }

  public function delete(){

    $id = Input::get('id');
    AppGroup::whereIn('id', $id)->delete();

    return response()->json([
      'status' => true
    ]);
  }

  public function anyForm($source){
    $form = FormBuilder::source($source);
    $form->title(__('app.app.group.title'));
    $form->add('name', __('app.app.group.name'), 'text')->rule('required|max:100');
    // $form->add('category', 'Kategori Produk', 'multiple')->attributes([
    //   'ng-options' => 'item.id as item.text for item in response.optionProductCategory'
    // ]);
    return $form;
  }

  public function getSource(){
    $appRole = AppRole::all()->toArray();

    $dataResponse = [];
    $dataApplication = AppApplication::all();
    foreach($dataApplication as $val){
      $module = [];
      $dataModule = AppModule::where('application', $val->id)->get();
      foreach($dataModule as $vmodule){
        $role = [];
        $dataRole = json_decode($vmodule->role);
        foreach($dataRole as $vrole){
          $searchRole = collect($appRole)->first(function($valFilter) use($vrole){
            return $valFilter['code'] == $vrole;
          });
          if($searchRole){

            if(isset($searchRole)){
              $role[] = [
                'name' => $searchRole['name'],
                'code' => $searchRole['code'],
              ];
            }else{
              // not found
            }
          }

        }
        $module[] = [
          'code' => $vmodule->code,
          'id' => $vmodule->id,
          'name' => $vmodule->name,
          'role' => $role
        ];
      }
      $dataResponse[] = [
        'code' => $val->code,
        'id' => $val->id,
        'name' => $val->name,
        'module' => $module
      ];

    }

    return $dataResponse;
  }

}
