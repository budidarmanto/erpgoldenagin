<?php
namespace App\Http\Controllers\Inventory;

use App\Models\ProductMaster;
use App\Models\ProductCategory;
use App\Models\ProductUomCategory;
use App\Models\ProductUom;
use App\Models\ProductSupplier;
use App\Models\ProductAttribute;
use App\Models\ProductAttributeValue;
use App\Models\ProductProduct;
use App\Models\ProductVariant;
use App\Models\ProductBom;
use App\Models\ProductAlternative;
use App\Models\InventoryStock;
use App\Models\InventoryStockMovement;
use App\Models\InventoryStockMovementProduct;
use App\Models\InventoryWarehouse;
use App\Models\InventoryWarehouseLocation;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Params;
use Carbon\Carbon;
use Helper;
use Excel;
use DB;

class StockCard{

  // Params: Product ID
  public function index($id, $warehouse = null, $whLoc = null){

    $dataUom = ProductUom::select(['id', 'code', 'name', 'uom_category', 'ratio', 'type'])->get()->toArray();

    $fromDate = (empty(Input::get('datestart'))) ? Carbon::parse(date('Y-m-d')) : Carbon::parse(Input::get('datestart'));
    $toDate = (empty(Input::get('dateend'))) ? Carbon::parse(date('Y-m-d')) : Carbon::parse(Input::get('dateend'));

    $toDate->addDays(1);

    $fromDate = $fromDate->toDateTimeString();
    $toDate = $toDate->toDateTimeString();
    $params = [
        'product' => $id,
        'warehouse' => $warehouse,
        'warehouse_location' => $whLoc,
    ];
    
    /*
    $query = ProductProduct::getQuery();
    if(!empty($id)){
      $query->where('product_product.id', $id);
    }
    $query->orderBy('product_master.name', 'ASC');
    $data = $query->get()->toArray();
    */

    $optionWarehouse = InventoryWarehouse::getOption();
  
    $warehouseId = [];
    if(!empty($warehouse)) {
      $warehouse = InventoryWarehouse::find($warehouse);
      if($warehouse) {
        $warehouseId = [$warehouse->id];
      }
    } else {
      $warehouseId = InventoryWarehouse::all()->pluck('id')->toArray();
    }

    $warehouseLocation = [];
    if(!empty($whLoc)) {
      $listIdWarehouseLocation = [$whLoc];
    } else {
      if(!empty($warehouseId)){
        $warehouseLocation = InventoryWarehouseLocation::whereIn('warehouse', $warehouseId)->get();
      }else{
        $warehouseLocation = InventoryWarehouseLocation::all();
      }
      $listIdWarehouseLocation = $warehouseLocation->pluck('id')->toArray();
    }

    //dd($listIdWarehouseLocation);

    //$data = ProductProduct::renderProductAttribute($data);
    //$data = ProductProduct::getRelatedData($data, $warehouse);
    //$dataProduct = collect($data)->first();

    $dataProduct = ProductProduct::find($id);

    $beginQty = $this->getStock($dataProduct['id'], $listIdWarehouseLocation, $fromDate);
    $endQty = $this->getStock($dataProduct['id'], $listIdWarehouseLocation, $toDate);
    $beginQty = (empty($beginQty)) ? 0 : $beginQty['begin_qty'];
    $endQty = (empty($endQty)) ? 0 : $endQty['end_qty'];

    //DB::enableQueryLog();

    $query = InventoryStockMovementProduct::select([
      'inventory_stock_movement_product.product',
      'inventory_stock_movement_product.qty',
      'inventory_stock_movement_product.qty AS move',
      DB::raw("'-' AS qty_in"),
      DB::raw("'-' AS qty_out"),
      'inventory_stock_movement_product.uom',
      'inventory_stock_movement.type',
      'inventory_stock_movement.type AS type_name',
      'inventory_stock_movement.status',
      'inventory_stock_movement.source',
      'inventory_stock_movement.destination',
      'inventory_stock_movement.description',
      'wls.warehouse as wh_source',
      'wld.warehouse as wh_destination',
      'inventory_stock_movement.code',
      'inventory_stock_movement_log.created_at as date',
      DB::raw("CONCAT(whs.name,'/',wls.name) AS source_name"),
      DB::raw("CONCAT(whd.name,'/',wld.name) AS destination_name"),
      'product_uom.name AS uom_name'
    ])
    ->join('inventory_stock_movement', 'inventory_stock_movement.id', '=', 'inventory_stock_movement_product.stock_movement')
    ->join('inventory_stock_movement_log', function($join){
      $join->on('inventory_stock_movement_log.stock_movement', '=', 'inventory_stock_movement.id')
      ->whereIn('inventory_stock_movement_log.status', ['3AA', '3IT']);
    })
    ->join('product_uom', 'product_uom.id', '=', 'inventory_stock_movement_product.uom')
    ->leftJoin('inventory_warehouse_location as wls', 'inventory_stock_movement.source', '=', 'wls.id')
    ->leftJoin('inventory_warehouse_location as wld', 'inventory_stock_movement.destination', '=', 'wld.id')
    ->leftJoin('inventory_warehouse as whs', 'wls.warehouse', '=', 'whs.id')
    ->leftJoin('inventory_warehouse as whd', 'wld.warehouse', '=', 'whd.id')
    ->where('inventory_stock_movement_product.product', $dataProduct['id']);

    if($listIdWarehouseLocation) {
      $query->where(function($q) use($listIdWarehouseLocation) {
        $q->whereIn('inventory_stock_movement.source', $listIdWarehouseLocation)
        ->orWhereIn('inventory_stock_movement.destination', $listIdWarehouseLocation);
      });  
    }

    $query->whereBetween('inventory_stock_movement_log.created_at', [$fromDate, $toDate])
    ->orderBy('inventory_stock_movement_log.created_at', 'ASC')
    ->orderBy('inventory_stock_movement_product.id', 'ASC');
    
    $query_last_stock = $query;

    $get_per_page = Input::get('perpage')?:20;
    $get_page = Input::get('page')?:0;

    $query_last_stock_get = false;
    if($get_page > 1) {
      $query_last_stock->take($get_per_page * ($get_page-1));
    
      $query_last_stock_get = $query_last_stock->get();
    }

    /*
    $addSlashes = str_replace('?', "'?'", $query->toSql());
    dd(vsprintf(str_replace('?', '%s', $addSlashes), $query->getBindings()));
    */
    
    $dg = Datagrid::source($query);

    $dg->title($dataProduct['name']);
    $dg->filter('keyword', function($query, $value){
      if($value != ''){
        return $query->where(function($q) use ($value){
           $q->where('inventory_stock_movement.code', 'ilike', '%'.$value.'%');
           return $q;
        });
      }

      return $query;
    });
    $dg->filter('datestart', function($query, $value){
      return $query;
    });
    $dg->filter('dateend', function($query, $value){
      return $query;
    });

    $dg->add('date', 'Date', false)->render(function($data){
      return Carbon::parse($data['date'])->format('d/m/Y');
    });
    $dg->add('code', 'No', false);
    $dg->add('description', 'Description', false);
    $dg->add('source_name', 'Source', false);
    $dg->add('destination_name', 'Destination', false);
    /*
    $dg->add('type_name', 'Tipe', false)->options([
      'RC' => 'Penerimaan',
      'AD' => 'Penyesuaian Persediaan',
      'IT' => 'Mutasi Internal',
      'CH' => 'Kasir',
      'DO' => 'Pengiriman',
    ]);
    */
    //$dg->add('move', 'Bergerak', false);
    $dg->add('qty_in', 'In', false);
    $dg->add('qty_out', 'Out', false);
    $dg->add('qty', 'Stock', false);
    //$dg->add('uom_name', 'Satuan Ukuran', true);

    $datagrid = $dg->build();
//    dd($datagrid);
  
    $datagrid['beginQty'] = $beginQty;
    $datagrid['endQty'] = $endQty;

    $countStock = $beginQty;

// dd($datagrid['data']);

    if($query_last_stock_get) {
      foreach($query_last_stock_get as $value){
        $increment = 0;
        if($value['type'] == 'AD') {
          if($value['qty'] > 0) {
            $value['qty_in'] = $value['qty'];
          } else {
            $value['qty_out'] = $value['qty'] * -1;
          }
          $increment = $value['qty'];
        }
        elseif($value['type'] == 'IT') {
          if(in_array($value['source'], $listIdWarehouseLocation)) {
            if(!in_array($value['destination'], $listIdWarehouseLocation)) {
              $value['qty_out'] = $value['qty'];
              $increment = $value['qty'] * -1;
            }
          }
          else {
            if(!in_array($value['source'], $listIdWarehouseLocation)) {
              $value['qty_in'] = $value['qty'];
              $increment = $value['qty'];
            }
          }
        }
        
        $currentUom = collect($dataUom)->where('id', $value['uom'])->first();

        $countStock += $increment * $currentUom['ratio'];
        $value['count_stock'] = $countStock;
      } 
    }

    foreach($datagrid['data'] as $key => $value){
      $increment = 0;
      if($value['type'] == 'AD') {
        if($value['qty'] > 0) {
          $value['qty_in'] = $value['qty'];
        } else {
          $value['qty_out'] = $value['qty'] * -1;
        }
        $increment = $value['qty'];
      }
      elseif($value['type'] == 'IT') {
        if(in_array($value['source'], $listIdWarehouseLocation)) {
          if(!in_array($value['destination'], $listIdWarehouseLocation)) {
            $value['qty_out'] = $value['qty'];
            $increment = $value['qty'] * -1;
          }
        }
        else {
          if(!in_array($value['source'], $listIdWarehouseLocation)) {
            $value['qty_in'] = $value['qty'];
            $increment = $value['qty'];
          }
        }
      }

      // if($value['code'] == 'GAN01.IT/PRK/18/06/0006') {
      //   dd($value);
      // }

      $currentUom = collect($dataUom)->where('id', $value['uom'])->first();

      $value['qty'] = number_format($value['qty'] * $currentUom['ratio'], 2);
      $value['move'] = $value['qty'];
      $value['qty_in'] = ($value['qty_in'] != '-') ? number_format($value['qty_in'] * $currentUom['ratio'], 2) : '-';
      $value['qty_out'] = ($value['qty_out'] != '-') ? number_format($value['qty_out'] * $currentUom['ratio'], 2) : '-';

      $basicUnit = collect($dataUom)->where('type', 'd')->where('uom_category', $currentUom['uom_category'])->first();
      if(!empty($basicUnit)){
        $value['uom_name'] = $basicUnit['name'];
      }

      $countStock += $increment * $currentUom['ratio'];

      $value['count_stock'] = $countStock;
      $datagrid['data'][$key] = $value;
    }

    $datagrid['endQty'] = $countStock;
    
    $datagrid['params'] = $params;

    //dd($countStock);

    $export = Input::get('export');
    if(!empty($export)){
      $data  = $datagrid['data'];
      //dd($data);
      $productName = $dataProduct['name'];
      $excelToDate = Carbon::parse($toDate);
      $excelToDate->subDays(1);
      Excel::create('Stock', function($excel) use ($data, $fromDate, $excelToDate, $productName){
        $excel->sheet('Sheet 1', function($sheet) use ($data, $fromDate, $excelToDate, $productName) {
          $sheet->loadView('export.stock-card', compact('data', 'fromDate', 'excelToDate', 'productName'));
        });
      })->download('xls');
      return;
    }
    
    return response()->json($datagrid);
  }

  public function getStock($product, $whloc = null, $date){

    $beginQty = 0;
    $endQty = 0;

    $inventoryStock = InventoryStock::select([
      'inventory_stock.product',
      'inventory_stock.uom',
      'inventory_stock.warehouse_location',
      'inventory_stock.begin_qty',
      'inventory_stock.plus_qty',
      'inventory_stock.min_qty',
      'inventory_stock.end_qty'
    ])
    ->where('inventory_stock.product', $product)
    ->whereDate('inventory_stock.date', $date);
    if(!empty($whloc)) {
      $inventoryStock->whereIn('inventory_stock.warehouse_location', $whloc);
    }
    $inventoryStock = $inventoryStock->get();
    if(count($inventoryStock) > 0){
      foreach($inventoryStock as $val){
        $beginQty += $val['begin_qty'];
        $endQty += $val['end_qty'];
      }
    }
    return [
      'begin_qty' => $beginQty,
      'end_qty' => $endQty
    ];

  }
  
    public function getAbnormal($id, $warehouse = null, $whLoc = null) {
        ini_set('max_execution_time', 3600);
        
        if($warehouse == 'null')
          $warehouse = null;

        $dataUom = ProductUom::select(['id', 'code', 'name', 'uom_category', 'ratio', 'type'])->get()->toArray();

        $fromDate = (empty(Input::get('datestart'))) ? Carbon::parse(date('Y-m-d')) : Carbon::parse(Input::get('datestart'));
        $toDate = (empty(Input::get('dateend'))) ? Carbon::parse(date('Y-m-d')) : Carbon::parse(Input::get('dateend'));

        $toDate->addDays(1);

        $fromDate = $fromDate->toDateTimeString();
        $toDate = $toDate->toDateTimeString();

        $optionWarehouse = InventoryWarehouse::getOption();

        $warehouseId = [];
        if(!empty($warehouse)) {
            $warehouse = InventoryWarehouse::find($warehouse);
            if($warehouse) {
                $warehouseId = [$warehouse->id];
            }
        } else {
            $warehouseId = InventoryWarehouse::all()->pluck('id')->toArray();
        }

        $warehouseLocation = [];
        $listIdWarehouseLocation = [];
        if(!empty($whLoc)) {
            $listIdWarehouseLocation = [$whLoc];
        } else {
            if(!empty($warehouseId)){
                $warehouseLocation = InventoryWarehouseLocation::whereIn('warehouse', $warehouseId)->get();
            } else{
                $warehouseLocation = InventoryWarehouseLocation::all();
            }
            $listIdWarehouseLocation = $warehouseLocation->pluck('id')->toArray();
        }
        
        $arrListWarehouseLocation = array_map(function ($warehouse) {
            return '\''. $warehouse . '\'';
	      }, $listIdWarehouseLocation);
        $strListWarehouseLocation = implode(',', $arrListWarehouseLocation);
    
        $query = InventoryStock::select([
            'inventory_stock.product',
            DB::raw('SUM(inventory_stock.begin_qty) as begin_qty'),
            DB::raw('SUM(inventory_stock.plus_qty) as plus_qty'),
            DB::raw('SUM(inventory_stock.min_qty) as min_qty'),
            DB::raw('SUM(inventory_stock.end_qty) as end_qty'),
            'inventory_stock.date',
            DB::raw('0 as end_qty_actual'),
            DB::raw('coalesce((
                select
                    -1*COALESCE(sum(c.ratio * a.qty), 0) as sum
                from inventory_stock_movement_product a
                inner join inventory_stock_movement b on b.id = a.stock_movement
                left join product_uom c on a.uom = c.id
                where 
                    b.type = \'AD\'
                    and b.status = \'3AA\'
                    and b.updated_at::date = inventory_stock.date
                    and a.product = \''. $id .'\'
                    and b.source in (' . $strListWarehouseLocation . ')
                having
                    COALESCE(sum(c.ratio * a.qty), 0) < 0
            ),0) + coalesce((
                select
                    COALESCE(sum(c.ratio * a.qty), 0) as sum
                from inventory_stock_movement_product a
                inner join inventory_stock_movement b on b.id = a.stock_movement
                left join product_uom c on a.uom = c.id
                where 
                    b.type = \'IT\'
                    and b.status = \'3IT\'
                    and b.updated_at::date = inventory_stock.date
                    and a.product = \''. $id .'\'
                    and b.source in (' . $strListWarehouseLocation . ')
                    and b.destination not in (' . $strListWarehouseLocation . ')
            ),0) as transaction_min
            '),
            DB::raw('coalesce((
                select
                    COALESCE(sum(c.ratio * a.qty), 0) as sum
                from inventory_stock_movement_product a
                inner join inventory_stock_movement b on b.id = a.stock_movement
                left join product_uom c on a.uom = c.id
                where 
                    b.type = \'AD\'
                    and b.status = \'3AA\'
                    and b.updated_at::date = inventory_stock.date
                    and a.product = \''. $id .'\'
                    and b.source in (' . $strListWarehouseLocation . ')
                having
                    COALESCE(sum(c.ratio * a.qty), 0) > 0
            ),0) + coalesce((
                select
                    COALESCE(sum(c.ratio * a.qty), 0) as sum
                from inventory_stock_movement_product a
                inner join inventory_stock_movement b on b.id = a.stock_movement
                left join product_uom c on a.uom = c.id
                where 
                    b.type = \'IT\'
                    and b.status = \'3IT\'
                    and b.updated_at::date = inventory_stock.date
                    and a.product = \''. $id .'\'
                    and b.destination in (' . $strListWarehouseLocation . ')
                    and b.source not in (' . $strListWarehouseLocation . ')
            ),0) as transaction_plus
            ')
        ])
        ->where('inventory_stock.product', $id)
        ->whereBetween('inventory_stock.date', [$fromDate, $toDate])
        ->where(function($q) use ($id, $strListWarehouseLocation) {
            $q->where('inventory_stock.plus_qty', '<>', DB::raw('coalesce((
                select
                    COALESCE(sum(c.ratio * a.qty), 0) as sum
                from inventory_stock_movement_product a
                inner join inventory_stock_movement b on b.id = a.stock_movement
                left join product_uom c on a.uom = c.id
                where 
                    b.type = \'AD\'
                    and b.status = \'3AA\'
                    and b.updated_at::date = inventory_stock.date
                    and a.product = \''. $id .'\'
                    and b.source in (' . $strListWarehouseLocation . ')
                having
                    COALESCE(sum(c.ratio * a.qty), 0) > 0
            ),0) + coalesce((
                select
                    COALESCE(sum(c.ratio * a.qty), 0) as sum
                from inventory_stock_movement_product a
                inner join inventory_stock_movement b on b.id = a.stock_movement
                left join product_uom c on a.uom = c.id
                where 
                    b.type = \'IT\'
                    and b.status = \'3IT\'
                    and b.updated_at::date = inventory_stock.date
                    and a.product = \''. $id .'\'
                    and b.destination in (' . $strListWarehouseLocation . ')
                    and b.source not in (' . $strListWarehouseLocation . ')
            ),0)
            '))
            ->orWhere('inventory_stock.min_qty', '<>', DB::raw('coalesce((
                select
                    -1*COALESCE(sum(c.ratio * a.qty), 0) as sum
                from inventory_stock_movement_product a
                inner join inventory_stock_movement b on b.id = a.stock_movement
                left join product_uom c on a.uom = c.id
                where 
                    b.type = \'AD\'
                    and b.status = \'3AA\'
                    and b.updated_at::date = inventory_stock.date
                    and a.product = \''. $id .'\'
                    and b.source in (' . $strListWarehouseLocation . ')
                having
                    COALESCE(sum(c.ratio * a.qty), 0) < 0
            ),0) + coalesce((
                select
                    COALESCE(sum(c.ratio * a.qty), 0) as sum
                from inventory_stock_movement_product a
                inner join inventory_stock_movement b on b.id = a.stock_movement
                left join product_uom c on a.uom = c.id
                where 
                    b.type = \'IT\'
                    and b.status = \'3IT\'
                    and b.updated_at::date = inventory_stock.date
                    and a.product = \''. $id .'\'
                    and b.source in (' . $strListWarehouseLocation . ')
                    and b.destination not in (' . $strListWarehouseLocation . ')
            ),0)
            '));
        });
        if(!empty($listIdWarehouseLocation)) {
            $query->whereIn('inventory_stock.warehouse_location', $listIdWarehouseLocation);
        }
        $query->groupBy(['date','product']);
        
        $dg = Datagrid::source($query);

        $dg->title('Stock Abnormal');

        $dg->add('date', 'Date', false)->render(function($data){
          return Carbon::parse($data['date'])->format('d/m/Y');
        });
        $dg->add('begin_qty', 'Begin Qty', false);
        $dg->add('end_qty', 'End Qty', false);
        $dg->add('end_qty_actual', 'Actual End Qty', false)->render(function($data) {
            return ((float) $data['begin_qty']) + ((float) $data['transaction_plus']) - ((float) $data['transaction_min']);
        });
        $dg->add('plus_qty', 'In', false);
        $dg->add('transaction_plus', 'Actual In', false);
        $dg->add('min_qty', 'Out', false);
        $dg->add('transaction_min', 'Actual Out', false);

        $datagrid = $dg->build();
        
        return response()->json($datagrid);
    }

    public function getAbnormal2($id, $warehouse = null, $whLoc = null) {

      if($warehouse == 'null')
        $warehouse = null;

      $dataUom = ProductUom::select(['id', 'code', 'name', 'uom_category', 'ratio', 'type'])->get()->toArray();

      $warehouseLocation = [];
      if(!empty($whLoc)) {
        $listIdWarehouseLocation = [$whLoc];
      } else {
        if(!empty($warehouse)){
          $warehouseLocation = InventoryWarehouseLocation::where('warehouse', $warehouse)->get();
        }else{
          $warehouseLocation = InventoryWarehouseLocation::all();
        }
        $listIdWarehouseLocation = $warehouseLocation->pluck('id')->toArray();
      }
      //dd($listIdWarehouseLocation);

      $get_transaction = InventoryStockMovementProduct::select([
        'inventory_stock_movement_product.product',
        'inventory_stock_movement_product.qty',
        'inventory_stock_movement_product.qty AS move',
        DB::raw("0 AS qty_in"),
        DB::raw("0 AS qty_out"),
        'inventory_stock_movement_product.uom',
        'inventory_stock_movement.type',
        'inventory_stock_movement.type AS type_name',
        'inventory_stock_movement.status',
        'inventory_stock_movement.source',
        'inventory_stock_movement.destination',
        'inventory_stock_movement.description',
        'wls.warehouse as wh_source',
        'wld.warehouse as wh_destination',
        'inventory_stock_movement.code',
        'inventory_stock_movement_log.created_at as date',
        DB::raw("CONCAT(whs.name,'/',wls.name) AS source_name"),
        DB::raw("CONCAT(whd.name,'/',wld.name) AS destination_name"),
        'product_uom.name AS uom_name'
      ])
      ->join('inventory_stock_movement', 'inventory_stock_movement.id', '=', 'inventory_stock_movement_product.stock_movement')
      ->join('inventory_stock_movement_log', function($join){
        $join->on('inventory_stock_movement_log.stock_movement', '=', 'inventory_stock_movement.id')
        ->whereIn('inventory_stock_movement_log.status', ['3AA', '3IT']);
			})
      ->join('product_uom', 'product_uom.id', '=', 'inventory_stock_movement_product.uom')
      ->leftJoin('inventory_warehouse_location as wls', 'inventory_stock_movement.source', '=', 'wls.id')
      ->leftJoin('inventory_warehouse_location as wld', 'inventory_stock_movement.destination', '=', 'wld.id')
      ->leftJoin('inventory_warehouse as whs', 'wls.warehouse', '=', 'whs.id')
      ->leftJoin('inventory_warehouse as whd', 'wld.warehouse', '=', 'whd.id')
      ->where('inventory_stock_movement_product.product', $id)
      ->whereDate('inventory_stock_movement_log.created_at', '<', date('Y-m-d'));

      if($listIdWarehouseLocation) {
        $get_transaction->where(function($q) use($listIdWarehouseLocation) {
          $q->whereIn('inventory_stock_movement.source', $listIdWarehouseLocation)
          ->orWhereIn('inventory_stock_movement.destination', $listIdWarehouseLocation);
        });  
      }

      $get_transaction->orderBy('inventory_stock_movement_log.created_at', 'ASC')
      ->orderBy('inventory_stock_movement_product.id', 'ASC');

      $get_transaction = $get_transaction->get()->toArray();

      $data_transaction = [];

      $last_stock = 0;
      foreach($get_transaction as $value) {
        
        $date = strtotime(date('Y-m-d', strtotime($value['date'])));

        $currentUom = collect($dataUom)->where('id', $value['uom'])->first();

        $value['qty'] = $value['qty'] * $currentUom['ratio'];

        $increment = 0;
        if($value['type'] == 'AD') {
          if($value['qty'] > 0) {
            $value['qty_in'] = $value['qty'];
          } else {
            $value['qty_out'] = $value['qty'] * -1;
          }
          $increment = $value['qty'];
        }
        elseif($value['type'] == 'IT') {
          if(in_array($value['source'], $listIdWarehouseLocation)) {
            if(!in_array($value['destination'], $listIdWarehouseLocation)) {
              $value['qty_out'] = $value['qty'];
              $increment = $value['qty'] * -1;
            }
          }
          else {
            if(!in_array($value['source'], $listIdWarehouseLocation)) {
              $value['qty_in'] = $value['qty'];
              $increment = $value['qty'];
            }
          }
        }

        if(!isset($data_transaction[$date])) {
          $data_transaction[$date] = [
            'date' => date('Y-m-d', strtotime($value['date'])),
            'actual_begin_qty' => $last_stock,
            'actual_plus_qty' => 0,
            'actual_min_qty' => 0,
            'actual_end_qty' => $last_stock,
          ];
        }

        $last_stock += $increment;

        $data_transaction[$date]['actual_plus_qty'] += $value['qty_in'];
        $data_transaction[$date]['actual_min_qty'] += $value['qty_out'];
        $data_transaction[$date]['actual_end_qty'] += $increment;
      }

      $get_inventory_stock = InventoryStock::select([
        DB::raw('SUM(inventory_stock.begin_qty) as begin_qty'),
        DB::raw('SUM(inventory_stock.plus_qty) as plus_qty'),
        DB::raw('SUM(inventory_stock.min_qty) as min_qty'),
        DB::raw('SUM(inventory_stock.end_qty) as end_qty'),
        'inventory_stock.date',
      ])
      ->where('product', $id)->orderBy('date', 'asc')->groupBy(['product','date']);

      if($listIdWarehouseLocation) {
        $get_inventory_stock->where(function($q) use($listIdWarehouseLocation) {
          $q->whereIn('warehouse_location', $listIdWarehouseLocation);
        });  
      }
      $get_inventory_stock = $get_inventory_stock->get();

      $data_stock = [];
      foreach($get_inventory_stock as $stock) {
        $date = strtotime(date('Y-m-d', strtotime($stock->date)));

        if(!isset($data_transaction[$date])) {
          continue;
        }

        if( number_format(($stock->plus_qty - $stock->min_qty),2) != number_format(($data_transaction[$date]['actual_plus_qty'] - $data_transaction[$date]['actual_min_qty']),2) ) {
          $data_transaction[$date]['begin_qty'] = (float)$stock->begin_qty;
          $data_transaction[$date]['plus_qty'] = (float)$stock->plus_qty;
          $data_transaction[$date]['min_qty'] = (float)$stock->min_qty;
          $data_transaction[$date]['end_qty'] = (float)$stock->end_qty;
          $data_transaction[$date]['date'] = Carbon::parse($data_transaction[$date]['date'])->format('d/m/Y');
          $data_stock[] = $data_transaction[$date];
        }
      }

      //dd($data_stock); 

      return response()->json(['data' => $data_stock]);
    }
}
