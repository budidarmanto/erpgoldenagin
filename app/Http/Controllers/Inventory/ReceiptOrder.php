<?php

namespace App\Http\Controllers\Inventory;

use App\Models\CrmContact;
use App\Models\CrmContactDetail;
use App\Models\InventoryWarehouse;
use App\Models\InventoryWarehouseLocation;
use App\Models\SettingCompany;
use App\Models\SettingUser;
use App\Models\SettingWorkflow;
use App\Models\ProductProduct;
use App\Models\InventoryStockMovement;
use App\Models\InventoryStockMovementProduct;
use App\Models\InventoryStockMovementLog;
use App\Models\InventoryStockMovementComment;
use App\Models\SettingCurrency;
use App\Models\SettingTax;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Params;
use Helper;
use Auth;
use Carbon\Carbon;

class ReceiptOrder extends Controller{

  public $company;

  public function index() {

    $query = InventoryStockMovement::select([
      'inventory_stock_movement.*'
    ])->where([
      'inventory_stock_movement.company' => Helper::currentCompany(),
      'inventory_stock_movement.type' => 'RC'
    ])->orderBy('inventory_stock_movement.code', 'DESC');

    $dg = Datagrid::source($query);
    $dg->title('Penerimaan');
    $dg->filter('keyword', function($query, $value){
      if($value != ''){
        return $query->where(function($q) use ($value) {
          $q->where('inventory_stock_movement.code', 'ilike', '%'.$value.'%')->orWhere('inventory_stock_movement.description', 'ilike', '%'.$value.'%');
          return $q;
        });
      }

      return $query;
    });
    $dg->filter('date', function($query, $value){
      if($value != '')
          return $query->whereDate('inventory_stock_movement.date', $value);

      return $query;
    });

    $dg->filter('workflow', function($query, $value){
      if($value != '')
          return $query->where('inventory_stock_movement.status', $value);

      return $query;
    });

    $dg->add('code', 'No. Penerimaan', true);
    $dg->add('date', 'Tanggal', true)->render(function($data){
      return Carbon::parse($data['date'])->format('d/m/Y');
    });
    $dg->add('description', 'Keterangan');
    $dg->add('status','Status');
    $datagrid = $dg->build();
    $datagrid['optionWorkflow'] = SettingWorkflow::getOption('RC');
    return response()->json($datagrid);
  }

  public function create($withOrWithoutPO){
    $withPo = ($withOrWithoutPO == 'with-po') ? true : false;

    $form = $this->anyForm(new InventoryStockMovement());

    $defaultWorkflow = SettingWorkflow::workflowDefault('RC');

    $form->pre(function($data) use($defaultWorkflow){
      $isCurrency = (Params::get('MULTI_CURRENCY') == 1) ? true : false;
      $defaultCurrency = SettingCurrency::getDefaultCurrency();
      if(!$isCurrency){
        $data['currency'] = $defaultCurrency;
      }
      $data['date'] = date('Y-m-d');
      $data['code'] = InventoryStockMovement::generateId('RC');
      $data['company'] = Helper::currentCompany();
      $data['type'] = 'RC';
      $data['status'] = $defaultWorkflow['code'];
      return $data;
    });
    $dataForm = $form->build();

    if($form->hasRequest()){

      $this->processDataAfterSave($form, 'create');
      InventoryStockMovementLog::addLog($form->model->id, $defaultWorkflow['code']);



      if($form->saved()){
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }

    $dataForm = $this->getDataResponse($dataForm);
    $dataForm['data']['code'] = InventoryStockMovement::generateId('RC');
    $dataForm['data']['diajukan'] = Auth::user()->fullname;
    $dataForm['data']['date'] = Carbon::now()->format('d/m/Y');
    $dataForm['data']['reference'] = '';
    $dataForm['withPo'] = $withPo;

    $product = Input::get('q');
    if(!empty($product) && $withPo){
      $product = explode('.', $product);
      $poProduct = InventoryStockMovementProduct::find($product[0]);
      $po = InventoryStockMovement::find($poProduct->stock_movement);
      if(!empty($po)){
        $dataForm['data']['reference'] = $po->id;
        $dataForm['data']['reference_code'] = $po->code;
        $dataForm['dataProduct'] = $this->getDataIncoming($po->id, function($query) use ($product){
          return $query->whereIn('inventory_stock_movement_product.id', $product);
        });

        $dataForm['dataProduct'] = collect($dataForm['dataProduct'])->map(function($data){
          $data['maxQty'] = $data['qty'];
          return $data;
        });
      }
    }

    return response()->json($dataForm);
  }

  public function modify($id = null) {

    $withPo = false;
    $stockMovement = InventoryStockMovement::find($id);
    if(!empty($stockMovement->reference)){
      $stockMovementReference = InventoryStockMovement::find($stockMovement->reference);
      if(!empty($stockMovementReference)){
        $withPo = true;
      }
    }

    $form = $this->anyForm($stockMovement);
    $form->pre(function($data) use($id){
      unset($data['date']);

      // Reset data qty
      $stockMovementProduct = InventoryStockMovementProduct::where('stock_movement', $id)->get();
      if(count($stockMovementProduct) > 0){
        foreach($stockMovementProduct as $val){
          $reference = InventoryStockMovementProduct::find($val['reference']);
          if(!empty($reference)){
            $reference->rcp_qty -= $val->qty;
            $reference->save();
          }
        }
      }

      return $data;
    });
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){

        $this->processDataAfterSave($form, 'modify');

        return response()->json([
          'status' => true,
          'dataProduct' => $this->getDataProduct($id, $stockMovement->reference)
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }

    $dataForm['data']['date'] = Carbon::parse($dataForm['data']['date'])->format('d/m/Y');
    $user = SettingUser::where('username', $stockMovement->created_by)->first();
    $dataForm['data']['diajukan'] = (!empty($user)) ? $user->fullname : '';
    $dataForm = $this->getDataResponse($dataForm);
    $dataProduct = $this->getDataProduct($id, $stockMovement->reference);
    $dataProduct = collect($dataProduct)->map(function($dt){
      $dt['maxQty'] = $dt['qty'] + $dt['maxQty'];
      return $dt;
    })->all();

    $dataForm['dataProduct'] = $dataProduct;
    $dataForm['dataComment'] = $this->getDataComment($id);
    $dataForm['withPo'] = $withPo;
    if($withPo){
      $dataForm['data']['reference_code'] = $stockMovementReference->code;
    }

    return response()->json($dataForm);
  }

  public function printOut($id){

    $stockMovement = InventoryStockMovement::find($id);
    $stockMovement->printed += 1;
    $stockMovement->save();
    $company = SettingCompany::find($stockMovement->company);
    $warehouseLocation = InventoryWarehouseLocation::find($stockMovement->destination);
    $warehouse = InventoryWarehouse::find($warehouseLocation->warehouse);
    $dataProduct = $this->getDataProduct($id, $stockMovement->reference);
    $reference = InventoryStockMovement::find($stockMovement->reference);
// dd($reference->toArray());
    return view('print.receipt-order', compact('stockMovement', 'dataProduct', 'company', 'warehouse', 'warehouseLocation', 'reference'));
  }

  public function delete(){

    $id = Input::get('id');
    InventoryStockMovement::whereIn('id', $id)->delete();

    return response()->json([
      'status' => true
    ]);
  }

  public function changeStatus($id, $status){
    $stockMovement = InventoryStockMovement::find($id);
    if(!empty($stockMovement)){
      if($stockMovement->status != $status) {
        $stockMovement->status = $status;
        $stockMovement->save();        
        InventoryStockMovementLog::addLog($id, $status);
      }

      if($status == '2RC'){
        $receiptProduct = InventoryStockMovementProduct::where('stock_movement', $stockMovement->id)->get();
        if(count($receiptProduct) > 0){
          foreach($receiptProduct as $val){
            if(!empty($val->reference)){
                $purchaseProduct = InventoryStockMovementProduct::find($val->reference);
                if(!empty($purchaseProduct)){
                  $purchaseProduct->rcp_qty -= $val->qty;
                  $purchaseProduct->save();
                }
            }
          }
        }
      }

      if(!empty($stockMovement->reference)){
        $purchase = InventoryStockMovement::find($stockMovement->reference);
        if($purchase->type == 'PO'){
          $this->processStatusPoReceipt($purchase);
        }else if($purchase->type == 'TF'){
          $this->processStatusTfReceipt($purchase, $stockMovement);
        }
      }
      return response()->json([
        'status' => true,
        'dataComment' => $this->getDataComment($id)
      ]);
    }
  }

  public function anyForm($source){
    $form = FormBuilder::source($source);
    $form->title('Penerimaan');
    $form->add('code', 'No. Penerimaan', 'text')->rule('required|max:25')->attributes([
      'readonly' => true
    ]);
    $form->add('reference_code', 'No. Pembelian', 'text')->attributes([
      'readonly' => true,
      'required' => true // required only in javascript
    ]);
    $form->add('date', 'Tanggal', 'text')->attributes([
      'readonly' => true
    ]);
    $form->add('description', 'Keterangan', 'textarea');
    $form->add('diajukan', 'Diterima Oleh', 'text')->attributes([
      'readonly' => true
    ]);
    $form->add('destination', 'Tujuan', 'select')->attributes([
      'ng-options' => 'item.id as item.text for item in response.optionWarehouse',
    ])->rule('required');

    return $form;
  }

  public function getDataResponse($data){
    $data['optionCurrency'] = SettingCurrency::getOption();
    $data['defaultCurrency'] = SettingCurrency::getDefaultCurrency();
    $data['optionWarehouse'] = InventoryWarehouseLocation::getOption();
    $data['optionTax'] = SettingTax::getOption();
    $data['dataProduct'] = [];
    $data['dataComment'] = [];
    $data['optionWorkflow'] = SettingWorkflow::getOption('RC');
    return $data;
  }

  public function getDataProduct($id, $reference){
    $dataProduct = InventoryStockMovementProduct::getDataProduct($id);

    $dataIncoming = $this->getDataIncoming($reference);


    $data = [];
    if(count($dataProduct) > 0){
      foreach($dataProduct as $val){

        $incomming = collect($dataIncoming)->filter(function($value) use ($val){
          return $value['reference'] == $val['reference'];
        })->first();

        $maxQty = (!empty($incomming)) ? $incomming['qty'] : 0;

        $data[] = [
          'id' => $val['id'],
          'reference' => $val['reference'],
          'product' => $val['product'],
          'name' => $val['name'],
          'qty' => $val['qty'],
          'maxQty' => $maxQty,
          'uom' => $val['uom'],
          'uom_name' => $val['uom_name'],
          'optionUom' => $val['optionUom'],
        ];
      }
    }
    return $data;
  }

  public function processDataAfterSave($form = null, $action = null){
    if(empty($form)){
      return;
    }


    // Save Movement Product
    $listId = [];
    if(!empty($form->dataPost['dataProduct'])){
      foreach($form->dataPost['dataProduct'] as $val){
        $stockMovementProduct = new InventoryStockMovementProduct();
        $val['stock_movement'] = $form->model->id;
        if(!empty($val['id'])){
          $stockMovementProduct = InventoryStockMovementProduct::find($val['id']);
        }

        $stockMovementProduct->fill($val);
        $stockMovementProduct->save();
        $listId[] = $stockMovementProduct->id;

        if(!empty($val['reference'])){
          $reference = InventoryStockMovementProduct::find($val['reference']);
          if(!empty($reference)){
            $reference->rcp_qty += $val['qty'];
            $reference->save();
          }
        }
      }
    }

    InventoryStockMovementProduct::where('stock_movement', $form->model->id)->whereNotIn('id', $listId)->delete();
  }

  public function dataReference(){
    // Estimasi No Pembelian Tanggal Pemasok Produk Jumlah Satuan Tujuan Tindakan

    $query = InventoryStockMovementProduct::select([
      'inventory_stock_movement.id',
      'inventory_stock_movement.code',
      'inventory_stock_movement.date',
      'inventory_stock_movement.type',
      'crm_contact.name AS supplier',
      'setting_company.name AS company_source',
      'inventory_stock_movement.destination',
      'inventory_warehouse.name AS warehouse_name'
    ])
    ->selectRaw('SUM(inventory_stock_movement_product.qty - inventory_stock_movement_product.rcp_qty) AS qty')
    ->join('inventory_stock_movement', 'inventory_stock_movement.id', '=', 'inventory_stock_movement_product.stock_movement')
    ->leftJoin('crm_contact', 'crm_contact.id', '=', 'inventory_stock_movement.contact')
    ->leftJoin('inventory_warehouse', 'inventory_warehouse.id', '=', 'inventory_stock_movement.destination')
    ->leftJoin('setting_company', 'setting_company.id', '=', 'inventory_stock_movement.company_destination')
    ->where([
      'inventory_stock_movement.company' => Helper::currentCompany(),
    ])
    ->where(function($q){
      $q->where(function($q){
        $q->where('inventory_stock_movement.type', 'PO')
        ->whereIn('inventory_stock_movement.status', ['3PO', '5PR']);
        return $q;
      })->orWhere(function($q){
        $q->where('inventory_stock_movement.type', 'TF')
        ->whereIn('inventory_stock_movement.status', ['1TO', '2TR']);
        return $q;
      });
      return $q;
    })
    ->whereRaw("(inventory_stock_movement_product.qty - inventory_stock_movement_product.rcp_qty) > 0")
    ->groupBy([
      'inventory_stock_movement.id',
      'inventory_stock_movement.code',
      'inventory_stock_movement.date',
      'inventory_stock_movement.type',
      'crm_contact.name',
      'setting_company.name',
      'inventory_stock_movement.destination',
      'inventory_warehouse.name'
    ]);
// dd($query->get()->toArray());
    $dg = Datagrid::source($query);
    $dg->title('Barang Masuk');
    $dg->filter('keyword', function($query, $value){
      if($value != '')
          return $query->where('inventory_stock_movement.code', 'ilike', '%'.$value.'%');

      return $query;
    });

    $dg->add('code', 'No. Pembelian', false);
    $dg->add('date', 'Tanggal', false)->render(function($data){
      return Carbon::parse($data['date'])->format('d/m/Y');
    });
    $dg->add('supplier', 'Pemasok', false)->render(function($data){
      return ($data['type'] == 'TF') ? $data['company_source'] : $data['supplier'];
    });

    $dg->add('qty', 'Jumlah', false);
    $dg->add('warehouse_name', 'Tujuan', false);
    $datagrid = $dg->build();

    return response()->json($datagrid);
  }

  public function getDataIncoming($id, $modifierFn = null){

    $query = InventoryStockMovementProduct::select([
      'inventory_stock_movement_product.id AS reference',
      'inventory_stock_movement_product.product',
      'inventory_stock_movement_product.uom',
      'product_product.attribute',
      'product_master.name',
      'product_uom.id AS uom',
      'product_uom.name AS uom_name',
    ])
    ->selectRaw('(inventory_stock_movement_product.qty - inventory_stock_movement_product.rcp_qty) AS qty')
    ->join('inventory_stock_movement', 'inventory_stock_movement.id', '=', 'inventory_stock_movement_product.stock_movement')
    ->join('product_product', 'product_product.id', '=', 'inventory_stock_movement_product.product')
    ->join('product_master', 'product_master.id', '=', 'product_product.product')
    ->join('product_uom', 'product_uom.id', '=', 'inventory_stock_movement_product.uom')
    ->where([
      'inventory_stock_movement.id' => $id,
      'inventory_stock_movement.company' => Helper::currentCompany(),
    ])
    ->where(function($q){
      $q->where(function($q){
        $q->where('inventory_stock_movement.type', 'PO')
        ->whereIn('inventory_stock_movement.status', ['3PO', '5PR']);
        return $q;
      })->orWhere(function($q){
        $q->where('inventory_stock_movement.type', 'TF')
        ->whereIn('inventory_stock_movement.status', ['1TO', '2TR']);
        return $q;
      });
      return $q;
    })
    ->whereRaw("(inventory_stock_movement_product.qty - inventory_stock_movement_product.rcp_qty) > 0");

    if (is_a($modifierFn, '\Closure')) {
      $query = call_user_func_array($modifierFn, [$query]);
    }

    $data = $query->get()->toArray();
    $data = ProductProduct::renderProductAttribute($data);
    if(count($data) > 0){
      foreach($data as $key => $val){
        $data[$key]['optionUom'] = [
          ['id' => $val['uom'], 'text' => $val['uom_name']]
        ];
      }
    }

    return $data;
  }

  // Called after select No. PO
  // Params PoID
  public function dataIncoming($id){
    $stockMovement = InventoryStockMovement::find($id);
    if(empty($stockMovement)){
      return;
    }

    $data = $this->getDataIncoming($id);

    $response = [
      'data' => $data,
      'optionWarehouse' => InventoryWarehouseLocation::getOption($stockMovement->destination)
    ];

    return response()->json($response);
  }

  public function processStatusPoReceipt($purchase){

    if(!empty($purchase)){
      $purchaseProduct = InventoryStockMovementProduct::where('stock_movement', $purchase->id)->get();
      $totalQty = 0;
      $totalReceipt = 0;
      if(count($purchaseProduct) > 0){
        foreach($purchaseProduct as $val){
            $totalQty += $val->qty;
            $totalReceipt += $val->rcp_qty;
        }
      }

      $status = '';
      if($totalQty == $totalReceipt){
        // Product Receipt All
        $status = '6PC';
      }else if($totalQty != $totalReceipt){
        if($totalReceipt == 0){
          // All product not recieved yet
          $status = '3PO';
        }else{
          // Product Already partially accepted
          $status = '5PR';
        }
      }

      if($purchase->status != $status){
          $purchase->status = $status;
          $purchase->save();
          InventoryStockMovementLog::addLog($purchase->id, $status);
      }

    }

  }

  public function processStatusTfReceipt($purchase, $stockMovement){

    if(!empty($purchase)){
      $purchaseProduct = InventoryStockMovementProduct::where('stock_movement', $purchase->id)->get();
      $totalQty = 0;
      $totalReceipt = 0;
      if(count($purchaseProduct) > 0){
        foreach($purchaseProduct as $val){
            $totalQty += $val->qty;
            $totalReceipt += $val->rcp_qty;
        }
      }

      $status = '';
      if($totalQty == $totalReceipt){
        // Product Receipt All
        $status = '3TC';
      }else if($totalQty != $totalReceipt){
        if($totalReceipt == 0){
          // All product not recieved yet
          $status = '1TO';
        }else{
          // Product Already partially accepted
          $status = '2TR';
        }
      }

      if($purchase->status != $status){
          $purchase->status = $status;
          $purchase->save();

          InventoryStockMovementLog::addLog($purchase->id, $status);
          // Update DO with ET status to delivered if all product receipted
          if($status == '3TC'){
            InventoryStockMovement::find($purchase->reference)->update([
              'status' => '4DD'
            ]);
            InventoryStockMovementLog::addLog($purchase->reference, '4DD');
          }


      }


      $doTransfer = InventoryStockMovement::find($purchase->reference);
      if(!empty($doTransfer)){
        $transfer = InventoryStockMovement::find($doTransfer->reference);
        if(!empty($transfer)){
          $requestNote = InventoryStockMovement::find($transfer->reference);
          $this->processStatusRequestNote($requestNote, $stockMovement);
        }
      }

    }

  }

  public function processStatusRequestNote($requestNote, $stockMovement){

    if(!empty($requestNote)){

      $receiptProduct = InventoryStockMovementProduct::where('stock_movement', $stockMovement->id)->get();
      if(count($receiptProduct) > 0){
        foreach($receiptProduct as $val){
          if(!empty($val->reference)){
            $doProduct = InventoryStockMovementProduct::find($val->reference);
            $etProduct = InventoryStockMovementProduct::find($doProduct->reference);
            $rnProduct = InventoryStockMovementProduct::find($etProduct->reference);
            $rnProduct->rcp_qty += $val->qty;
            $rnProduct->save();
          }
        }
      }

      $requestNoteProduct = InventoryStockMovementProduct::where('stock_movement', $requestNote->id)->get();
      $totalQty = 0;
      $totalReceipt = 0;
      if(count($requestNoteProduct) > 0){
        foreach($requestNoteProduct as $val){
            $totalQty += $val->qty;
            $totalReceipt += $val->rcp_qty;
        }
      }

      $status = '';
      if($totalQty == $totalReceipt){
        // Product Receipt All
        $status = '6RC';
      }else if($totalQty != $totalReceipt){
        if($totalReceipt == 0){
          // All product not recieved yet
          $status = '3RR';
        }else{
          // Product Already partially accepted
          $status = '5RR';
        }
      }

      if($requestNote->status != $status){
          $requestNote->status = $status;
          $requestNote->save();
          InventoryStockMovementLog::addLog($requestNote->id, $status);
      }

    }
  }

  public function getDataComment($id){
    $dataLog = InventoryStockMovementLog::getData($id, 'RC', 'Receipt Order');
    $dataComment = InventoryStockMovementComment::getData($id);
    $data = collect($dataComment);
    $data = $data->merge($dataLog);
    return $data->all();
  }

}
