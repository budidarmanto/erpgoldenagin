<?php

namespace App\Http\Controllers\Inventory;

use App\Models\CrmContact;
use App\Models\CrmContactDetail;
use App\Models\InventoryWarehouse;
use App\Models\InventoryWarehouseLocation;
use App\Models\SettingUser;
use App\Models\SettingWorkflow;
use App\Models\ProductProduct;
use App\Models\InventoryStockMovement;
use App\Models\InventoryStockMovementProduct;
use App\Models\InventoryStockMovementLog;
use App\Models\InventoryStockMovementComment;
use App\Models\SettingCurrency;
use App\Models\SettingTax;
use App\Models\SettingCompany;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Params;
use Helper;
use Auth;
use Carbon\Carbon;

class ExternalTransfer extends Controller{

  public $company;

  public function index() {

    $query = InventoryStockMovement::select([
      'inventory_stock_movement.*',
      'setting_company.name AS company_name',
    ])
    ->join('setting_company', 'setting_company.id', '=', 'inventory_stock_movement.company_destination')
    ->where([
      'inventory_stock_movement.company' => Helper::currentCompany(),
      'inventory_stock_movement.type' => 'ET',
    ])->orderBy('inventory_stock_movement.code', 'DESC');

    $dg = Datagrid::source($query);
    $dg->title('Mutasi Eksternal');
    $dg->filter('keyword', function($query, $value){
      if($value != ''){
        return $query->where(function($q) use ($value) {
          $q->where('inventory_stock_movement.code', 'ilike', '%'.$value.'%')->orWhere('inventory_stock_movement.description', 'ilike', '%'.$value.'%');
          return $q;
        });
      }

      return $query;
    });
    $dg->filter('date', function($query, $value){
      if($value != '')
          return $query->whereDate('inventory_stock_movement.date', $value);

      return $query;
    });

    $dg->filter('workflow', function($query, $value){
      if($value != '')
          return $query->where('inventory_stock_movement.status', $value);

      return $query;
    });

    $dg->add('code', 'No. Mutasi', true);
    $dg->add('expected_date', 'Estimasi Pengiriman', true)->render(function($data){
      return Carbon::parse($data['date'])->format('d/m/Y');
    });
    $dg->add('company_name', 'Tujuan');
    $dg->add('description', 'Description');

    $dg->add('status','Status');
    $datagrid = $dg->build();
    $datagrid['optionWorkflow'] = SettingWorkflow::getOption('ET');
    return response()->json($datagrid);
  }

  public function create($withOrWithoutRN){
    $withRn = ($withOrWithoutRN == 'with-rn') ? true : false;

    $form = $this->anyForm(new InventoryStockMovement());

    $defaultWorkflow = SettingWorkflow::workflowDefault('ET');

    $form->pre(function($data) use($defaultWorkflow){
      $isCurrency = (Params::get('MULTI_CURRENCY') == 1) ? true : false;
      $defaultCurrency = SettingCurrency::getDefaultCurrency();
      if(!$isCurrency){
        $data['currency'] = $defaultCurrency;
      }
      $data['date'] = date('Y-m-d');
      $data['code'] = InventoryStockMovement::generateId('ET');
      $data['company'] = Helper::currentCompany();
      $data['type'] = 'ET';
      $data['status'] = $defaultWorkflow['code'];
      return $data;
    });
    $dataForm = $form->build();

    if($form->hasRequest()){

      if($form->saved()){

        $this->processDataAfterSave($form);
        InventoryStockMovementLog::addLog($form->model->id, $defaultWorkflow['code']);
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }

    $dataForm = $this->getDataResponse($dataForm);
    $dataForm['data']['code'] = InventoryStockMovement::generateId('ET');
    $dataForm['data']['diajukan'] = Auth::user()->fullname;
    $dataForm['data']['date'] = Carbon::now()->format('d/m/Y');

    $dataForm['data']['reference'] = '';
    $dataForm['withRn'] = $withRn;

    $product = Input::get('q');
    if(!empty($product) && $withRn){
      $product = explode('.', $product);
      $rnProduct = InventoryStockMovementProduct::find($product[0]);
      $rn = InventoryStockMovement::find($rnProduct->stock_movement);
      if(!empty($rn)){
        $dataForm['data']['reference'] = $rn->id;
        $dataForm['data']['reference_code'] = $rn->code;
        $dataForm['data']['company_destination'] = $rn->company;
        $dataForm['dataProduct'] = $this->getDataIncoming($rn->id, function($query) use ($product){
          return $query->whereIn('inventory_stock_movement_product.id', $product);
        });

        $dataForm['dataProduct'] = collect($dataForm['dataProduct'])->map(function($data){
          $data['maxQty'] = $data['qty'];
          return $data;
        });
      }
    }
    return response()->json($dataForm);
  }

  public function modify($id = null) {

    // $this->getDataProduct($id);
    $stockMovement = InventoryStockMovement::find($id);

    $withRn = false;
    $stockMovement = InventoryStockMovement::find($id);
    if(!empty($stockMovement->reference)){
      $stockMovementReference = InventoryStockMovement::find($stockMovement->reference);
      if(!empty($stockMovementReference)){
        $withRn = true;
      }
    }

    $form = $this->anyForm($stockMovement);
    $form->pre(function($data){
      unset($data['date']);
      return $data;
    });
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){

        $this->processDataAfterSave($form);

        return response()->json([
          'status' => true,
          'dataProduct' => $this->getDataProduct($id, $stockMovement->reference)
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }

    $dataForm['data']['date'] = Carbon::parse($dataForm['data']['date'])->format('d/m/Y');
    $user = SettingUser::where('username', $stockMovement->created_by)->first();
    $dataForm['data']['diajukan'] = (!empty($user)) ? $user->fullname : '';
    $dataForm = $this->getDataResponse($dataForm);
    $dataForm['dataProduct'] = $this->getDataProduct($id, $stockMovement->reference);
    $dataForm['dataComment'] = $this->getDataComment($id);
    $dataForm['withRn'] = $withRn;
    if($withRn){
      $dataForm['data']['reference_code'] = $stockMovementReference->code;
    }
    $reference = $dataForm['data']['reference'];
    if(!empty($reference)){
      $stockMoveReference = InventoryStockMovement::find($reference);
      if(!empty($stockMoveReference)){
        $dataForm['data']['reference'] = $stockMoveReference->code;
      }
    }

    $dataForm['optionWarehouseDestination'] = $this->getWarehouseDestination($stockMovement->company_destination);
    return response()->json($dataForm);
  }

  public function delete(){

    $id = Input::get('id');
    InventoryStockMovement::whereIn('id', $id)->delete();

    return response()->json([
      'status' => true
    ]);
  }

  public function changeStatus($id, $status){
    $stockMovement = InventoryStockMovement::find($id);
    if(!empty($stockMovement)){
      if($stockMovement->status != $status) {
        $stockMovement->status = $status;
        $stockMovement->save();        
        InventoryStockMovementLog::addLog($id, $status);
      }
      
      return response()->json([
        'status' => true,
        'dataComment' => $this->getDataComment($id)
      ]);

    }
  }

  public function anyForm($source){
    $form = FormBuilder::source($source);
    $form->title('Mutasi Eksternal');
    $form->add('code', 'No. Mutasi', 'text')->rule('required|max:25')->attributes([
      'readonly' => true
    ]);
    $form->add('reference_code', 'No. Pengajuan', 'text')->attributes([
      'readonly' => true,
      'required' => true // required only in javascript
    ]);
    $form->add('date', 'Tanggal', 'text')->attributes([
      'readonly' => true
    ]);
    $form->add('diajukan', 'Diajukan Oleh', 'text')->attributes([
      'readonly' => true
    ]);
    $form->add('source', 'Asal', 'select')->attributes([
      'ng-options' => 'item.id as item.text for item in response.optionWarehouse',
    ])->rule('required');
    $form->add('destination', 'Gudang', 'select')->attributes([
      'ng-options' => 'item.id as item.text for item in optionWarehouseDestination',
    ])->rule('required');
    $form->add('company_destination', 'Tujuan', 'select')->attributes([
      'ng-options' => 'item.id as item.text for item in response.optionCompany',
      'ng-change' => 'getWarehouseDestination()'
    ])->rule('required');
    $form->add('expected_date', 'Estimasi Pengiriman', 'date')->rule('required');
    $form->add('description', 'Keterangan', 'textarea');


    return $form;
  }

  public function warehouseDestination($company){
    $data = $this->getWarehouseDestination($company);
    return response()->json($data);
  }

  public function getWarehouseDestination($company){
    $opt = [];
    $warehouse = InventoryWarehouse::all();
    if(!empty($warehouse)){
      foreach($warehouse as $val){
        $dataCompany = json_decode($val->company, true);
        if(in_array($company, $dataCompany)){
          $opt[] = [
            'id' => $val->id,
            'text' => $val->name
          ];
        }
      }
    }

    return $opt;

  }

  public function getDataResponse($data){
    $data['optionCurrency'] = SettingCurrency::getOption();
    $data['defaultCurrency'] = SettingCurrency::getDefaultCurrency();
    $data['optionWarehouse'] = InventoryWarehouse::getOption();
    $data['dataProduct'] = [];
    $data['dataComment'] = [];
    $data['optionWorkflow'] = SettingWorkflow::getOption('ET');
    $data['optionCompany'] = SettingCompany::getOption();
    $data['optionWarehouseDestination'] = [];
    return $data;
  }

  public function getDataProduct($id, $reference){
    $dataProduct = InventoryStockMovementProduct::getDataProduct($id);
    $dataIncoming = $this->getDataIncoming($reference);

    $data = [];
    if(count($dataProduct) > 0){
      foreach($dataProduct as $val){

        $incomming = collect($dataIncoming)->filter(function($value) use ($val){
          return $value['reference'] == $val['reference'];
        })->first();

        $maxQty = (!empty($incomming)) ? $incomming['qty'] : 0;

        $data[] = [
          'id' => $val['id'],
          'reference' => $val['reference'],
          'product' => $val['product'],
          'name' => $val['name'],
          'qty' => $val['qty'],
          'maxQty' => $maxQty,
          'uom' => $val['uom'],
          'uom_name' => $val['uom_name'],
          'optionUom' => $val['optionUom'],
        ];
      }
    }
    return $data;
  }

  public function processDataAfterSave($form = null){
    if(empty($form)){
      return;
    }

    // Save Movement Product
    $listId = [];
    if(!empty($form->dataPost['dataProduct'])){
      foreach($form->dataPost['dataProduct'] as $val){
        $stockMovementProduct = new InventoryStockMovementProduct();
        $val['stock_movement'] = $form->model->id;
        $val['expected_date'] = null;
        if(!empty($val['id'])){
          $stockMovementProduct = InventoryStockMovementProduct::find($val['id']);
        }
        $val['discount'] = (isset($val['discount'])) ? json_encode($val['discount']) : null;

        $stockMovementProduct->fill($val);
        $stockMovementProduct->save();
        $listId[] = $stockMovementProduct->id;
      }
    }
    InventoryStockMovementProduct::where('stock_movement', $form->model->id)->whereNotIn('id', $listId)->delete();

    InventoryStockMovement::refreshTotal($form->model->id);
  }

  public function dataReference(){
    //Estimasi No Pembelian Tanggal Pemasok Produk Jumlah Satuan Tujuan Tindakan

    $query = InventoryStockMovementProduct::select([
      'inventory_stock_movement.id',
      'inventory_stock_movement.code',
      'inventory_stock_movement.date',
      'inventory_stock_movement.type',
      'inventory_stock_movement.company',
      // 'crm_contact.name AS supplier',
      'setting_company.name AS company_source',
      'inventory_stock_movement.destination',
      // 'inventory_warehouse.name AS warehouse_name'
    ])
    ->selectRaw('SUM(inventory_stock_movement_product.qty - inventory_stock_movement_product.rcp_qty) AS qty')
    ->join('inventory_stock_movement', 'inventory_stock_movement.id', '=', 'inventory_stock_movement_product.stock_movement')
    ->leftJoin('crm_contact', 'crm_contact.id', '=', 'inventory_stock_movement.contact')
    ->leftJoin('inventory_warehouse', 'inventory_warehouse.id', '=', 'inventory_stock_movement.destination')
    ->leftJoin('setting_company', 'setting_company.id', '=', 'inventory_stock_movement.company_destination')
    ->where([
      'inventory_stock_movement.company_destination' => Helper::currentCompany(),
    ])
    ->where(function($q){
      $q->where(function($q){
        $q->where('inventory_stock_movement.type', 'RN')
        ->whereIn('inventory_stock_movement.status', ['3RR', '5RR']);
        return $q;
      });
      return $q;
    })
    ->whereRaw("(inventory_stock_movement_product.qty - inventory_stock_movement_product.rcp_qty) > 0")
    ->groupBy([
      'inventory_stock_movement.id',
      'inventory_stock_movement.code',
      'inventory_stock_movement.date',
      'inventory_stock_movement.type',
      // 'crm_contact.name',
      'setting_company.name',
      'inventory_stock_movement.destination',
      // 'inventory_warehouse.name'
    ]);
// dd($query->get()->toArray());
    $dg = Datagrid::source($query);
    $dg->title('Barang Masuk');
    $dg->filter('keyword', function($query, $value){
      if($value != '')
          return $query->where('inventory_stock_movement.code', 'ilike', '%'.$value.'%');

      return $query;
    });

    $dg->add('code', 'No. Pengajuan', false);
    $dg->add('date', 'Tanggal', false)->render(function($data){
      return Carbon::parse($data['date'])->format('d/m/Y');
    });
    $dg->add('supplier', 'Pemasok', false)->render(function($data){
      return ($data['type'] == 'TF') ? $data['company_source'] : $data['supplier'];
    });

    $dg->add('qty', 'Jumlah', false);
    // $dg->add('warehouse_name', 'Tujuan', false);
    $datagrid = $dg->build();

    return response()->json($datagrid);
  }

  public function getDataIncoming($id, $modifierFn = null){

    $query = InventoryStockMovementProduct::select([
      'inventory_stock_movement_product.id AS reference',
      'inventory_stock_movement_product.product',
      'inventory_stock_movement_product.uom',
      'product_product.attribute',
      'product_master.name',
      'product_uom.id AS uom',
      'product_uom.name AS uom_name',
    ])
    ->selectRaw('(inventory_stock_movement_product.qty - inventory_stock_movement_product.rcp_qty) AS qty')
    ->join('inventory_stock_movement', 'inventory_stock_movement.id', '=', 'inventory_stock_movement_product.stock_movement')
    ->join('product_product', 'product_product.id', '=', 'inventory_stock_movement_product.product')
    ->join('product_master', 'product_master.id', '=', 'product_product.product')
    ->join('product_uom', 'product_uom.id', '=', 'inventory_stock_movement_product.uom')
    ->where([
      'inventory_stock_movement.id' => $id,
      'inventory_stock_movement.company_destination' => Helper::currentCompany(),
    ])
    ->where(function($q){
      $q->where(function($q){
        $q->where('inventory_stock_movement.type', 'RN')
        ->whereIn('inventory_stock_movement.status', ['3RR', '5RR']);
        return $q;
      });
      return $q;
    })
    ->whereRaw("(inventory_stock_movement_product.qty - inventory_stock_movement_product.rcp_qty) > 0");

    if (is_a($modifierFn, '\Closure')) {
      $query = call_user_func_array($modifierFn, [$query]);
    }

    $data = $query->get()->toArray();
    $data = ProductProduct::renderProductAttribute($data);
    if(count($data) > 0){
      foreach($data as $key => $val){
        $data[$key]['optionUom'] = [
          ['id' => $val['uom'], 'text' => $val['uom_name']]
        ];
      }
    }

    return $data;
  }

  public function dataIncoming($id){
    $stockMovement = InventoryStockMovement::find($id);
    if(empty($stockMovement)){
      return;
    }

    $data = $this->getDataIncoming($id);

    $response = [
      'data' => $data,
      'optionWarehouse' => InventoryWarehouseLocation::getOption($stockMovement->source)
    ];

    return response()->json($response);
  }

  public function getDataComment($id){
    $dataLog = InventoryStockMovementLog::getData($id, 'ET', 'External Transfer');
    $dataComment = InventoryStockMovementComment::getData($id);
    $data = collect($dataComment);
    $data = $data->merge($dataLog);

    return $data->all();
  }

}
