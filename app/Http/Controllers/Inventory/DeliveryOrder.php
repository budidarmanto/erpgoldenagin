<?php

namespace App\Http\Controllers\Inventory;

use App\Models\CrmContact;
use App\Models\CrmContactDetail;
use App\Models\InventoryWarehouse;
use App\Models\InventoryWarehouseLocation;
use App\Models\SettingCompany;
use App\Models\SettingUser;
use App\Models\SettingWorkflow;
use App\Models\ProductProduct;
use App\Models\InventoryStockMovement;
use App\Models\InventoryStockMovementProduct;
use App\Models\InventoryStockMovementLog;
use App\Models\InventoryStockMovementComment;
use App\Models\SettingCurrency;
use App\Models\SettingTax;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Params;
use Helper;
use Auth;
use Carbon\Carbon;

class DeliveryOrder extends Controller{

  public $company;

  public function index() {

    $query = InventoryStockMovement::select([
      'inventory_stock_movement.*',
      'ism.code AS reference_code',
      'ism.type AS reference_type'
    ])
    ->leftJoin('inventory_stock_movement as ism', 'ism.id', '=', 'inventory_stock_movement.reference')
    ->where([
      'inventory_stock_movement.company' => Helper::currentCompany(),
      'inventory_stock_movement.type' => 'DO'
    ])->orderBy('inventory_stock_movement.code', 'DESC');

    $dg = Datagrid::source($query);
    $dg->title('Pengiriman');
    $dg->filter('keyword', function($query, $value){
      if($value != ''){
        return $query->where(function($q) use ($value) {
          $q->where('inventory_stock_movement.code', 'ilike', '%'.$value.'%')->orWhere('inventory_stock_movement.description', 'ilike', '%'.$value.'%');
          return $q;
        });
      }

      return $query;
    });
    $dg->filter('date', function($query, $value){
      if($value != '')
          return $query->whereDate('inventory_stock_movement.date', $value);

      return $query;
    });

    $dg->filter('workflow', function($query, $value){
      if($value != '')
          return $query->where('inventory_stock_movement.status', $value);

      return $query;
    });

    $dg->add('code', 'No. Pengiriman', true);
    $dg->add('date', 'Tanggal', true)->render(function($data){
      return Carbon::parse($data['date'])->format('d/m/Y');
    });
    $dg->add('description', 'Keterangan');
    $dg->add('status','Status');
    $datagrid = $dg->build();
    $datagrid['optionWorkflow'] = SettingWorkflow::getOption('DO');
    return response()->json($datagrid);
  }

  public function create($withOrWithoutPO){
    $withSo = ($withOrWithoutPO == 'with-so') ? true : false;

    $form = $this->anyForm(new InventoryStockMovement());

    $defaultWorkflow = SettingWorkflow::workflowDefault('DO');

    $form->pre(function($data) use($defaultWorkflow){
      $isCurrency = (Params::get('MULTI_CURRENCY') == 1) ? true : false;
      $defaultCurrency = SettingCurrency::getDefaultCurrency();
      if(!$isCurrency){
        $data['currency'] = $defaultCurrency;
      }
      $data['date'] = date('Y-m-d');
      $data['code'] = InventoryStockMovement::generateId('DO');
      $data['company'] = Helper::currentCompany();
      $data['type'] = 'DO';
      $data['status'] = $defaultWorkflow['code'];
      return $data;
    });
    $dataForm = $form->build();

    if($form->hasRequest()){

      $this->processDataAfterSave($form, 'create');
      InventoryStockMovementLog::addLog($form->model->id, $defaultWorkflow['code']);



      if($form->saved()){
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }

    $dataForm = $this->getDataResponse($dataForm);
    $dataForm['data']['code'] = InventoryStockMovement::generateId('DO');
    $dataForm['data']['diajukan'] = Auth::user()->fullname;
    $dataForm['data']['date'] = Carbon::now()->format('d/m/Y');
    $dataForm['data']['reference'] = '';
    $dataForm['withSo'] = $withSo;

    $product = Input::get('q');
    if(!empty($product) && $withSo){
      $product = explode('.', $product);
      $poProduct = InventoryStockMovementProduct::find($product[0]);
      $po = InventoryStockMovement::find($poProduct->stock_movement);
      if(!empty($po)){
        $dataForm['data']['reference'] = $po->id;
        $dataForm['data']['reference_code'] = $po->code;
        $dataForm['dataProduct'] = $this->getDataOutgoing($po->id, function($query) use ($product){
          return $query->whereIn('inventory_stock_movement_product.id', $product);
        });

        $dataForm['dataProduct'] = collect($dataForm['dataProduct'])->map(function($data){
          $data['maxQty'] = $data['qty'];
          return $data;
        });
      }
    }

    return response()->json($dataForm);
  }

  public function modify($id = null) {

    $withSo = false;
    $stockMovement = InventoryStockMovement::find($id);
    if(!empty($stockMovement->reference)){
      $stockMovementReference = InventoryStockMovement::find($stockMovement->reference);
      if(!empty($stockMovementReference)){
        $withSo = true;
      }
    }

    $form = $this->anyForm($stockMovement);
    $form->pre(function($data) use ($id){
      unset($data['date']);

      // Reset data qty
      $stockMovementProduct = InventoryStockMovementProduct::where('stock_movement', $id)->get();
      if(count($stockMovementProduct) > 0){
        foreach($stockMovementProduct as $val){
          $reference = InventoryStockMovementProduct::find($val['reference']);
          if(!empty($reference)){
            $reference->do_qty -= $val->qty;
            $reference->save();
          }
        }
      }
      return $data;
    });
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){

        $this->processDataAfterSave($form, 'modify');

        return response()->json([
          'status' => true,
          'dataProduct' => $this->getDataProduct($id, $stockMovement->reference)
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }

    $dataForm['data']['date'] = Carbon::parse($dataForm['data']['date'])->format('d/m/Y');
    $user = SettingUser::where('username', $stockMovement->created_by)->first();
    $dataForm['data']['diajukan'] = (!empty($user)) ? $user->fullname : '';
    $dataForm = $this->getDataResponse($dataForm);

    $dataProduct = $this->getDataProduct($id, $stockMovement->reference);
    $dataProduct = collect($dataProduct)->map(function($dt){
      $dt['maxQty'] = $dt['qty'] + $dt['maxQty'];
      return $dt;
    })->all();

    $dataForm['dataProduct'] = $dataProduct;
    $dataForm['dataComment'] = $this->getDataComment($id);
    $dataForm['withSo'] = $withSo;
    if($withSo){
      $dataForm['data']['reference_code'] = $stockMovementReference->code;
      $dataForm['data']['reference_type'] = $stockMovementReference->type;
    }

    return response()->json($dataForm);
  }

  public function delete(){

    $id = Input::get('id');
    InventoryStockMovement::whereIn('id', $id)->delete();

    return response()->json([
      'status' => true
    ]);
  }

  public function changeStatus($id, $status){
    $stockMovement = InventoryStockMovement::find($id);
    if(!empty($stockMovement)){
      if($stockMovement->status != $status) {
        $stockMovement->status = $status;
        $stockMovement->save();        
        InventoryStockMovementLog::addLog($id, $status);
      }

      $reference = InventoryStockMovement::find($stockMovement->reference);


      if($status == '2DC'){
        $receiptProduct = InventoryStockMovementProduct::where('stock_movement', $stockMovement->id)->get();
        if(count($receiptProduct) > 0){
          foreach($receiptProduct as $val){
            if(!empty($val->reference)){
                $salesProduct = InventoryStockMovementProduct::find($val->reference);
                if(!empty($salesProduct)){
                  $salesProduct->do_qty -= $val->qty;
                  $salesProduct->save();
                }
            }
          }
        }
        if(!empty($reference) && $reference->type == 'SO'){
            $this->processStatusSoDelivery($reference);
        }
      }
      if($status == '4DD'){
        if(!empty($stockMovement->reference) && $reference->type == 'SO'){
            $this->processStatusSoDelivery($reference);
        }
      }

      if($status == '3DO' && $reference->type == 'ET'){
          $defaultWorkflow = SettingWorkflow::workflowDefault('TF');

          $data = $reference->toArray();
          $data['id'] = null;
          $isCurrency = (Params::get('MULTI_CURRENCY') == 1) ? true : false;
          $defaultCurrency = SettingCurrency::getDefaultCurrency();
          if(!$isCurrency){
            $data['currency'] = $defaultCurrency;
          }
          $data['date'] = date('Y-m-d');
          $data['code'] = InventoryStockMovement::generateId('TF', $reference->company_destination);
          $data['company'] = $reference->company_destination;
          $data['company_destination'] = $reference->company;
          $data['type'] = 'TF';
          $data['source'] = null;
          $data['status'] = $defaultWorkflow['code'];
          $data['reference'] = $stockMovement->id;
          // dd($data);
          $newStockMovement = InventoryStockMovement::create($data);

          $dataProduct = InventoryStockMovementProduct::where('stock_movement', $id)->get()->toArray();
          if(!empty($dataProduct)){
            foreach($dataProduct as $val){
              $val['id'] = '';
              $val['stock_movement'] = $newStockMovement->id;
              $val['status'] = 0;
              $val['expected_date'] = $reference->expected_date;
              InventoryStockMovementProduct::create($val);
            }
          }
          InventoryStockMovementLog::addLog($newStockMovement->id, $defaultWorkflow['code']);
      }

      return response()->json([
        'status' => true,
        'dataComment' => $this->getDataComment($id)
      ]);
    }
  }

  public function anyForm($source){
    $form = FormBuilder::source($source);
    $form->title('Pengiriman');
    $form->add('code', 'No. Pengiriman', 'text')->rule('required|max:25')->attributes([
      'readonly' => true
    ]);
    $form->add('reference_code', 'No. Penjualan', 'text')->attributes([
      'readonly' => true,
      'required' => true // required only in javascript
    ]);
    $form->add('date', 'Tanggal', 'text')->attributes([
      'readonly' => true
    ]);
    $form->add('description', 'Keterangan', 'textarea');
    $form->add('diajukan', 'Diterima Oleh', 'text')->attributes([
      'readonly' => true
    ]);
    $form->add('source', 'Asal', 'select')->attributes([
      'ng-options' => 'item.id as item.text for item in response.optionWarehouse',
    ])->rule('required');

    return $form;
  }

  public function getDataResponse($data){
    $data['optionCurrency'] = SettingCurrency::getOption();
    $data['defaultCurrency'] = SettingCurrency::getDefaultCurrency();
    $data['optionWarehouse'] = InventoryWarehouseLocation::getOption();
    $data['optionTax'] = SettingTax::getOption();
    $data['dataProduct'] = [];
    $data['dataComment'] = [];
    $data['optionWorkflow'] = SettingWorkflow::getOption('DO');
    return $data;
  }

  public function getDataProduct($id, $reference){
    $dataProduct = InventoryStockMovementProduct::getDataProduct($id);

    $dataOutgoing = $this->getDataOutgoing($reference);


    $data = [];
    if(count($dataProduct) > 0){
      foreach($dataProduct as $val){

        $incomming = collect($dataOutgoing)->filter(function($value) use ($val){
          return $value['reference'] == $val['reference'];
        })->first();

        $maxQty = (!empty($incomming)) ? $incomming['qty'] : 0;

        $data[] = [
          'id' => $val['id'],
          'reference' => $val['reference'],
          'product' => $val['product'],
          'name' => $val['name'],
          'qty' => $val['qty'],
          'maxQty' => $maxQty,
          'uom' => $val['uom'],
          'optionUom' => $val['optionUom'],
        ];
      }
    }
    return $data;
  }

  public function processDataAfterSave($form = null, $action = null){
    if(empty($form)){
      return;
    }


    // Save Movement Product
    $listId = [];
    if(!empty($form->dataPost['dataProduct'])){
      foreach($form->dataPost['dataProduct'] as $val){
        $stockMovementProduct = new InventoryStockMovementProduct();
        $val['stock_movement'] = $form->model->id;
        if(!empty($val['id'])){
          $stockMovementProduct = InventoryStockMovementProduct::find($val['id']);
        }

        $stockMovementProduct->fill($val);
        $stockMovementProduct->save();
        $listId[] = $stockMovementProduct->id;

        if(!empty($val['reference'])){
          $reference = InventoryStockMovementProduct::find($val['reference']);
          if(!empty($reference)){
            $reference->do_qty += $val['qty'];
            $reference->save();
          }
        }
      }
    }
    InventoryStockMovementProduct::where('stock_movement', $form->model->id)->whereNotIn('id', $listId)->delete();
  }

  public function dataReference(){
    //Estimasi No Penjualan Tanggal Pelanggan Produk Jumlah Satuan Asal Tindakan

    $query = InventoryStockMovementProduct::select([
      'inventory_stock_movement.id',
      'inventory_stock_movement.code',
      'inventory_stock_movement.date',
      'crm_contact.name AS customer',
      'inventory_stock_movement.source',
      'inventory_warehouse.name AS warehouse_name'
    ])
    ->selectRaw('SUM(inventory_stock_movement_product.qty - inventory_stock_movement_product.do_qty) AS qty')
    ->join('inventory_stock_movement', 'inventory_stock_movement.id', '=', 'inventory_stock_movement_product.stock_movement')
    ->leftJoin('crm_contact', 'crm_contact.id', '=', 'inventory_stock_movement.contact')
    ->join('inventory_warehouse', 'inventory_warehouse.id', '=', 'inventory_stock_movement.source')
    ->where([
      'inventory_stock_movement.company' => Helper::currentCompany(),
    ])
    ->where(function($q){
      $q->where(function($q){
        $q->where([
          'inventory_stock_movement.type' => 'SO',
          'inventory_stock_movement.status' => '3SO',
        ]);
        return $q;
      })->orWhere(function($q){
        $q->where([
          'inventory_stock_movement.type' => 'ET',
          'inventory_stock_movement.status' => '3ED',
        ]);
        return $q;
      });
      return $q;
    })
    ->whereRaw("(inventory_stock_movement_product.qty - inventory_stock_movement_product.do_qty) > 0")
    ->groupBy([
      'inventory_stock_movement.id',
      'inventory_stock_movement.code',
      'inventory_stock_movement.date',
      'crm_contact.name',
      'inventory_stock_movement.source',
      'inventory_warehouse.name'
    ]);
// dd($query->get()->toArray());
    $dg = Datagrid::source($query);
    $dg->title('Barang Masuk');
    $dg->filter('keyword', function($query, $value){
      if($value != '')
          return $query->where('inventory_stock_movement.code', 'ilike', '%'.$value.'%');

      return $query;
    });

    $dg->add('code', 'No. Penjualan', false);
    $dg->add('date', 'Tanggal', false)->render(function($data){
      return Carbon::parse($data['date'])->format('d/m/Y');
    });
    $dg->add('customer', 'Pelanggan', false);

    $dg->add('qty', 'Jumlah', false);
    $dg->add('warehouse_name', 'Asal', false);
    $datagrid = $dg->build();

    return response()->json($datagrid);
  }

  public function getDataOutgoing($id, $modifierFn = null){

    $query = InventoryStockMovementProduct::select([
      'inventory_stock_movement_product.id AS reference',
      'inventory_stock_movement_product.product',
      'inventory_stock_movement_product.uom',
      'product_product.attribute',
      'product_master.name',
      'product_uom.id AS uom',
      'product_uom.name AS uom_name',
    ])
    ->selectRaw('(inventory_stock_movement_product.qty - inventory_stock_movement_product.do_qty) AS qty')
    ->join('inventory_stock_movement', 'inventory_stock_movement.id', '=', 'inventory_stock_movement_product.stock_movement')
    ->join('product_product', 'product_product.id', '=', 'inventory_stock_movement_product.product')
    ->join('product_master', 'product_master.id', '=', 'product_product.product')
    ->join('product_uom', 'product_uom.id', '=', 'inventory_stock_movement_product.uom')
    ->where([
      'inventory_stock_movement.id' => $id,
      'inventory_stock_movement.company' => Helper::currentCompany(),
    ])
    ->where(function($q){
      $q->where(function($q){
        $q->where([
          'inventory_stock_movement.type' => 'SO',
          'inventory_stock_movement.status' => '3SO',
        ]);
        return $q;
      })->orWhere(function($q){
        $q->where([
          'inventory_stock_movement.type' => 'ET',
          'inventory_stock_movement.status' => '3ED',
        ]);
        return $q;
      });
      return $q;
    })
    ->whereRaw("(inventory_stock_movement_product.qty - inventory_stock_movement_product.do_qty) > 0");

    if (is_a($modifierFn, '\Closure')) {
      $query = call_user_func_array($modifierFn, [$query]);
    }

    $data = $query->get()->toArray();
    $data = ProductProduct::renderProductAttribute($data);
    if(count($data) > 0){
      foreach($data as $key => $val){
        $data[$key]['optionUom'] = [
          ['id' => $val['uom'], 'text' => $val['uom_name']]
        ];
      }
    }

    return $data;
  }

  // Called after select No. PO
  // Params PoID
  public function dataOutgoing($id){
    $stockMovement = InventoryStockMovement::find($id);
    if(empty($stockMovement)){
      return;
    }

    $data = $this->getDataOutgoing($id);

    $response = [
      'data' => $data,
      'optionWarehouse' => InventoryWarehouseLocation::getOption($stockMovement->source)
    ];

    return response()->json($response);
  }

  public function processStatusSoDelivery($sales){

    if(!empty($sales)){
      $salesProduct = InventoryStockMovementProduct::where('stock_movement', $sales->id)->get();
      $totalQty = 0;
      $totalReceipt = 0;
      if(count($salesProduct) > 0){
        foreach($salesProduct as $val){
            $totalQty += $val->qty;
            $totalReceipt += $val->do_qty;
        }
      }

      $status = '3SO';
      if($totalQty == $totalReceipt){
        // Product Delivered All
        $status = '4CP';
      }
      // Just For Incasae if workflow changed in future
      /*
      else if($totalQty != $totalReceipt){
        if($totalReceipt == 0){
          // All product not recieved yet
          $status = '3PO';
        }else{
          // Product Already partially accepted
          $status = '4DO';
        }
      }
      */

      if($sales->status != $status){
          $sales->status = $status;
          $sales->save();
          InventoryStockMovementLog::addLog($sales->id, $status);
      }

    }

  }

  public function getDataComment($id){
    $dataLog = InventoryStockMovementLog::getData($id, 'DO', 'Delivery Order');
    $dataComment = InventoryStockMovementComment::getData($id);
    $data = collect($dataComment);
    $data = $data->merge($dataLog);

    return $data->all();
  }

}
