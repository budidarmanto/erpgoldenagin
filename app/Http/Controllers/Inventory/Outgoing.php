<?php

namespace App\Http\Controllers\Inventory;

use App\Models\CrmContact;
use App\Models\CrmContactDetail;
use App\Models\InventoryWarehouse;
use App\Models\SettingCompany;
use App\Models\SettingUser;
use App\Models\SettingWorkflow;
use App\Models\ProductProduct;
use App\Models\InventoryStockMovement;
use App\Models\InventoryStockMovementProduct;
use App\Models\InventoryStockMovementLog;
use App\Models\SettingCurrency;
use App\Models\SettingTax;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Params;
use Helper;
use Auth;
use Carbon\Carbon;

class Outgoing extends Controller{

  public function index($soId = null){

    //Estimasi No Pembelian Tanggal Pemasok Produk Jumlah Satuan Tujuan Tindakan
    $query = InventoryStockMovementProduct::select([
      'inventory_stock_movement_product.id',
      'inventory_stock_movement.id AS so',
      // 'inventory_stock_movement_product.expected_date',
      'inventory_stock_movement.expected_date',
      'inventory_stock_movement.code',
      'inventory_stock_movement.date',
      'crm_contact.name AS customer',
      'inventory_stock_movement_product.product',
      'inventory_stock_movement_product.uom',
      'inventory_stock_movement.source',
      'product_master.name AS product_name',
      'product_uom.name AS uom_name',
      'inventory_warehouse.name AS warehouse_name',
    ])
    ->selectRaw('(inventory_stock_movement_product.qty - inventory_stock_movement_product.do_qty) AS qty')
    ->join('inventory_stock_movement', 'inventory_stock_movement.id', '=', 'inventory_stock_movement_product.stock_movement')
    ->leftJoin('crm_contact', 'crm_contact.id', '=', 'inventory_stock_movement.contact')
    ->join('product_product', 'product_product.id', '=', 'inventory_stock_movement_product.product')
    ->join('product_master', 'product_master.id', '=', 'product_product.product')
    ->join('product_uom', 'product_uom.id', '=', 'inventory_stock_movement_product.uom')
    ->leftJoin('inventory_warehouse', 'inventory_warehouse.id', '=', 'inventory_stock_movement.source')
    ->where([
      'inventory_stock_movement.company' => Helper::currentCompany(),
    ])
    ->where(function($q){
      $q->where(function($q){
        $q->where([
          'inventory_stock_movement.type' => 'SO',
          'inventory_stock_movement.status' => '3SO',
        ]);
        return $q;
      })->orWhere(function($q){
        $q->where([
          'inventory_stock_movement.type' => 'ET',
          'inventory_stock_movement.status' => '3ED',
        ]);
        return $q;
      })->orWhere(function($q){
        $q->where([
          'inventory_stock_movement.type' => 'RT',
          'inventory_stock_movement.status' => '3RR',
        ]);
        return $q;
      });
      return $q;
    })
    ->whereRaw("(inventory_stock_movement_product.qty - inventory_stock_movement_product.do_qty) > 0");
    if(!empty($soId)){
      $query->where('inventory_stock_movement.id', $soId);
    }
    $query->orderBy('inventory_stock_movement.code', 'DESC');
// dd($query->get()->toArray());
    $dg = Datagrid::source($query);
    $dg->title('Barang Keluar');
    $dg->filter('keyword', function($query, $value){
      if($value != ''){
        $query->where(function($q) use ($value){
          return $q->where('inventory_stock_movement.code', 'ilike', '%'.$value.'%')->orWhere('product_master.name', 'ilike', '%'.$value.'%');
        });
      }

      return $query;
    });
    $dg->filter('date', function($query, $value){
      if($value != '')
          return $query->whereDate('inventory_stock_movement.expected_date', $value);

      return $query;
    });

    $dg->add('product_name', 'Produk', true);
    $dg->add('expected_date', 'Estimasi Pengiriman', false)->render(function($data){
      return Carbon::parse($data['expected_date'])->format('d/m/Y');
    });
    $dg->add('code', 'No. Penjualan', false);
    $dg->add('date', 'Tanggal', false)->render(function($data){
      return Carbon::parse($data['date'])->format('d/m/Y');
    });
    $dg->add('customer', 'Pelanggan', false);

    $dg->add('qty', 'Jumlah', false);
    $dg->add('uom_name', 'Satuan Ukuran', false);
    $dg->add('warehouse_name', 'Asal', false);
    $datagrid = $dg->build();


    $listProductId = collect($datagrid['data'])->pluck('product')->unique()->toArray();
    if(!empty($listProductId)){
      $dataProduct = ProductProduct::getData(function($query) use ($listProductId) {
        $query->whereIn('product_product.id', $listProductId);
        return $query;
      });
      $dataProduct = ProductProduct::getRelatedData($dataProduct);
    }

    foreach($datagrid['data'] as $key => $val){
      $product = collect($dataProduct)->filter(function($dt) use($val){
        return ($dt['id'] == $val['product']);
      })->first();
      if(!empty($product)){
          $datagrid['data'][$key]['product_name'] = $product['name'];
      }
      $datagrid['data'][$key]['optionUom'] = [
        ['id' => $val['uom'], 'text' => $val['uom_name']]
      ];
    }

    return response()->json($datagrid);

  }

}
