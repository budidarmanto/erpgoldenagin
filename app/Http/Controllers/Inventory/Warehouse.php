<?php

namespace App\Http\Controllers\Inventory;

use App\Models\InventoryWarehouse;
use App\Models\InventoryWarehouseLocation;
use App\Models\SettingCompany;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Params;

class Warehouse extends Controller
{

  public function index() {

    $dg = Datagrid::source(InventoryWarehouse::query());
    $dg->title('Gudang');
    $dg->filter('keyword', function($query, $value){
      if($value != '')
          return $query->where('name', 'ilike', '%'.$value.'%');

      return $query;
    });
    $dg->add('code', 'Kode');
    $dg->add('name', 'Nama');
    $dg->add('address', 'Alamat');
    $dg->add('active', 'Aktif')->options([1 => 'Aktif', 0 => 'Tidak Aktif']);
    $datagrid = $dg->build();
    return response()->json($datagrid);
  }

  public function create(){
    $form = $this->anyForm(new InventoryWarehouse());
    $form->pre(function($data){
      $data['company'] = (isset($data['company'])) ? json_encode($data['company']) : null;
      return $data;
    });
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){

        //save warehouse detail
        if(!empty($form->dataPost['dataLocation'])){
          foreach($form->dataPost['dataLocation'] as $val){
            $uom = new InventoryWarehouseLocation();
            $val['warehouse'] = $form->model->id;
            $uom->fill($val);
            $uom->save();
          }
        }

        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }

    $dataForm['dataLocation'] = [];
    $dataForm['optionCountry'] = Params::getOption('OPTION_COUNTRY');
    $dataForm['optionCompany'] = SettingCompany::getOption();
    return response()->json($dataForm);
  }

  public function modify($id = null) {

    $form = $this->anyForm(InventoryWarehouse::find($id));
    $form->pre(function($data){
      $data['company'] = (isset($data['company'])) ? json_encode($data['company']) : null;
      return $data;
    });
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){

        //save uom detail
        $listId = [];
        if(!empty($form->dataPost['dataLocation'])){
          foreach($form->dataPost['dataLocation'] as $val){
            $uom = new InventoryWarehouseLocation();
            $val['warehouse'] = $form->model->id;
            if(!empty($val['id'])){
              $uom = InventoryWarehouseLocation::find($val['id']);
            }else{
              $warehouseLocation = InventoryWarehouseLocation::where([
                  'code' => $val['code'],
                  'warehouse' => $id,
              ])->first();
              if(!empty($warehouseLocation)){
                return response()->json([
                  'status' => false,
                  'messages' => "Kode lokasi sudah terdaftar"
                ]);
              }
            }
            $uom->fill($val);
            $uom->save();
            $listId[] = $uom->id;
          }
        }
        InventoryWarehouseLocation::where('warehouse', $id)->whereNotIn('id', $listId)->delete();

        $dataLocation = InventoryWarehouseLocation::select(['id', 'code','name', 'active'])->where('warehouse', $id)->get()->toArray();

        return response()->json([
          'status' => true,
          'dataLocation' => $dataLocation
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }

    $dataLocation = InventoryWarehouseLocation::select(['id', 'code','name', 'active'])->where('warehouse', $id)->get()->toArray();
    $dataForm['dataLocation'] = $dataLocation;
    $dataForm['data']['company'] = json_decode($dataForm['data']['company']);
    $dataForm['optionCountry'] = Params::getOption('OPTION_COUNTRY');
    $dataForm['optionCompany'] = SettingCompany::getOption();
    return response()->json($dataForm);
  }

  public function delete(){

    $id = Input::get('id');
    InventoryWarehouse::whereIn('id', $id)->delete();

    return response()->json([
      'status' => true
    ]);
  }

  public function anyForm($source){
    $form = FormBuilder::source($source);
    $form->title('Gudang');
    $form->add('code', 'Kode', 'text')->rule('required|max:50');
    $form->add('name', 'Nama', 'text')->rule('required|max:100');
    $form->add('address', 'Alamat', 'textarea');
    $form->add('city', 'Kota', 'text')->rule('max:50');
    $form->add('postal_code', 'Kode Pos','text')->rule('max:5');
    $form->add('state', 'Provinsi', 'text')->rule('max:50');
    $form->add('country', 'Negara', 'select')->attributes([
      'ng-options' => 'item.id as item.text for item in response.optionCountry',
      'data-live-search' => true
    ]);
    $form->add('phone', 'Telepon', 'text')->rule('max:50');
    $form->add('fax', 'Fax', 'text')->rule('max:50');
    $form->add('company', 'Perusahaan', 'multiple')->attributes([
      'ng-options' => 'item.id as item.text for item in response.optionCompany'
    ]);
    return $form;
  }

}
