<?php

namespace App\Http\Controllers\Inventory;
use App\Models\ProductMaster;
use App\Models\ProductCategory;
use App\Models\ProductUomCategory;
use App\Models\ProductUom;
use App\Models\ProductSupplier;
use App\Models\ProductAttribute;
use App\Models\ProductAttributeValue;
use App\Models\ProductProduct;
use App\Models\ProductVariant;
use App\Models\ProductBom;
use App\Models\ProductAlternative;
use App\Models\InventoryWarehouse;
use App\Models\InventoryWarehouseLocation;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Params;
use Excel;
use Auth;
use Helper;
use PDF;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class Stock extends Controller
{
  public function index($productId = null){
	$today = date('Y-m-d'); ///----
    
	$now = strtotime('now');
	//cron stock jalan 1 am
	$one_am = strtotime(date('Y-m-d 01:00:00'));
	if($now <= $one_am){
		//jika kurang dari 1 am, ambil stock kemarin
		//karena stock hari ini baru ada setelah 1 am
		$today = date('Y-m-d', strtotime('yesterday'));
    }

    //delete later
    // $today = Carbon::parse(date('Y-m-d'));
    // $today->addDays(-2);
    // $today = $today->toDateTimeString();

	$nilai_param = 0;
	$listCategory = ProductCategory::all()->toArray();
	$listCategory = Helper::toFlatOption('id', 'name', $listCategory);
	$keyword = Input::get('keyword');

	$get_category = Input::get('category');
	if($get_category) {
		$category = ProductCategory::find($get_category);
		$listCategory = $category->getDescendantsAndSelf()->pluck('id')->toArray();
	} else {
		$category = null;
		$listCategory = [];
	}

	$type = Input::get('type');
	
	$optionWarehouse = InventoryWarehouse::getOption();
	
        if(count($optionWarehouse) > 0){
            foreach($optionWarehouse as $key => $val){
                $warehouseLocation  = InventoryWarehouseLocation::where('warehouse', $val['id'])->where('active', true)->get()->toArray();
                $optionWarehouseLocation = Helper::toOption('id', 'name', $warehouseLocation);
                $optionWarehouse[$key]['warehouseLocation'] = $optionWarehouseLocation;
            }
        }

        $warehouse = '';
        $status = false;
        $warehouseLocation = Input::get('warehouse_location');
        $warehouse = Input::get('warehouse');

        if(!empty($warehouse)){
            $warehouseName = InventoryWarehouse::where('id', $warehouse)->where('active', true)->get()->toArray();
            $warehouseName = $warehouseName[0]['name'];
        }
        else{
            $warehouseName = '';
        }
        if(!empty($warehouseLocation)){
            $warehouseLocationName = InventoryWarehouseLocation::where('id', $warehouseLocation)->where('active', true)->get()->toArray();
            $warehouseLocationName = $warehouseLocationName[0]['name'];
        }
        else{
            $warehouseLocationName = '';
        }
		
		
        $listWarehouseLocation = [];
        if(!empty($warehouseLocation)){
            $listWarehouseLocation[] =  $warehouseLocation;
        }
        else if(!empty($warehouse)){
            $dtWarehouseLocation = InventoryWarehouseLocation::where('warehouse', $warehouse)->where('active', true)->get();
            foreach($dtWarehouseLocation as $val){
                $listWarehouseLocation[] =  $val->id;
            }
        }
        else{
            $dtWarehouseLocation = InventoryWarehouseLocation::where('active', true)->get();
            foreach($dtWarehouseLocation as $val){
                $listWarehouseLocation[] =  $val->id;
            }
        }

    $arrListWarehouseLocation = $listWarehouseLocation;

	$listWarehouseLocation = array_map(function ($warehouse) {
		return '\''. $warehouse . '\'';
	}, $listWarehouseLocation);
    $strListWarehouseLocation = implode(',', $listWarehouseLocation);
    
    //DB::enableQueryLog();

        $query = ProductProduct::select(DB::raw(''
                    .'product_master.name AS name, '
                    .'product_master.code AS code,'
                    .'product_master.category AS category,'
                    .'product_category.name AS category_name,'
                    .'product_master.uom AS uom,'
                    .'product_master.revision AS revision,'
                    .'product_uom.uom_category AS uom_category,'
                    .'product_master.type AS type,'
                    .'product_master.barcode AS barcode,'
                    .'product_master.picture AS picture,'
                    .'product_master.sale_price AS sale_price,'
                    .'product_master.cost_price AS cost_price,'
                    .'product_master.pos AS pos,'
                    .'product_master.tax AS tax,'
                    .'product_master.discount AS discount,'
                    .'product_master.active AS active,'
                    .'product_product.id,'
                    .'product_master.id AS product_id,'
                    .'product_product.attribute,'
                    .'product_uom_default.id as uom,'
                    .'product_uom_default.name as uom_name,'
                    .'COALESCE(stock.stock,0) as stock,'
                    .'COALESCE(adjustment.qty_ad, 0) as adjustment,'
                    .'COALESCE(aa.qty_mit, 0) as minIternal,'
                    .'COALESCE(bb.qty_pit, 0) as plusInternal,'
                    .'COALESCE(stock.stock, 0) + COALESCE(adjustment.qty_ad, 0) - COALESCE(aa.qty_mit, 0) + COALESCE(bb.qty_pit, 0) as stock_akhir'
                ))
                ->leftJoin('product_master','product_master.id', '=' ,'product_product.product')
                ->leftJoin('product_category','product_category.id', '=' ,'product_master.category')
                ->leftJoin('product_uom',  'product_uom.id', '=', 'product_master.uom')
                ->leftJoin(DB::raw('(select * from product_uom where id in (select max(id) from product_uom where type =\'d\' and active = true group by uom_category)) as product_uom_default'), function ($join) {
                    $join->on('product_uom_default.uom_category', '=', 'product_uom.uom_category');
                })
                ->leftJoin(DB::raw('(select product,  COALESCE(sum(begin_qty), 0) as stock from inventory_stock where date = \'' . $today . '\'  and warehouse_location in (' . $strListWarehouseLocation . ')  group by product) as stock'), function ($join) {
                    $join->on('stock.product', '=', 'product_product.id');
                })
                ->leftJoin(DB::raw('(select inventory_stock_movement_product.product,inventory_stock_movement.company,
                    inventory_stock_movement.type,inventory_stock_movement.status,
                    COALESCE(sum(inventory_stock_movement_product.qty), 0) as sum_ad, COALESCE(sum(product_uom.ratio*inventory_stock_movement_product.qty), 0) as qty_ad
                    from inventory_stock_movement_product 
                    join inventory_stock_movement on inventory_stock_movement.id = inventory_stock_movement_product.stock_movement 
                    left join crm_contact on crm_contact.id = inventory_stock_movement.contact 
                    left join setting_company on setting_company.id = inventory_stock_movement.company_destination 
                    left join product_uom on inventory_stock_movement_product.uom = product_uom.id
                    join inventory_stock_movement_log on inventory_stock_movement_log.stock_movement = inventory_stock_movement.id and inventory_stock_movement_log.status = \'3AA\'
                    where inventory_stock_movement_log.created_at::date = \'' . $today . '\'
                    and inventory_stock_movement.source in (' . $strListWarehouseLocation . ')
                    group by inventory_stock_movement_product.product, inventory_stock_movement.company, inventory_stock_movement.type, inventory_stock_movement.status) as adjustment'), function ($join) {
                    $join->on('adjustment.product', '=', 'product_product.id');
                })
                ->leftJoin(DB::raw('(select inventory_stock_movement_product.product,inventory_stock_movement.company,
                    inventory_stock_movement.type,inventory_stock_movement.status,
                    COALESCE(sum(inventory_stock_movement_product.qty), 0) as sum_mit, COALESCE(sum(product_uom.ratio*inventory_stock_movement_product.qty), 0) as qty_mit 
                    from inventory_stock_movement_product 
                    join inventory_stock_movement on inventory_stock_movement.id = inventory_stock_movement_product.stock_movement 
                    left join crm_contact on crm_contact.id = inventory_stock_movement.contact 
                    left join setting_company on setting_company.id = inventory_stock_movement.company_destination 
                    left join product_uom on inventory_stock_movement_product.uom = product_uom.id
                    join inventory_stock_movement_log on inventory_stock_movement_log.stock_movement = inventory_stock_movement.id and inventory_stock_movement_log.status = \'3IT\'
                    where inventory_stock_movement_log.created_at::date = \'' . $today . '\'
                    and inventory_stock_movement.source in (' . $strListWarehouseLocation . ') and inventory_stock_movement.destination not in (' . $strListWarehouseLocation . ')
                    group by inventory_stock_movement_product.product, inventory_stock_movement.company, inventory_stock_movement.type, inventory_stock_movement.status) as aa'), function ($join) {
                    $join->on('aa.product', '=', 'product_product.id');
                })
                ->leftJoin(DB::raw('(select inventory_stock_movement_product.product,inventory_stock_movement.company,
                    inventory_stock_movement.type,inventory_stock_movement.status,
                    COALESCE(sum(inventory_stock_movement_product.qty), 0) as sum_pit, COALESCE(sum(product_uom.ratio*inventory_stock_movement_product.qty), 0) as qty_pit 
                    from inventory_stock_movement_product 
                    join inventory_stock_movement on inventory_stock_movement.id = inventory_stock_movement_product.stock_movement 
                    left join crm_contact on crm_contact.id = inventory_stock_movement.contact 
                    left join setting_company on setting_company.id = inventory_stock_movement.company_destination 
                    left join product_uom on inventory_stock_movement_product.uom = product_uom.id
                    join inventory_stock_movement_log on inventory_stock_movement_log.stock_movement = inventory_stock_movement.id and inventory_stock_movement_log.status = \'3IT\'
                    where inventory_stock_movement_log.created_at::date = \'' . $today . '\'
                    and inventory_stock_movement.destination in (' . $strListWarehouseLocation . ') and inventory_stock_movement.source not in (' . $strListWarehouseLocation . ')
                    group by inventory_stock_movement_product.product, inventory_stock_movement.company, inventory_stock_movement.type, inventory_stock_movement.status) as bb'), function ($join) {
                    $join->on('bb.product', '=', 'product_product.id');
                })
                ->where(DB::raw('COALESCE(stock.stock, 0) + COALESCE(adjustment.qty_ad, 0) - COALESCE(aa.qty_mit, 0) + COALESCE(bb.qty_pit, 0)'), '!=',
							$nilai_param);	
		
	if(!empty($productId)){
		$query->whereIn('product_product.id', explode('.', $productId));
	}

	$dg = Datagrid::source($query);
	$dg->title('Persediaan');
	
        $dg->filter('keyword', function($query, $value){
            if($value != ''){
                return $query->where(function($q) use ($value){
                    $q->where('product_master.name', 'ilike', '%'.$value.'%')->orWhere('product_master.code', 'ilike', '%'.$value.'%');
                        return $q;
                    });
                }
            return $query;
	});
	$dg->filter('category', function($query, $value) use($listCategory) {
            if($value != '' || $value != null){
                $query->whereIn('product_master.category', $listCategory);
            }
            return $query;
	});
	$dg->filter('type', function($query, $value){
            if($value != '' || $value != null)
            return $query->where('product_master.type', $value);
            return $query;
	});
	
        $dg->add('code', 'Kode', true);
	$dg->add('name', 'Nama', true);
	$dg->add('category', 'Kategori')->render(function($data) use ($listCategory){
					if(isset($listCategory[$data['category']])){
							return $listCategory[$data['category']];
					}
	});

    // $addSlashes = str_replace('?', "'?'", $query->toSql());
    // dd(vsprintf(str_replace('?', '%s', $addSlashes), $query->getBindings()));
    
    // dd($query->get()->toArray());

		$datagrid = $dg->build();

		$export = Input::get('export');
		$export_type = Input::get('format');
		if(!empty($export)){
			ini_set('max_execution_time', 3600);
			ini_set('memory_limit', '2048M');
			$data  = $datagrid['data'];

			if($export_type == 'excel') {
				Excel::create('Laporan Persediaan '.date('Y-m-d', strtotime($today)), function($excel) use ($data, $warehouseName, $warehouseLocationName, $today, $category, $type, $keyword) {

					$excel->sheet('Report', function($sheet) use ($data, $warehouseName, $warehouseLocationName, $today, $category, $type, $keyword) {
					
						$excelWarehouse = null;
						if($warehouseName || $warehouseLocationName) {
							$excelWarehouse = $warehouseName.' / '.$warehouseLocationName;
						}

						$rows = array();
						$rows[] = ['Laporan Persediaan', '', '', '', date('d/m/Y H:i:s')];
						if($excelWarehouse) {
                            $rows[] = ['Gudang: '.$excelWarehouse];
                            $sheet->mergeCells('A'.count($rows).':F'.count($rows));
                        }
                        if($category) {
                            $rows[] = ['Kategori: '.$category->name];
                            $sheet->mergeCells('A'.count($rows).':F'.count($rows));
						}
						if($type) {
                            $rows[] = ['Tipe: '.$type];
                            $sheet->mergeCells('A'.count($rows).':F'.count($rows));
						}
						$rows[] = [];

						$rows[] = ['No','Kode','Nama','Kategori','Satuan Ukuran','Stok'];
						$no = 1;
						foreach($data as $dt) {
							$rows[] = [$no++, $dt['code'], $dt['name'], $dt['category_name'], $dt['uom_name'],number_format($dt['stock_akhir'],2)];
						}

						$sheet->fromArray($rows, null, 'A1', true, false);

						$sheet->getStyle('A1')->applyFromArray(array(
						'font' => array(
							'bold' => true,
							'size' => 14
						)
						));

						$sheet->mergeCells('A1:D1');
						$sheet->mergeCells('E1:F1');
                        $sheet->cell('E1', function($cells) {
                            $cells->setAlignment('right');
                        });
					});
				})->export('xls');
			} else {
				$pdf = PDF::loadView('export.stock', compact('data', 'warehouseName', 'warehouseLocationName', 'today', 'type', 'category', 'keyword'));
				return $pdf->download('Laporan Persediaan '.date('Y-m-d', strtotime($today)).'.pdf');
				

				//return view('export.stock', compact('data', 'warehouseName', 'warehouseLocationName', 'today', 'type', 'category', 'keyword'));      
			}
			return;
		}
		
    $product_ids = [];
    foreach($datagrid['data'] as $dt) {
        $product_ids[] = $dt['id'];
    }

    $dataStock = ProductProduct::getInventoryDataStock($product_ids, $today, $arrListWarehouseLocation);

    foreach($datagrid['data'] as $key => $dt) {
        $datagrid['data'][$key]['dataStock'] = isset($dataStock[$dt['id']]) ? $dataStock[$dt['id']] : [];
    }
    //dd($dataStock);

//	$data = ProductProduct::renderProductAttribute($datagrid['data']);
//	dd($data);
//	$listProductId = collect($data)->pluck('id');
//	$dataStock = ProductProduct::getWarehouseLocationStock($listProductId, $warehouseLocation, $warehouse);
//	
//	foreach($data as $key => $val){
//            $id = $val['id'];
//            $stock = collect($dataStock)->where('product', $id)->first();
//            $data[$key]['uom'] = $stock['uom'];
//            $data[$key]['uom_name'] = $stock['uom_name'];
//	}
//	
//	$datagrid['data'] = $data;
        
	$category = ProductCategory::getOption();
	$datagrid['optionCategory'] = $category;
	$datagrid['optionType'] = Params::getOption('OPTION_PRODUCT_TYPE');
	$datagrid['optionWarehouse'] = $optionWarehouse;
	
	$datagrid['data'] = collect($datagrid['data'])->map(function($dt){
            $img = '<img src="https://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image"  style="width: 100%;"/>';
            if($dt['picture'] != null){
                $img = '<img src="img/product/'.$dt['picture'].'" style="width: 100%;" />';
            }
            $img = '<div class="img-wrapper" style="width: 100px;">'.$img.'</div>';
            $dt['picture'] = $img;
            return $dt;
        });
        
        return response()->json($datagrid);
    }
	
    function subArraysToString($ar, $sep = ', ') {
        $str = '';
        foreach ($ar as $val) {
            $str .= implode($sep, $val);
            $str .= $sep; // add separator between sub-arrays
        }
        $str = rtrim($str, $sep); // remove last separator
        return $str;
    }
}
