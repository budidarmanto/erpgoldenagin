<?php

namespace App\Http\Controllers\Inventory;

use App\Models\CrmContact;
use App\Models\CrmContactDetail;
use App\Models\InventoryWarehouse;
use App\Models\InventoryWarehouseLocation;
use App\Models\SettingCompany;
use App\Models\SettingUser;
use App\Models\SettingWorkflow;
use App\Models\ProductProduct;
use App\Models\InventoryStockMovement;
use App\Models\InventoryStockMovementProduct;
use App\Models\InventoryStockMovementLog;
use App\Models\InventoryStockMovementComment;
use App\Models\SettingCurrency;
use App\Models\SettingTax;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Params;
use Helper;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class InternalTransfer extends Controller{

  public $company;

  public function index() {
    //dd(Auth::user()->listWarehouseLocation());
    $lsWarehouse = Auth::user()->listWarehouseLocation();
    $fromDate = (empty(Input::get('datestart'))) ? null : Carbon::parse(Input::get('datestart'));
    $toDate = (empty(Input::get('dateend'))) ? null : Carbon::parse(Input::get('dateend'));

    $fromDate = (!empty($fromDate)) ? $fromDate->toDateTimeString() : null;
    $toDate = (!empty($toDate)) ? $toDate->toDateTimeString() : null;

    $query = InventoryStockMovement::select([
      'inventory_stock_movement.*',
      'setting_user.fullname AS created_user',
      'setting_user_changed.fullname as changed_user',
    ])
//    ->selectRaw('(
//        SELECT 
//            setting_user.fullname 
//        FROM inventory_stock_movement_log 
//        JOIN setting_user ON setting_user.username=inventory_stock_movement_log.created_by
//        WHERE stock_movement = inventory_stock_movement.id 
//        ORDER BY inventory_stock_movement_log.created_at DESC LIMIT 1
//    ) as changed_user')
    ->leftJoin('setting_user', 'setting_user.username', '=', 'inventory_stock_movement.created_by')
    ->leftJoin(DB::raw('
        (
            select
                distinct on (stock_movement)
                stock_movement,
                created_by
            from inventory_stock_movement_log
            order by stock_movement, created_at desc
        ) log
    '), 'log.stock_movement', '=', 'inventory_stock_movement.id')
    ->leftJoin('setting_user as setting_user_changed', 'setting_user_changed.username', '=', 'log.created_by')
    //->whereIn('inventory_stock_movement.source', Auth::user()->listWarehouseLocation())
    ->where([
      'inventory_stock_movement.company' => Helper::currentCompany(),
      'inventory_stock_movement.type' => 'IT',
    ])->orderBy('inventory_stock_movement.created_at', 'DESC');
    if($fromDate != null && $toDate != null){
      $query->whereBetween('inventory_stock_movement.date', [$fromDate, $toDate]);
    }

    $query->where(function($q) use ($lsWarehouse) {
      $q->whereIn('inventory_stock_movement.source', $lsWarehouse)
        ->orWhereIn('inventory_stock_movement.destination', $lsWarehouse);
      return $q;
    });
    $dg = Datagrid::source($query);
    $dg->title('Mutasi Internal');
    $dg->filter('keyword', function($query, $value){
      if($value != ''){
        return $query->where(function($q) use ($value) {
          $q->where('inventory_stock_movement.code', 'ilike', '%'.$value.'%')->orWhere('inventory_stock_movement.description', 'ilike', '%'.$value.'%');
          return $q;
        });
      }

      return $query;
    });
    $dg->filter('date', function($query, $value){
      if($value != '')
          return $query->whereDate('inventory_stock_movement.date', $value);

      return $query;
    });

    $dg->filter('workflow', function($query, $value){
      if($value != '')
          return $query->where('inventory_stock_movement.status', $value);

      return $query;
    });

    $dg->filter('source', function($query, $value){
      if($value != '')
          return $query->where('inventory_stock_movement.source', $value);

      return $query;
    });

    $dg->filter('destination', function($query, $value){
      if($value != '')
          return $query->where('inventory_stock_movement.destination', $value);

      return $query;
    });

    $optionWarehouseLocation = InventoryWarehouseLocation::getOption();
    // dd($optionWarehouseLocation);
    $dg->add('code', 'No. Penyesuaian', true);
    $dg->add('expected_date', 'Tanggal Pengiriman', true)->render(function($data){
      return Carbon::parse($data['date'])->format('d/m/Y');
    });
    $dg->add('source', 'Asal', true)->render(function($data) use($optionWarehouseLocation){
      $warehouse = collect($optionWarehouseLocation)->where('id', $data['source'])->first();
      if(!empty($warehouse)){
        return $warehouse['text'];
      }
      return '';
    });
    $dg->add('destination', 'Tujuan', true)->render(function($data) use($optionWarehouseLocation){
      $warehouse = collect($optionWarehouseLocation)->where('id', $data['destination'])->first();
      if(!empty($warehouse)){
        return $warehouse['text'];
      }
      return '';
    });

    $dg->add('description', 'Description');
    $dg->add('status','Status');
    $dg->add('created_user','Dibuat Oleh');
    $dg->add('changed_user','Diubah Oleh');
    $datagrid = $dg->build();
    
    $datagrid['optionWorkflow'] = SettingWorkflow::getOption('IT');
    $datagrid['optionWarehouseLocation'] = $optionWarehouseLocation;
    //dd(SettingWorkflow::getOption('IT'));
    return response()->json($datagrid);
  }

  public function create(){

    $form = $this->anyForm(new InventoryStockMovement());
    $defaultWorkflow = SettingWorkflow::workflowDefault('IT');

    $form->pre(function($data) use($defaultWorkflow){
      $isCurrency = (Params::get('MULTI_CURRENCY') == 1) ? true : false;
      $defaultCurrency = SettingCurrency::getDefaultCurrency();
      if(!$isCurrency){
        $data['currency'] = $defaultCurrency;
      }
      $data['date'] = date('Y-m-d');
      $data['code'] = InventoryStockMovement::generateId('IT', null, $data['source']);
      $data['company'] = Helper::currentCompany();
      $data['type'] = 'IT';
      $data['status'] = $defaultWorkflow['code'];
      return $data;
    });
    if($form->hasRequest()){
      $source = Input::get('source');
      $destination = Input::get('destination');
      if($source == $destination){
        return response()->json([
          'status' => false,
          'messages' => 'Lokasi Asal dan tujuan harus berbeda!'
        ]);
      }
    }
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){

        $this->processDataAfterSave($form);
        InventoryStockMovementLog::addLog($form->model->id, $defaultWorkflow['code']);

        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }

    $dataForm = $this->getDataResponse($dataForm);
    $warehouseLocId = null;
    if(isset($dataForm['optionWarehouse']) && count($dataForm['optionWarehouse']) > 0){
      $warehouseLocId = $dataForm['optionWarehouse'][0]['id'];
    }
    $dataForm['data']['code'] = InventoryStockMovement::generateId('IT', null, $warehouseLocId);
    $dataForm['data']['diajukan'] = Auth::user()->fullname;
    $dataForm['data']['date'] = Carbon::now()->format('d/m/Y');
    return response()->json($dataForm);
  }

  public function modify($id = null) {
    $this->getDataProduct($id);
    $stockMovement = InventoryStockMovement::find($id);
    $form = $this->anyForm($stockMovement);
    $form->pre(function($data){
      unset($data['date']);
      return $data;
    });
    if($form->hasRequest()){
      $source = Input::get('source');
      $destination = Input::get('destination');
      if($source == $destination){
        return response()->json([
          'status' => false,
          'messages' => 'Lokasi Asal dan tujuan harus berbeda!'
        ]);
      }
    }
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){

        $this->processDataAfterSave($form);

        return response()->json([
          'status' => true,
          'dataProduct' => $this->getDataProduct($id)
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }

    $dataForm['data']['date'] = Carbon::parse($dataForm['data']['date'])->format('d/m/Y');
    $user = SettingUser::where('username', $stockMovement->created_by)->first();
    $dataForm['data']['diajukan'] = (!empty($user)) ? $user->fullname : '';
    $dataForm = $this->getDataResponse($dataForm);
    $dataForm['dataProduct'] = $this->getDataProduct($id);
    $dataForm['dataComment'] = $this->getDataComment($id);
    $reference = $dataForm['data']['reference'];
    if(!empty($reference)){
      $stockMoveReference = InventoryStockMovement::find($reference);
      if(!empty($stockMoveReference)){
        $dataForm['data']['reference'] = $stockMoveReference->code;
      }
    }
    return response()->json($dataForm);
  }

  public function delete(){

    $id = Input::get('id');
    InventoryStockMovement::whereIn('id', $id)->delete();

    return response()->json([
      'status' => true
    ]);
  }

  public function changeStatus($id, $status){
    $stockMovement = InventoryStockMovement::find($id);
    if(!empty($stockMovement)){
      if($stockMovement->status != $status) {
        $stockMovement->status = $status;
        $stockMovement->save();        
        InventoryStockMovementLog::addLog($id, $status);
      }

      return response()->json([
        'status' => true,
        'dataComment' => $this->getDataComment($id)
      ]);
    }
  }

  public function anyForm($source){
    $form = FormBuilder::source($source);
    $form->title('Mutasi Internal');
    $form->add('code', 'No. Penyesuaian', 'text')->rule('required|max:25')->attributes([
      'readonly' => true
    ]);
    $form->add('date', 'Tanggal', 'text')->attributes([
      'readonly' => true
    ]);
    $form->add('expected_date', 'Pengiriman Barang', 'date')->rule('required');
    $form->add('diajukan', 'Diajukan Oleh', 'text')->attributes([
      'readonly' => true
    ]);
    $form->add('description', 'Keterangan', 'textarea');
    $form->add('source', 'Asal', 'select')->attributes([
      'ng-options' => 'item.id as item.text for item in response.optionWarehouseSource',
      'ng-change' => 'changeSelect()',
      'options-class' => "{'font-red': text.indexOf('TPS') !== -1}",
      'data-live-search' => true
    ])->rule('required');
    $form->add('destination', 'Tujuan', 'select')->attributes([
      'ng-options' => 'item.id as item.text for item in response.optionWarehouse',
      'ng-change' => 'changeSelect()',
      'options-class' => "{'font-red': text.indexOf('TPS') !== -1}",
      'data-live-search' => true
    ])->rule('required');
    return $form;
  }

  public function getDataResponse($data){
    $data['optionCurrency'] = SettingCurrency::getOption();
    $data['defaultCurrency'] = SettingCurrency::getDefaultCurrency();
    $data['optionWarehouse'] = InventoryWarehouseLocation::getOption();
    $data['optionWarehouseSource'] = InventoryWarehouseLocation::getAuthOption();
    $data['dataProduct'] = [];
    $data['dataComment'] = [];
    $data['optionWorkflow'] = SettingWorkflow::getOption('IT');
    return $data;
  }

  public function getDataProduct($id){
    return InventoryStockMovementProduct::getDataProduct($id);
  }

  public function processDataAfterSave($form = null){
    if(empty($form)){
      return;
    }

    // Save Movement Product
    $listId = [];
    if(!empty($form->dataPost['dataProduct'])){
      foreach($form->dataPost['dataProduct'] as $val){
        $stockMovementProduct = new InventoryStockMovementProduct();
        $val['stock_movement'] = $form->model->id;
        $val['expected_date'] = null;
        if(!empty($val['id'])){
          $stockMovementProduct = InventoryStockMovementProduct::find($val['id']);
        }
        $val['discount'] = (isset($val['discount'])) ? json_encode($val['discount']) : null;

        $stockMovementProduct->fill($val);
        $stockMovementProduct->save();
        $listId[] = $stockMovementProduct->id;
      }
    }
    InventoryStockMovementProduct::where('stock_movement', $form->model->id)->whereNotIn('id', $listId)->delete();

    InventoryStockMovement::refreshTotal($form->model->id);
  }

  public function getDataComment($id){
    $dataLog = InventoryStockMovementLog::getData($id, 'IT', 'Internal Transfer');
    $dataComment = InventoryStockMovementComment::getData($id);
    $data = collect($dataComment);
    $data = $data->merge($dataLog);

    return $data->all();
  }

  public function printOut($id){

    $stockMovement = InventoryStockMovement::find($id);
    $stockMovement->printed += 1;
    $stockMovement->save();
    // dd($stockMovement);
    $company = SettingCompany::find($stockMovement->company);

    $warehouseLocationSource = InventoryWarehouseLocation::find($stockMovement->source);
    $warehouseSource = InventoryWarehouse::find($warehouseLocationSource->warehouse);
    $warehouseLocationDestination = InventoryWarehouseLocation::find($stockMovement->destination);
    $warehouseDestination = InventoryWarehouse::find($warehouseLocationDestination->warehouse);

    $dataProduct = $this->getDataProduct($id);

    return view('print.internal-transfer', compact('stockMovement', 'dataProduct', 'company', 'warehouseLocationSource', 'warehouseSource', 'warehouseLocationDestination', 'warehouseDestination'));
  }

}
