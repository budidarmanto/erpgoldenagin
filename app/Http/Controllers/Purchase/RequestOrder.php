<?php

namespace App\Http\Controllers\Purchase;

use App\Models\CrmContact;
use App\Models\CrmContactDetail;
use App\Models\InventoryWarehouse;
use App\Models\SettingCompany;
use App\Models\SettingUser;
use App\Models\SettingWorkflow;
use App\Models\ProductProduct;
use App\Models\ProductCategory;
use App\Models\InventoryStockMovement;
use App\Models\InventoryStockMovementProduct;
use App\Models\InventoryStockMovementLog;
use App\Models\SettingCurrency;
use App\Models\InventoryStockMovementComment;
use App\Models\SettingTax;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Params;
use Helper;
use Auth;
use Carbon\Carbon;

class RequestOrder extends Controller{

  public $company;

  public function index() {
// dd(Auth::user()->getAccess());
   $listCategory = Auth::user()->authCategory();


    $query = InventoryStockMovement::select([
      'setting_user.fullname',
      'inventory_stock_movement.*'
    ])->where([
      'inventory_stock_movement.company' => Helper::currentCompany(),
      'inventory_stock_movement.type' => 'PR',
    ])
    ->whereIn('inventory_stock_movement.category', $listCategory)
    ->join('setting_user', 'inventory_stock_movement.created_by', '=', 'setting_user.username')
    ->orderBy('inventory_stock_movement.code', 'DESC');

    if(!$this->isRequester()){
      $query->whereNotIn('status', ['1PC']);
    }

    $dg = Datagrid::source($query);
    $dg->title('Pengajuan');
    $dg->filter('keyword', function($query, $value){
      if($value != ''){
        return $query->where(function($q) use ($value) {
          $q->where('inventory_stock_movement.code', 'ilike', '%'.$value.'%')->orWhere('inventory_stock_movement.description', 'ilike', '%'.$value.'%');
          return $q;
        });
      }

      return $query;
    });
    $dg->filter('date', function($query, $value){
      if($value != '')
          return $query->whereDate('inventory_stock_movement.date', $value);

      return $query;
    });

    $dg->filter('workflow', function($query, $value){
      if($value != '')
          return $query->where('inventory_stock_movement.status', $value);

      return $query;
    });

    $dg->add('code', 'No. Pengajuan', true);
    $dg->add('date', 'Tanggal', true)->render(function($data){
      return Carbon::parse($data['date'])->format('d/m/Y');
    });
    $dg->add('description', 'Keterangan');
    if(!$this->isRequester()){
        $dg->add('total','Total');
    }

    $dg->add('fullname','Diajukan Oleh');
    $dg->add('status','Status');
    $datagrid = $dg->build();
    $datagrid['optionWorkflow'] = $this->optionWorkflow();
    $datagrid['isRequester'] = $this->isRequester();
    $datagrid['isPurchaser'] = $this->isPurchaser();
    return response()->json($datagrid);
  }

  public function create(){

    $form = $this->anyForm(new InventoryStockMovement());

    $defaultWorkflow = SettingWorkflow::workflowDefault('PR');

    $form->pre(function($data) use($defaultWorkflow){
      $isCurrency = (Params::get('MULTI_CURRENCY') == 1) ? true : false;
      $defaultCurrency = SettingCurrency::getDefaultCurrency();
      if(!$isCurrency){
        $data['currency'] = $defaultCurrency;
      }
      $data['date'] = date('Y-m-d');
      $data['code'] = InventoryStockMovement::generateId('PR');
      $data['company'] = Helper::currentCompany();
      $data['type'] = 'PR';
      $data['status'] = $defaultWorkflow['code'];
      return $data;
    });
    $dataForm = $form->build();

    if($form->hasRequest()){

      $this->processDataAfterSave($form);
      InventoryStockMovementLog::addLog($form->model->id, $defaultWorkflow['code']);

      if($form->saved()){
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }

    $dataForm = $this->getDataResponse($dataForm);
    $dataForm['data']['code'] = InventoryStockMovement::generateId('PR');
    $dataForm['data']['diajukan'] = Auth::user()->fullname;
    $dataForm['data']['date'] = Carbon::now()->format('d/m/Y');
    return response()->json($dataForm);
  }

  public function modify($id = null) {
    $this->getDataProduct($id);
    $stockMovement = InventoryStockMovement::find($id);
    $form = $this->anyForm($stockMovement);
    $form->pre(function($data){
      unset($data['date']);
      return $data;
    });
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){

        $this->processDataAfterSave($form);

        return response()->json([
          'status' => true,
          'dataProduct' => $this->getDataProduct($id)
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }

    $dataForm['data']['date'] = Carbon::parse($dataForm['data']['date'])->format('d/m/Y');
    $user = SettingUser::where('username', $stockMovement->created_by)->first();
    $dataForm['data']['diajukan'] = (!empty($user)) ? $user->fullname : '';
    $dataForm = $this->getDataResponse($dataForm);
    $dataForm['dataProduct'] = $this->getDataProduct($id);
    // $dataForm['action'] = 'view';

    $dataForm['dataComment'] = $this->getDataComment($id);
    return response()->json($dataForm);
  }

  public function duplicate($id){
    $stockMovement = InventoryStockMovement::find($id);
    if(!empty($stockMovement) && Auth::user()->hasAccess('duplicate')){
        $defaultWorkflow = SettingWorkflow::workflowDefault('PR');

        $data = $stockMovement->toArray();
        $data['id'] = '';
        $isCurrency = (Params::get('MULTI_CURRENCY') == 1) ? true : false;
        $defaultCurrency = SettingCurrency::getDefaultCurrency();
        if(!$isCurrency){
          $data['currency'] = $defaultCurrency;
        }
        $data['date'] = date('Y-m-d');
        $data['code'] = InventoryStockMovement::generateId('PR');
        $data['company'] = Helper::currentCompany();
        $data['type'] = 'PR';
        $data['status'] = $defaultWorkflow['code'];
        $data['reference'] = '';
        // dd($data);
        $newStockMovement = InventoryStockMovement::create($data);

        $dataProduct = InventoryStockMovementProduct::where('stock_movement', $id)->get()->toArray();
        if(!empty($dataProduct)){
          foreach($dataProduct as $val){
            $val['id'] = '';
            $val['stock_movement'] = $newStockMovement->id;
            $val['status'] = 0;
            InventoryStockMovementProduct::create($val);
          }
        }
        InventoryStockMovementLog::addLog($newStockMovement->id, $defaultWorkflow['code']);
    }

    return response()->json(['status' => true]);
  }

  public function printOut($id){

    $stockMovement = InventoryStockMovement::find($id);
    $stockMovement->printed += 1;
    $stockMovement->save();
    $company = SettingCompany::find($stockMovement->company);
    $warehouse = InventoryWarehouse::find($stockMovement->destination);
    $dataProduct = $this->getDataProduct($id);

    return view('print.request-order', compact('stockMovement', 'dataProduct', 'company', 'warehouse'));
  }

  public function delete(){

    $id = Input::get('id');
    InventoryStockMovement::whereIn('id', $id)->delete();

    return response()->json([
      'status' => true
    ]);
  }

  public function changeStatus($id, $status){
    $stockMovement = InventoryStockMovement::find($id);
    if(!empty($stockMovement)){
      if($stockMovement->status != $status) {
        $stockMovement->status = $status;
        $stockMovement->save();        
        InventoryStockMovementLog::addLog($id, $status);
      }
            
      return response()->json([
        'status' => true,
        'dataComment' => $this->getDataComment($id)
      ]);
    }
  }

  public function anyForm($source){
    $form = FormBuilder::source($source);
    $form->title('Pengajuan');
    $form->add('code', 'No. Pengajuan', 'text')->rule('required|max:25')->attributes([
      'readonly' => true
    ]);
    $form->add('date', 'Tanggal', 'text')->attributes([
      'readonly' => true
    ]);
    $form->add('description', 'Keterangan', 'textarea');
    $form->add('diajukan', 'Diajukan Oleh', 'text')->attributes([
      'readonly' => true
    ]);
    $form->add('destination', 'Tujuan', 'select')->attributes([
      'ng-options' => 'item.id as item.text for item in response.optionWarehouse',
    ])->rule('required');
    $form->add('category', 'Kategori', 'select')->attributes([
      'ng-options' => 'item.id as item.text for item in response.optionCategory',
      'ng-change' => 'resetDataProduct()',
    ])->rule('required');

    $isCurrency = (Params::get('MULTI_CURRENCY') == 1) ? true : false;
    if($isCurrency){
      $form->add('currency', 'Mata Uang', 'select')->attributes([
        'ng-options' => 'item.id as item.text for item in response.optionCurrency'
      ])->rule('required');
      $form->add('currency_rate', 'Kurs', 'number');
    }

    return $form;
  }

  public function getDataResponse($data){
    $data['optionCurrency'] = SettingCurrency::getOption();
    $data['defaultCurrency'] = SettingCurrency::getDefaultCurrency();
    $data['optionWarehouse'] = InventoryWarehouse::getOption();
    $data['optionTax'] = SettingTax::getOption();
    $data['optionCategory'] = ProductCategory::getOption();
    $data['dataProduct'] = [];
    $data['dataComment'] = [];
    $data['optionWorkflow'] = $this->optionWorkflow();
    $data['isRequester'] = $this->isRequester();
    $data['isPurchaser'] = $this->isPurchaser();
    $taxSupplier = Params::get('TAX_PKP');
    $tax = SettingTax::find($taxSupplier);
    $data['taxSupplier'] = [
      'tax' => Params::get('TAX_PKP'),
      'tax_amount' => $tax->amount
    ];
    // dd($data);
    return $data;
  }

  public function getDataProduct($id){
    return InventoryStockMovementProduct::getDataProduct($id);
  }

  public function processDataAfterSave($form = null){
    if(empty($form)){
      return;
    }

    // Save Movement Product
    $listId = [];
    if(!empty($form->dataPost['dataProduct'])){
      foreach($form->dataPost['dataProduct'] as $val){
        $stockMovementProduct = new InventoryStockMovementProduct();
        $val['stock_movement'] = $form->model->id;
        if(!empty($val['id'])){
          $stockMovementProduct = InventoryStockMovementProduct::find($val['id']);
        }
        $val['discount'] = (isset($val['discount'])) ? json_encode($val['discount']) : null;
        $val['contact'] = $val['supplier'];
        $val['expected_date'] = Carbon::createFromFormat('d/m/Y', $val['expected_date'])->format('Y-m-d');

        $stockMovementProduct->fill($val);
        $stockMovementProduct->save();
        $listId[] = $stockMovementProduct->id;
      }
    }
    InventoryStockMovementProduct::where('stock_movement', $form->model->id)->whereNotIn('id', $listId)->delete();

    InventoryStockMovement::refreshTotal($form->model->id);
  }

  public function optionWorkflow(){
    $option = SettingWorkflow::getOption('PR');

    if(!$this->isRequester()){
      $option = collect($option)->filter(function($opt){
        return (!in_array($opt['id'], ['1PC']));
      })->values()->toArray();
    }

    return $option;
  }

  public function isRequester(){
    $groups = Auth::user()->groups();
    $requester = Params::get('GROUP_REQUESTER');
    $requester_approver = Params::get('GROUP_REQUEST_APPROVER');
    // dd($groups);
    if(in_array($requester, $groups) || in_array($requester_approver, $groups)){
      return true;
    }

    return false;
  }

  public function isPurchaser(){
    $groups = Auth::user()->groups();
    $purchaser = Params::get('GROUP_PURCHASER');
    if(in_array($purchaser, $groups)){
      return true;
    }

    return false;
  }

  public function getDataComment($id){
    $dataLog = InventoryStockMovementLog::getData($id, 'PR', 'Purchase Request');
    $dataComment = InventoryStockMovementComment::getData($id);
    $data = collect($dataComment);
    $data = $data->merge($dataLog);

    return $data->all();

    // $dataLog = collect($dataLog)->map(function($data) use ($stockMovement){
    //   if($data['status'] == '7PO'){
    //     $stockMovementPo = InventoryStockMovement::where('reference', $stockMovement->id)->get()->toArray();
    //
    //     if(count($stockMovementPo) > 0){
    //       $data['description'] .= '<br/><br/>'.count($stockMovementPo). ' PO telah dibuat<br/><br/>';
    //       foreach($stockMovementPo as $vpo){
    //         $data['description'] .= '# <a href="/#/purchase/purchase-order/modify/'.$vpo['id'].'">'.$vpo['code'].'</a><br/>';
    //       }
    //     }
    //   }
    //
    //   return $data;
    // });
  }

}
