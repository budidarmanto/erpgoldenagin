<?php

namespace App\Http\Controllers\Purchase;

use App\Models\CrmContact;
use App\Models\CrmContactDetail;
use App\Models\InventoryWarehouse;
use App\Models\SettingCompany;
use App\Models\SettingUser;
use App\Models\SettingWorkflow;
use App\Models\ProductProduct;
use App\Models\ProductCategory;
use App\Models\InventoryStockMovement;
use App\Models\InventoryStockMovementProduct;
use App\Models\InventoryStockMovementLog;
use App\Models\InventoryStockMovementComment;
use App\Models\SettingCurrency;
use App\Models\SettingTax;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Params;
use Helper;
use Auth;
use Carbon\Carbon;

class ApprovalOrder extends Controller{

  public $company;

  public function index() {
    $listCategory = Auth::user()->authCategory();

    $query = InventoryStockMovement::select([
      'setting_user.fullname',
      'inventory_stock_movement.*'
    ])->where([
      'inventory_stock_movement.company' => Helper::currentCompany(),
      'inventory_stock_movement.type' => 'PR',
    ])->whereNotIn('status', ['1PC', '2PS', '3PC', '4PQ'])
    ->whereIn('inventory_stock_movement.category', $listCategory)
    ->join('setting_user', 'inventory_stock_movement.created_by', '=', 'setting_user.username')
    ->orderBy('inventory_stock_movement.code', 'DESC');

    $dg = Datagrid::source($query);
    $dg->title('Persetujuan');
    $dg->filter('keyword', function($query, $value){
      if($value != ''){
        return $query->where(function($q) use ($value) {
          $q->where('inventory_stock_movement.code', 'ilike', '%'.$value.'%')->orWhere('inventory_stock_movement.description', 'ilike', '%'.$value.'%');
          return $q;
        });
      }

      return $query;
    });
    $dg->filter('date', function($query, $value){
      if($value != '')
          return $query->whereDate('inventory_stock_movement.date', $value);

      return $query;
    });

    $dg->filter('workflow', function($query, $value){
      if($value != '')
          return $query->where('inventory_stock_movement.status', $value);

      return $query;
    });

    $dg->add('code', 'No. Persetujuan', true);
    $dg->add('date', 'Tanggal', true)->render(function($data){
      return Carbon::parse($data['date'])->format('d/m/Y');
    });
    $dg->add('description', 'Keterangan');
    $dg->add('total','Total');
    $dg->add('fullname','Diajukan Oleh');
    $dg->add('status','Status');
    $datagrid = $dg->build();
    $datagrid['optionWorkflow'] = $this->optionWorkflow();
    return response()->json($datagrid);
  }

  public function modify($id = null) {
    $this->getDataProduct($id);
    $stockMovement = InventoryStockMovement::find($id);
    $form = $this->anyForm($stockMovement);
    $form->pre(function($data){
      unset($data['date']);
      return $data;
    });
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){

        $this->processDataAfterSave($form);

        return response()->json([
          'status' => true,
          'dataProduct' => $this->getDataProduct($id)
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }

    $dataForm['data']['date'] = Carbon::parse($dataForm['data']['date'])->format('d/m/Y');
    $user = SettingUser::where('username', $stockMovement->created_by)->first();
    $dataForm['data']['diajukan'] = (!empty($user)) ? $user->fullname : '';
    $dataForm = $this->getDataResponse($dataForm);
    $dataForm['dataProduct'] = $this->getDataProduct($id);
    $dataForm['dataComment'] = $this->getDataComment($id);
    $dataForm['action'] = 'view';
    return response()->json($dataForm);
  }

  public function changeStatus($id, $status){
    $stockMovement = InventoryStockMovement::find($id);
    if(!empty($stockMovement)){
      if($stockMovement->status != $status) {
        $stockMovement->status = $status;
        $stockMovement->save();        
        InventoryStockMovementLog::addLog($id, $status);
      }

      if($status == '6PR'){
        InventoryStockMovementProduct::where('stock_movement', $id)->update([
          'status' => 2
        ]);
      }else if($status == '7PO'){
        $defaultWorkflow = SettingWorkflow::workflowDefault('PO');
        if(empty($defaultWorkflow)){
          dd("Workflow is Empty");
        }
        $isCurrency = (Params::get('MULTI_CURRENCY') == 1) ? true : false;
        $defaultCurrency = SettingCurrency::getDefaultCurrency();

        $stockMovementProduct = InventoryStockMovementProduct::where('stock_movement', $id)->get();
        $groupBySupplier = [];
        if(count($stockMovementProduct) > 0){
          foreach($stockMovementProduct as $val){
            if(!empty($val['contact'])){
                $groupBySupplier[$val['contact']][] = $val;
            }
          }
        }

        $listPO = [];
        if(!empty($groupBySupplier)){
          foreach($groupBySupplier as $key => $val){

            // Validate PO Has a Valid Product
            $statusValid = false;
            foreach($val as $vproduct){
              if($vproduct['status'] == 0){
                InventoryStockMovementProduct::find($vproduct['id'])->update([
                  'status' => 1
                ]);
                $vproduct['status'] = 1;
              }
              if($vproduct['status'] == 1){
                $statusValid = true;
              }
            }

            if(!$statusValid){
              continue;
            }

            // Create PO By Supplier
            $codePO = InventoryStockMovement::generateId('PO');
            $stockMovementPO = InventoryStockMovement::create([
              'code' => $codePO,
              'company' => Helper::currentCompany(),
              'type' => 'PO',
              'status' => $defaultWorkflow['code'],
              'currency' => $stockMovement->currency,
              'currency_rate' => $stockMovement->currency_rate,
              'category' => $stockMovement->category,
              'contact' => $key,
              'description' => $stockMovement->description,
              'destination' => $stockMovement->destination,
              'expected_date' => $stockMovement->expected_date,
              'reference' => $id,
              'date' => Carbon::now()->format('Y-m-d H:i:s'),
              'total' => $stockMovement->total,
            ]);

            $listPO[] = $codePO;
            // Create PO Detail
            foreach($val as $vproduct){
              if($vproduct['status'] == 1 && $vproduct['qty'] > 0 && $vproduct['price'] > 0){
                InventoryStockMovementProduct::create([
                    'stock_movement' => $stockMovementPO->id,
                    'product' => $vproduct['product'],
                    'uom' => $vproduct['uom'],
                    'qty' => $vproduct['qty'],
                    'price' => $vproduct['price'],
                    'discount' => $vproduct['discount'],
                    'tax' => $vproduct['tax'],
                    'tax_amount' => $vproduct['tax_amount'],
                    'total' => $vproduct['total'],
                    'status' => 1,
                    'expected_date' => $vproduct['expected_date'],
                ]);
              }
            }
            InventoryStockMovementLog::addLog($stockMovementPO->id, $defaultWorkflow['code']);
            InventoryStockMovement::refreshTotal($stockMovementPO->id);
          }
        }
        return response()->json([
          'status' => true,
          'listPO' => $listPO,
          'dataComment' => $this->getDataComment($id)
        ]);
      }
    }

    return response()->json(['status' => true]);
  }

  public function anyForm($source){
    $form = FormBuilder::source($source);
    $form->title('Persetujuan');
    $form->add('code', 'No. Persetujuan', 'text')->rule('required|max:25')->attributes([
      'readonly' => true
    ]);
    $form->add('date', 'Tanggal', 'text')->attributes([
      'readonly' => true
    ]);
    $form->add('description', 'Keterangan', 'textarea');
    $form->add('diajukan', 'Diajukan Oleh', 'text')->attributes([
      'readonly' => true
    ]);
    $form->add('destination', 'Tujuan', 'select')->attributes([
      'ng-options' => 'item.id as item.text for item in response.optionWarehouse',
    ])->rule('required');

    $form->add('category', 'Kategori', 'select')->attributes([
      'ng-options' => 'item.id as item.text for item in response.optionCategory',
      'ng-change' => 'resetDataProduct()',
    ])->rule('required');

    $isCurrency = (Params::get('MULTI_CURRENCY') == 1) ? true : false;
    if($isCurrency){
      $form->add('currency', 'Mata Uang', 'select')->attributes([
        'ng-options' => 'item.id as item.text for item in response.optionCurrency'
      ])->rule('required');
      $form->add('currency_rate', 'Kurs', 'number');
    }

    return $form;
  }

  public function getDataResponse($data){
    $data['optionCurrency'] = SettingCurrency::getOption();
    $data['defaultCurrency'] = SettingCurrency::getDefaultCurrency();
    $data['optionWarehouse'] = InventoryWarehouse::getOption();
    $data['optionCategory'] = ProductCategory::getOption();
    $data['optionTax'] = SettingTax::getOption();
    $data['dataProduct'] = [];
    $data['dataComment'] = [];
    $data['optionWorkflow'] = $this->optionWorkflow();
    return $data;
  }

  public function getDataProduct($id){
    return InventoryStockMovementProduct::getDataProduct($id);
  }

  public function processDataAfterSave($form = null){
    if(empty($form)){
      return;
    }

    // Save Movement Product
    $listId = [];
    if(!empty($form->dataPost['dataProduct'])){
      foreach($form->dataPost['dataProduct'] as $val){
        $stockMovementProduct = new InventoryStockMovementProduct();
        $val['stock_movement'] = $form->model->id;
        if(!empty($val['id'])){
          $stockMovementProduct = InventoryStockMovementProduct::find($val['id']);
        }
        $val['discount'] = (isset($val['discount'])) ? json_encode($val['discount']) : null;
        $val['contact'] = $val['supplier'];
        $val['status'] = ($val['status'] == 0) ? 1 : $val['status'];
        $val['expected_date'] = Carbon::createFromFormat('d/m/Y', $val['expected_date'])->format('Y-m-d');

        $stockMovementProduct->fill($val);
        $stockMovementProduct->save();
        $listId[] = $stockMovementProduct->id;
      }
    }
    InventoryStockMovementProduct::where('stock_movement', $form->model->id)->whereNotIn('id', $listId)->delete();

    InventoryStockMovement::refreshTotal($form->model->id);
  }

  public function optionWorkflow(){
    $option = SettingWorkflow::getOption('PR');

    $option = collect($option)->filter(function($opt){
      return (!in_array($opt['id'], ['1PC', '2PS', '3PC', '4PQ']));
    })->values()->toArray();

    return $option;
  }

  public function getDataComment($id){
    $dataLog = InventoryStockMovementLog::getData($id, 'PR', 'Purchase Request');
    $dataComment = InventoryStockMovementComment::getData($id);
    $data = collect($dataComment);
    $data = $data->merge($dataLog);
    return $data;
  }

}
