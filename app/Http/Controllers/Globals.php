<?php

namespace App\Http\Controllers;

use App\Models\CrmContact;
use App\Models\SettingCompany;
use App\Models\ProductMaster;
use App\Models\ProductProduct;
use App\Models\ProductAttribute;
use App\Models\ProductAttributeValue;
use App\Models\ProductUom;
use App\Models\ProductUomCategory;
use App\Models\InventoryStockMovement;
use App\Models\InventoryStockMovementLog;
use App\Models\InventoryStockMovementProduct;
use App\Models\InventoryStockMovementComment;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Params;
use Helper;
use Auth;

class Globals extends Controller{

  // Get Options Supplier
  // Param: q, Method: GET
  public function getOptionSupplier(){
    $keyword = Input::get('q');

    $supplier = CrmContact::where(function($query) use($keyword){
      $query->where('name', 'ilike', '%'.$keyword.'%');
      $query->where('type', 'supplier');
    })->orWhere('id', $keyword)->limit(20);
    $optSupplier = CrmContact::getOptionSupplier($supplier);

    return response()->json($optSupplier);
  }

  public function getOptionCustomer(){
    $keyword = Input::get('q');

    $customer = CrmContact::where(function($query) use($keyword){
      $query->where('name', 'ilike', '%'.$keyword.'%');
      $query->where('type', 'customer');
    })->orWhere('id', $keyword)->limit(20);
    $optCustomer = CrmContact::getOption($customer);

    return response()->json($optCustomer);
  }

  // Get Options Product
  // Param: q, Method: GET
  public function getOptionProduct(){
    $keyword = Input::get('q');

    $dataProduct = ProductProduct::getData(function($model) use($keyword){
      $model->where('product_master.name', 'ilike', '%'.$keyword.'%')
      ->orWhere('product_master.code', 'ilike', '%'.$keyword.'%')
      ->orWhere('product_product.id', $keyword)->limit(20);
      return $model;
    });
    $dataProduct = collect($dataProduct)->transform(function($item) {
      $item['name'] = $item['name'].' - '.$item['code'];
      return $item;
    })->toArray();
    $opt = Helper::toOption('id', 'name', $dataProduct);
    return response()->json($opt);
  }

  // Get Option Product UOM
  // Param: ProductProduct::id, Method: Segment
  public function getOptionProductUom($id){
    $isMultiUom = (Params::get('MULTI_UOM') == '1') ? true : false;


    $product = ProductProduct::find($id);
    $optionUom = [];
    if(!empty($product)){
      $productMaster = $product->product();
      if(!empty($productMaster)){
        $uom = ProductUom::find($productMaster->uom);
        if(!empty($uom)){
          if($isMultiUom){
            $dataUom = ProductUom::where('uom_category', $uom->uom_category);
            $optionUom = ProductUom::getOption($dataUom);
          }else{
            $optionUom[] = [
              'id' => $uom->id,
              'name' => $uom->name
            ];
          }

        }
      }
    }

    return response()->json($optionUom);
  }

  public function isPkp($supplier){
    $contact = CrmContact::find($supplier);
    $status = false;
    if(!empty($contact)){
      $status = $contact->pkp;
    }

    return response()->json([
      'status' => $status
    ]);
  }

  public function countAlert(){
    if(Auth::user() == false){
      return;
    }
    $lsWarehouse = Auth::user()->listWarehouseLocation();
    $listCategory = Auth::user()->authCategory();
    $groups = Auth::user()->groups();
    $requester = Params::get('GROUP_REQUESTER');
    $requester_approver = Params::get('GROUP_REQUEST_APPROVER');
    $purchaser = Params::get('GROUP_PURCHASER');

    $isRequester = false;
    $isPurchaser = false;

    if(in_array($requester, $groups) || in_array($requester_approver, $groups)){
      $isRequester = true;
    }

    if(in_array($purchaser, $groups)){
      $isPurchaser = true;
    }

    $totalRequest = 0;
    $totalApproval = 0;
    $totalPurchase = 0;
    $totalIncoming = 0;
    $totalReceipt = 0;
    $totalOutgoing = 0;
    $totalDelivery = 0;
    $totalInternalTransfer = 0;
    $totalExternalTransfer = 0;
    $totalAdjustment = 0;
    $totalSales = 0;
    $totalRequestNote = 0;
    $totalIncomingRequest = 0;

    if($isRequester){
      $totalRequest = InventoryStockMovement::where([
        'inventory_stock_movement.company' => Helper::currentCompany(),
        'inventory_stock_movement.type' => 'PR',
        // 'status' => '1PC'
      ])->whereIn('status', ['1PC', '4PQ'])
      ->whereIn('inventory_stock_movement.category', $listCategory)->count();
    }else {
      $totalRequest = InventoryStockMovement::where([
        'inventory_stock_movement.company' => Helper::currentCompany(),
        'inventory_stock_movement.type' => 'PR',
        'status' => '2PS'
      ])->whereIn('inventory_stock_movement.category', $listCategory)->count();
    }

    $totalApproval = InventoryStockMovement::where([
      'inventory_stock_movement.company' => Helper::currentCompany(),
      'inventory_stock_movement.type' => 'PR',
      'status' => '5PA'
    ])->whereIn('inventory_stock_movement.category', $listCategory)->count();

    $totalPurchase = InventoryStockMovement::where([
      'inventory_stock_movement.company' => Helper::currentCompany(),
      'inventory_stock_movement.type' => 'PO',
      'status' => '3PO'
    ])->whereIn('inventory_stock_movement.category', $listCategory)
    ->whereIn('status', ['3PO', '2PA'])
    ->where('inventory_stock_movement.contact', '!=', Params::get('SUPPLIER_AGRINESIA'))->count();

    $totalPurchaseLbs = InventoryStockMovement::where([
      'inventory_stock_movement.company' => Helper::currentCompany(),
      'inventory_stock_movement.type' => 'PO',
      'status' => '3PO'
    ])->whereIn('status', ['3PO', '2PA'])
    ->where('inventory_stock_movement.contact', Params::get('SUPPLIER_AGRINESIA'))->count();

    $totalIncoming = InventoryStockMovementProduct::join('inventory_stock_movement', 'inventory_stock_movement.id', '=', 'inventory_stock_movement_product.stock_movement')
    ->where([
      'inventory_stock_movement.company' => Helper::currentCompany()
    ])
    ->where(function($q){
      $q->where(function($q){
        $q->where('inventory_stock_movement.type', 'PO')
        ->whereIn('inventory_stock_movement.status', ['3PO', '5PR']);
        return $q;
      })->orWhere(function($q){
        $q->where('inventory_stock_movement.type', 'TF')
        ->whereIn('inventory_stock_movement.status', ['1TO', '2TR']);
        return $q;
      });
      return $q;
    })
    ->whereRaw("(inventory_stock_movement_product.qty - inventory_stock_movement_product.rcp_qty) > 0")->count();

    $totalReceipt = InventoryStockMovement::where([
      'inventory_stock_movement.company' => Helper::currentCompany(),
      'inventory_stock_movement.type' => 'RC',
      'status' => '1RR'
    ])->count();

    $totalOutgoing = InventoryStockMovementProduct::join('inventory_stock_movement', 'inventory_stock_movement.id', '=', 'inventory_stock_movement_product.stock_movement')
    ->where([
      'inventory_stock_movement.company' => Helper::currentCompany()
    ])
    ->where(function($q){
      $q->where(function($q){
        $q->where([
          'inventory_stock_movement.type' => 'SO',
          'inventory_stock_movement.status' => '3SO',
        ]);
        return $q;
      })->orWhere(function($q){
        $q->where([
          'inventory_stock_movement.type' => 'ET',
          'inventory_stock_movement.status' => '3ED',
        ]);
        return $q;
      })->orWhere(function($q){
        $q->where([
          'inventory_stock_movement.type' => 'RT',
          'inventory_stock_movement.status' => '3RR',
        ]);
        return $q;
      });
      return $q;
    })
    ->whereRaw("(inventory_stock_movement_product.qty - inventory_stock_movement_product.do_qty) > 0")->count();

    $totalDelivery = InventoryStockMovement::where([
      'inventory_stock_movement.company' => Helper::currentCompany(),
      'inventory_stock_movement.type' => 'DO',
      'status' => '1DR'
    ])->count();

    $totalSales = InventoryStockMovement::where([
      'inventory_stock_movement.company' => Helper::currentCompany(),
      'inventory_stock_movement.type' => 'SO',
      'status' => '1SR'
    ])->count();

    $totalAdjustment = InventoryStockMovement::where([
      'inventory_stock_movement.company' => Helper::currentCompany(),
      'inventory_stock_movement.type' => 'AD',
      'status' => '1AC'
    ])->where(function($q) use ($lsWarehouse) {
      $q->whereIn('inventory_stock_movement.source', $lsWarehouse)
        ->orWhereIn('inventory_stock_movement.destination', $lsWarehouse);
      return $q;
    })->count();

    $totalInternalTransfer = InventoryStockMovement::where([
      'inventory_stock_movement.company' => Helper::currentCompany(),
      'inventory_stock_movement.type' => 'IT',
      'status' => '1IP'
    ])->where(function($q) use ($lsWarehouse) {
      $q->whereIn('inventory_stock_movement.source', $lsWarehouse)
        ->orWhereIn('inventory_stock_movement.destination', $lsWarehouse);
      return $q;
    })->count();

    $totalExternalTransfer = InventoryStockMovement::where([
      'inventory_stock_movement.company' => Helper::currentCompany(),
      'inventory_stock_movement.type' => 'ET',
      'status' => '1EP'
    ])->count();

    $totalReturn = InventoryStockMovement::where([
      'inventory_stock_movement.company' => Helper::currentCompany(),
      'inventory_stock_movement.type' => 'RT',
      'status' => '1RP'
    ])->count();

    $totalRequestNote = InventoryStockMovement::where([
      'inventory_stock_movement.company' => Helper::currentCompany(),
      'inventory_stock_movement.type' => 'RN',
      'status' => '1RC'
    ])->count();

    $totalIncomingRequest = InventoryStockMovementProduct::join('inventory_stock_movement', 'inventory_stock_movement.id', '=', 'inventory_stock_movement_product.stock_movement')
    ->where([
      'inventory_stock_movement.company_destination' => Helper::currentCompany(),
    ])
    ->where(function($q){
      $q->where(function($q){
        $q->where('inventory_stock_movement.type', 'RN')
        ->whereIn('inventory_stock_movement.status', ['3RR', '5RR']);
        return $q;
      });
      return $q;
    })
    ->whereRaw("(inventory_stock_movement_product.qty - inventory_stock_movement_product.rcp_qty) > 0")->count();

    $totalAllInventory = 0;
    $totalAllPurchase = 0;
    $totalAllSales = 0;
    Auth::user()->buildAccess();
    if(Auth::user()->hasApplication('purchase')){
      if(Auth::user()->hasModule('PurchaseRequestorder')){
        $totalAllPurchase += $totalRequest;
      }
      if(Auth::user()->hasModule('PurchaseApprovalorder')){
        $totalAllPurchase += $totalApproval;
      }
      if(Auth::user()->hasModule('PurchasePurchaseorder')){
        $totalAllPurchase += $totalPurchase;
      }
      if(Auth::user()->hasModule('PurchasePurchaselbs')){
        $totalAllPurchase += $totalPurchaseLbs;
      }
      if(Auth::user()->hasModule('PurchaseRequestnote')){
        $totalAllPurchase += $totalRequestNote;
      }
    }

    if(Auth::user()->hasApplication('inventory')){
      if(Auth::user()->hasModule('InventoryIncoming')){
        $totalAllInventory += $totalIncoming;
      }
      if(Auth::user()->hasModule('InventoryReceiptorder')){
        $totalAllInventory += $totalReceipt;
      }
      if(Auth::user()->hasModule('InventoryOutgoing')){
        $totalAllInventory += $totalOutgoing;
      }
      if(Auth::user()->hasModule('InventoryDeliveryorder')){
        $totalAllInventory += $totalDelivery;
      }
      if(Auth::user()->hasModule('InventoryExternaltransfer')){
        $totalAllInventory += $totalExternalTransfer;
      }
      if(Auth::user()->hasModule('InventoryInternaltransfer')){
        $totalAllInventory += $totalInternalTransfer;
      }
      if(Auth::user()->hasModule('InventoryAdjustment')){
        $totalAllInventory += $totalAdjustment;
      }
      if(Auth::user()->hasModule('InventoryReturnorder')){
        $totalAllInventory += $totalReturn;
      }
      if(Auth::user()->hasModule('InventoryIncomingrequest')){
        $totalAllInventory += $totalIncomingRequest;
      }
    }
    if(Auth::user()->hasApplication('sales')){
      if(Auth::user()->hasModule('SalesSalesorder')){
        $totalAllSales += $totalSales;
      }
    }


    return response()->json([
      'totalRequest' => $totalRequest,
      'totalPurchase' => $totalPurchase,
      'totalPurchaseLbs' => $totalPurchaseLbs,
      'totalApproval' => $totalApproval,
      'totalIncoming' => $totalIncoming,
      'totalReceipt' => $totalReceipt,
      'totalOutgoing' => $totalOutgoing,
      'totalDelivery' => $totalDelivery,
      'totalInternalTransfer' => $totalInternalTransfer,
      'totalExternalTransfer' => $totalExternalTransfer,
      'totalAdjustment' => $totalAdjustment,
      'totalSales' => $totalSales,
      'totalReturn' => $totalReturn,
      'totalAllInventory' => $totalAllInventory,
      'totalAllPurchase' => $totalAllPurchase,
      'totalAllSales' => $totalAllSales,
      'totalRequestNote' => $totalRequestNote,
      'totalIncomingRequest' => $totalIncomingRequest,
    ]);

  }

  public function addStockMovementComment($stockMovementId){
    $stockMovement = InventoryStockMovement::find($stockMovementId);

    $comment = Input::get('comment');
    if(empty($comment)){
      return response()->json([
        'status' => false,
        'message' => 'Message empty!'
      ]);
    }

    InventoryStockMovementComment::create([
      'stock_movement' => $stockMovementId,
      'comment' => $comment,
      'attachment' => '',
      'active' => true
    ]);
    $dataComment = InventoryStockMovementComment::getData($stockMovementId);

    $type = $stockMovement->type;
    $typeTitle = '';
    if($type == 'PR'){
      $typeTitle = 'Purchase Request';
    }else if($type == 'PO'){
      $typeTitle = 'Purchase Order';
    }else if($type == 'RC'){
      $typeTitle = 'Receipt Order';
    }else if($type == 'SO'){
      $typeTitle = 'Sales Order';
    }else if($type == 'SO'){
      $typeTitle = 'Delivery Order';
    }else if($type == 'AD'){
      $typeTitle = 'Adjustment';
    }else if($type == 'ET'){
      $typeTitle = 'External Transfer';
    }else if($type == 'IT'){
      $typeTitle = 'Internal Transfer';
    }


    $dataLog = InventoryStockMovementLog::getData($stockMovementId, $stockMovement->type, $typeTitle);
    $dataComment = InventoryStockMovementComment::getData($stockMovementId);
    $data = collect($dataComment);
    $data = $data->merge($dataLog);
    return $data;

    return response()->json($dataComment);
  }

  public function availableStock(){
    $product = Input::get('products');
    $warehouseLocation = Input::get('warehouse_location');
    $product = json_decode($product, true);
    if(count($product) > 0 && !empty($warehouseLocation)){
      $stock = ProductProduct::getWarehouseLocationStock($product, $warehouseLocation);
    }

    return response()->json($stock);

  }
  
  public function optionUom(){
    $product = Input::get('product');
    $productModel = ProductProduct::select([
                'product_uom.uom_category',
            ])
            ->join('product_master', 'product_master.id', '=', 'product_product.product')
            ->join('product_uom', 'product_master.uom', '=', 'product_uom.id')
            ->where('product_product.id', $product)
            ->first();
    if($productModel) {
        $uom = ProductUom::select(['id', 'code', 'name as text', 'uom_category', 'ratio', 'type'])
                ->where('uom_category', $productModel->uom_category)
                ->where('active', true)
                ->get()
                ->toArray();
        return response()->json($uom);
    }
        
    return response()->json([]);

  }

}
