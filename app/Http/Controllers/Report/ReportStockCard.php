<?php

namespace App\Http\Controllers\Report;
use App\Models\ProductMaster;
use App\Models\ProductCategory;
use App\Models\ProductUomCategory;
use App\Models\ProductUom;
use App\Models\ProductSupplier;
use App\Models\ProductAttribute;
use App\Models\ProductAttributeValue;
use App\Models\ProductProduct;
use App\Models\ProductVariant;
use App\Models\ProductBom;
use App\Models\ProductAlternative;
use App\Models\InventoryWarehouse;
use App\Models\InventoryWarehouseLocation;
use App\Models\InventoryStock;
use App\Models\InventoryStockMovement;
use App\Models\InventoryStockMovementProduct;
use App\Http\Controllers\Controller;
use FormBuilder;
use PDF;
use Datagrid;
use Request;
use Input;
use Params;
use Excel;
use Auth;
use Helper;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ReportStockCard extends Controller
{
  public function index(){
  ini_set('max_execution_time', 3600);

  $dataUom = ProductUom::select(['id', 'code', 'name', 'active', 'uom_category', 'ratio', 'type'])->get()->toArray();
  
  $fromDate = (empty(Input::get('datestart'))) ? Carbon::parse(date('Y-m-d')) : Carbon::parse(Input::get('datestart'));
  $toDate = (empty(Input::get('dateend'))) ? Carbon::parse(date('Y-m-d')) : Carbon::parse(Input::get('dateend'));
  
  $now = strtotime('now');
	//cron stock jalan 1 am
	$one_am = strtotime(date('Y-m-d 01:00:00'));
	if($now <= $one_am){
		//jika kurang dari 1 am, ambil stock kemarin
		//karena stock hari ini baru ada setelah 1 am
		$fromDate = date('Y-m-d', strtotime('yesterday'));
  }
  
  $toDate->addDays(1);
  
  //delete later
  //$fromDate->addDays(-2);
  //$toDate->addDays(-2);

  $fromDate = $fromDate->toDateTimeString();
  $toDateAdded = $toDate->toDateTimeString();
  $toDate = $toDate->addDays(-1)->toDateTimeString();

  $nilai_param = 0;
    
  $fiter_category = Input::get('category');
  $listCategory = null;

  $category = false;
  if($fiter_category != '' || $fiter_category != null){
    $category = ProductCategory::find($fiter_category);
    $listCategory = $category->getDescendantsAndSelf()->pluck('id')->toArray();
  }
  
	$keyword = Input::get('keyword');
  
$optionWarehouse = InventoryWarehouse::getOption();

    if(count($optionWarehouse) > 0){
        foreach($optionWarehouse as $key => $val){
            $warehouseLocation  = InventoryWarehouseLocation::where('warehouse', $val['id'])->where('active', true)->get()->toArray();
            $optionWarehouseLocation = Helper::toOption('id', 'name', $warehouseLocation);
            $optionWarehouse[$key]['warehouseLocation'] = $optionWarehouseLocation;
        }
    }

    $warehouse = '';
    $status = false;
    $warehouseLocation = Input::get('warehouse_location');
    $warehouse = Input::get('warehouse');

    if(!empty($warehouse)){
        $warehouseName = InventoryWarehouse::where('id', $warehouse)->where('active', true)->get()->toArray();
        $warehouseName = $warehouseName[0]['name'];
    }
    else{
        $warehouseName = '';
    }
    if(!empty($warehouseLocation)){
        $warehouseLocationName = InventoryWarehouseLocation::where('id', $warehouseLocation)->where('active', true)->get()->toArray();
        $warehouseLocationName = $warehouseLocationName[0]['name'];
    }
    else{
        $warehouseLocationName = '';
    }


    $listWarehouseLocation = [];
    if(!empty($warehouseLocation)){
        $listWarehouseLocation[] =  $warehouseLocation;
    }
    else if(!empty($warehouse)){
        $dtWarehouseLocation = InventoryWarehouseLocation::where('warehouse', $warehouse)->where('active', true)->get();
        foreach($dtWarehouseLocation as $val){
            $listWarehouseLocation[] =  $val->id;
        }
    }
    else{
        $dtWarehouseLocation = InventoryWarehouseLocation::where('active', true)->get();
        foreach($dtWarehouseLocation as $val){
            $listWarehouseLocation[] =  $val->id;
        }
    }

	$listWarehouseLocation = array_map(function ($warehouse) {
		return '\''. $warehouse . '\'';
	}, $listWarehouseLocation);
	$strListWarehouseLocation = implode(',', $listWarehouseLocation);
        
  
  $get_product = ProductProduct::select(DB::raw(''
    .'NULL AS stock_card, '
    .'product_master.name AS name, '
    .'product_master.code AS code,'
    .'product_master.category AS category,'
    .'product_master.uom AS uom,'
    .'product_master.revision AS revision,'
    .'product_uom.uom_category AS uom_category,'
    .'product_master.type AS type,'
    .'product_master.barcode AS barcode,'
    .'product_master.picture AS picture,'
    .'product_master.sale_price AS sale_price,'
    .'product_master.cost_price AS cost_price,'
    .'product_master.pos AS pos,'
    .'product_master.tax AS tax,'
    .'product_master.discount AS discount,'
    .'product_master.active AS active,'
    .'product_product.id,'
    .'product_master.id AS product_id,'
    .'product_product.attribute,'
    .'product_uom_default.id as uom,'
    .'product_uom_default.name as uom_name,'
    .'COALESCE(stock.stock,0) as stock'
  ))
  ->leftJoin('product_master','product_master.id', '=' ,'product_product.product')
  ->leftJoin('product_uom',  'product_uom.id', '=', 'product_master.uom')
  ->leftJoin(DB::raw('(select * from product_uom where id in (select max(id) from product_uom where type =\'d\' and active = true group by uom_category)) as product_uom_default'), function ($join) {
    $join->on('product_uom_default.uom_category', '=', 'product_uom.uom_category');
  })
  ->leftJoin(DB::raw('(select product,  COALESCE(sum(begin_qty), 0) as stock from inventory_stock where date = \'' . $fromDate . '\' and warehouse_location in (' . $strListWarehouseLocation . ')  group by product) as stock'), function ($join) {
      $join->on('stock.product', '=', 'product_product.id');
  });
  // ->where(DB::raw('COALESCE(stock.stock, 0)'), '!=',
  // $nilai_param);
  if($listCategory) {
    $get_product = $get_product->whereIn('product_master.category', $listCategory);
  }
  $get_product->orderBy('product_master.name', 'ASC');
  //delete later
  //$get_product->limit(20);
      
  $dg = Datagrid::source($get_product);

  $dg->filter('keyword', function($query, $value){
    if($value != ''){
        return $query->where(function($q) use ($value){
            $q->where('product_master.name', 'ilike', '%'.$value.'%')->orWhere('product_master.code', 'ilike', '%'.$value.'%');
                return $q;
            });
        }
    return $query;
  });

  $datagrid = $dg->build();

  $product_ids = [];
  $last_stock = [];

  foreach($datagrid['data'] as $row_stock) {
    $key = $row_stock['id'];
    $last_stock[$key]['begin'] = $row_stock['stock'];
    $last_stock[$key]['end'] = $row_stock['stock'];
    $product_ids[] = $row_stock['id'];
  }

  $data_stock_card = [];

  $warehouseId = [];
  if(!empty($warehouse)) {
    $warehouse = InventoryWarehouse::find($warehouse);
    $warehouseId = [$warehouse->id];
  } else {
    $warehouseId = InventoryWarehouse::all()->pluck('id')->toArray();
  }
  
  if(!empty($warehouseLocation)) {
    $listIdWarehouseLocation = [$warehouseLocation];
  } else {
    if(!empty($warehouseId)){
      $warehouseLocation = InventoryWarehouseLocation::whereIn('warehouse', $warehouseId)->get();
    }else{
      $warehouseLocation = InventoryWarehouseLocation::all();
    }
    $listIdWarehouseLocation = $warehouseLocation->pluck('id')->toArray();
  }
        
  //DB::enableQueryLog();
  
  $get_stock_card = InventoryStockMovementProduct::select([
    'inventory_stock_movement_product.id',
    'inventory_stock_movement_product.product',
    'inventory_stock_movement_product.qty',
    'inventory_stock_movement_product.qty AS move',
    DB::raw("'-' AS qty_in"),
    DB::raw("'-' AS qty_out"),
    'inventory_stock_movement_product.uom',
    'inventory_stock_movement.type',
    'inventory_stock_movement.type AS type_name',
    'inventory_stock_movement.status',
    'inventory_stock_movement.source',
    'inventory_stock_movement.destination',
    'inventory_stock_movement.description',
    'wls.warehouse as wh_source',
    'wld.warehouse as wh_destination',
    'inventory_stock_movement.code',
    'inventory_stock_movement_log.created_at as date',
    DB::raw("CONCAT(whs.name,'/',wls.name) AS source_name"),
    DB::raw("CONCAT(whd.name,'/',wld.name) AS destination_name"),
    'product_uom.name AS uom_name'
  ])
  ->join('inventory_stock_movement', 'inventory_stock_movement.id', '=', 'inventory_stock_movement_product.stock_movement')
  ->join('inventory_stock_movement_log', function($join){
    $join->on('inventory_stock_movement_log.stock_movement', '=', 'inventory_stock_movement.id')
    ->whereIn('inventory_stock_movement_log.status', ['3AA', '3IT']);
  })
  ->join('product_uom', 'product_uom.id', '=', 'inventory_stock_movement_product.uom')
  ->leftJoin('inventory_warehouse_location as wls', 'inventory_stock_movement.source', '=', 'wls.id')
  ->leftJoin('inventory_warehouse_location as wld', 'inventory_stock_movement.destination', '=', 'wld.id')
  ->leftJoin('inventory_warehouse as whs', 'wls.warehouse', '=', 'whs.id')
  ->leftJoin('inventory_warehouse as whd', 'wld.warehouse', '=', 'whd.id')
  ->whereIn('inventory_stock_movement_product.product', $product_ids);

  if($listIdWarehouseLocation) {
    $get_stock_card->where(function($q) use($listIdWarehouseLocation) {
      $q->whereIn('inventory_stock_movement.source', $listIdWarehouseLocation)
      ->orWhereIn('inventory_stock_movement.destination', $listIdWarehouseLocation);
    });  
  }

  $get_stock_card->whereBetween('inventory_stock_movement_log.created_at', [$fromDate, $toDateAdded])
  ->orderBy('inventory_stock_movement_log.created_at', 'ASC')
  ->orderBy('inventory_stock_movement_product.id', 'ASC');

  $get_stock_card = $get_stock_card->get()->toArray();

  // $addSlashes = str_replace('?', "'?'", $get_stock_card->toSql());
  // dd(vsprintf(str_replace('?', '%s', $addSlashes), $get_stock_card->getBindings()));

  foreach($get_stock_card as $key => $value) {


    //delete later
    //$value['description'] = $value['id'];

    $value['date'] = Carbon::parse($value['date'])->format('d/m/Y');

    $stock_akhir = isset($last_stock[$value['product']]['end']) ? $last_stock[$value['product']]['end'] : 0;

    if(!isset($data_stock_card[$value['product']])) {
      $data_stock_card[$value['product']] = [];
    }

    $increment = 0;
    if($value['type'] == 'AD') {
      if($value['qty'] > 0) {
        $value['qty_in'] = $value['qty'];
      } else {
        $value['qty_out'] = $value['qty'] * -1;
      }
      $increment = $value['qty'];
    }
    elseif($value['type'] == 'IT') {
      if(in_array($value['source'], $listIdWarehouseLocation)) {
        if(!in_array($value['destination'], $listIdWarehouseLocation)) {
          $value['qty_out'] = $value['qty'];
          $increment = $value['qty'] * -1;
        }
      }
      else {
        if(!in_array($value['source'], $listIdWarehouseLocation)) {
          $value['qty_in'] = $value['qty'];
          $increment = $value['qty'];
        }
      }
    }

    $currentUom = collect($dataUom)->where('id', $value['uom'])->first();

    $value['qty'] = number_format($value['qty'] * $currentUom['ratio'], 2);
    $value['move'] = $value['qty'];
    $value['qty_in'] = ($value['qty_in'] != '-') ? number_format($value['qty_in'] * $currentUom['ratio'], 2) : '-';
    $value['qty_out'] = ($value['qty_out'] != '-') ? number_format($value['qty_out'] * $currentUom['ratio'], 2) : '-';

    $basicUnit = collect($dataUom)->where('type', 'd')->where('uom_category', $currentUom['uom_category'])->first();
    if(!empty($basicUnit)){
      $value['uom_name'] = $basicUnit['name'];
    }

    $stock_akhir += $increment * $currentUom['ratio'];

    $last_stock[$value['product']]['end'] = $stock_akhir; 

    $value['stock_akhir'] = $stock_akhir;

    // if($value['id'] == '5b3d68f6a020fc75ab') {
    //   dd($value);
    // }

    $data_stock_card[$value['product']][] = collect($value)->toArray();
  }

  //dd($get_stock_card);

  foreach($datagrid['data'] as $key => $item) {
    $datagrid['data'][$key]['stock_card'] = isset($data_stock_card[$item['id']])?$data_stock_card[$item['id']]:[];
    $datagrid['data'][$key]['last_stock'] = isset($last_stock[$item['id']])?$last_stock[$item['id']]:[];

    // if($item['id'] == '5a4ba4f94731a1349f') {
    //   dd($data_stock_card[$item['id']]);
    // }
  }

  $export = Input::get('export');
  $export_type = Input::get('format');
  if(!empty($export)){
    ini_set('memory_limit', '2048M');
    $data  = $datagrid['data'];	  
          
    if($export_type == 'excel') {
      Excel::create('Laporan Stock Card '.date('Y-m-d', strtotime($fromDate)).' - '.date('Y-m-d', strtotime($toDate)), function($excel) use ($data, $warehouseName, $warehouseLocationName, $fromDate, $toDate, $category, $keyword) {

        $excel->sheet('Report', function($sheet) use ($data, $warehouseName, $warehouseLocationName, $fromDate, $toDate, $category, $keyword) {
            $excelWarehouse = 'Gudang: ';
            if($warehouseName || $warehouseLocationName) {
              $excelWarehouse .= $warehouseName.' / '.$warehouseLocationName;
            }

            $rows = array();
            $rows[] = ['Laporan Stock Card', $excelWarehouse];
            $rows[] = [null, 'Tanggal: '.date('d/m/Y', strtotime($fromDate)).' - '.date('d/m/Y', strtotime($toDate))];
            if($category) {
              $rows[] = [null, 'Kategori: '.$category->name];
            }
            $rows[] = [];

            $merged_rows = [];
            foreach($data as $val) {
              $rows[] = ['Kode: '.$val['code']];
              $merged_rows[] = count($rows);
              $rows[] = ['Nama Barang: '.$val['name']];
              $merged_rows[] = count($rows);
              $rows[] = ['Date','No','Description','Source','Destination','In','Out','Stock'];
              $sheet->cell('A'.count($rows).':'.'H'.count($rows), function($cells) {
                  $cells->setFontWeight('bold');
              });
              $rows[] = ['Stok Awal', '','','','','','', (float)$val['last_stock']['begin']];
              $sheet->cell('A'.count($rows), function($cells) {
                  $cells->setFontWeight('bold');
              });
              $sheet->cell('H'.count($rows), function($cells) {
                  $cells->setAlignment('right');
              });
              foreach ($val['stock_card'] as $dt) {
                $rows[] = [$dt['date'], $dt['code'], $dt['description'], $dt['source_name'], $dt['destination_name'], (float)$dt['qty_in'], (float)$dt['qty_out'], (float)$dt['stock_akhir']];
                $sheet->cell('H'.count($rows), function($cells) {
                    $cells->setAlignment('right');
                });
              }
              $rows[] = ['Stok Akhir', '','','','','','', (float)$val['last_stock']['end']];
              $sheet->cell('A'.count($rows), function($cells) {
                  $cells->setFontWeight('bold');
              });
              $sheet->cell('H'.count($rows), function($cells) {
                  $cells->setAlignment('right');
              });
              $rows[] = [];
            }

            $sheet->fromArray($rows, null, 'A1', true, false);

            $sheet->getStyle('A1')->applyFromArray(array(
              'font' => array(
                  'bold' => true,
                  'size' => 14
              )
            ));

            $sheet->setWidth(array(
              'A'     =>  20,
              'B'     =>  20,
              'C'     =>  50,
              'D'     =>  30,
              'E'     =>  30,
              'F'     =>  10,
              'G'     =>  10,
              'H'     =>  10,
            ));
            $sheet->mergeCells('B1:H1');
            $sheet->mergeCells('B2:H2');
            if($category) {
              $sheet->mergeCells('B3:H3');
            }
            foreach($merged_rows as $row) {
              $sheet->mergeCells('A'.$row.':H'.$row);
            }
        });
      })->export('xls');
    } else {
      $pdf = PDF::loadView('export.report-stock-card', compact('data', 'warehouseName', 'warehouseLocationName', 'fromDate', 'toDate', 'category', 'keyword'));
      return $pdf->download('Laporan Stock Card '.date('Y-m-d', strtotime($fromDate)).' - '.date('Y-m-d', strtotime($toDate)).'.pdf');
      
      //return view('export.report-stock-card', compact('data', 'warehouseName', 'warehouseLocationName', 'fromDate', 'toDate', 'category', 'keyword'));      
    }
    return;
  }

  $category = ProductCategory::getOption();
  $datagrid['optionCategory'] = $category;
  $datagrid['optionWarehouse'] = $optionWarehouse;
  $datagrid['title'] = 'Laporan Stock Card';
  return response()->json($datagrid);
  }

  function subArraysToString($ar, $sep = ', ') {
      $str = '';
      foreach ($ar as $val) {
          $str .= implode($sep, $val);
          $str .= $sep; // add separator between sub-arrays
      }
      $str = rtrim($str, $sep); // remove last separator
      return $str;
  }

  public function getStock($products, $whloc = null, $date){

      $data = [];
      $inventoryStock = InventoryStock::select([
      'inventory_stock.product',
      'inventory_stock.uom',
      'inventory_stock.warehouse_location',
      'inventory_stock.begin_qty',
      'inventory_stock.plus_qty',
      'inventory_stock.min_qty',
      'inventory_stock.end_qty'
    ])
    ->join('inventory_warehouse_location', 'inventory_warehouse_location.id', '=', 'inventory_stock.warehouse_location')
    ->whereIn('inventory_stock.product', $products)
    ->whereDate('inventory_stock.date', $date);
    if(!empty($whloc)) {
      $inventoryStock->whereIn('inventory_warehouse_location.id', $whloc);
    }
    $inventoryStock = $inventoryStock->get();
    if(count($inventoryStock) > 0){
      foreach($inventoryStock as $val){
        if(!isset($data[$val['product']])) {
          $data[$val['product']] = [
            'begin_qty' => 0,
            'end_qty' => 0
          ];
        }
        $data[$val['product']]['begin_qty'] += $val['begin_qty'];
        $data[$val['product']]['end_qty'] += $val['end_qty'];
      }
    }
    
    return $data;
  }
}
