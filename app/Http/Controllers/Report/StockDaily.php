<?php
namespace App\Http\Controllers\Report;

use App\Models\ProductMaster;
use App\Models\ProductCategory;
use App\Models\ProductUomCategory;
use App\Models\ProductUom;
use App\Models\ProductSupplier;
use App\Models\ProductAttribute;
use App\Models\ProductAttributeValue;
use App\Models\ProductProduct;
use App\Models\ProductVariant;
use App\Models\ProductBom;
use App\Models\ProductAlternative;
use App\Models\InventoryStock;
use App\Models\InventoryStockMovement;
use App\Models\InventoryStockMovementProduct;
use App\Models\InventoryWarehouse;
use App\Models\InventoryWarehouseLocation;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Params;
use Carbon\Carbon;
use Excel;

class StockDaily{

  // Params: Product ID
  public function index(){

    $date = Input::get('date');
    if(empty($date)){
      $date = date('Y-m-d');
    }

    $dataUom = ProductUom::select(['id', 'code', 'name', 'uom_category', 'ratio', 'type'])->get()->toArray();

    $query = ProductProduct::getQuery();
    $query->orderBy('product_master.name', 'ASC');
    $dg = Datagrid::source($query);
    $dg->title('Laporan Perubahan Stok');
    $dg->filter('keyword', function($query, $value){
      if($value != ''){
        return $query->where(function($q) use ($value){
           $q->where('product_master.name', 'ilike', '%'.$value.'%')->orWhere('product_master.code', 'ilike', '%'.$value.'%');
           return $q;
        });
      }

      return $query;
    });
    $dg->filter('category', function($query, $value){
      if($value != '' || $value != null)
          return $query->where('product_master.category', $value);

      return $query;
    });
    $dg->filter('type', function($query, $value){
      if($value != '' || $value != null)
          return $query->where('product_master.type', $value);

      return $query;
    });
    $dg->add('code', 'Kode', true);
    $dg->add('name', 'Nama', true);
    $datagrid = $dg->build();


    $optionWarehouse = InventoryWarehouse::getOption();
    $warehouse = Input::get('warehouse');
    if(empty($warehouse)){
      $warehouse = $optionWarehouse[0]['id'];
    }
    $warehouse = InventoryWarehouse::find($warehouse);

    $warehouseLocation = InventoryWarehouseLocation::where('warehouse', $warehouse->id)->get();
    $listIdWarehouseLocation = collect($warehouseLocation)->pluck('id')->toArray();

    $data = ProductProduct::renderProductAttribute($datagrid['data']);

    if(count($data) > 0){
      foreach($data as $key => $val){

        $totalReceipt = $this->getTotalStock($val['id'], 'RC', ['3RA'], $listIdWarehouseLocation, $dataUom, $date);
        $totalAdjustment = $this->getTotalStock($val['id'], 'AD', ['3AA'], $listIdWarehouseLocation, $dataUom, $date);
        $totalDelivery = $this->getTotalStock($val['id'], 'DO', ['4DD'], $listIdWarehouseLocation, $dataUom, $date);
        $totalChasier = $this->getTotalStock($val['id'], 'CH', ['1CS'], $listIdWarehouseLocation, $dataUom, $date);
        $getStock = $this->getStock($val['id'], $warehouse->id, $date);
        $val['begin_qty'] = (empty($getStock)) ? 0 : $getStock['begin_qty'];
        $val['total_receipt'] = $totalReceipt;
        $val['total_adjustment'] = $totalAdjustment;
        $val['total_delivery'] = $totalDelivery;
        $val['total_chasier'] = $totalChasier;
        $val['end_qty'] = $val['begin_qty'] + ( ($totalReceipt + $totalAdjustment) - ($totalDelivery + $totalChasier));

        $currentUom = collect($dataUom)->where('id', $val['uom'])->first();
        $basicUnit = collect($dataUom)->where('type', 'd')->where('uom_category', $currentUom['uom_category'])->first();
        if(!empty($basicUnit)){
          $val['uom_name'] = $basicUnit['name'];
        }

        $data[$key] = $val;
      }
    }
    $datagrid['data'] = $data;

    $datagrid['optionCategory'] = ProductCategory::getOption();
    $datagrid['optionType'] = Params::getOption('OPTION_PRODUCT_TYPE');
    $datagrid['optionWarehouse'] = $optionWarehouse;

    $export = Input::get('export');
    if(!empty($export)){
      $date = Carbon::parse($date)->format('d/m/Y');

      Excel::create('Laporan Perubahan Stok', function($excel) use ($data, $warehouse, $date){
        $excel->sheet('Sheet 1', function($sheet) use ($data, $warehouse, $date) {
          $sheet->loadView('export.stock-daily', compact('data', 'warehouse', 'date'));
        });
      })->download('xls');
    }


    return response()->json($datagrid);

  }

  public function getTotalStock($product, $type, $status, $listIdWarehouseLocation, $dataUom, $date){

    if($type == 'RC'){
      $loc = 'destination';
    }else if(in_array($type, ['AD', 'CH', 'DO'])){
      $loc = 'source';
    }

    $data = InventoryStockMovementProduct::select([
      'inventory_stock_movement_product.qty',
      'inventory_stock_movement_product.uom',
      'inventory_stock_movement.type',
      'inventory_stock_movement.code',
      'inventory_stock_movement_product.product',
    ])
    ->join('inventory_stock_movement', 'inventory_stock_movement.id', '=', 'inventory_stock_movement_product.stock_movement')
    ->where('inventory_stock_movement.type', $type)
    ->whereIn('inventory_stock_movement.status', $status)
    ->whereIn('inventory_stock_movement.'.$loc, $listIdWarehouseLocation)
    ->where('inventory_stock_movement_product.product', $product)
    ->whereDate('inventory_stock_movement.date', $date)
    ->get()->toArray();

    $total = 0;
    if(count($data) > 0){
      foreach($data as $val){
        $currentUom = collect($dataUom)->where('id', $val['uom'])->first();
        $val['qty'] = $val['qty'] * $currentUom['ratio'];
        $total += $val['qty'];
      }
    }


    return $total;
  }

  public function getStock($product, $warehouse = null, $date){

    $beginQty = 0;
    $endQty = 0;

    $inventoryStock = InventoryStock::select([
      'inventory_stock.product',
      'inventory_stock.uom',
      'inventory_stock.warehouse_location',
      'inventory_stock.begin_qty',
      'inventory_stock.plus_qty',
      'inventory_stock.min_qty',
      'inventory_stock.end_qty'
    ])
    ->join('inventory_warehouse_location', 'inventory_warehouse_location.id', '=', 'inventory_stock.warehouse_location')
    ->where('inventory_stock.product', $product)
    ->whereDate('inventory_stock.date', $date);
    if(!empty($warehouse)){
      $inventoryStock->where('inventory_warehouse_location.warehouse', $warehouse);
    }
    $inventoryStock = $inventoryStock->get();
    if(count($inventoryStock) > 0){
      foreach($inventoryStock as $val){
        $beginQty += $val['begin_qty'];
        $endQty += $val['end_qty'];
      }
    }
    return [
      'begin_qty' => $beginQty,
      'end_qty' => $endQty
    ];

  }

}
