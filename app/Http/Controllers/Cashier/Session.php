<?php

namespace App\Http\Controllers\Cashier;

use App\Models\CashierSession;
use App\Models\SettingUser;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Params;
use Helper;
use Auth;
use Carbon\Carbon;

class Session extends Controller{

  public function index() {
    $query = CashierSession::select([
      'cashier_session.id',
      'cashier_session.code',
      'cashier_session.start',
      'cashier_session.end',
      'cashier_session.balance',
      'cashier_session.status',
      'cashier_session.status AS status_label',
      'setting_user.fullname'
    ])
    ->join('setting_user', 'setting_user.username', '=', 'cashier_session.created_by')
    ->where('cashier_session.company', Helper::currentCompany());

    $dg = Datagrid::source($query);
    $dg->title('Sesi');
    $dg->filter('keyword', function($query, $value){
      if($value != '')
          return $query->where('setting_user.fullname', 'ilike', '%'.$value.'%');

      return $query;
    });
    $dg->add('code', 'Kode Sesi');
    $dg->add('start', 'Mulai')->render(function($data){
      return Carbon::parse($data['start'])->format('d/m/Y H:i');
    });
    $dg->add('end', 'Selesai')->render(function($data){
      return Carbon::parse($data['end'])->format('d/m/Y H:i');
    });
    $dg->add('balance', 'Saldo Kas')->number();
    $dg->add('fullname', 'Penanggung Jawab');
    $dg->add('status_label', 'Status')->options([
      0 => 'Aktif',
      1 => 'Selesai'
    ]);
    $datagrid = $dg->build();
    return response()->json($datagrid);
  }

  public function create(){
    $form = $this->anyForm(new CashierSession());
    $form->pre(function($data){
      $data['code'] = CashierSession::generateId();
      $data['start'] = date('Y-m-d H:i:s');
      $data['status'] = 0;
      $data['company'] = Helper::currentCompany();
      return $data;
    });
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }

    $dataForm['data']['code'] = CashierSession::generateId();
    $dataForm['data']['start'] = Carbon::now()->format('d/m/Y H:i');
    $dataForm['data']['name'] = Auth::user()->fullname;
    return response()->json($dataForm);
  }

  public function modify($id = null) {
    $session = CashierSession::find($id);
    $form = $this->anyForm($session);
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }
    $user = SettingUser::where('username', $session->created_by)->first();
    $dataForm['data']['name'] = (!empty($user)) ? $user->fullname : '';
    $dataForm['data']['start'] = Carbon::parse($session->start)->format('d/m/Y H:i');
    $dataForm['data']['end'] = (!empty($session->end)) ? Carbon::parse($session->end)->format('d/m/Y H:i') : '';
    return response()->json($dataForm);
  }

  public function delete(){

    $id = Input::get('id');
    CashierSession::whereIn('id', $id)->delete();

    return response()->json([
      'status' => true
    ]);
  }

  public function anyForm($source){
    $form = FormBuilder::source($source);
    $form->title('Sesi');
    $form->add('code', 'Kode Sesi', 'text')->attributes([
      'readonly' => true
    ]);
    $form->add('start', 'Mulai', 'text')->attributes([
      'readonly' => true
    ]);
    $form->add('end', 'Selesai', 'text')->attributes([
      'readonly' => true
    ]);
    $form->add('balance', 'Saldo Kas', 'number')->rule('required');
    $form->add('name', 'Penanggung Jawab', 'text')->attributes([
      'readonly' => true
    ]);
    return $form;
  }

  public function close($id){
    CashierSession::where('id', $id)->update([
      'status' => 1,
      'end' => date('Y-m-d H:i:s')
    ]);

    return response()->json([
      'status' => true
    ]);
  }

}
