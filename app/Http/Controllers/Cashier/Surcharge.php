<?php

namespace App\Http\Controllers\Cashier;

use App\Models\CashierEdc;
use App\Models\CashierBank;
use App\Models\CashierSurcharge;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Params;

class Surcharge extends Controller
{

  public function index() {

    $query = CashierSurcharge::select([
      'cashier_surcharge.id',
      'cashier_surcharge.surcharge',
      'cashier_surcharge.payment_type',
      'cashier_bank.name AS bank_name',
      'cashier_edc.name AS edc',
    ])->join('cashier_edc', 'cashier_edc.id', '=', 'cashier_surcharge.edc')
    ->join('cashier_bank', 'cashier_bank.id', '=', 'cashier_surcharge.bank');

    $dg = Datagrid::source($query);
    $dg->title('Surcharge');
    $dg->filter('keyword', function($query, $value){
      if($value != '')
          return $query->where('cashier_edc.name', 'ilike', '%'.$value.'%');

      return $query;
    });
    $dg->add('edc', 'EDC');
    $dg->add('bank_name', 'Nama Bank');
    $dg->add('payment_type', 'Tipe Pembayaran')->options(Params::getJson('OPTION_PAYMENT_TYPE'));
    $dg->add('surcharge', 'Surcharge')->render(function($data){
      return $data['surcharge'].'%';
    });
    $datagrid = $dg->build();
    return response()->json($datagrid);
  }

  public function create(){
    $form = $this->anyForm(new CashierSurcharge());
    if($form->hasRequest()){
      $valid = $this->validateData();
      if(!$valid){
        return response()->json([
          'status' => false,
          'messages' => 'Data sudah pernah diinput'
        ]);
      }
    }
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }

    $dataForm['optionEdc'] = CashierEdc::getOption();
    $dataForm['optionPaymentType'] = Params::getOption('OPTION_PAYMENT_TYPE');
    $dataForm['optionBank'] = CashierBank::getOption();
    // $dataForm['optionCardType'] = Params::getOption('OPTION_CARD_TYPE');
    return response()->json($dataForm);
  }

  public function modify($id = null) {

    $form = $this->anyForm(CashierSurcharge::find($id));
    if($form->hasRequest()){
      $valid = $this->validateData($id);
      if(!$valid){
        return response()->json([
          'status' => false,
          'messages' => 'Data sudah pernah diinput'
        ]);
      }
    }
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }


    $dataForm['optionEdc'] = CashierEdc::getOption();
    $dataForm['optionBank'] = CashierBank::getOption();
    $dataForm['optionPaymentType'] = Params::getOption('OPTION_PAYMENT_TYPE');
    // $dataForm['optionCardType'] = Params::getOption('OPTION_CARD_TYPE');
    return response()->json($dataForm);
  }

  public function validateData($id = null){
    $edc = Input::get('edc');
    $bank = Input::get('bank');

    $checkData = CashierSurcharge::where(['edc' => $edc, 'bank' => $bank]);
    if(!empty($id)){
      $checkData->where('id', '!=', $id);
    }
    $checkData = $checkData->first();
    if(empty($checkData)){
      return true;
    }else{
      return false;
    }
  }

  public function delete(){

    $id = Input::get('id');
    CashierSurcharge::whereIn('id', $id)->delete();

    return response()->json([
      'status' => true
    ]);
  }

  public function anyForm($source){
    $form = FormBuilder::source($source);
    $form->title('EDC');
    $form->add('edc', 'EDC', 'select')->attributes([
      'ng-options' => 'item.id as item.text for item in response.optionEdc',
    ])->rule('required|max:25');
    $form->add('bank', 'Bank', 'select')->attributes([
      'ng-options' => 'item.id as item.text for item in response.optionBank',
    ])->rule('required|max:25');
    $form->add('payment_type', 'Tipe Pembayaran', 'select')->attributes([
      'ng-options' => 'item.id as item.text for item in response.optionPaymentType',
    ])->rule('required|max:25');
    // $form->add('card_type', 'Tipe Kartu', 'select')->attributes([
    //   'ng-options' => 'item.id as item.text for item in response.optionCardType',
    // ])->rule('required|max:60');
    $form->add('surcharge', 'Surcharge', 'number')->rule('required|max:100');
    return $form;
  }

}
