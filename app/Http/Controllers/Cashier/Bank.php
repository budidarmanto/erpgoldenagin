<?php

namespace App\Http\Controllers\Cashier;

use App\Models\CashierBank;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;

class Bank extends Controller
{

  public function index() {

    $dg = Datagrid::source(CashierBank::query());
    $dg->title('Bank');
    $dg->filter('keyword', function($query, $value){
      if($value != '')
          return $query->where('code', 'ilike', '%'.$value.'%')->orWhere('name', 'ilike', '%'.$value.'%');

      return $query;
    });
    $dg->add('code', 'Kode Bank');
    $dg->add('name', 'Nama Bank');
    $datagrid = $dg->build();
    return response()->json($datagrid);
  }

  public function create(){
    $form = $this->anyForm(new CashierBank());
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }
    return response()->json($dataForm);
  }

  public function modify($id = null) {

    $form = $this->anyForm(CashierBank::find($id));
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }
    return response()->json($dataForm);
  }

  public function delete(){

    $id = Input::get('id');
    CashierBank::whereIn('id', $id)->delete();

    return response()->json([
      'status' => true
    ]);
  }

  public function anyForm($source){
    $form = FormBuilder::source($source);
    $form->title('Bank');
    $form->add('code', 'Kode Bank', 'text')->rule('required|max:60');
    $form->add('name', 'Nama Bank', 'text')->rule('required|max:60');
    return $form;
  }

}
