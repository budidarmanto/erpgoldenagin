<?php

namespace App\Http\Controllers\Product;

use App\Models\ProductCategory;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;

class Category extends Controller
{

  public function index() {

    $dg = Datagrid::source(ProductCategory::orderBy('left'));
    $dg->title('Kategori Produk');
    $dg->filter('keyword', function($query, $value){
      if($value != '')
          return $query->where('name', 'ilike', '%'.$value.'%');

      return $query;
    });

    $dg->add('name', 'Kategori', false)->render(function($data){
      return str_repeat('&nbsp;&nbsp;', $data['depth']).' '.$data['name'];
    });
    $datagrid = $dg->build();
    return response()->json($datagrid);
  }

  public function create(){
    $form = $this->anyForm(new ProductCategory());
    $form->pre(function($data){
      $data['revision'] = 1;
      return $data;
    });
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }

    $dataForm['optionCategory'] = ProductCategory::getOption(null, true);
    return response()->json($dataForm);
  }

  public function modify($id = null) {
    $productCategory = ProductCategory::find($id);
    $form = $this->anyForm($productCategory);
    $form->pre(function($data) use($productCategory){
      $data['revision'] = $productCategory->revision + 1;
      return $data;
    });
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }

    $dataForm['optionCategory'] = ProductCategory::getOption(null, true);
    return response()->json($dataForm);
  }

  public function delete(){

    $id = Input::get('id');
    ProductCategory::whereIn('id', $id)->delete();

    return response()->json([
      'status' => true
    ]);
  }

  public function anyForm($source){
    $form = FormBuilder::source($source);
    $form->title('Kategori Produk');
    $form->add('name', 'Kategori', 'text')->rule('required|max:100');
    $form->add('parent', 'Induk', 'select')->attributes([
      'ng-options' => 'item.id as item.text for item in response.optionCategory',
      'default-option' => "--  Kategori Induk  --"
    ]);
    return $form;
  }

}
