<?php

namespace App\Http\Controllers\Product;

use App\Models\ProductMaster;
use App\Models\ProductCategory;
use App\Models\ProductUomCategory;
use App\Models\ProductUom;
use App\Models\ProductSupplier;
use App\Models\ProductAttribute;
use App\Models\ProductAttributeValue;
use App\Models\ProductProduct;
use App\Models\ProductVariant;
use App\Models\ProductBom;
use App\Models\SettingTax;
use App\Models\ProductAlternative;
use App\Models\ManufactureRouting;
use App\Http\Controllers\Controller;
use App\Models\AppConfig;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Params;
use Auth;
use Excel;

class Master extends Controller
{

  public function index() {

    // $listCategory = Auth::user()->authCategory();

    $data = ProductMaster::select(['product_uom.name as uom_name', 'product_master.name as name', 'product_master.code', 'product_master.id', 'product_master.active', 'product_category.name AS category_name', 'product_master.barcode', 'product_master.sale_price', 'product_master.picture AS picture'])
    ->join('product_uom', 'product_uom.id', '=', 'product_master.uom')
    ->join('product_category', 'product_category.id', '=', 'product_master.category')
    // ->whereIn('product_master.category', $listCategory)
    ->orderBy('product_master.created_at', 'DESC');
    $dg = Datagrid::source($data);
    $dg->title('Barang/Jasa');
    $dg->filter('keyword', function($query, $value){
      if($value != '')
          return $query->where('product_master.name', 'ilike', '%'.$value.'%')->orWhere('product_master.code', 'ilike', '%'.$value.'%');

      return $query;
    });
    $dg->filter('category', function($query, $value){
      if($value != '' || $value != null){
        $category = ProductCategory::find($value);
        $listCategory = $category->getDescendantsAndSelf()->pluck('id')->toArray();
        $query->whereIn('product_master.category', $listCategory);
      }

      return $query;
    });
    $dg->filter('type', function($query, $value){
      if($value != '' || $value != null)
          return $query->where('product_master.type', $value);

      return $query;
    });

    // $dg->add('picture', '', false)->render(function($dt){
    //
    // });
    $dg->add('code', 'Kode');
    $dg->add('name', 'Nama');
    $dg->add('uom_name', 'Satuan Ukuran');
    $dg->add('active', 'Aktif')->options([1 => 'Aktif', 0 => 'Tidak Aktif']);
    $datagrid = $dg->build();

    $category = ProductCategory::getOption();
    $datagrid['optionCategory'] = $category;
    $datagrid['optionType'] = Params::getOption('OPTION_PRODUCT_TYPE');
    $data = $datagrid['data'];

    $datagrid['data'] = collect($data)->map(function($dt){
      $img = '<img src="https://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image"  style="width: 100%;"/>';
      if($dt['picture'] != null){
          $img = '<img src="img/product/'.$dt['picture'].'" style="width: 100%;" />';
      }

      $img = '<div class="img-wrapper" style="width: 100px;">'.$img.'</div>';

      $dt['picture'] = $img;

      return $dt;
    });



    // dd($data);
    $export = Input::get('export');
    if(!empty($export)){
      Excel::create('Product', function($excel) use ($data){
        $excel->sheet('Sheet 1', function($sheet) use ($data) {
          $sheet->loadView('export.product', compact('data'));
        });
      })->download('xls');
    }

    return response()->json($datagrid);
  }

  public function create(){
    $form = $this->anyForm(new ProductMaster());
    $form->pre(function($data){
      $data['revision'] = 1;
      return $data;
    });
    if($form->hasRequest()){
      $codeExists = ProductMaster::productCodeExists(Input::get('code'));
      if($codeExists){
        return response()->json([
          'status' => false,
          'messages' => 'Kode produk sudah terdaftar!'
        ]);
      }
    }

    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){

        $this->processDataAfterSave($form);

        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }
    $dataForm = $this->getDataResponse($dataForm);
    return response()->json($dataForm);
  }

  public function modify($id = null) {

    $product = ProductMaster::find($id);
    $form = $this->anyForm($product);
    $form->pre(function($data) use($product){
      $data['revision'] = $product->revision + 1;
      return $data;
    });

    if($form->hasRequest()){
      $codeExists = ProductMaster::productCodeExists(Input::get('code'), $id);
      if($codeExists){
        return response()->json([
          'status' => false,
          'messages' => 'Kode produk sudah terdaftar!'
        ]);
      }
    }
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){

        $this->processDataAfterSave($form);

        return response()->json([
          'status' => true,
          'dataSupplier' => $this->getDataSupplier($id),
          'dataVariant' => $this->getDataVariant($id),
          'dataBom' => $this->getDataBom($id)
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }
    $uom = ProductUom::find($product->uom);
    $dataForm['data']['uom_category'] = (!empty($uom)) ? $uom->uom_category : '';
    $dataForm = $this->getDataResponse($dataForm);
    $dataForm['dataSupplier'] = $this->getDataSupplier($id);
    $dataForm['dataVariant'] = $this->getDataVariant($id);
    $dataForm['dataBom'] = $this->getDataBom($id);
    $dataForm['dataAlternative'] = $this->getDataAlternative($id);
    return response()->json($dataForm);
  }

  public function delete(){

    $id = Input::get('id');
    ProductMaster::whereIn('id', $id)->delete();

    return response()->json([
      'status' => true
    ]);
  }

  public function anyForm($source){
    $form = FormBuilder::source($source);
    $form->title('Barang/Jasa');
    $form->add('picture', 'Foto Produk', 'image')->attributes([
      'moveTo' => 'img/product'
    ]);
    $form->add('code', 'Kode', 'text')->rule('required|max:50');
    $form->add('name', 'Nama', 'text')->rule('required|max:200');
    $form->add('barcode', 'Barkode', 'text')->rule('max:100');
    $form->add('category', 'Kategori', 'select')->attributes([
      'ng-options' => 'item.id as item.text for item in response.optionCategory',
    ])->rule('required');

    $form->add('uom', 'Satuan Ukuran', 'select')->attributes([
      'ng-options' => 'item.id as item.text for item in response.optionUom',
    ])->rule('required');

    $form->add('uom_category', 'Kategori Satuan Ukuran', 'select')->attributes([
      'ng-options' => 'item.id as item.text for item in response.optionUomCategory',
      'ng-change' => 'changeUomCategory()'
    ])->rule('required');


    $form->add('type', 'Tipe', 'select')->attributes([
      'ng-options' => 'item.id as item.text for item in response.optionType',
    ])->rule('required');
    
    $form->add('sale_price', 'Harga Jual', 'number')->rule('required');
    $form->add('tax', 'Pajak', 'select')->attributes([
      'ng-options' => 'item.id as item.text for item in response.optionTax',
      'default-option' => 'Pilihan Pajak'
    ]);

    $form->add('description', 'Deskripsi', 'textarea');
    // $form->add('discount', 'Diskon', 'number')->rule('max:100');
    $form->add('active', 'Aktif', 'switch');
    // $form->add('pos', 'Tersedia pada aplikasi kasir', 'switch');
    $form->add('weight', 'Berat', 'number');
    $form->add('routing', 'Routing', 'select')->attributes([
      'ng-options' => 'item.id as item.text for item in response.optionRouting',
    ]);

    return $form;
  }

  public function getDataResponse($dataForm){
    $dataForm['optionType'] = $this->getOption('OPTION_PRODUCT_TYPE');
    $dataForm['optionBomType'] = $this->getOption('OPTION_BOM_TYPE');
    $dataForm['optionCategory'] = ProductCategory::getOption();
    $dataForm['optionUomCategory'] = ProductUomCategory::getOption();
    $dataForm['optionUom'] = ProductUom::getOption();
    $dataForm['optionRouting'] = ManufactureRouting::getOption();
    $dataForm['optionAttribute'] = ProductAttribute::getOption();
    $dataForm['optionAttributeValue'] = ProductAttributeValue::getOption();
    $dataForm['optionTax'] = SettingTax::getOption();
    $dataForm['dataSupplier'] = [];
    $dataForm['dataVariant'] = [];
    $dataForm['dataBom'] = [];
    $dataForm['dataAlternative'] = [];
    $useVariant = Params::get('PRODUCT_VARIANT');
    $dataForm['useVariant'] = ($useVariant == '1') ? true : false;
    return $dataForm;
  }

  public function processDataAfterSave($form = null){
    if(empty($form)){
      return;
    }

    $useVariant = Params::get('PRODUCT_VARIANT');
    $useVariant = ($useVariant == '1') ? true : false;

    //save supplier
    $listId = [];
    if(!empty($form->dataPost['dataSupplier'])){
      foreach($form->dataPost['dataSupplier'] as $val){
        $productSupplier = new ProductSupplier();
        $val['product'] = $form->model->id;
        if(!empty($val['id'])){
          $productSupplier = ProductSupplier::find($val['id']);
        }
        $productSupplier->fill($val);
        $productSupplier->save();
        $listId[] = $productSupplier->id;
      }
    }
    ProductSupplier::where('product', $form->model->id)->whereNotIn('id', $listId)->delete();

    //save variant
    ProductVariant::where('product', $form->model->id)->delete();
    if(!empty($form->dataPost['dataVariant'])){
      foreach($form->dataPost['dataVariant'] as $val){
        $attribute = $val['attribute'];
        $attributeValue = $val['attribute_value'];
        if(!empty($attributeValue)){
          foreach($attributeValue as $vval){
            $productVariant = new ProductVariant();
            $productVariant->fill([
              'product' => $form->model->id,
              'attribute' => $attribute,
              'attribute_value' => $vval
            ]);
            $productVariant->save();
          }
        }
      }
    }

    // Save BOM
    $listId = [];
    if(!empty($form->dataPost['dataBom'])){
      foreach($form->dataPost['dataBom'] as $val){
        $productBom = new ProductBom();
        $val['product_master'] = $form->model->id;
        if(!empty($val['id'])){
          $productBom = ProductBom::find($val['id']);
        }
        $productBom->fill($val);
        $productBom->save();
        $listId[] = $productBom->id;
      }
    }
    ProductBom::where('product_master', $form->model->id)->whereNotIn('id', $listId)->delete();

    // Generate Variant
    $this->generateVariant($form->model->id);

    // Save Alternative
    $listId = [];

    if(!empty($form->dataPost['dataAlternative'])){
      foreach($form->dataPost['dataAlternative'] as $val){
        $productAlternative = new ProductAlternative();
        $val['product_master'] = $form->model->id;
        if(!empty($val['id'])){
          $productAlternative = ProductAlternative::find($val['id']);
        }
        $productAlternative->fill($val);
        $productAlternative->save();

        $listId[] = $productAlternative->id;

        // Applu to Target
        $productTarget = ProductProduct::where('id', $val['product'])->first();
        $currentProduct = ProductProduct::where('product', $form->model->id)->get();
        $currentProduct = collect($currentProduct)->pluck('id')->toArray();
        ProductAlternative::where('product_master', $productTarget->product)->whereIn('product', $currentProduct)->delete();
        if(count($productTarget) > 0){
          foreach($currentProduct as $vcurrent){
            ProductAlternative::create([
              'product_master' => $productTarget->product,
              'product' => $vcurrent,
            ]);
          }
        }

      }
    }
    ProductAlternative::where('product_master', $form->model->id)->whereNotIn('id', $listId)->delete();


  }

  public function getDataSupplier($productId){
    $dataSupplier = ProductSupplier::select(['product_supplier.id', 'product_supplier.product', 'product_supplier.supplier', 'product_supplier.price', 'product_supplier.tax', 'product_supplier.discount', 'crm_contact.name'])->join('crm_contact', 'crm_contact.id', '=', 'product_supplier.supplier')->where('product', $productId)->get()->toArray();
    return $dataSupplier;
  }

  public function getDataVariant($productId){
    $data = [];

    $dataVariant = ProductVariant::where('product', $productId)->get();
    if(count($dataVariant) > 0){
      $tmp = [];
      foreach($dataVariant as $val){
        $tmp[$val['attribute']][] = $val['attribute_value'];
      }

      if(!empty($tmp)){
        foreach($tmp as $key => $val){
          $data[] = [
            'attribute' => $key,
            'attribute_value' => $val
          ];
        }
      }
    }
    return $data;
  }

  public function getDataBom($productId){

    $dataBom = ProductBom::select([
      'product_bom.id', 'product_bom.product', 'product_bom.uom', 'product_bom.qty', 'product_uom.name AS uom_name'
    ])
    ->join('product_uom', 'product_bom.uom', '=', 'product_uom.id')->where('product_master', $productId)->get()->toArray();
    if(count($dataBom) > 0){
      $prductBomId = collect($dataBom)->pluck('product')->toArray();

      $dataProduct = ProductProduct::getData(function($query) use($productId, $prductBomId){
        $query->whereIn('product_product.id', $prductBomId);
        return $query;
      });
      foreach($dataBom as $key => $val){

        $getName = collect($dataProduct)->filter(function($data) use ($val){
          return $data['id'] == $val['product'];
        })->first();

        $dataBom[$key]['product_name'] = (isset($getName['name'])?$getName['name']:NULL);
        $dataBom[$key]['product_code'] = (isset($getName['code'])?$getName['code']:NULL);
      }

    }

    return $dataBom;
  }

  public function getDataAlternative($productId){

    $dataAlternative = ProductAlternative::where('product_master', $productId)->get()->toArray();

    if(count($dataAlternative) > 0){
      $productAlternativeId = collect($dataAlternative)->pluck('product')->toArray();

      $dataProduct = ProductProduct::getData(function($query) use($productId, $productAlternativeId){
        $query->whereIn('product_product.id', $productAlternativeId);
        return $query;
      });

      foreach($dataAlternative as $key => $val){
        $getName = collect($dataProduct)->filter(function($data) use ($val){
          return $data['id'] == $val['product'];
        })->first();

        $getName = (!empty($getName)) ? $getName['name'] : '';
        $dataAlternative[$key]['product_name'] = $getName;
      }

    }

    return $dataAlternative;
  }

  public function generateVariant($productId){

    $existingProduct = ProductProduct::where([
      'product' => $productId,
      'active' => true
    ])->get();

    // ProductProduct::where([
    //   'product' => $productId,
    //   'active' => true
    // ])->update([
    //   'active' => false
    // ]);

    $useVariant = Params::get('PRODUCT_VARIANT');
    $useVariant = ($useVariant == '1') ? true : false;

    // DO NOT DELETE!!
    // $dataVariant = ProductVariant::select([
    //   'product_attribute.name as attName',
    //   'product_attribute_value.name as attValName',
    //   'product_attribute.id as attribute',
    //   'product_attribute_value.id as attribute_value'
    // ])
    // ->join('product_attribute_value', 'product_attribute_value.id', '=', 'product_variant.attribute_value')
    // ->join('product_attribute', 'product_attribute.id', '=', 'product_attribute_value.attribute')
    // ->where('product', $productId)->get();


    // DO NOT DELETE!!
    $dataVariant = ProductVariant::where('product', $productId)->get();

    if(!$useVariant || count($dataVariant) <= 0){
      if(count($existingProduct) > 0){
        $existingProduct = $existingProduct[0];
        ProductProduct::where([
          'id' => $existingProduct['id']
        ])->update([
          'active' => true
        ]);
      }else{
        ProductProduct::create([
          'product' => $productId,
          'price' => 0,
          'active' => true
        ]);
      }

      return;
    }

    $tmp = [];
    if(count($dataVariant) > 0){
      foreach($dataVariant as $val){
        $tmp[$val['attribute']][] = $val['attribute_value'];
      }
    }

    $dataTree = $this->dataTree($tmp);
    $dataProduct = $this->processTree($dataTree, $productId);

    if(!empty($dataProduct)){
      foreach($dataProduct as $val){

        $attr = collect($val)->pluck('attribute_value')->all();
        $attr = collect($attr)->sort()->values()->all();

        $exists = false;
        $listId = [];
        if(count($existingProduct) > 0){
          foreach($existingProduct as $valEx){
            $dataJson = json_decode($valEx['attribute']);
            $dataJson = collect($dataJson)->pluck('attribute_value')->all();
            $dataJson = collect($dataJson)->sort()->values()->all();

            if($attr == $dataJson){
              $exists = true;
              $listId[] = $valEx['id'];
            }
          }
        }

        if(!empty($listId)){
          ProductProduct::whereIn('id', $listId)->update([
            'active' => true
          ]);
        }

        if(!$exists){
          ProductProduct::create([
            'product' => $productId,
            'attribute' => json_encode($val),
            'price' => 0,
            'active' => true
          ]);
        }

      }
    }
  }

  public function processTree($dataTree, $productId, $attribute = [], $index = 0){

    $allData = [];
    if(!empty($dataTree)){
      foreach($dataTree as $val){
        $data = $val['data'];
        $sub = $val['sub'];
        $newAttribute = $attribute;
        $newAttribute[] = $data;

        if(!empty($sub)){
          $getSub = $this->processTree($sub, $productId, $newAttribute, $index + 1);
          if(!empty($getSub)){
            foreach($getSub as $vgetSub){
              $allData[] = $vgetSub;
            }
          }
        }else{
          // if(!empty($attribute)){
          //   return [$newAttribute];
          // }
          if(!empty($newAttribute)){
              $allData[] = $newAttribute;
          }
        }
      }
    }

    return $allData;
  }

  public function dataTree($data = array(), $index = 0) {
      $d = [];
      if (!empty($data)) {
          foreach ($data as $key => $val) {
              unset($data[$key]);
              if (!empty($val)) {
                  foreach ($val as $kkey => $vval) {
                      $d[$key . '-' . $vval]['data'] = array(
                          'attribute' => $key,
                          'attribute_value' => $vval
                      );
                      $d[$key . '-' . $vval]['sub'] = $this->dataTree($data);
                  }
              }
              break;
          }
      }
      return $d;
  }

  public function getJson($key = ''){
      $dataConfig = AppConfig::all();
        $datatmp = [];
        foreach($dataConfig as $val){
            $datatmp[$val->code] = $val->value;
        }

      if(isset($datatmp[$key])){
          return json_decode($datatmp[$key], true);
      }
      return [];
    }

    public function getOption($key = ''){
      $data = $this->getJson($key);
      $options = [];
      if(!empty($data)){
        foreach($data as $key => $val){
          $options[] = [
            'id' => $key,
            'text' => $val
          ];
        }
      }

      return $options;
    }

}
