<?php

namespace App\Http\Controllers\Manufacture;

use App\Models\ManufactureRouting;
use App\Models\ManufactureRoutingOperation;
use App\Models\ManufactureWorkCenter;
use App\Models\InventoryWarehouseLocation;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Devplus\Helper\Helper;
use Devplus\Params\Params;
use App\Models\AppConfig;

class Routing extends Controller
{
    public function index() {
        $dg = Datagrid::source(ManufactureRouting::where('company', Helper::currentCompany()));
        $dg->title('Routing');
        $dg->filter('keyword', function($query, $value){
            if($value != '') {
                return $query->where('name', 'ilike', '%'.$value.'%')->orWhere('code', 'ilike', '%'.$value.'%');
            }
            return $query;
        });
        $dg->add('code', 'Kode');
        $dg->add('name', 'Routing');
        $datagrid = $dg->build();
        return response()->json($datagrid);
    }

    public function create(\Illuminate\Http\Request $request){
        $form = $this->anyForm(new ManufactureRouting());
        $form->title('Routing');
        $form->pre(function($data){
            $data['code'] = ManufactureRouting::generateId();
            $data['company'] = Helper::currentCompany();
            return $data;
        });
        $dataForm = $form->build();

        if($form->hasRequest()){
            if($form->saved()){
                //save Operation
                $this->processDataAfterSave($form, $request);

                return response()->json([
                  'status' => true
                ]);
            }else{
                return response()->json([
                  'errorMessage' => $form->validatorMessages
                ]);
            }
        }

        $dataForm['data']['code'] = ManufactureRouting::generateId();
        $dataForm['dataOperation'] = [];
        $dataForm['optionWorkCenter'] = ManufactureWorkCenter::getOption();
        $dataForm['optionLocation'] = InventoryWarehouseLocation::getAuthOption();
        #$dataForm['optionOperationReource'] = Params::getOption('OPTION_ROUTING_OPERATION_RESOURCE');
        #$dataForm['optionDurationUom'] = Params::getOption('OPTION_DURATION'); 
        
        

        $dataForm['optionOperationReource'] = $this->getOption('OPTION_ROUTING_OPERATION_RESOURCE');
        $dataForm['optionDurationUom'] = $this->getOption('OPTION_DURATION'); 
        return response()->json($dataForm);
    }

    public function getJson($key = ''){
      $dataConfig = AppConfig::all();
        $datatmp = [];
        foreach($dataConfig as $val){
            $datatmp[$val->code] = $val->value;
        }

      if(isset($datatmp[$key])){
          return json_decode($datatmp[$key], true);
      }
      return [];
    }

    public function getOption($key = ''){
      $data = $this->getJson($key);
      $options = [];
      if(!empty($data)){
        foreach($data as $key => $val){
          $options[] = [
            'id' => $key,
            'text' => $val
          ];
        }
      }

      return $options;
    }

    public function modify(\Illuminate\Http\Request $request, $id = null) {
        //return;
        $form = $this->anyForm(ManufactureRouting::find($id));
        $form->title('Routing');
        $dataForm = $form->build();

        if($form->hasRequest()){
            if($form->saved()){
                $this->processDataAfterSave($form, $request);
                return response()->json([
                    'status' => true
                ]);
            }else{
                return response()->json([
                    'errorMessage' => $form->validatorMessages
                ]);
            }
        }

        $dataOperation = ManufactureRoutingOperation::select([
            'manufacture_routing_operation.id',
            'manufacture_routing_operation.work_center',
            'manufacture_routing_operation.name',
            'manufacture_routing_operation.duration',
            'manufacture_routing_operation.duration_uom',
            'manufacture_routing_operation.capacity',
            'manufacture_routing_operation.resource',
            'manufacture_routing_operation.worksheet',
            'manufacture_routing_operation.description',
            'manufacture_routing_operation.sequence',
            'manufacture_routing_operation.next_sequence',
            'manufacture_work_center.name AS workcenter_name',
            \Illuminate\Support\Facades\DB::raw('CONCAT(\'' . url('/') . '/uploads/routing_operation_worksheet/' . '\', manufacture_routing_operation.id, manufacture_routing_operation.worksheet) as link'),
        ])
        ->join('manufacture_work_center', 'manufacture_work_center.id', '=', 'manufacture_routing_operation.work_center')
        ->where('manufacture_routing_operation.routing', $id)
        ->orderBy('sequence', 'ASC')
        ->get()->toArray();
        $dataForm['dataOperation'] = $dataOperation;
        $dataForm['optionWorkCenter'] = ManufactureWorkCenter::getOption();
        $dataForm['optionOperationReource'] = Params::getOption('OPTION_ROUTING_OPERATION_RESOURCE');
        $dataForm['optionDurationUom'] = Params::getOption('OPTION_DURATION');
        $dataForm['optionLocation'] = InventoryWarehouseLocation::getAuthOption();
        if(count($dataOperation) > 0) {
            $dataForm['lastSequence'] = $dataOperation[count($dataOperation) - 1]['sequence'];
        }
        return response()->json($dataForm);
    }

    public function processDataAfterSave($form, $request){
        $listId = [];
        if(!empty($form->dataPost['dataOperation'])){
            foreach($form->dataPost['dataOperation'] as $key => $val){
                $operation = new ManufactureRoutingOperation();

                if(!empty($val['id'])){
                    $operation = ManufactureRoutingOperation::find($val['id']);
                }
                $dataInput = [
                    'routing' => $form->model->id,
                    'work_center' => $val['work_center'],
                    'name' => $val['name'],
                    'duration' => $val['duration'],
                    'duration_uom' => $val['duration_uom'],
                    'description' => $val['description'],
                    'capacity' => $val['capacity'],
                    'resource' => $val['resource'],
                    'sequence' => $val['sequence'],
                    'next_sequence' => $val['next_sequence'] == 'null' ? null : intval($val['next_sequence']),
                    'worksheet' => $val['worksheet'],
                ];
                $operation->fill($dataInput);
                $saved = $operation->save();
                $listId[] = $operation->id;
                
                if($request->hasFile('dataOperation' . $key . 'worksheetFile')) {
                    $worksheet = $request->file('dataOperation' . $key . 'worksheetFile');

                    $filename = $operation->id . $worksheet->getClientOriginalName();
                    $uploaded = false;
                    if($saved) {
                        $uploaded = $worksheet->storeAs('uploads/routing_operation_worksheet', $filename, 'public_uploads');
                    }
                }
            }
        }
        
        $notUsedOperation = ManufactureRoutingOperation::where('routing', $form->model->id)->whereNotIn('id', $listId)->get();
        //dd($notUsedOperation->toArray());
        foreach($notUsedOperation as $data) {
            $filePath = public_path() . '/uploads/routing_operation_worksheet/' . $data->id . $data->worksheet;
            if(!empty($data->worksheet) && file_exists($filePath)) {
                unlink($filePath);
            }
            $data->delete();
        }
    }

    public function delete(){
        $id = Input::get('id');
        $routings = ManufactureRouting::whereIn('id', $id)->get();
        foreach($routings as $routing) {
            $routingOperation = ManufactureRoutingOperation::where('routing', $routing->id)->get();
            foreach($routingOperation as $data) {
                $filePath = public_path() . '/uploads/routing_operation_worksheet/' . $data->id . $data->worksheet;
                if(!empty($data->worksheet) && file_exists($filePath)) {
                    unlink($filePath);
                }
                $data->delete();
            }
            $routing->delete();
        }
        return response()->json([
            'status' => true
        ]);
    }

    public function anyForm($source){
        $form = FormBuilder::source($source);
        $form->title('Routing');
        $form->add('code', 'Kode', 'text')->rule('required|max:25')->attributes([
            'readonly' => true
        ]);
        $form->add('name', 'Nama Routing', 'text')->rule('required|max:100');
        $form->add('location_raw', 'Lokasi Bahan Baku', 'select')->rule('required')->attributes([
          'ng-options' => 'item.id as item.text for item in response.optionLocation',
        ]);
        $form->add('location_result', 'Lokasi Bahan Jadi', 'select')->rule('required')->attributes([
          'ng-options' => 'item.id as item.text for item in response.optionLocation',
        ]);
        $form->add('location_scrap', 'Lokasi Scrap', 'select')->rule('required')->attributes([
          'ng-options' => 'item.id as item.text for item in response.optionLocation',
        ]);
        $form->add('location_waste', 'Lokasi Waste', 'select')->rule('required')->attributes([
          'ng-options' => 'item.id as item.text for item in response.optionLocation',
        ]);
        $form->add('description', 'Keterangan', 'textarea');
        return $form;
    }
}
