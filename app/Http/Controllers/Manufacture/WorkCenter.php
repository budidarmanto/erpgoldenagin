<?php

namespace App\Http\Controllers\Manufacture;

use App\Models\ManufactureWorkCenter;
use App\Http\Controllers\Controller;
use App\Models\InventoryWarehouseLocation;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Devplus\Helper\Helper;

class WorkCenter extends Controller
{

    public function index() { 
        $dg = Datagrid::source(ManufactureWorkCenter::where('company', Helper::currentCompany()));
        $dg->title('Work Center');
        $dg->filter('keyword', function($query, $value){
          if($value != '')
              return $query->where('code', 'ilike', '%'.$value.'%')->orWhere('name', 'ilike', '%'.$value.'%');

          return $query;
        });
        $dg->add('code', 'Kode');
        $dg->add('name', 'Work Center');
        $datagrid = $dg->build();
        return response()->json($datagrid);
    }

    public function create(){
        $form = $this->anyForm(new ManufactureWorkCenter());
        $form->pre(function($input){
            $input['company'] = Helper::currentCompany();
            return $input;
        });
        $dataForm = $form->build();

        if($form->hasRequest()){
            if($form->saved()){
                return response()->json([
                    'status' => true
                ]);
            }
            else{
                return response()->json([
                    'status' => false,
                    'errorMessage' => $form->validatorMessages
                ]);
            }
        }
        $dataForm = $this->getDataResponse($dataForm);
        return response()->json($dataForm);
    }

    public function modify($id = null) {
        $form = $this->anyForm(ManufactureWorkCenter::find($id));
        $dataForm = $form->build();

        if($form->hasRequest()){
            if($form->saved()){
                return response()->json([
                    'status' => true
                ]);
            }else{
                return response()->json([
                    'status' => false,
                    'errorMessage' => $form->validatorMessages
                ]);
            }
        }
        $dataForm = $this->getDataResponse($dataForm);
        return response()->json($dataForm);
    }

    public function delete(){
        $id = Input::get('id');
        ManufactureWorkCenter::whereIn('id', $id)->delete();

        return response()->json([
            'status' => true
        ]);
    }

    public function anyForm($source){
        $form = FormBuilder::source($source);
        $form->title('Work Center');
        $form->add('code', 'Kode', 'text')->rule('required|max:60');
        $form->add('name', 'Work Center', 'text')->rule('required|max:60');
        $form->add('description', 'Deskripsi', 'textarea');
       /* $form->add('warehouse_location', 'Lokasi Penyimpanan', 'select')->attributes([
              'ng-options' => 'item.id as item.text for item in response.optionWarehouseLocation',
              'data-live-search' => true,
              'ng-hid' => true
        ])->rule('required');
        $form->add('capacity', 'Kapasitas', 'number')->attributes([
            'decimals' => 0
        ]);*/
        return $form;
    }
    
    
    public function getDataResponse($data){
        $data['optionWarehouseLocation'] = InventoryWarehouseLocation::getAuthOption();
        return $data;
    }
}
