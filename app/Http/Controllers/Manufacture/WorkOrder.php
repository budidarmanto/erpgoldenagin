<?php

namespace App\Http\Controllers\Manufacture;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Models\ManufactureOrder;
use App\Models\ManufactureWorkOrder;
use App\Models\ManufactureWorkOrderPending;
use App\Models\ManufactureOrderBom;
use App\Models\SettingWorkflow;
use App\Models\SettingUser;
use Devplus\Params\Params;
use Devplus\Datagrid\Datagrid;
use Devplus\Helper\Helper;
use Devplus\FormBuilder\FormBuilder;
use App\Models\ProductBom;
use Auth;
use Excel;

class WorkOrder extends Controller{
    public function index() {
        $workcenterList = Auth::user()->listWorkcenter();
        
        //dd($workcenterList);

        #$x = Auth::user()->id; echo "<pre> user : "; print_r($x); die;
        $query = ManufactureWorkOrder::select([
            DB::raw('manufacture_order.code as mo'),
            DB::raw('manufacture_routing_operation.name as ro'),
            DB::raw('manufacture_routing_operation.next_sequence'),
            DB::raw('manufacture_work_center.code as wc'),
            DB::raw('CONCAT(\'[\', product_master.code, \'] \', product_master.name) as product'),
            DB::raw('manufacture_order.qty as qty'),
            DB::raw('product_uom.name as uom'),
            DB::raw('setting_user.fullname as pic_name'),
            'manufacture_work_order.*',
        ])
        ->whereNotNull('manufacture_work_order.status')
        //->where('manufacture_work_order.status', '!=', '5WD')
        #->where('manufacture_order.pic', '=', Auth::user()->id)
        ->leftJoin('manufacture_order', 'manufacture_order.id', '=', 'manufacture_work_order.manufacture_order')
        ->leftJoin('product_master', 'product_master.id', '=', 'manufacture_order.product')
        ->leftJoin('product_uom', 'product_uom.id', '=', 'product_master.uom')
        ->leftJoin('manufacture_routing_operation', 'manufacture_routing_operation.id', '=', 'manufacture_work_order.routing_operation')
        ->leftJoin('manufacture_work_center', 'manufacture_work_center.id', '=', 'manufacture_routing_operation.work_center')
        ->leftJoin('setting_user', 'setting_user.id', '=', 'manufacture_order.pic')
        ->whereIn('manufacture_routing_operation.work_center', $workcenterList);

        $dateFrom = Input::get('from');
        $dateTo = Input::get('to');
        if(!empty($dateFrom)) {
            if(empty($dateTo)){
                $dateTo = $dateFrom;
            }
            $dateTo = Carbon::parse($dateTo)->addDay(1)->toDateTimeString();
            
            $query->where(function($q) use ($dateFrom, $dateTo) {
                $q->whereBetween('manufacture_work_order.date_start', [$dateFrom, $dateTo])
                        ->orWhereBetween('manufacture_work_order.date_end', [$dateFrom, $dateTo]);
                return $q;
            });
        }
        $query->orderBy('manufacture_work_order.created_at', 'asc');
        $dg = Datagrid::source($query);
        $dg->title('Work Order');
        $dg->filter('keyword', function($query, $value){
            if($value != ''){
                return $query->where(function($q) use ($value) {
                    $q->where('manufacture_routing_operation.name', 'ilike', '%'.$value.'%')
                            ->orWhere('manufacture_work_center.code', 'ilike', '%'.$value.'%')
                            ->orWhere('manufacture_work_center.name', 'ilike', '%'.$value.'%')
                            ->orWhere('manufacture_order.code', 'ilike', '%'.$value.'%')
                            ->orWhere('product_master.code', 'ilike', '%'.$value.'%')
                            ->orWhere('product_master.name', 'ilike', '%'.$value.'%');
                    return $q;
                });
            }

            return $query;
        });
        
        $dg->filter('workflow', function($query, $value){
            if($value != '') {
              return $query->where('manufacture_work_order.status', $value);
            }
            return $query;
        });

        $dg->add('ro', 'Work Order', true);
        $dg->add('date_start', 'Start', true)->render(function($data){
            return Carbon::parse($data['date_start'])->format('d/m/Y');
        });
        $dg->add('wc', 'Work Center', false);
        $dg->add('mo', 'Manufacture Order', false);
        $dg->add('product', 'Product', false);
        $dg->add('qty', 'Original Production Quantity', false)->render(function($data){
            return $data['qty'] . ' ' . $data['uom'];
        });
        $dg->add('status','Status');
        $datagrid = $dg->build();

        $export = Input::get('export');
		if(!empty($export)){
			ini_set('max_execution_time', 3600);
			ini_set('memory_limit', '2048M');
            $data  = $datagrid['data'];
            
            $statusList = [];
            $getStatus = SettingWorkflow::getOption('WO');
            foreach($getStatus as $status) {
                $statusList[$status['id']] = $status['label'];
            }

            Excel::create('Work Order '.date('d/m/Y H:i:s'), function($excel) use ($data, $dateTo, $dateFrom, $statusList) {

                $excel->sheet('Work Order', function($sheet) use ($data, $dateTo, $dateFrom, $statusList) {
                    
                    $status = null;
                    $keyword = null;

                    if(Input::get('workflow')) {
                        $status = Input::get('workflow');
                        $status = @$statusList[$status];
                    }
                    if(Input::get('keyword')) {
                        $keyword = Input::get('keyword');
                    }

                    $rows = array();
                    $rows[] = ['Work Order', '', '', '', '', '', '', date('d/m/Y H:i:s')];
                    if($dateTo) {
                        $rows[] = ['Tanggal: '.date('d/m/Y', strtotime($dateFrom)).' - '.date('d/m/Y', strtotime($dateTo. '-1 day'))];
                        $sheet->mergeCells('A'.count($rows).':I'.count($rows));
                    }

                    if($status) {
                        $rows[] = ['Status: '.$status];
                        $sheet->mergeCells('A'.count($rows).':I'.count($rows));
                    }
                    if($keyword) {
                        $rows[] = ['Keyword: '.$keyword];
                        $sheet->mergeCells('A'.count($rows).':I'.count($rows));
                    }
                    $rows[] = [];

                    $sheet->getStyle('A1')->applyFromArray(array(
                    'font' => array(
                        'bold' => true,
                        'size' => 14
                    )
                    ));

                    $sheet->mergeCells('A1:G1');
                    $sheet->mergeCells('H1:I1');
                    $sheet->mergeCells('A2:I2');
                    $sheet->cell('H1', function($cells) {
                        $cells->setAlignment('right');
                    });
                    $sheet->cell('A1', function($cells) {
                        $cells->setAlignment('left');
                    });

                    $rows[] = ['No','Work Order','Start','Work Center','Manufacture Order','Product','Original Production Quantity','Status'];
                    $no = 1;
                    foreach($data as $dt) {
                        $rows[] = [$no++, $dt['ro'], $dt['date_start'], $dt['wc'], $dt['mo'], $dt['product'], $dt['qty'], @$statusList[$dt['status']]];
                        $sheet->cell('A'.count($rows), function($cells) {
                            $cells->setAlignment('left');
                        });
                    }

                    $sheet->fromArray($rows, null, 'A1', true, false);
                });
            })->export('xls');
			return;
        }

        $datagrid['optionWorkflow'] = SettingWorkflow::getOption('WO');
        #echo "<pre>"; print_r($datagrid); die;
        return response()->json($datagrid);
    }
    
    public function detail($id) {
        $wo = ManufactureWorkOrder::query()
                ->select([
                    DB::raw('manufacture_order.code as mo'),
                    DB::raw('manufacture_routing_operation.name as ro'),
                    DB::raw('manufacture_routing_operation.next_sequence'),
                    DB::raw('manufacture_routing_operation.duration as ro_duration'),
                    DB::raw('manufacture_routing_operation.duration_uom as ro_duration_uom'),
                    DB::raw('manufacture_work_center.code as wc'),
                    DB::raw('CONCAT(\'[\', product_master.code, \'] \', product_master.name) as product'),
                    DB::raw('manufacture_order.qty as qty'),
                    DB::raw('product_uom.name as uom'),
                    'manufacture_work_order.*',
                ])
                ->leftJoin('manufacture_order', 'manufacture_order.id', '=', 'manufacture_work_order.manufacture_order')
                ->leftJoin('product_master', 'product_master.id', '=', 'manufacture_order.product')
                ->leftJoin('product_uom', 'product_uom.id', '=', 'product_master.uom')
                ->leftJoin('manufacture_routing_operation', 'manufacture_routing_operation.id', '=', 'manufacture_work_order.routing_operation')
                ->leftJoin('manufacture_work_center', 'manufacture_work_center.id', '=', 'manufacture_routing_operation.work_center')
                ->where('manufacture_work_order.id', $id)
                ->first();
        $woPending = ManufactureWorkOrderPending::where('work_order', $id)->orderBy('pending_start', 'asc')->get()->toArray();
        
        $dataWoStatus = SettingWorkflow::getOption('WO');
        $dataOptionDuration = Params::getOption('OPTION_DURATION');
        
        return response()->json([
            'data' => $wo,
            'dataWoPending' => $woPending,
            'dataWoStatus' => $dataWoStatus,
            'dataOptionDuration' => $dataOptionDuration,
        ]);
    }
    
    public function changeStatus($id, $status){
        
        $wo_valid = 1;
        $wo = ManufactureWorkOrder::where('id', $id)->first();
        if (!empty(Input::get('scrap_item'))) {
            $scrap = Input::get('scrap_item');
        } else {
            $scrap_bb = Input::get('scrap_bb');
            $scrap_bj = Input::get('scrap_bj');
            $scrap = $scrap_bj;
        }
        
        $qtyConsumed = Input::get('qty_consumed'); 
        if(!empty($wo)){
            $mo = ManufactureOrder::where('id', $wo->manufacture_order)->first();
            if(empty($mo)) {
                return response()->json([
                    'status' => false,
                    'messages' => 'Manufacture order tidak ditemukan!',
                ]);
                
                $siblings = ManufactureWorkOrder::where('manufacture_order', $wo->manufacture_order)->get();
                foreach($siblings as $sibling) {
                    $sibling->delete();
                }
                $wo_valid = 0;
            }
            else {
                if($mo->status == '1MD' || $mo->status == '2MC') {
                    return response()->json([
                        'status' => false,
                        'messages' => 'Manufacture order belum dimulai!',
                    ]);
                    $wo_valid = 0;
                }
                
                if(!empty($qtyConsumed)) {
                    $boms = ManufactureOrderBom::where('manufacture_order', $mo->id)->get();
                    foreach($boms as $bom) {
                        if(isset($qtyConsumed[$bom->id])) {
                            $bom->qty_consumed = $bom->qty_total - $qtyConsumed[$bom->id];
                            $bom->save();
                        }
                    }
                }
            }
            $wo->status = $status;
            if(!empty($scrap)) {
                $wo->scrap = $scrap;
            }

            if ($wo_valid == 1) {
                $wo->save();
            } 
            

            $woPending = ManufactureWorkOrderPending::where('work_order', $id)->orderBy('pending_start', 'asc')->get()->toArray();
            return response()->json([
                'status' => true,
                'dataWoPending' => $woPending,
            ]);
        }
    }
    
    public function bomData($id) { 
        $wo = ManufactureWorkOrder::where('id', $id)->first();
        
        $mo = ManufactureOrder::where('id', $wo->manufacture_order)->first();
        
        $woLast = ManufactureWorkOrder::select('manufacture_order')->where('manufacture_order', $wo->manufacture_order)->whereNull('date_end')->get();
        
        $woLastCount = $woLast->count() > 1 ? false : true;
        
        $boms = ManufactureOrderBom::select([
            'manufacture_order_bom.id',
            'product_master.name',
            'product_uom.name as unit_name'
        ])
        ->where('manufacture_order_bom.manufacture_order', $mo->id)
        ->leftJoin('product_bom', 'product_bom.id', '=', 'manufacture_order_bom.product_bom')
        ->leftJoin('product_product', 'product_product.id', '=', 'product_bom.product')
        ->leftJoin('product_master', 'product_master.id', '=', 'product_product.product')
        ->leftJoin('product_uom', 'product_uom.id', '=', 'product_master.uom')
        ->get()
        ->toArray();
        
        return response()->json([
            'boms' => $boms,
            'last_wo' => $woLastCount
        ]);
    }

    public function bomData_old($id) { 
        $wo = ManufactureWorkOrder::where('id', $id)->first();
        
        $mo = ManufactureOrder::where('id', $wo->manufacture_order)->first();
        #echo "<pre>"; print_r($mo->product); die;
        $pb = ProductBom::where('product_master', $mo->product)->first();
        #echo "<pre>"; print_r($pb->id); die;

        $woLast = ManufactureWorkOrder::select('manufacture_order')->where('manufacture_order', $wo->manufacture_order)->whereNull('date_end')->get();
            //
            
        $woLastCount = $woLast->count() > 1 ? false : true;
        #echo "<pre>"; print_r($woLast->count()); die;
        if (!empty($pb)) {
            $bomCheck = ManufactureOrderBom::select([
                'manufacture_order_bom.qty_consumed as qty_wo',
            ])
            ->where('manufacture_order_bom.manufacture_order', $mo->id)
            #->where('manufacture_order_bom.product_bom', '!=',$pb->id)
            ->whereNotNull('manufacture_order_bom.qty_consumed')
            ->get()
            ->toArray();
            //if ($woLastCount > 1 ) {
                if (empty($bomCheck)) {
                    $boms = ManufactureOrderBom::select([
                        'manufacture_order_bom.id',
                        'product_master.name'
                    ])
                    ->where('manufacture_order_bom.manufacture_order', $mo->id)
                    ->where('manufacture_order_bom.product_bom', '!=',$pb->id)
                    ->leftJoin('product_bom', 'product_bom.id', '=', 'manufacture_order_bom.product_bom')
                    ->leftJoin('product_product', 'product_product.id', '=', 'product_bom.product')
                    ->leftJoin('product_master', 'product_master.id', '=', 'product_product.product')
                    ->get()
                    ->toArray();
                } else {
                    $boms = ManufactureOrderBom::select([
                        'manufacture_order_bom.id',
                        'product_master.name'
                    ])
                    ->where('manufacture_order_bom.manufacture_order', $mo->id)
                    ->where('manufacture_order_bom.product_bom', '=',$pb->id)
                    ->leftJoin('product_bom', 'product_bom.id', '=', 'manufacture_order_bom.product_bom')
                    ->leftJoin('product_product', 'product_product.id', '=', 'product_bom.product')
                    ->leftJoin('product_master', 'product_master.id', '=', 'product_product.product')
                    ->get()
                    ->toArray();
                }
            /*} else {
                if (empty($bomCheck)) {
                    $boms = ManufactureOrderBom::select([
                        'manufacture_order_bom.id',
                        'product_master.name'
                    ])
                    ->where('manufacture_order_bom.manufacture_order', $mo->id)
                    ->where('manufacture_order_bom.product_bom', '!=',$pb->id)
                    ->leftJoin('product_bom', 'product_bom.id', '=', 'manufacture_order_bom.product_bom')
                    ->leftJoin('product_product', 'product_product.id', '=', 'product_bom.product')
                    ->leftJoin('product_master', 'product_master.id', '=', 'product_product.product')
                    ->get()
                    ->toArray();
                } else {
                    $boms = ManufactureOrderBom::select([
                        'manufacture_order_bom.id',
                        'product_master.name'
                    ])
                    ->where('manufacture_order_bom.manufacture_order', $mo->id)
                    ->where('manufacture_order_bom.product_bom', '=',$pb->id)
                    ->leftJoin('product_bom', 'product_bom.id', '=', 'manufacture_order_bom.product_bom')
                    ->leftJoin('product_product', 'product_product.id', '=', 'product_bom.product')
                    ->leftJoin('product_master', 'product_master.id', '=', 'product_product.product')
                    ->get()
                    ->toArray();
                }
            }*/
            
        } else {
            $boms = '';
        }
        
        return response()->json([
            'boms' => $boms,
            'last_wo' => $woLastCount
        ]);
    }
}
