<?php

namespace App\Http\Controllers\Manufacture;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Models\ManufactureOrder as ManufactureOrderModel;
use App\Models\InventoryStock;
use App\Models\ProductBom;
use App\Models\ProductProduct;
use App\Models\ProductMaster;
use App\Models\ManufactureOrderBom;
use App\Models\InventoryWarehouseLocation;
use App\Models\ManufactureRouting;
use App\Models\SettingUser;
use App\Models\SettingWorkflow;
use Devplus\Params\Params;
use Devplus\Datagrid\Datagrid;
use Devplus\Helper\Helper;
use Devplus\FormBuilder\FormBuilder;
use App\Models\ManufactureWorkOrder as ManufactureWorkOrder;
use Excel;

class ManufactureOrder extends Controller{
    public function index() {
        $query = ManufactureOrderModel::select([
            DB::raw('setting_user.fullname as pic_name'),
            DB::raw('product_master.name as product_name'),
            DB::raw('product_uom.name as product_uom_name'),
            'manufacture_order.*',
        ])->where([
            'manufacture_order.company' => Helper::currentCompany(),
        ])
        ->leftJoin('product_master', 'product_master.id', '=', 'manufacture_order.product')
        ->leftJoin('product_uom', 'product_uom.id', '=', 'product_master.uom')
        ->leftJoin('setting_user', 'setting_user.id', '=', 'manufacture_order.pic');

        $dateFrom = Input::get('from');
        $dateTo = Input::get('to');
        $sort = Input::get('sort');

        if(!empty($sort)) {
            $sortValue = substr($sort,0,1)=='-'?'ASC':'DESC';
            $sort = str_ireplace('-','',$sort);
            $query->orderBy($sort, $sortValue);
        }

        if(!empty($dateFrom)) {
            if(empty($dateTo)){
                $dateTo = $dateFrom;
            }
            $dateTo = Carbon::parse($dateTo)->addDay(1)->toDateTimeString();
            
            $query->where(function($q) use ($dateFrom, $dateTo) {
                $q->whereBetween('manufacture_order.date_start', [$dateFrom, $dateTo])
                        ->orWhereBetween('manufacture_order.date_end', [$dateFrom, $dateTo]);
                return $q;
            });
        }
        $query->orderBy('manufacture_order.created_at', 'asc');
        $dg = Datagrid::source($query);
        $dg->title('Manufacture Order');
        $dg->filter('keyword', function($query, $value){
            if($value != ''){
                return $query->where(function($q) use ($value) {
                    $q->where('product_master.name', 'ilike', '%'.$value.'%')
                        ->orWhere('setting_user.fullname', 'ilike', '%'.$value.'%')
                        ->orWhere('manufacture_order.code', 'ilike', '%'.$value.'%');;
                    return $q;
                });
            }

            return $query;
        });
        
        $dg->filter('workflow', function($query, $value){
            if($value != '') {
              return $query->where('manufacture_order.status', $value);
            }
            return $query;
        });

        $dg->add('code', 'No. MO', true);
        $dg->add('product_name', 'Product', false);
        $dg->add('qty', 'Qty', false)->render(function($data){
            return $data['qty'] . ' ' . $data['product_uom_name'];
        });
        $dg->add('date_start', 'Start', true)->render(function($data){
            return Carbon::parse($data['date_start'])->format('d/m/Y');
        });
        $dg->add('date_end', 'End', true)->render(function($data){
            return Carbon::parse($data['date_end'])->format('d/m/Y');
        });
        $dg->add('pic_name','Responsible');
        $dg->add('status','Status');
        $datagrid = $dg->build();

        $export = Input::get('export');
		if(!empty($export)){
			ini_set('max_execution_time', 3600);
			ini_set('memory_limit', '2048M');
            $data  = $datagrid['data'];
            
            $statusList = [];
            $getStatus = SettingWorkflow::getOption('MO');
            foreach($getStatus as $status) {
                $statusList[$status['id']] = $status['label'];
            }

            Excel::create('Manufacture Order '.date('d/m/Y H:i:s'), function($excel) use ($data, $dateTo, $dateFrom, $statusList) {

                $excel->sheet('Manufacture Order', function($sheet) use ($data, $dateTo, $dateFrom, $statusList) {
                    
                    $status = null;
                    $keyword = null;

                    if(Input::get('workflow')) {
                        $status = Input::get('workflow');
                        $status = @$statusList[$status];
                    }
                    if(Input::get('keyword')) {
                        $keyword = Input::get('keyword');
                    }

                    $rows = array();
                    $rows[] = ['Manufacture Order', '', '', '', '', '', date('d/m/Y H:i:s')];
                    if($dateTo) {
                        $rows[] = ['Tanggal: '.date('d/m/Y', strtotime($dateFrom)).' - '.date('d/m/Y', strtotime($dateTo. '-1 day'))];
                        $sheet->mergeCells('A'.count($rows).':H'.count($rows));
                    }

                    if($status) {
                        $rows[] = ['Status: '.$status];
                        $sheet->mergeCells('A'.count($rows).':H'.count($rows));
                    }
                    if($keyword) {
                        $rows[] = ['Keyword: '.$keyword];
                        $sheet->mergeCells('A'.count($rows).':H'.count($rows));
                    }
                    $rows[] = [];

                    $sheet->getStyle('A1')->applyFromArray(array(
                    'font' => array(
                        'bold' => true,
                        'size' => 14
                    )
                    ));

                    $sheet->mergeCells('A1:F1');
                    $sheet->mergeCells('G1:H1');
                    $sheet->mergeCells('A2:H2');
                    $sheet->cell('G1', function($cells) {
                        $cells->setAlignment('right');
                    });
                    $sheet->cell('A1', function($cells) {
                        $cells->setAlignment('left');
                    });

                    $rows[] = ['No','No. MO','Product','Qty','Start','End','Responsible','Status'];
                    $no = 1;
                    foreach($data as $dt) {
                        $rows[] = [$no++, $dt['code'], $dt['product_name'], (float)$dt['qty'], $dt['date_start'], $dt['date_end'], $dt['pic_name'], @$statusList[$dt['status']]];
                        $sheet->cell('A'.count($rows), function($cells) {
                            $cells->setAlignment('left');
                        });
                    }

                    $sheet->fromArray($rows, null, 'A1', true, false);
                });
            })->export('xls');
			return;
        }
        
        $datagrid['optionWorkflow'] = SettingWorkflow::getOption('MO');
        return response()->json($datagrid);
    }
    
    public function show(Order $order){
        \DB::connection()->enableQueryLog();
        $data = $order->all();
        $queries = \DB::getQueryLog();
        return dd($queries);
    }

    public function create(){ 
        $form = $this->anyForm(new ManufactureOrderModel());
        $form->title('Manufacture Order');
        $code = ManufactureOrderModel::generateId(); 
 
        $form->pre(function($data) use ($code) {            
            $data['code'] = $code;
            $data['company'] = Helper::currentCompany();
            
            return $data;
        }); #echo '<pre> data : '; print_r($form); die;
        $dataForm = $form->build(); #echo "<pre>okokok"; print_r($dataForm); die;


        if($form->hasRequest()){
            if($form->saved()){

                $this->processDataAfterSave($form);
                return response()->json([
                    'status' => true
                ]);
            }else{
                return response()->json([
                    'status' => true
                ]);
                /*return response()->json([
                    'status' => false,
                    'messages' => $form->validatorMessages
                ]);*/
            }
        }

        $selected_product = Input::get('product');
        if($selected_product) {
            $productMaster = ProductMaster::select([
                'product_master.*',
            ])
            ->join('product_product', 'product_product.product', '=', 'product_master.id')
            ->where('product_master.id', $selected_product)
            ->orWhere('product_product.id', $selected_product)
            ->first();

            $dataForm['data']['product'] = $productMaster->id;
            $dataForm['data']['productName'] = $productMaster->name;
            $dataForm['dataBom'] = $this->bomData($productMaster->id, null, true);
        }

        $dataForm['data']['code'] = $code;
        $dataForm['dataMoStatus'] = SettingWorkflow::getOption('MO');
        $dataForm['data']['status'] = $dataForm['dataMoStatus'][0]['id'];
        $dataForm['bomStatusAvailable'] = Params::get('STOCK_AVAILABLE');
        $dataForm['bomStatusPartialAvailable'] = Params::get('STOCK_PARTIAL_AVAILABLE');
        $dataForm['bomStatusNotAvailable'] = Params::get('STOCK_NOT_AVAILABLE');
        $dataForm['optionRouting'] = ManufactureRouting::getOption();
        $dataForm['optionLocation'] = InventoryWarehouseLocation::getOption();
        return response()->json($dataForm);
    }
    
    public function modify($id = null){
        $mo = ManufactureOrderModel::where('id', $id)->first(); 
        $form = $this->anyForm($mo);
        $form->title('Manufacture Order');
        $dataForm = $form->build();
        $productMaster = ProductMaster::where('id', $mo->product)->first();

        $dataForm['dataMoStatus'] = SettingWorkflow::getOption('MO');
        $dataForm['bomStatusAvailable'] = Params::get('STOCK_AVAILABLE');
        $dataForm['bomStatusPartialAvailable'] = Params::get('STOCK_PARTIAL_AVAILABLE');
        $dataForm['bomStatusNotAvailable'] = Params::get('STOCK_NOT_AVAILABLE');
        
        if($form->hasRequest()){
            if($mo->status == '1MD' && $form->model->status == '2MS') {
                $boms = $this->bomData($productMaster->id, $mo->id, true);
                foreach($boms as $bom) {
                    if($bom['status'] !== $dataForm['bomStatusAvailable']) {
                        return response()->json([
                            'status' => false,
                            'messages' => "Bahan baku belum mencukupi untuk memulai.",
                        ]);
                    }
                }
            }
            
            if($form->model->status == '2MS') {
                $form->model->status = '3MW';
                $form->model->date_start = date('Y-m-d');
            }
            
            if($form->saved()){
                $this->processDataAfterSave($form);
                return response()->json([
                    'status' => true
                ]);
            }else{
                return response()->json([
                    'status' => false,
                    'messages' => $form->validatorMessages
                ]);
            }
        }

        if(!empty($productMaster)) {
            $dataForm['data']['productName'] = $productMaster->name;
        }
        $user = SettingUser::where('id', $mo->pic)->first();
        if(!empty($user)) {
            $dataForm['data']['picName'] = $user->fullname;
        }
        if(!empty($mo->routing) && !empty($mo->product)) {
            $dataForm['routingDisabled'] = true;
        }
        
        $dataForm['dataBom'] = $this->bomData($productMaster->id, $mo->id, true);
        $dataForm['isEditing'] = true;
        $dataForm['optionRouting'] = ManufactureRouting::getOption();
        $dataForm['optionLocation'] = InventoryWarehouseLocation::getAuthOption();
        if($mo->status !== '1MD') {
            $dataForm['action'] = 'view';
        }
        return response()->json($dataForm);
    }

    public function anyForm($source){ 
        $form = FormBuilder::source($source);
        $form->title('Manufacture Order');
        $form->add('code', 'Code', 'text')->rule('required|max:25')->attributes([
            'readonly' => true
        ]);
        $form->add('product', 'Product', 'hidden')->rule('required');
        $form->add('qty', 'Quantity to Produce', 'number')->rule('required');
        /*
        $form->add('location', 'Location', 'select')->rule('required')->attributes([
          'ng-options' => 'item.id as item.text for item in response.optionLocation',
        ]);
        */
        $form->add('pic', 'Responsible', 'hidden')->rule('required');
        
        $form->add('routing', 'Routing', 'select')->attributes([
            'ng-options' => 'item.id as item.text for item in response.optionRouting',
            'data-live-search'=> true,
            'ng-change' => 'changeRoute()',
        ])->rule('required');

        return $form;
    }
    
    public function processDataAfterSave($form = null){
        if(empty($form)){
            return;
        }

        //save bom
        $listId = [];
        if(!empty($form->dataPost['dataBom'])){
            foreach($form->dataPost['dataBom'] as $val){
                $moBom = new ManufactureOrderBom();
                if(!empty($val['id'])){
                    $moBom = ManufactureOrderBom::find($val['id']);
                }
                $dataBom = [
                    'manufacture_order' => $form->model->id,
                    'product_bom' => $val['product_bom'],
                    'qty_total' => $val['required'],
                    'status' => $val['status'],
                ];
                $moBom->fill($dataBom);
                $moBom->save();
                $listId[] = $moBom->id;
            }
        }
        ManufactureOrderBom::where('manufacture_order', $form->model->id)->whereNotIn('id', $listId)->delete();
    }

    public function delete(){
        $id = Input::get('id');
        ManufactureOrderModel::whereIn('id', $id)->delete();

        return response()->json([
            'status' => true
        ]);
    }
    
    public function bomData($productId = null, $moId = null, $asFunction = false) {
       ini_set('max_execution_time', 3000);
       ini_set('memory_limit', '256M');
        if(empty($moId)) {
            $moId = Input::get('mo');
        }
        $mo = null;
        #echo "<pre>"; print_r($productId); die;
        $productMaster = ProductMaster::select([
                            'product_master.*',
                        ])
                        ->join('product_product', 'product_product.product', '=', 'product_master.id')
                        ->where('product_master.id', $productId)
                        ->orWhere('product_product.id', $productId)
                        ->first();
        if(empty($productMaster)) {
            return $asFunction ? [] : response()->json([]);
        }
        
        if(!empty($moId)) {
            $mo = ManufactureOrderModel::where('id', $moId)->first();
        }
        
        $boms_main = [];
        if(empty($mo)) {
            
            $boms = ProductBom::select([
							'product_master.name',
							'product_category.name as category',
							'product_bom.*',
							'product_master.routing',
							'product_master.id as product_master_id',
							'product_uom.name as unit_name',
							'product_uom.ratio'
						])
						->join('product_product', 'product_product.id', '=', 'product_bom.product')
						->join('product_master', 'product_master.id', '=', 'product_product.product')
						->leftJoin('product_uom', 'product_uom.id', '=', 'product_bom.uom')
						->join('product_category', 'product_master.category','=', 'product_category.id')
						->where('product_bom.product_master', $productMaster->id)
						->get()
						->toArray();
            
            #echo "<pre> md5 : "; print_r($boms); die;
            $boms_product_ids = [];
            $sub_boms_ids = [];
            foreach ($boms as $key => $value) {
                $boms_product_ids[] = $value['product'];
                $sub_boms_ids[] = $value['product_master_id'];
                $boms[$key]['create_mo'] = 0;
                if (!empty($value['routing'])) {
                    $boms[$key]['mo_created'] = 1;
                    $boms[$key]['create_mo'] = 1;
                    //open all
                } else {
                    $boms[$key]['mo_created'] = 0;
                }
            } #echo "<pre> md5 : "; print_r($boms); die;

            $sub_boms = ProductBom::select([
                    'product_bom.*',
                    'product_product.id as product_id',
                ])
                ->join('product_product', 'product_product.product', '=', 'product_bom.product_master')
                ->whereIn('product_bom.product_master', $sub_boms_ids)
                ->get();
            
            $product_sub_boms = [];
            foreach ($sub_boms as $row) {
                $product_sub_boms[$row->product_id][] = $row->product;
            }

            if(Input::get('routing') && Input::get('routing') != 'undefined') {
                $get_routing = ManufactureRouting::find(Input::get('routing'));
                $dataStock = InventoryStock::getProductStock(implode(';', $boms_product_ids), null, $get_routing->location_raw);
		//$dataStock = InventoryStock::getProductStock(implode('.', $boms_product_ids), null, $get_routing->location_raw);
            } else {
		$dataStock = InventoryStock::getProductStock(implode(';', $boms_product_ids));
                //$dataStock = InventoryStock::getProductStock(implode('.', $boms_product_ids));
            }
    

            $arrDataStock = [];
            foreach($dataStock as $row) {
                $arrDataStock[$row['id']] = $row;
            }

            $stockWarehouse = ProductProduct::getInventoryDataStock($boms_product_ids);

            //dd($stockWarehouse);
            
            $boms = array_map(function ($bom) use($arrDataStock, $product_sub_boms, $stockWarehouse) {
                $dtStockWarehouse = isset($stockWarehouse[$bom['product']]) ? $stockWarehouse[$bom['product']] : [];
                
                $bom['product_bom'] = $bom['id'];
                unset($bom['id']);
                $dataStock = $arrDataStock[$bom['product']];
                if(!empty($dataStock)) {
                    $bom['stock'] = $dataStock['stock_akhir'];
                } else {
                    $bom['stock'] = 0;
                }
                
                $bom['stock'] *= $bom['ratio'];

                $bom['dataStock'] = $dtStockWarehouse;
                $bom['total_stock'] = array_reduce($dtStockWarehouse, function($init, $item) {
                        return $init + $item['qty'];
                }, 0);

                if(isset($product_sub_boms[$bom['product']])) 
                    $bom['create_mo'] = true;
                    
                /* if($bom['stock'] == 0 && $bom['mo_created']) {
                    $bom['create_mo'] = 1;
                } */

                if(!empty($mo) && $mo !== null) {
                    $bom['required'] = $mo->qty * $bom['qty'];
                }

                return $bom;
            }, $boms); 
        }
        else {
            
            $boms = ManufactureOrderBom::select([
                    'product_master.name',
                    'product_master.id as pm_id',
                    'manufacture_order_bom.*',
                    'product_uom.name as unit_name'
                ])
                ->join('product_bom', 'product_bom.id', '=', 'manufacture_order_bom.product_bom')
                ->leftJoin('product_product', 'product_product.id', '=', 'product_bom.product')
                ->leftJoin('product_master', 'product_master.id', '=', 'product_product.product')
                ->leftJoin('product_uom', 'product_uom.id', '=', 'product_bom.uom')
                ->where('manufacture_order_bom.manufacture_order', $mo->id)->get()->toArray();

            $boms_product_boms = [];
            foreach ($boms as $key => $value) {
                $boms_product_boms[] = $value['product_bom'];
            } 

            $sub_boms = ProductBom::select([
                    'product_bom.*'
                ])
                ->whereIn('product_bom.id', $boms_product_boms)
                ->get()
                ->toArray();

            $data_product_bom = [];
            $product_bom_products = [];
            foreach ($sub_boms as $row) {
                $product_bom_products[] = $row['product'];
                $data_product_bom[$row['id']] = $row;
            }

            $dataStock = InventoryStock::getProductStock(implode(';', $product_bom_products), null, $mo->get_routing->location_raw);
            //$dataStock = InventoryStock::getProductStock(implode('.', $product_bom_products), null, $mo->get_routing->location_raw);
            $arrDataStock = [];
            foreach($dataStock as $row) {
                $arrDataStock[$row['id']] = $row;
            }

            $stockWarehouse = ProductProduct::getInventoryDataStock($product_bom_products);

            $boms = array_map(function ($bom) use ($mo, $arrDataStock, $data_product_bom, $stockWarehouse) {
                $productBom = $data_product_bom[$bom['product_bom']];
                if(!empty($productBom)) {
                    $dtStockWarehouse = isset($stockWarehouse[$productBom['product']]) ? $stockWarehouse[$productBom['product']] : [];
                    $bom['qty'] = $productBom['qty'];
                    $dataStock = @$arrDataStock[$productBom['product']];
                    $bom['stock'] = empty($dataStock) ? 0 : $dataStock['stock_akhir'];
                    $bom['dataStock'] = $dtStockWarehouse;
                    $bom['total_stock'] = array_reduce($dtStockWarehouse, function($init, $item) {
                        return $init + $item['qty'];
                    }, 0);
                }
                $bom['required'] = $bom['qty_total'];
                return $bom;
            }, $boms);
        }
        
        if(!empty($mo) && $mo !== null) {
            foreach($boms as $index => $bom) {
                $moBom = ManufactureOrderBom::where([
                    'product_bom' => $bom['product_bom'],
                    'manufacture_order' => $mo->id,
                ])
                ->first();

                if ($productMaster->type == 'SAL') {
                    $bomStatusAvailable = Params::get('STOCK_AVAILABLE');
                    $moBom->qty_total = $bom['required'];
                    $moBom->status = $bomStatusAvailable;
                    $moBom->save();
                    $boms[$index]['status'] = $moBom->status;
                } else {
                    $bomStatusAvailable = Params::get('STOCK_AVAILABLE');
                    $bomStatusPartialAvailable = Params::get('STOCK_PARTIAL_AVAILABLE');
                    $bomStatusNotAvailable = Params::get('STOCK_NOT_AVAILABLE');
                    $moBom->qty_total = $bom['required'];
                    $moBom->status = $bom['stock'] > 0 ? ($bom['stock'] >= $bom['required'] ? $bomStatusAvailable : $bomStatusPartialAvailable) : $bomStatusNotAvailable;
                    $moBom->save();
                    $boms[$index]['status'] = $moBom->status;
                }
                
            }
        }
        #echo "<pre>"; print_r($boms); die;
        return $asFunction ? $boms : response()->json($boms);
    }
    
    
    public function changeStatus($id, $status){
        DB::enableQueryLog();
        #echo "<pre> id : "; print_r($id); die;
        #echo "<pre> status : "; print_r($status); 
        $mo = ManufactureOrderModel::where('id', $id)->first();
        #echo "<pre> mo : "; print_r($mo->id);
        $bomStatusAvailable = Params::get('STOCK_AVAILABLE');
        #echo "<pre> bomStatusAvailable : "; print_r($bomStatusAvailable); die;
        if(!empty($mo)){
            if($mo->status == '1MD' && $status == '2MS') {
                $boms = $this->bomData($mo->product, $mo->id, true);
                #echo "<pre> boms : "; print_r($boms); die;
                foreach($boms as $bom) {
                    //if($bom['status'] !== $bomStatusAvailable && $bom['pm_id'] !== $mo->product) {
                    if($bom['stock'] < $bom['required']) {
                        return response()->json([
                            'status' => false,
                            'messages' => "Bahan baku belum mencukupi untuk memulai.",
                        ]);
                    }
                }
            }
            $mo->status = $status;
            $mo->save();
            #echo "<pre> status MO : "; print_r($mo->status); 
            if ($mo->status == '2MS') {
                $wo = ManufactureWorkOrder::where('manufacture_order', $id)->first();
                #echo "<pre> wo :"; print_r($wo);
                if (!empty($wo) && $wo->status == '1WD') {
                    $wo->status = '2WR';
                    #echo "<pre> wo :"; print_r($wo);
                    #echo "<pre> WO status : "; print_r($wo->status);
                    

                    #$fetchedPost = ManufactureOrderModel::find($mo->id);
                    #dd($fetchedPost);
                    #echo "<pre>"; print_r($moChange); 
                    $wo->save();
                    #$queries = \DB::getQueryLog();
                    #die;
                }
            }
            return response()->json([
                'status' => true,
            ]);
        } else {
            return response()->json([
                'status' => true,
            ]);
        }
    }
}
