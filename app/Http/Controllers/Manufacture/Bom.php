<?php

namespace App\Http\Controllers\Manufacture;

use App\Models\ManufactureBom;
use App\Models\ManufactureBomProduct;
use App\Models\ManufactureRouting;
use App\Models\ManufactureRoutingOperation;
use App\Models\ManufactureWorkCenter;
use App\Models\ProductProduct;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Helper;

class Bom extends Controller
{
  public function index() {
    $query = ManufactureBom::select([
      'manufacture_bom.id',
      'manufacture_bom.product',
      'manufacture_bom.routing',
      'manufacture_bom.name AS bom_name',
      'manufacture_bom.qty',
      'manufacture_bom.uom',
      'manufacture_bom.status',
      'product_product.attribute',
      'product_master.name AS name',
      'manufacture_routing.name AS routing_name'
    ])
    ->join('product_product', 'product_product.id', '=', 'manufacture_bom.product')
    ->join('product_master', 'product_master.id', '=', 'product_product.product')
    ->join('product_uom', 'product_uom.id', '=', 'manufacture_bom.uom')
    ->join('manufacture_routing', 'manufacture_routing.id', '=', 'manufacture_bom.routing')
    ->where('product_product.active', true);

    $dg = Datagrid::source(ManufactureBom::query());
    $dg->title('Bill of Materials');
    $dg->filter('keyword', function($query, $value){
      if($value != '')
          return $query->where('name', 'ilike', '%'.$value.'%')->orWhere('code', 'ilike', '%'.$value.'%');

      return $query;
    });
    $dg->add('bom_name', 'Nama');
    $dg->add('name', 'Produk');
    $dg->add('qty', 'Jumlah');
    $dg->add('uom_name', 'Satuan Ukuran');
    $dg->add('routing_name', 'Routing');
    $dg->add('status', 'Status');
    $datagrid = $dg->build();

    $datagrid['data'] = ProductProduct::getRelatedData($datagrid['data']);

    return response()->json($datagrid);
  }

  public function create(){
    $form = $this->anyForm(new ManufactureBom());

    // $form->pre(function($data){
    //   $data['code'] = ManufactureRouting::generateId();
    //   return $data;
    // });
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){

        //save Operation
        $this->processDataAfterSave($form);

        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }

    return response()->json($dataForm);
  }

  public function modify(){

  }

  public function delete(){

  }

  public function processDataAfterSave($form = null){
    if(empty($form)){
      return false;
    }


    return $form;
  }
}
