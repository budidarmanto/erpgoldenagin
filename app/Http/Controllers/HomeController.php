<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProductMaster;
use App\Models\ProductBom;
use App\Models\ProductProduct;
use App\Models\ProductCategory;
use App\Models\ProductUom;
use DB;
use Excel;
use PHPExcel_IOFactory;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function import_products($index)
    {
        ini_set('max_execution_time', 7200);

        $get_category = DB::table('product_category')->get();
        $db_category = [];
        $db_category_data = [];
        foreach($get_category as $row) {
            $db_category[strtolower(str_replace(' ','', $row->name))] = $row->id;
            $db_category_data[$row->id] = $row;
        }

        $get_uom = DB::table('product_uom')->get();
        $db_uom = [];
        $db_uom_data = [];
        foreach($get_uom as $row) {
            $db_uom[strtolower(str_replace(' ','', $row->code))] = $row->id;
            $db_uom_data[$row->id] = $row;
        }

        $filename = null;
        $max_sheet = 0;
        switch($index) {
            case 1: 
                $filename = 'import_products.xlsx';
                $max_sheet = 7;
            break;
            case 2: 
                $filename = 'import_products2.xlsx';
                $max_sheet = 1;
            break;
            case 3: 
                $filename = 'import_products3.xlsx';
                $max_sheet = 5;
            break;
            case 4: 
                $filename = 'import_products4.xlsx';
                $max_sheet = 7;
            break;
            case 5: 
                $filename = 'import_products5.xlsx';
                $max_sheet = 5;
            break;
            case 6: 
                $filename = 'import_products6.xlsx';
                $max_sheet = 1;
            break;
        }

        $file = base_path().'/public/imports/'.$filename;

        $objPHPExcel = PHPExcel_IOFactory::load($file);

        $datetime = date('d-m-Y H:i:s');

        $data = [];

        $count_new_products = 0;

        $sheets_failed = [];
        
        $errors_cat = [];
        $errors_uom = [];

        for($i=0;$i<=$max_sheet;$i++) {
            $worksheet = $objPHPExcel->getSheet($i);
            
            $skipto = 0;

            $lastRow = $worksheet->getHighestRow();

            $cell_collection = $worksheet->getCellCollection();

            foreach ($cell_collection as $cell) {
                $get_cell = $worksheet->getCell($cell);
                $column = $get_cell->getColumn();
                $row = $get_cell->getRow();
                $data_value = trim($get_cell->getValue());
                
                $key = 'r'.$i.$row;

                if($row <= 3)
                    continue;

                if($skipto && $row < $skipto)
                    continue;
                
                if(!$data_value)
                    continue;
                    
                switch ($column) {
                    case 'C':
                        if(in_array(strtolower(str_replace(' ','', $data_value)), array_keys($db_category))) {
                            $data_value = $db_category[strtolower(str_replace(' ','', $data_value))];
                        } else {
                            $new_category = new ProductCategory;
                            $new_category->name = $data_value;
                            $new_category->depth = 0;
                            $new_category->created_by = 'SYSTEM';
                            $new_category->updated_by = 'SYSTEM';
                            if($new_category->save()) {
                                $db_category[strtolower(str_replace(' ','', $data_value))] = $new_category->id;
                                $data_value = $new_category->id;
                            } else {
                                $data[$key]['category']['error'] = 'Kategori tidak diketahui';
                                $errors_cat[] = $key;
                            }
                        }
                        $data[$key]['category']['value'] = $data_value;
                    break;
                    case 'D':
                        $data[$key]['code']['value'] = $data_value;
                    break;
                    case 'E':
                        $data[$key]['name']['value'] = $data_value;
                    break;
                    case 'F':
                        if(in_array(strtolower(str_replace(' ','', $data_value)), array_keys($db_uom))) {
                            $data_value = $db_uom[strtolower(str_replace(' ','', $data_value))];
                        } else {
                            $new_category = new ProductUom;
                            $new_category->uom_category = "5979d75f820df023c8";
                            $new_category->type = 'd';
                            $new_category->code = $data_value;
                            $new_category->name = $data_value;
                            $new_category->ratio = 1;
                            $new_category->active = true;
                            $new_category->created_by = 'SYSTEM';
                            $new_category->updated_by = 'SYSTEM';
                            if($new_category->save()) {
                                $db_uom[strtolower(str_replace(' ','', $data_value))] = $new_category->id;
                                $data_value = $new_category->id;
                            } else {
                                $data[$key]['uom']['error'] = 'Unit tidak diketahui';
                                $errors_uom[] = $key;
                            }
                        }
                        $data[$key]['uom']['value'] = $data_value;
                    break;
                    default:
                        $data_value = '<span style="color:grey">'.$data_value.'</span>';
                    break;
                }
                /* if($row != @$lastRow) {
                    echo '<br>';
                }
                echo $data_value.'<br>'; */
                $lastRow = $row;
            }
        }

        $n = 0;
        
        $skip = false;
        $limit = false;

        foreach ($data as $key_row => $row) {
            $n++;

            if($skip && $skip >= $n) {
                continue;
            }

            if($limit && $n > $limit) {
                break;
            }

            $code = null;

            echo 'No : '.$n.'<br>';
            foreach ($row as $key => $value) {
                echo $key.' : '.$value['value'].'<br>';
                if($key == 'code') {
                    $code = $value['value'];
                }
            }
            echo '<br>';
            
            if($code) {                
                $check_exists = ProductMaster::where('code', $code)->first();
                if($check_exists) {
                    continue;
                }
            }

            $new_product = new ProductMaster;
            foreach ($row as $key => $value) {
                $new_product->{$key} = $value['value'];
            }
            $new_product->type = 'STO';
            $new_product->created_by = 'SYSTEM';
            $new_product->updated_by = 'SYSTEM';
            
            if($new_product->save()) {
                $count_new_products++;
            }
        }
        echo 'new products: '.$count_new_products.'<br>';

        var_dump($errors_cat);
        var_dump($errors_uom);
        
    }

    public function import_product_product()
    {
        $get_products = ProductMaster::select('product_master.id', 'product_product.id as product_product')->leftJoin('product_product', 'product_product.product', '=', 'product_master.id')->whereRaw('product_product is null')->get();
        
        $count = 0;

        foreach($get_products as $product) {

            $new = new ProductProduct;
            $new->product = $product->id;
            if($new->save())
                $count++;
        }

        echo $count.' products saved';
    }

    public function import_products_bom()
    {
        $get_uom = DB::table('product_uom')->get();
        $db_uom = [];
        $db_uom_data = [];
        foreach($get_uom as $row) {
            $db_uom[strtolower(str_replace(' ','', $row->name))] = $row->id;
            $db_uom[strtolower(str_replace(' ','', $row->code))] = $row->id;
            $db_uom_data[$row->id] = $row;
        }

        $filename = 'product_bom2.xlsx';

        $file = base_path().'/public/imports/'.$filename;

        $objPHPExcel = PHPExcel_IOFactory::load($file);

        $datetime = date('d-m-Y H:i:s');

        $data = [];

        $product_codes = [];

        $skip_products = [];

        $errors_uom = [];

        for($i=0;$i<=13;$i++) {
            $worksheet = $objPHPExcel->getSheet($i);
            
            $skipto = 0;

            $lastRow = $worksheet->getHighestRow();

            $cell_collection = $worksheet->getCellCollection();

            $key = null;
            $sub_key = null;
            $start_mapping = false;

            foreach ($cell_collection as $cell) {
                $get_cell = $worksheet->getCell($cell);
                $column = $get_cell->getColumn();
                $row = $get_cell->getRow();
                $data_value = trim($get_cell->getCalculatedValue());
                
                if(!$start_mapping && $column == 'A' && $data_value == 1) {
                    $start_mapping = true;
                }

                if(!$start_mapping)
                    continue;

                if($skipto && $row < $skipto)
                    continue;
                
                if(!$data_value)
                    continue;
                    
                switch ($column) {
                    case 'B':
                        // product_bom.xlsx
                        // if($data_value == 'PLS-PLP')
                        //     $data_value = 'PLS-PLB01';
                        // else if($data_value == '6Q24')
                        //     $data_value = '16Q24';

                        // product_bom2.xlsx
                        if($data_value == '75.03.02.00')
                            $data_value = '75.03.02';
                        else if($data_value == '88.01.00')
                            $data_value = '75PR.01.00';
                        else if($data_value == '6Q24')
                            $data_value = '16Q24';
                        else if($data_value == 'PLS-PLP')
                            $data_value = 'PLS-PLB01';

                        if(!isset($data[$data_value])) {
                            $data[$data_value] = [
                                'id' => null,
                                'name' => null,
                                'bom' => []
                            ];
                        }
                        $key = $data_value;
                        $product_codes[] = $this->clear_whitespace($data_value);
                    break;
                    case 'C':
                        $data[$key]['name'] = $data_value;
                    break;
                    case 'D':
                        // product_bom.xlsx
                        // if($data_value == 'PLS-PLP')
                        //     $data_value = 'PLS-PLB01';
                        // else if($data_value == '6Q24')
                        //     $data_value = '16Q24';
                            
                        // product_bom2.xlsx
                        if($data_value == '75.03.02.00')
                            $data_value = '75.03.02';
                        else if($data_value == '88.01.00')
                            $data_value = '75PR.01.00';
                        else if($data_value == '6Q24')
                            $data_value = '16Q24';
                        else if($data_value == 'PLS-PLP')
                            $data_value = 'PLS-PLB01';

                        //if($data_value == '508000000')
                        //    dd($data_value);

                        $sub_key = $data_value;
                        $data[$key]['bom'][$sub_key]['code'] = $data_value;
                        $product_codes[] = $this->clear_whitespace($data_value);
                    break;
                    case 'E':
                        $data[$key]['bom'][$sub_key]['name'] = $data_value;
                    break;
                    case 'F':
                        $data[$key]['bom'][$sub_key]['qty'] = $data_value;
                    break;
                    case 'G':
                        if(in_array(strtolower(str_replace(' ','', $data_value)), array_keys($db_uom))) {
                            $data_value = $db_uom[strtolower(str_replace(' ','', $data_value))];
                        } else {
                            // $new_category = new ProductUom;
                            // $new_category->uom_category = "5979d75f820df023c8";
                            // $new_category->type = 'd';
                            // $new_category->code = $data_value;
                            // $new_category->name = $data_value;
                            // $new_category->ratio = 1;
                            // $new_category->active = true;
                            // $new_category->created_by = 'SYSTEM';
                            // $new_category->updated_by = 'SYSTEM';
                            // if($new_category->save()) {
                            //     $db_uom[strtolower(str_replace(' ','', $data_value))] = $new_category->id;
                            //     $data_value = $new_category->id;
                            // } else {
                            //     $data[$key]['uom']['error'] = 'Unit tidak diketahui';
                            //     $errors_uom[] = $data_value;
                            // }
                            $skip_products[] = $key;
                        }
                        $data[$key]['bom'][$sub_key]['uom'] = $data_value;
                    break;
                    default:
                        $data_value = '<span style="color:grey">'.$data_value.'</span>';
                    break;
                }
            }
        }
        
        $product_codes = array_unique($product_codes);

        $db_products = [];
        $get_products = ProductMaster::select('product_master.code', 'product_master.id as product_master_id', 'product_product.id as product_product_id')->whereIn('code', $product_codes)->leftJoin('product_product', 'product_product.product', 'product_master.id')->get();
        foreach($get_products as $product) {
            $db_products[$product->code]['product_master_id'] = $product->product_master_id;
            $db_products[$product->code]['product_product_id'] = $product->product_product_id;
        }

        //var_dump($data);
        //return;

        $data = collect($data)->transform(function($row, $key) use($db_products) {
            if(!isset($db_products[$this->clear_whitespace($key)])) {
                //dd($row, $key);
                return $row;
            }
            $row['id'] = $db_products[$this->clear_whitespace($key)]['product_master_id'];
            $row['bom'] = collect($row['bom'])->transform(function($row_sub) use($db_products) {

                if(!isset($db_products[$this->clear_whitespace($row_sub['code'])])) {
                    //dd($row_sub, $row_sub['code']);
                    return $row_sub;
                }

                $row_sub['id'] = $db_products[$this->clear_whitespace($row_sub['code'])]['product_product_id'];
                return $row_sub;
            })->toArray();
            return $row;
        })->toArray();

        // dd($data);
        // return;

        $n = 0;
        
        $skip = false;
        $limit = false;

        $count_new_products = 0;

        foreach ($data as $key_row => $row) {
            $n++;

            if($skip && $skip >= $n) {
                continue;
            }

            if($limit && $n > $limit) {
                break;
            }

            if(in_array($key_row, $skip_products))
                continue;

            foreach($row['bom'] as $bom) {
                if(!isset($bom['qty']) || !isset($bom['uom']) || !isset($bom['id'])) {
                    continue;
                }

                $check_exists = ProductBom::where('product_master', $row['id'])->where('product', $bom['id']);
                if($check_exists)
                    $check_exists->delete();

                $new_product = new ProductBom;
                $new_product->product_master = $row['id'];
                $new_product->product = $bom['id'];
                $new_product->uom = $bom['uom'];
                $new_product->qty = (float)$bom['qty'];
                $new_product->created_by = 'SYSTEM';
                $new_product->updated_by = 'SYSTEM';
                
                if($new_product->save()) {
                    $count_new_products++;
                }
            }
        }
        echo 'new bom: '.$count_new_products.'<br>';

        var_dump($errors_uom);
    }

    private function clear_whitespace($text) {
        return urldecode(str_replace('+%E2%80%83%E2%80%82', '', urlencode($text)));
    }
}
