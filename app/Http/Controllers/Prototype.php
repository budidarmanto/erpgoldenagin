<?php

namespace App\Http\Controllers;

use App\Models\CrmContact;
use App\Models\SettingCompany;
use App\Models\ProductMaster;
use App\Models\ProductProduct;
use App\Models\ProductAttribute;
use App\Models\ProductAttributeValue;
use App\Models\ProductUom;
use App\Models\ProductUomCategory;
use App\Models\InventoryStockMovement;
use App\Models\InventoryStockMovementProduct;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Params;
use Helper;
use Auth;

class Prototype extends Controller{
  public function index(){
    $arr = [
      ['id' => 1, 'type' => 'AD'],
      ['id' => 2, 'type' => 'PO'],
      ['id' => 3, 'type' => 'SO'],
    ];

    $arr = collect($arr)->filter(function($dt){
      return ($dt['id'] == 2 && $dt['type'] == 'PO');
    })->toArray();

    dd(key($arr));
  }
}
