<?php

namespace App\Http\Controllers\Api;

use App\Models\CrmContact;
use App\Models\CrmContactDetail;
use App\Models\SettingCompany;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Params;
use Helper;

class Customer extends Controller
{

  public function index() {

    $dataCustomer = CrmContact::select([
      'id', 'name', 'revision'
    ])->where('type', 'customer')->get();

    $xml = '<page>';
    if(count($dataCustomer) > 0){
      foreach($dataCustomer as $val){

        $customerName = htmlspecialchars($val['name'], ENT_XML1, 'UTF-8');
        $xml .= '<customer id="'.$val['id'].'" name="'.$customerName.'" revision="'.$val['revision'].'"/>';
      }
    }
    $xml .= '</page>';

    header('Content-type: text/xml');
    echo $xml;
    exit;

  }
}
