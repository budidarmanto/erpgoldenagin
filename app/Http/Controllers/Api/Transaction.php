<?php

namespace App\Http\Controllers\Api;

use App\Models\CrmContact;
use App\Models\CrmContactDetail;
use App\Models\InventoryWarehouse;
use App\Models\InventoryWarehouseLocation;
use App\Models\SettingCompany;
use App\Models\SettingUser;
use App\Models\SettingWorkflow;
use App\Models\ProductCategory;
use App\Http\Controllers\Controller;
use App\Models\ProductMaster;
use App\Models\ProductProduct;
use App\Models\InventoryStockMovement;
use App\Models\InventoryStockMovementProduct;
use App\Models\InventoryStockMovementLog;
use App\Models\InventoryStockMovementComment;
use App\Models\SettingCurrency;
use App\Models\SettingTax;
use App\Models\ProductUom;
use App\Models\CashierSession;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Params;
use Helper;

class Transaction extends Controller
{

  public function index() {

    // format
    // [
    //   "username" => "", #cashier user
    //   "company" => "",
    //   "session" => "",
    //   "reference" => "", #gofood, gojek
    //   "reference_no" => "", #orderNo
    //   "payment_type" => "",  #cash, card
    //   "bank" => "",
    //   "edc" => "",
    //   "surcharge" => "",
    //   "email" => "",
    //   "date" => "",
    //   "data_product" => [
    //     {"product" : "", "qty" : "", "price" : ""},
    //     {"product" : "", "qty" : "", "price" : ""},
    //     {"product" : "", "qty" : "", "price" : ""},
    //   ]
    // ]

    $username = Input::get('username');
    $companyId = Input::get('company');
    $sessionId = Input::get('session');
    $date = Input::get('date');
    $reference = Input::get('reference');
    $referenceNo = Input::get('reference_no');
    $paymentType = Input::get('payment_type');
    $bank = Input::get('bank');
    $edc = Input::get('edc');
    $surcharge = Input::get('surcharge');
    $email = Input::get('email');
    $cardNumber = Input::get('card_number');

    $dataProduct = Input::get('data_product');
    $dataProduct = json_decode($dataProduct, true);

    $cashierSession = CashierSession::find($sessionId);

    if(empty($sessionId) || empty($companyId)){
      return response()->json([
        'status' => false
      ]);
    }

    if(empty($cashierSession)){
      return response()->json([
        'status' => false,
        'message' => 'Sesi tidak ditemukan'
      ]);
    }else{
      if($cashierSession->status != 0){
        return response()->json([
          'status' => false,
          'message' => 'Sesi telah berakhir'
        ]);
      }
    }

    $company = SettingCompany::where('code', $companyId)->first();


    $defaultWorkflow = SettingWorkflow::workflowDefault('CH');


    $dataInventory = InventoryWarehouse::getWarehouseCompany($company->id);

    $warehouse = (count($dataInventory) > 0) ? $dataInventory[0] : null;
    if(empty($warehouse)){
      return response()->json([
        'status' => false,
        'message' => 'Data Warehouse Kosong'
      ]);
    }

    $warehouseLocation = InventoryWarehouseLocation::where('warehouse', $warehouse->id)->where('pos', true)->first();
    if(empty($warehouseLocation)){
      return response()->json([
        'status' => false,
        'message' => 'Data Warehouse Location Kosong'
      ]);
    }


    $data = [];
    $data['id'] = null;
    $isCurrency = (Params::get('MULTI_CURRENCY') == 1) ? true : false;
    $defaultCurrency = SettingCurrency::getDefaultCurrency();
    if(!$isCurrency){
      $data['currency'] = $defaultCurrency;
    }
    $data['date'] = $date;
    $data['code'] = InventoryStockMovement::generateId('CH', $company);
    $data['company'] = $company->id;
    $data['type'] = 'CH';
    $data['source'] = $warehouseLocation->id;
    $data['status'] = $defaultWorkflow['code'];
    $data['reference'] = $sessionId;
    $data['created_by'] = $username;
    $data['cashier_session'] = $sessionId;
    $data['cashier_reference'] = $reference;
    $data['cashier_card_number'] = $cardNumber;
    $data['cashier_reference_no'] = $referenceNo;
    $data['cashier_payment_type'] = $paymentType;
    $data['cashier_bank'] = $bank;
    $data['cashier_edc'] = $edc;
    $data['cashier_surcharge'] = $surcharge;
    $data['cashier_customer_email'] = $email;
    // dd($data);
    $newStockMovement = InventoryStockMovement::create($data);


    if(!empty($dataProduct)){
      foreach($dataProduct as $val){

      $product = ProductProduct::find($val['product']);
      if(empty($product)){
        continue;
      }
      $productMaster = ProductMaster::find($product->product);
      $uom = ProductUom::find($productMaster->uom);
      $baseUom = ProductUom::where([
        'uom_category' => $uom->uom_category,
        'type' => 'd'
      ])->first();

      $dt = [];
        $dt['stock_movement'] = $newStockMovement->id;
        $dt['product'] = $val['product'];
        $dt['uom'] = $baseUom->id;
        $dt['qty'] = $val['qty'];
        $dt['price'] = $val['price'];
        $dt['total'] = $val['price'] * $val['qty'];
        $dt['status'] = 0;
        $dt['expected_date'] = null;
        InventoryStockMovementProduct::create($dt);
      }
    }
    InventoryStockMovementLog::addLog($newStockMovement->id, $defaultWorkflow['code']);
    InventoryStockMovement::refreshTotal($newStockMovement->id);

    return response()->json([
      'status' => true
    ]);

  }
}
