<?php

namespace App\Http\Controllers\Api;

use App\Models\ProductMaster;
use App\Models\ProductCategory;
use App\Models\ProductUomCategory;
use App\Models\ProductUom;
use App\Models\ProductSupplier;
use App\Models\ProductAttribute;
use App\Models\ProductAttributeValue;
use App\Models\ProductProduct;
use App\Models\ProductVariant;
use App\Models\ProductBom;
use App\Models\CrmContact;
use App\Models\CrmContactDetail;
use App\Models\ProductAlternative;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Params;
use DB;

class Migration extends Controller{

  public function index(){
    $error = '';
    // Migrate Customer
//     $customer = DB::connection('pgsql2')->table('customer')->get()->toArray();
//     $customer = collect($customer)->map(function($d){
//       return collect($d)->toArray();
//     });
// // dd($customer);
//     foreach($customer as $val){
//       $crmContact = CrmContact::create([
//         'company' => null,
//         'name' => $val['customer_name'],
//         'picture' => null,
//         'address' => $val['customer_address'],
//         'city' => $val['customer_city'],
//         'postal_code' => $val['customer_postal_code'],
//         'state' => $val['customer_state'],
//         'country' => null,
//         'website' => $val['customer_website'],
//         'phone' => $val['customer_phone'],
//         'mobile' => $val['customer_mobile'],
//         'fax' => $val['customer_fax'],
//         'email' => $val['customer_email'],
//         'tax_no' => $val['customer_tax_id'],
//         'type' => ($val['customer_is_customer'] == 1) ? 'customer' : 'supplier',
//         'active' => $val['customer_active'],
//         'pkp' => $val['supplier_pkp'],
//         'revision' => 1,
//         'reference' => $val['customer_id'],
//       ]);
//       $customerContact = DB::connection('pgsql2')->table('customer_contact')->where('customer_id', $val['customer_id'])->get()->toArray();
//       $customerContact = collect($customerContact)->map(function($d){
//         return collect($d)->toArray();
//       });
//
//       if(count($customerContact) > 0){
//         foreach($customerContact as $vval){
//           CrmContactDetail::create([
//             'contact' => $crmContact->id,
//             'type' => $vval['cust_contact_type'],
//             'name' => $vval['cust_contact_name'],
//             'position' => $vval['cust_contact_position'],
//             'address' => $vval['cust_contact_address'],
//             'city' => $vval['cust_contact_city'],
//             'postal_code' => $vval['cust_contact_postal_code'],
//             'state' => $vval['cust_contact_state'],
//             'country' => $vval['cust_contact_country'],
//             'website' => null,
//             'phone' => $vval['cust_contact_phone'],
//             'mobile' => $vval['cust_contact_mobile'],
//             'fax' => $vval['cust_contact_fax'],
//             'email' => $vval['cust_contact_email']
//           ]);
//         }
//       }
//     }
    // End Migrate Customer



    // Migrate Product Category
    // $dataProductCategory = DB::connection('pgsql2')->table('product_category')->get()->toArray();
    // foreach($dataProductCategory as $val){
    //   // dd($val);
    //   ProductCategory::create([
    //     'name' => $val->prod_category_name,
    //     'revision' => 1,
    //     'reference' => $val->prod_category_id
    //   ]);
    // }
    // End Migrate Product Category


    // Migrate Product

    // $dataProduct = DB::connection('pgsql2')->table('product')->get()->toArray();
    // foreach($dataProduct as $val){
    //   $product = collect($val)->toArray();
    //
    //   $category = $product['prod_category_id'];
    //   $category = ProductCategory::where('reference', $category)->first();
    //   $uom = ProductUom::where('code', $product['uom_id'])->first();
    //   if(!empty($category)){
    //     if(!empty($uom)){
    //       $prod = ProductMaster::create([
    //         'code' => $product['product_id'],
    //         'barcode' => $product['product_barcode'],
    //         'name' => $product['product_name'],
    //         'uom' => $uom->id,
    //         'category' => $category->id,
    //         'picture' => '',
    //         'type' => 'ASS',
    //         'sale_price' => $product['product_sale_price'],
    //         'cost_price' => $product['product_cost_price'],
    //         'pos' => $product['product_pos'],
    //         'active' => ($product['product_active'] == 1) ? true : false,
    //         'revision' => 1
    //       ]);
    //
    //       ProductProduct::create([
    //         'product' => $prod->id,
    //         'price' => 0,
    //         'active' => true
    //       ]);
    //     }else{
    //       $error .= 'Uom '.$product['uom_id'].' tidak terdaftar<br/>';
    //     }
    //   }else{
    //     $error .= 'Kategori '.$product['prod_category_id'].' tidak terdaftar<br/>';
    //   }
    //
    //
    // }

    // End Migrate Product


    // Migrate Product Customer
    // $productCustomer = DB::connection('pgsql2')->table('product_customer')->get()->toArray();
    // $productCustomer = collect($productCustomer)->map(function($d){
    //   return collect($d)->toArray();
    // });
    // // dd($productCustomer);
    // if(count($productCustomer) > 0){
    //   foreach($productCustomer as $val){
    //
    //     $productMaster = ProductMaster::where('code', $val['product_id'])->first();
    //     $supplier = CrmContact::where('reference', $val['customer_id'])->first();
    //
    //     ProductSupplier::create([
    //       'product' => $productMaster->id,
    //       'supplier' => $supplier->id,
    //       'price' => $val['product_cust_price']
    //     ]);
    //   }
    // }
    // End Migrate Product Customer


    // Migrate Product BOM
    // $productBom = DB::connection('pgsql2')->table('product_bom')->get()->toArray();
    // $productBom = collect($productBom)->map(function($d){
    //   return collect($d)->toArray();
    // });
    // // dd($productBom);
    // if(count($productBom) > 0){
    //   foreach($productBom as $val){
    //
    //     $master = ProductMaster::where('code', $val['product_header'])->first();
    //     $bom = ProductMaster::where('code', $val['product_id'])->first();
    //     $product = ProductProduct::where('product', $bom->id)->first();
    //     $uom = ProductUom::where('code', $val['uom_id'])->first();
    //
    //     ProductBom::create([
    //       'product_master' => $master->id,
    //       'product' => $product->id,
    //       'uom' => $uom->id,
    //       'qty' => $val['product_qty']
    //     ]);
    //   }
    // }
    // End Migrate Product BOM

    // echo $error;
    // echo "<br/>finish";
    // exit();
    // dd("success");
  }

}
