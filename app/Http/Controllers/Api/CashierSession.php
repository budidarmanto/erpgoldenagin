<?php

namespace App\Http\Controllers\Api;

use App\Models\CashierSession as CashierSessionModel;
use App\Models\SettingUser;
use App\Http\Controllers\Controller;
use App\Models\SettingCompany;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Params;
use Helper;
use Auth;
use Carbon\Carbon;

class CashierSession extends Controller{

  public function index($company){

    $company = SettingCompany::where('code', $company)->first();
    if(!empty($company)){

      $cashierSession = CashierSessionModel::where([
        'company' => $company->id,
        'status' => 0
      ])->first();

      $xml = '<page>';
      if(!empty($cashierSession)){

        $xml .=  '<session>
                    <id>'.$cashierSession->id.'</id>
                    <code>'.$cashierSession->code.'</code>
                    <balance>'.$cashierSession->balance.'</balance>
                    <start>'.$cashierSession->start.'</start>
                    <end>'.$cashierSession->end.'</end>
                  </session>';
      }
      $xml .= '</page>';


      header('Content-type: text/xml');
      echo $xml;
      exit;
    }

  }

}
