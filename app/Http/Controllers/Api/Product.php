<?php

namespace App\Http\Controllers\Api;

use App\Models\ProductMaster;
use App\Models\ProductCategory;
use App\Models\ProductUomCategory;
use App\Models\ProductUom;
use App\Models\ProductSupplier;
use App\Models\ProductAttribute;
use App\Models\ProductAttributeValue;
use App\Models\ProductProduct;
use App\Models\ProductVariant;
use App\Models\ProductBom;
use App\Models\ProductAlternative;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Params;

class Product extends Controller
{

  public function index() {

    $query = ProductProduct::getQuery();
    $query->where('product_master.pos', true);
    $query->where('product_master.active', true);
    $dataProduct = $query->get()->toArray();

    $data = ProductProduct::renderProductAttribute($dataProduct);
    // $data = ProductProduct::getRelatedData($data);
    // dd($data);
    $xml = '<page>';
    if(count($data) > 0){
      foreach($data as $val){

        $image = (empty($val['picture'])) ? '' : Request::root().'/img/product/'.$val['picture'];
        $productName = htmlspecialchars($val['name'], ENT_XML1, 'UTF-8');
        $productCode = htmlspecialchars($val['code'], ENT_XML1, 'UTF-8');
        $url = Request::root().'/api/product-detail/'.$val['id'];
        // dd($val);
        $uom = ProductUom::find($val['uom']);
        $ratio = $uom->ratio;
        $price = $val['sale_price'] / $uom->ratio;

        $xml .= '<product id="'.$val['id'].'" code="'.$productCode.'" barcode="'.$val['barcode'].'" name="'.$productName.'" price="'.$price.'" category_id="'.$val['category'].'" image="'.$image.'" url="'.$url.'" revision="'.$val['revision'].'"/>';
      }
    }
    $xml .= '</page>';

    header('Content-type: text/xml');
    echo $xml;
    exit;
  }
}
