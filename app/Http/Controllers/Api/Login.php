<?php

namespace App\Http\Controllers\Api;

use App\Models\SettingUser;
use App\Models\SettingCompany;
use App\Models\CashierSurcharge;
use App\Models\CashierSession;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\CashierBank;
use App\Models\CashierEdc;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Params;

class Login extends Controller
{

  // Return
  // array:4 [
  //   "status" => true
  //   "message" => "Berhasil!"
  //   "company" => array:8 [
  //     "id" => "5984af8bb6e6e1ce0a"
  //     "code" => "AR"
  //     "name" => "KI Sentul"
  //     "address" => null
  //     "city" => null
  //     "phone" => null
  //     "postal_code" => null
  //     "email" => null
  //   ]
  //   "session" => array:3 [
  //     "id" => "59a7b78adbfdcd80fb"
  //     "code" => "AR.SS1708310001"
  //     "balance" => "9000000.00"
  //   ]
  // ]

  public function index() {

    $input = Input::all();

    $rules = [
        'username' => 'required',
        'password' => 'required|min:6',
    ];

    $validator = Validator::make($input, $rules);
    // If Validation Fails
    if ($validator->fails()) {
      return response()->json(['status' => false, 'message' => 'Username atau password salah!']);
      exit;
    }else{
      $user = SettingUser::where('username', $input['username'])->orWhere('email', $input['username'])->first();

      if ($user) {

        if($user->active != 1){
          return response()->json(['status' => false, 'message' => 'User tidak aktif!']);
          exit;
        }else{
          if (Hash::check($input['password'], $user->password)) {

              $session = [
                  'username' => $user->username,
                  'password' => $input['password']
              ];

              $company = json_decode($user->company);
              $company = collect($company)->first();
              $dataCompany = [];
              $dataSession = [];
              if(!empty($company)){
                $company = SettingCompany::find($company);
                $company = collect($company)->toArray();
                $dataCompany = [
                  'id' => $company['id'],
                  'code' => $company['code'],
                  'name' => $company['name'],
                  'address' => $company['address'],
                  'city' => $company['city'],
                  'phone' => $company['phone'],
                  'postal_code' => $company['postal_code'],
                  'email' => $company['email'],
                ];

                $chasierSession = CashierSession::where([
                  'company' => $dataCompany['id'],
                  'status' => 0
                ])->first();
                if(empty($chasierSession)){
                  return response()->json(['status' => false, 'message' => 'Sesi tidak ditemukan!']);
                  exit;
                }
                $dataSession = [
                  'id' => $chasierSession['id'],
                  'code' => $chasierSession['code'],
                  'balance' => $chasierSession['balance'],
                ];

              }

              $dataResponse = ['status' => true, 'message' => 'Berhasil!', 'company' => $dataCompany, 'session' => $dataSession];

              return response()->json($dataResponse);
              exit;

          } else {
            return response()->json(['status' => false, 'message' => 'Username atau password salah!']);
            exit;
          }
        }
      }else{
        return response()->json(['status' => false, 'message' => 'User tidak ditemukan!']);
        exit;
      }
    }
    exit;
  }
}
