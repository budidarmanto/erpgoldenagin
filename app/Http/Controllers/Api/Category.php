<?php

namespace App\Http\Controllers\Api;

use App\Models\ProductCategory;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;

class Category extends Controller
{

  public function index() {

    $dataCategory = ProductCategory::select([
      'product_category.id',
      'product_category.name',
      'product_category.revision',
      ])
      ->join('product_master', 'product_master.category', 'product_category.id')
      ->where('product_master.pos', true)
      ->groupBy(['product_category.id', 'product_category.name', 'product_category.revision'])->get();

    // dd($dataCategory->toArray());

    $xml = '<page>';
    if(count($dataCategory) > 0){
      foreach($dataCategory as $val){
        $categoryName = htmlspecialchars($val['name'], ENT_XML1, 'UTF-8');
        $xml .= '<category id="'.$val['id'].'" name="'.$categoryName.'" revision="'.$val['revision'].'"/>';
      }
    }
    $xml .= '</page>';

    header('Content-type: text/xml');
    echo $xml;
    exit;

  }
}
