<?php

namespace App\Http\Controllers\Api;

use App\Models\ProductCategory;
use App\Models\CashierSurcharge;
use App\Http\Controllers\Controller;
use App\Models\CashierBank;
use App\Models\CashierEdc;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Params;

class System extends Controller
{

  public function reference() {
// dd("fire");
    $reference = Params::getJson('OPTION_ORDER_REFERENCE');

    // $dataCategory = ProductCategory::all();

    $xml = '<page>';
    if(count($reference) > 0){
      foreach($reference as $key => $val){
        $name = htmlspecialchars($val, ENT_XML1, 'UTF-8');
        $xml .= '<reference id="'.$key.'" name="'.$name.'" />';
      }
    }
    $xml .= '</page>';

    header('Content-type: text/xml');
    echo $xml;
    exit;
  }

  public function bank(){
    $dataBank = CashierBank::all();

    $xml = '<page>';
    if(count($dataBank) > 0){
      foreach($dataBank as $val){
        $name = htmlspecialchars($val->name, ENT_XML1, 'UTF-8');
        $xml .= '<bank id="'.$val->id.'" name="'.$name.'" />';
      }
    }
    $xml .= '</page>';

    header('Content-type: text/xml');
    echo $xml;
    exit;
  }

  public function surcharge(){
    $dataSurcharge = CashierSurcharge::all();

    $xml = '<page>';
    if(count($dataSurcharge) > 0){
      foreach($dataSurcharge as $val){
        $xml .= '<bank edc="'.$val->edc.'" bank="'.$val->bank.'" surcharge="'.$val->surcharge.'" payment_type="'.$val->payment_type.'"/>';
      }
    }
    $xml .= '</page>';

    header('Content-type: text/xml');
    echo $xml;
    exit;
  }

  public function edc(){
    $dataEdc = CashierEdc::all();

    $xml = '<page>';
    if(count($dataEdc) > 0){
      foreach($dataEdc as $val){
        $name = htmlspecialchars($val->name, ENT_XML1, 'UTF-8');
        $xml .= '<edc id="'.$val->id.'" name="'.$name.'" />';
      }
    }
    $xml .= '</page>';

    header('Content-type: text/xml');
    echo $xml;
    exit;
  }
}
