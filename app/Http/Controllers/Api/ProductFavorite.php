<?php

namespace App\Http\Controllers\Api;

use App\Models\ProductMaster;
use App\Models\ProductCategory;
use App\Models\ProductUomCategory;
use App\Models\ProductUom;
use App\Models\ProductSupplier;
use App\Models\ProductAttribute;
use App\Models\ProductAttributeValue;
use App\Models\ProductProduct;
use App\Models\ProductVariant;
use App\Models\ProductBom;
use App\Models\ProductAlternative;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Params;

class ProductFavorite extends Controller
{

  public function index() {

    $query = ProductProduct::getQuery();
    $query->select([
      'product_master.name AS name',
      'product_master.code AS code',
      'product_master.category AS category',
      'product_master.uom AS uom',
      'product_master.revision AS revision',
      'product_uom.uom_category AS uom_category',
      'product_master.type AS type',
      'product_master.barcode AS barcode',
      'product_master.picture AS picture',
      'product_master.sale_price AS sale_price',
      'product_master.cost_price AS cost_price',
      'product_master.pos AS pos',
      'product_master.active AS active',
      'product_product.id',
      'product_master.id AS product_id',
      'product_product.attribute',
      'product_favorite.sold'
    ]);
    $query->join('product_favorite', 'product_favorite.product', '=', 'product_product.id');
    $query->where('product_master.pos', true);
    $query->where('product_master.active', true);
    $query->where('product_favorite.sold', '>', '0');
    $query->orderBy('product_favorite.sold', 'DESC');
    $dataProduct = $query->get()->toArray();

    $data = ProductProduct::renderProductAttribute($dataProduct);
    // $data = ProductProduct::getRelatedData($data);
    // dd($data);
    $xml = '<page>';
    if(count($data) > 0){
      foreach($data as $val){

        $image = (empty($val['picture'])) ? '' : Request::root().'/img/product/'.$val['picture'];
        $productName = htmlspecialchars($val['name'], ENT_XML1, 'UTF-8');
        $productCode = htmlspecialchars($val['code'], ENT_XML1, 'UTF-8');
        $url = Request::root().'/api/product-detail/'.$val['id'];
        // dd($val);
        $uom = ProductUom::find($val['uom']);
        $ratio = $uom->ratio;
        $price = $val['sale_price'] / $uom->ratio;

        $xml .= '<product id="'.$val['id'].'" code="'.$productCode.'" barcode="'.$val['barcode'].'" name="'.$productName.'" price="'.$price.'" category_id="'.$val['category'].'" image="'.$image.'" url="'.$url.'" revision="'.$val['revision'].'" favorite="'.$val['sold'].'"/>';
      }
    }
    $xml .= '</page>';

    header('Content-type: text/xml');
    echo $xml;
    exit;
  }
}
