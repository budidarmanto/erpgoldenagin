<?php

namespace App\Http\Controllers\Setting;

use App\Models\SettingUser;
use App\Models\SettingCompany;
use App\Models\InventoryWarehouse;
use App\Models\InventoryWarehouseLocation;
use App\Models\ManufactureWorkCenter;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Models\AppGroup;
use FormBuilder;
use Datagrid;
use Request;
use Input;
use Helper;
use Carbon\Carbon;

class User extends Controller
{

  public function index() {

    $optionGroup = AppGroup::all();
    $optionGroup = Helper::toFlatOption('id', 'name', $optionGroup);

    $optionCompany = SettingCompany::all();
    $optionCompany = Helper::toFlatOption('id', 'name', $optionCompany);

    $dg = Datagrid::source(SettingUser::query());
    $dg->title(__('app.setting.user.title'));
    $dg->filter('keyword', function($query, $value){
      if($value != ''){
        return $query->where('username', 'ilike', '%'.$value.'%')
        ->orWhere('fullname', 'ilike', '%'.$value.'%')
        ->orWhere('email', 'ilike', '%'.$value.'%');
      }

      return $query;
    });
    $dg->add('username', __('app.setting.user.username'));
    $dg->add('fullname',  __('app.setting.user.fullname'));
    $dg->add('group',  __('app.setting.user.group'))->render(function($data) use($optionGroup){
      $groupDecode = json_decode($data['group']);
      $dataGroup = [];
      if(count($groupDecode) > 0){
        foreach($groupDecode as $val){
          if(isset($optionGroup[$val])){
              $dataGroup[] = $optionGroup[$val];
          }
        }
      }

      return implode(',', $dataGroup);
    });
    $dg->add('company',  __('app.setting.user.company'))->render(function($data) use($optionCompany){
      $companyDecode = json_decode($data['company']);
      $dataCompany = [];
      if(count($companyDecode) > 0){
        foreach($companyDecode as $val){
          if(isset($optionCompany[$val]))
            $dataCompany[] = $optionCompany[$val];
        }
      }

      return implode(',', $dataCompany);
    });

    // $dg->add('email',  __('app.setting.user.email'));
    $dg->add('active',  __('app.setting.user.active'), false)->options([1 => 'Aktif', 0 => 'Tidak Aktif']);
    $dg->add('last_activity',  __('app.setting.user.last_activity'))->render(function($data){
      return Carbon::parse($data['last_activity'])->format('d/m/Y H:i');
    });

    $datagrid = $dg->build();
    return response()->json($datagrid);
  }

  public function create(){
    $form = $this->anyForm(new SettingUser());
    $form->pre(function($input){
      if(isset($input['password']) && $input['password'] != ''){
        $input['password'] = Hash::make($input['password']);
      }
      if(isset($input['group'])){
        $input['group'] = json_encode($input['group']);
      }
      if(isset($input['company'])){
        $input['company'] = json_encode($input['company']);
      }
      if(isset($input['warehouse_location'])){
        $input['warehouse_location'] = json_encode($input['warehouse_location']);
      }
      if(isset($input['workcenter'])){
        $input['workcenter'] = json_encode($input['workcenter']);
      }
      return $input;
    });

    if($form->hasRequest()){
      $validateUsername = SettingUser::where('username', Input::get('username'))->first();
      $validateEmail = SettingUser::where('email', Input::get('email'))->first();

      if(!empty($validateUsername)){
        return response()->json([
          'status' => false,
          'messages' => 'Username sudah digunakan!'
        ]);
      }

      if(!empty($validateEmail)){
        return response()->json([
          'status' => false,
          'messages' => 'Email sudah digunakan!'
        ]);
      }
    }

    $dataForm = $form->build();

    if($form->hasRequest()){



      if($form->saved()){
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }
    $dataForm['optionCompany'] = SettingCompany::getOption();
    $dataForm['optionGroup'] = AppGroup::getOption();
    $dataForm['optionWarehouse'] = InventoryWarehouseLocation::getOption();
    $dataForm['optionWorkcenter'] = ManufactureWorkCenter::getOption();
    return response()->json($dataForm);
  }

  public function modify($id = null) {
    $settingUser = new SettingUser();
    $select = $settingUser->getFillable();
    unset($select[array_search('password', $select)]);

    $form = $this->anyForm(SettingUser::select($select)->find($id));
    $form->pre(function($input){
      if(isset($input['password']) && $input['password'] != ''){
        $input['password'] = Hash::make($input['password']);
      }
      if(isset($input['group'])){
        $input['group'] = json_encode($input['group']);
      }
      if(isset($input['company'])){
        $input['company'] = json_encode($input['company']);
      }
      if(isset($input['warehouse_location'])){
        $input['warehouse_location'] = json_encode($input['warehouse_location']);
      }
      if(isset($input['workcenter'])){
        $input['workcenter'] = json_encode($input['workcenter']);
      }
      return $input;
    });
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }

    $dataForm['optionCompany'] = SettingCompany::getOption();
    $dataForm['optionGroup'] = AppGroup::getOption();
    $dataForm['optionWarehouse'] = InventoryWarehouseLocation::getOption();
    $dataForm['optionWorkcenter'] = ManufactureWorkCenter::getOption();
    $dataForm['data']['group'] = json_decode($dataForm['data']['group']);
    $dataForm['data']['company'] = json_decode($dataForm['data']['company']);
    $dataForm['data']['warehouse_location'] = json_decode($dataForm['data']['warehouse_location']);
    $dataForm['data']['workcenter'] = json_decode($dataForm['data']['workcenter']);
    return response()->json($dataForm);
  }

  public function delete(){

    $id = Input::get('id');
    SettingUser::whereIn('id', $id)->delete();

    return response()->json([
      'status' => true
    ]);
  }

  public function anyForm($source){
    $form = FormBuilder::source($source);
    $form->title(__('app.setting.user.title'));
    $form->add('username', __('app.setting.user.title'), 'text')->rule('required|max:25');
    $form->add('fullname', __('app.setting.user.fullname'), 'text')->rule('required|max:50');
    $form->add('nickname', __('app.setting.user.nickname'), 'text')->rule('max:50');
    $form->add('password', __('app.setting.user.password'), 'password')->rule(($source->exists) ? 'max:60|min:6' : 'required|max:60|min:6');
    $form->add('retypePassword', __('app.setting.user.retypePassword'), 'password')->rule('same:password');
    $form->add('group', __('app.setting.user.group'), 'multiple')->attributes([
      'ng-options' => 'item.id as item.text for item in response.optionGroup'
    ]);
    $form->add('company', __('app.setting.user.company'), 'multiple')->attributes([
      'ng-options' => 'item.id as item.text for item in response.optionCompany'
    ]);
    $form->add('email', __('app.setting.user.email'), 'email')->rule('required|email|max:255');

    $form->add('picture', __('app.setting.user.picture'), 'image')->attributes([
      'moveTo' => 'img/user'
    ]);
    $form->add('active', __('app.setting.user.active'), 'switch');
    $form->add('warehouse_location', 'Gudang', 'multiple')->attributes([
      'ng-options' => 'item.id as item.text for item in response.optionWarehouse'
    ]);
    $form->add('workcenter', 'Work Center', 'multiple')->attributes([
      'ng-options' => 'item.id as item.text for item in response.optionWorkcenter'
    ]);

    return $form;
  }
}
