<?php

namespace App\Http\Controllers\Setting;

use App\Models\SettingTax;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;

class Tax extends Controller
{

  public function index() {

    $dg = Datagrid::source(SettingTax::query());
    $dg->title('Pajak');
    $dg->filter('keyword', function($query, $value){
      if($value != '')
          return $query->where('name', 'ilike', '%'.$value.'%');

      return $query;
    });

    $dg->add('name', 'Nama', true);
    $dg->add('amount', 'Jumlah (%)', true);
    $dg->add('active', 'Aktif')->options([1 => 'Aktif', 0 => 'Tidak Aktif']);
    $datagrid = $dg->build();
    return response()->json($datagrid);
  }

  public function create(){
    $form = $this->anyForm(new SettingTax());
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }
    return response()->json($dataForm);
  }

  public function modify($id = null) {

    $form = $this->anyForm(SettingTax::find($id));
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }
    return response()->json($dataForm);
  }

  public function delete(){

    $id = Input::get('id');
    SettingTax::whereIn('id', $id)->delete();

    return response()->json([
      'status' => true
    ]);
  }

  public function anyForm($source){
    $form = FormBuilder::source($source);
    $form->title('Pajak');
    $form->add('name', 'Nama', 'text')->rule('required|max:50');
    $form->add('amount', 'Nilai (%)', 'number')->rule('required');
    $form->add('active', 'Aktif', 'switch');
    return $form;
  }

}
