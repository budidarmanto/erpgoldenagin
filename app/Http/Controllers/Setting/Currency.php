<?php

namespace App\Http\Controllers\Setting;

use App\Models\SettingCurrency;
use App\Http\Controllers\Controller;
use FormBuilder;
use Datagrid;
use Request;
use Input;

class Currency extends Controller
{

  public function index() {

    $dg = Datagrid::source(SettingCurrency::query());
    $dg->title('Mata Uang');
    $dg->filter('keyword', function($query, $value){
      if($value != '')
          return $query->where('code', 'ilike', '%'.$value.'%')->orWhere('name', 'ilike', '%'.$value.'%');

      return $query;
    });

    $dg->add('code', 'Kode', true);
    $dg->add('name', 'Nama', true);
    $dg->add('default', 'Default')->options([1 => 'Default', 0 => '']);
    $datagrid = $dg->build();
    return response()->json($datagrid);
  }

  public function create(){
    $form = $this->anyForm(new SettingCurrency());
    $form->pre(function($data){
      if($data['default']){
        SettingCurrency::query()->update(['default' => false]);
      }
      return $data;
    });
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }
    return response()->json($dataForm);
  }

  public function modify($id = null) {

    $form = $this->anyForm(SettingCurrency::find($id));
    $form->pre(function($data){
      if($data['default']){
        SettingCurrency::query()->update(['default' => false]);
      }
      return $data;
    });
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'errorMessage' => $form->validatorMessages
        ]);
      }
    }
    return response()->json($dataForm);
  }

  public function delete(){

    $id = Input::get('id');
    SettingCurrency::whereIn('id', $id)->delete();

    return response()->json([
      'status' => true
    ]);
  }

  public function anyForm($source){
    $form = FormBuilder::source($source);
    $form->title('Mata Uang');
    $form->add('code', 'Kode', 'text')->rule('required|max:25');
    $form->add('name', 'Nama', 'text')->rule('required|max:50');
    $form->add('default', 'Default', 'switch');
    $form->add('active', 'Aktif', 'switch');
    return $form;
  }

}
