<?php

 namespace App\Http\Controllers\Setting;

 use App\Models\SettingPeriod;
 use App\Models\SettingCompany;
 use App\Http\Controllers\Controller;
 use FormBuilder;
 use Datagrid;
 use Request;
 use Input;

class Period extends Controller{

  public function index() {
    $dg = Datagrid::source(SettingPeriod::query());
    $dg->title(__('app.setting.period.title'));
    $dg->filter('keyword', function($query, $value){
      if($value != '')
          return $query->where('year', 'ilike', '%'.$value.'%');

      return $query;
    });

    $dg->add('year', __('app.setting.period.year'));
    $datagrid = $dg->build();
    return response()->json($datagrid);
  }

  public function create(){
    $form = $this->anyForm(new SettingPeriod());

    $data = [];
    for ($x = 1; $x <= 12; $x++) {
        $data[] = [
          'month' => $x,
          'status' => 0,
        ];
    }
    $form->pre(function($input) use($data){

      $data = json_encode($data);
      $input['month'] = $data;
      return $input;
    });

    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'error' => $form->validatorMessages
        ]);
      }
    }
    $dataForm['dataDetail'] = $data;
    return response()->json($dataForm);
  }

  public function modify($id = null){
    $form = $this->anyForm(SettingPeriod::find($id));
    $form->pre(function($input){
      $input['month'] = json_encode($input['month']);
      return $input;
    });
    $dataForm = $form->build();

    if($form->hasRequest()){
      if($form->saved()){
        return response()->json([
          'status' => true
        ]);
      }else{
        return response()->json([
          'error' => $form->validatorMessages
        ]);
      }
    }
    $dataForm['data']['month'] = json_decode($dataForm['data']['month']);
    return response()->json($dataForm);
  }

  public function delete(){
    $id = Input::get('id');
    SettingPeriod::whereIn('id', $id)->delete();

    return response()->json([
      'status' => true
    ]);
  }

  public function anyForm($source){
    $form = FormBuilder::source($source);
    $form->title(__('app.setting.period.title'));
    $form->add('year', __('app.setting.period.year'), 'text')->rule('required|max:4');
    $form->add('status', __('app.setting.period.status'), 'switch');
    return $form;
  }

  public function close($id, $mth){

    $data = SettingPeriod::find($id);

    $months = json_decode($data->month);
    $dataMonth = [];
    foreach($months as $month){
      if($month->month == $mth){
        $month->status = 1;
      }
      $dataMonth[] = $month;

    }
    $encodeMonth = json_encode($dataMonth);
    SettingPeriod::where('id',$id)->update(
      ['month' => $encodeMonth ]
    );
    return response()->json([
      'status' => true
    ]);

  }
}
