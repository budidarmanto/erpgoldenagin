<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Params;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      if (strpos(php_sapi_name(), 'cli') === false) {
        Params::init();
      }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
		$this->app->bind('path.public', function() {
			return base_path().'/public_html';
		});
    }
}
