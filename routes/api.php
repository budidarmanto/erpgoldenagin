<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::get('product', ['as' => 'ProductApi', 'uses' => 'Api\Product@index']);
Route::get('product-favorite', ['as' => 'ProductFavoriteApi', 'uses' => 'Api\ProductFavorite@index']);
Route::get('product-detail/{id}', ['as' => 'ProductDetailApi', 'uses' => 'Api\ProductDetail@index']);
Route::get('category', ['as' => 'CategoryApi', 'uses' => 'Api\Category@index']);
Route::get('customer', ['as' => 'CustomerApi', 'uses' => 'Api\Customer@index']);
Route::get('cashier-session/{company}', ['as' => 'CashiersessionApi', 'uses' => 'Api\CashierSession@index']);
Route::any('transaction', ['as' => 'Transaction', 'uses' => 'Api\Transaction@index']);
Route::any('order-reference', ['as' => 'OrderReferenceApi', 'uses' => 'Api\System@reference']);
Route::any('bank', ['as' => 'BankApi', 'uses' => 'Api\System@bank']);
Route::any('edc', ['as' => 'EdcApi', 'uses' => 'Api\System@edc']);
Route::any('surcharge', ['as' => 'SurchargeApi', 'uses' => 'Api\System@surcharge']);
Route::any('login', ['as' => 'LoginApi', 'uses' => 'Api\Login@index']);
