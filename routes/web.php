<?php
use Devplus\FormBuilder\FormBuilder;
use App\Models\PajakPegawai;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
App::setLocale('id');

Route::get('clear_mo', ['as' => 'Home', 'uses' => 'Home@clearWOMO']);
Route::get('test_stock_closing', ['as' => 'TestClosing', 'uses' => 'Home@test_stock_closing']);
Route::get('compare_stock', ['as' => 'CompareStock', 'uses' => 'Home@compare_stock']);

// Auth
Route::get('login', ['as' => 'AuthLogin', 'uses' => 'Home@login']);
Route::post('login', ['uses' => 'Home@dologin']);
Route::get('forgot', ['as' => 'AuthForgot', 'uses' => 'Home@forgotPassword']);
Route::post('forgot', ['uses' => 'Home@doForgotPassword']);
Route::get('forgot-success', ['as' => 'AuthForgotSuccess', 'uses' => 'Home@forgotPasswordSuccess']);
Route::any('reset', ['as' => 'AuthReset', 'uses' => 'Home@resetPassword']);
Route::post('reset', ['uses' => 'Home@doResetPassword']);


//import products from excel
Route::get('import_products/{index}', 'HomeController@import_products');
Route::get('import_product_product', 'HomeController@import_product_product');
Route::get('import_products_bom', 'HomeController@import_products_bom');

Route::group(['middleware' => 'auth'], function () {
  // Dashboard, Logout

  Route::get('/', ['as' => 'home', 'uses' => 'Home@index', 'middleware' => 'optimize.html']);
  Route::get('logout', ['as' => 'AuthLogout', 'uses' => 'Home@logout']);
  Route::any('my-profile', ['as' => 'MyProfile', 'uses' => 'Setting\MyProfile@index']);
  Route::any('change-password', ['as' => 'ChangePassword', 'uses' => 'Setting\MyProfile@changePassword']);
});
/**
 * Route Group For Authenticated User
 */
Route::group(['middleware' => ['auth', 'auth.role']], function () {

  // System Configuration
  Route::group(['namespace' => 'App', 'prefix' => 'app'], function(){

    Route::group(['prefix' => 'role'], function() {
      Route::get('/', ['as' => 'AppRole', 'uses' => 'Role@index']);
      Route::any('create', ['as' => 'AppRoleCreate', 'uses' => 'Role@create']);
      Route::any('modify/{id}', ['as' => 'AppRoleModify', 'uses' => 'Role@modify']);
      Route::any('delete', ['as' => 'AppRoleDelete', 'uses' => 'Role@delete']);
    });

    Route::group(['prefix' => 'group'], function() {
      Route::get('/', ['as' => 'AppGroup', 'uses' => 'Group@index']);
      Route::any('create', ['as' => 'AppGroupCreate', 'uses' => 'Group@create']);
      Route::any('modify/{id}', ['as' => 'AppGroupModify', 'uses' => 'Group@modify']);
      Route::any('delete', ['as' => 'AppGroupDelete', 'uses' => 'Group@delete']);
    });

    Route::group(['prefix' => 'application'], function() {
      Route::get('/', ['as' => 'AppApplication', 'uses' => 'Application@index']);
      Route::any('create', ['as' => 'AppApplicationCreate', 'uses' => 'Application@create']);
      Route::any('modify/{id}', ['as' => 'AppApplicationModify', 'uses' => 'Application@modify']);
      Route::any('delete', ['as' => 'AppApplicationDelete', 'uses' => 'Application@delete']);
    });

    Route::group(['prefix' => 'config'], function() {
      Route::get('/', ['as' => 'AppConfig', 'uses' => 'Config@index']);
      Route::any('create', ['as' => 'AppConfigCreate', 'uses' => 'Config@create']);
      Route::any('modify/{id}', ['as' => 'AppConfigModify', 'uses' => 'Config@modify']);
      Route::any('delete', ['as' => 'AppConfigDelete', 'uses' => 'Config@delete']);
    });
  });
  //--- End System Configuration

  // Setting
  Route::group(['namespace' => 'Setting', 'prefix' => 'setting'], function(){

    Route::group(['prefix' => 'user'], function() {
      Route::get('/', ['as' => 'SettingUser', 'uses' => 'User@index']);
      Route::any('create', ['as' => 'SettingUserCreate', 'uses' => 'User@create']);
      Route::any('modify/{id}', ['as' => 'SettingUserModify', 'uses' => 'User@modify']);
      Route::any('delete', ['as' => 'SettingUserDelete', 'uses' => 'User@delete']);
    });

    Route::group(['prefix' => 'company'], function() {
      Route::get('/', ['as' => 'SettingCompany', 'uses' => 'Company@index']);
      Route::any('create', ['as' => 'SettingCompanyCreate', 'uses' => 'Company@create']);
      Route::any('modify/{id}', ['as' => 'SettingCompanyModify', 'uses' => 'Company@modify']);
      Route::any('delete', ['as' => 'SettingCompanyDelete', 'uses' => 'Company@delete']);
    });

    Route::group(['prefix' => 'period'], function() {
      Route::get('/', ['as' => 'SettingPeriod', 'uses' => 'Period@index']);
      Route::any('create', ['as' => 'SettingPeriodCreate', 'uses' => 'Period@create']);
      Route::any('modify/{id}', ['as' => 'SettingPeriodModify', 'uses' => 'Period@modify']);
      Route::any('delete', ['as' => 'SettingPeriodDelete', 'uses' => 'Period@delete']);
      Route::any('close/{id}/{month}', ['as' => 'SettingPeriodClose', 'uses' => 'Period@close']);
    });

    Route::group(['prefix' => 'workflow'], function() {
      Route::get('/', ['as' => 'SettingWorkflow', 'uses' => 'Workflow@index']);
      Route::any('create', ['as' => 'SettingWorkflowCreate', 'uses' => 'Workflow@create']);
      Route::any('modify/{id}', ['as' => 'SettingWorkflowModify', 'uses' => 'Workflow@modify']);
      Route::any('delete', ['as' => 'SettingWorkflowDelete', 'uses' => 'Workflow@delete']);
    });

    Route::group(['prefix' => 'currency'], function() {
      Route::get('/', ['as' => 'SettingCurrency', 'uses' => 'Currency@index']);
      Route::any('create', ['as' => 'SettingCurrencyCreate', 'uses' => 'Currency@create']);
      Route::any('modify/{id}', ['as' => 'SettingCurrencyModify', 'uses' => 'Currency@modify']);
      Route::any('delete', ['as' => 'SettingCurrencyDelete', 'uses' => 'Currency@delete']);
    });

    Route::group(['prefix' => 'tax'], function() {
      Route::get('/', ['as' => 'SettingTax', 'uses' => 'Tax@index']);
      Route::any('create', ['as' => 'SettingTaxCreate', 'uses' => 'Tax@create']);
      Route::any('modify/{id}', ['as' => 'SettingTaxModify', 'uses' => 'Tax@modify']);
      Route::any('delete', ['as' => 'SettingTaxDelete', 'uses' => 'Tax@delete']);
    });

  });
  //--- Setting

  // CRM
  Route::group(['namespace' => 'Crm', 'prefix' => 'crm'], function(){

    Route::group(['prefix' => 'contact'], function() {
      Route::get('/', ['as' => 'CrmContact', 'uses' => 'Contact@index']);
      Route::any('create', ['as' => 'CrmContactCreate', 'uses' => 'Contact@create']);
      Route::any('modify/{id}', ['as' => 'CrmContactModify', 'uses' => 'Contact@modify']);
      Route::any('delete', ['as' => 'CrmContactDelete', 'uses' => 'Contact@delete']);
    });
  });
  //--- End CRM

  // Product
  Route::group(['namespace' => 'Product', 'prefix' => 'product'], function(){
    // UOM
    Route::group(['prefix' => 'uom'], function() {
      Route::get('/', ['as' => 'ProductUom', 'uses' => 'Uom@index']);
      Route::any('create', ['as' => 'ProductUomCreate', 'uses' => 'Uom@create']);
      Route::any('modify/{id}', ['as' => 'ProductUomModify', 'uses' => 'Uom@modify']);
      Route::any('delete', ['as' => 'ProductUomDelete', 'uses' => 'Uom@delete']);
    });
    // Category
    Route::group(['prefix' => 'category'], function() {
      Route::get('/', ['as' => 'ProductCategory', 'uses' => 'Category@index']);
      Route::any('create', ['as' => 'ProductCategoryCreate', 'uses' => 'Category@create']);
      Route::any('modify/{id}', ['as' => 'ProductCategoryModify', 'uses' => 'Category@modify']);
      Route::any('delete', ['as' => 'ProductCategoryDelete', 'uses' => 'Category@delete']);
    });
    // Attribute
    Route::group(['prefix' => 'attribute'], function() {
      Route::get('/', ['as' => 'ProductAttribute', 'uses' => 'Attribute@index']);
      Route::any('create', ['as' => 'ProductAttributeCreate', 'uses' => 'Attribute@create']);
      Route::any('modify/{id}', ['as' => 'ProductAttributeModify', 'uses' => 'Attribute@modify']);
      Route::any('delete', ['as' => 'ProductAttributeDelete', 'uses' => 'Attribute@delete']);
    });

    // Master Product
    Route::group(['prefix' => 'master'], function() {
      Route::get('/', ['as' => 'ProductMaster', 'uses' => 'Master@index']);
      Route::any('create', ['as' => 'ProductMasterCreate', 'uses' => 'Master@create']);
      Route::any('modify/{id}', ['as' => 'ProductMasterModify', 'uses' => 'Master@modify']);
      Route::any('delete', ['as' => 'ProductMasterDelete', 'uses' => 'Master@delete']);
    });

  });
  //--- End Product

  // Inventory
  Route::group(['namespace' => 'Inventory', 'prefix' => 'inventory'], function(){
    // UOM
    Route::group(['prefix' => 'warehouse'], function() {
      Route::get('/', ['as' => 'InventoryWarehouse', 'uses' => 'Warehouse@index']);
      Route::any('create', ['as' => 'InventoryWarehouseCreate', 'uses' => 'Warehouse@create']);
      Route::any('modify/{id}', ['as' => 'InventoryWarehouseModify', 'uses' => 'Warehouse@modify']);
      Route::any('delete', ['as' => 'InventoryWarehouseDelete', 'uses' => 'Warehouse@delete']);
    });

    // Incoming Order
    Route::group(['prefix' => 'incoming'], function() {
      Route::get('/{poId?}', ['as' => 'InventoryIncoming', 'uses' => 'Incoming@index']);
    });

    // Receipt Order
    Route::group(['prefix' => 'receipt-order'], function() {
      Route::get('/', ['as' => 'InventoryReceiptorder', 'uses' => 'ReceiptOrder@index']);
      Route::any('create/{withOrWithoutPO}', ['as' => 'InventoryReceiptorderCreate', 'uses' => 'ReceiptOrder@create']);
      Route::any('modify/{id}', ['as' => 'InventoryReceiptorderModify', 'uses' => 'ReceiptOrder@modify']);
      Route::any('delete', ['as' => 'InventoryReceiptorderDelete', 'uses' => 'ReceiptOrder@delete']);
      Route::any('change-status/{id}/{status}', ['as' => 'InventoryReceiptorderChange', 'uses' => 'ReceiptOrder@changeStatus']);
      Route::any('data-reference/', ['as' => 'InventoryReceiptorderReference', 'uses' => 'ReceiptOrder@dataReference']);
      Route::any('data-incoming/{id}', ['as' => 'InventoryReceiptorderIncoming', 'uses' => 'ReceiptOrder@dataIncoming']);
      Route::any('print/{id}', ['as' => 'InventoryReceiptorderPrint', 'uses' => 'ReceiptOrder@printOut']);
    });

    // Stock
    Route::group(['prefix' => 'stock'], function() {
      Route::get('/', ['as' => 'InventoryStock', 'uses' => 'Stock@index']);
    });

    // Outgoing Order
    Route::group(['prefix' => 'outgoing'], function() {
      Route::get('/{soId?}', ['as' => 'InventoryOutgoing', 'uses' => 'Outgoing@index']);
    });

    // Delivery Order
    Route::group(['prefix' => 'delivery-order'], function() {
      Route::get('/', ['as' => 'InventoryDeliveryorder', 'uses' => 'DeliveryOrder@index']);
      Route::any('create/{withOrWithoutSO}', ['as' => 'InventoryDeliveryorderCreate', 'uses' => 'DeliveryOrder@create']);
      Route::any('modify/{id}', ['as' => 'InventoryDeliveryorderModify', 'uses' => 'DeliveryOrder@modify']);
      Route::any('delete', ['as' => 'InventoryDeliveryorderDelete', 'uses' => 'DeliveryOrder@delete']);
      Route::any('change-status/{id}/{status}', ['as' => 'InventoryDeliveryorderChange', 'uses' => 'DeliveryOrder@changeStatus']);
      Route::any('data-reference/', ['as' => 'InventoryDeliveryorderReference', 'uses' => 'DeliveryOrder@dataReference']);
      Route::any('data-outgoing/{id}', ['as' => 'InventoryDeliveryorderIncoming', 'uses' => 'DeliveryOrder@dataOutgoing']);
    });

    // Adjustment
    Route::group(['prefix' => 'adjustment'], function() {
      Route::get('/', ['as' => 'InventoryAdjustment', 'uses' => 'Adjustment@index']);
      Route::any('create', ['as' => 'InventoryAdjustmentCreate', 'uses' => 'Adjustment@create']);
      Route::any('modify/{id}', ['as' => 'InventoryAdjustmentModify', 'uses' => 'Adjustment@modify']);
      Route::any('delete', ['as' => 'InventoryAdjustmentDelete', 'uses' => 'Adjustment@delete']);
      Route::any('change-status/{id}/{status}', ['as' => 'InventoryAdjustmentChange', 'uses' => 'Adjustment@changeStatus']);
      Route::any('print/{id}', ['as' => 'InventoryAdjustmentPrint', 'uses' => 'Adjustment@printOut']);
    });

    // Incoming Order
    Route::group(['prefix' => 'incoming-request'], function() {
      Route::get('/{rnId?}', ['as' => 'InventoryIncomingrequest', 'uses' => 'IncomingRequest@index']);
    });

    // External Transfer
    Route::group(['prefix' => 'external-transfer'], function() {
      Route::get('/', ['as' => 'InventoryExternaltransfer', 'uses' => 'ExternalTransfer@index']);
      Route::any('create/{withOrWithoutRN}', ['as' => 'InventoryExternaltransferCreate', 'uses' => 'ExternalTransfer@create']);
      Route::any('modify/{id}', ['as' => 'InventoryExternaltransferModify', 'uses' => 'ExternalTransfer@modify']);
      Route::any('delete', ['as' => 'InventoryExternaltransferDelete', 'uses' => 'ExternalTransfer@delete']);
      Route::any('change-status/{id}/{status}', ['as' => 'InventoryExternaltransferChange', 'uses' => 'ExternalTransfer@changeStatus']);
      Route::any('warehouse-destination/{company}', ['as' => 'InventoryExternaltransferWarehousedestination', 'uses' => 'ExternalTransfer@warehouseDestination']);
      Route::any('data-reference/', ['as' => 'InventoryExternaltransferReference', 'uses' => 'ExternalTransfer@dataReference']);
      Route::any('data-incoming/{id}', ['as' => 'InventoryExternaltransferIncoming', 'uses' => 'ExternalTransfer@dataIncoming']);
    });

    // Internal Transfer
    Route::group(['prefix' => 'internal-transfer'], function() {
      Route::get('/', ['as' => 'InventoryInternaltransfer', 'uses' => 'InternalTransfer@index']);
      Route::any('create', ['as' => 'InventoryInternaltransferCreate', 'uses' => 'InternalTransfer@create']);
      Route::any('modify/{id}', ['as' => 'InventoryInternaltransferModify', 'uses' => 'InternalTransfer@modify']);
      Route::any('delete', ['as' => 'InventoryInternaltransferDelete', 'uses' => 'InternalTransfer@delete']);
      Route::any('change-status/{id}/{status}', ['as' => 'InventoryInternaltransferChange', 'uses' => 'InternalTransfer@changeStatus']);
      Route::any('print/{id}', ['as' => 'InventoryInternaltransferPrint', 'uses' => 'InternalTransfer@printOut']);
    });

    // Return
    Route::group(['prefix' => 'return-order'], function() {
      Route::get('/', ['as' => 'InventoryReturnorder', 'uses' => 'ReturnOrder@index']);
      Route::any('create', ['as' => 'InventoryReturnorderCreate', 'uses' => 'ReturnOrder@create']);
      Route::any('modify/{id}', ['as' => 'InventoryReturnorderModify', 'uses' => 'ReturnOrder@modify']);
      Route::any('delete', ['as' => 'InventoryReturnorderDelete', 'uses' => 'ReturnOrder@delete']);
      Route::any('change-status/{id}/{status}', ['as' => 'InventoryReturnorderChange', 'uses' => 'ReturnOrder@changeStatus']);
    });

  });
  //--- End Inventory

  // Purchase
  Route::group(['namespace' => 'Purchase', 'prefix' => 'purchase'], function(){
    // Supplier
    Route::group(['prefix' => 'supplier'], function() {
      Route::get('/', ['as' => 'PurchaseSupplier', 'uses' => 'Supplier@index']);
      Route::any('create', ['as' => 'PurchaseSupplierCreate', 'uses' => 'Supplier@create']);
      Route::any('modify/{id}', ['as' => 'PurchaseSupplierModify', 'uses' => 'Supplier@modify']);
      Route::any('delete', ['as' => 'PurchaseSupplierDelete', 'uses' => 'Supplier@delete']);
    });

    // Request Order
    Route::group(['prefix' => 'request-order'], function() {
      Route::get('/', ['as' => 'PurchaseRequestorder', 'uses' => 'RequestOrder@index']);
      Route::any('create', ['as' => 'PurchaseRequestorderCreate', 'uses' => 'RequestOrder@create']);
      Route::any('modify/{id}', ['as' => 'PurchaseRequestorderModify', 'uses' => 'RequestOrder@modify']);
      Route::any('delete', ['as' => 'PurchaseRequestorderDelete', 'uses' => 'RequestOrder@delete']);
      Route::any('change-status/{id}/{status}', ['as' => 'PurchaseRequestorderChange', 'uses' => 'RequestOrder@changeStatus']);
      Route::any('print/{id}', ['as' => 'PurchaseRequestorderPrint', 'uses' => 'RequestOrder@printOut']);
      Route::any('duplicate/{id}', ['as' => 'PurchaseRequestorderDuplicate', 'uses' => 'RequestOrder@duplicate']);
      Route::any('history/{id}', ['as' => 'PurchaseRequestorderHistory', 'uses' => 'RequestOrder@getDataComment']);
    });

    // Approval Order
    Route::group(['prefix' => 'approval-order'], function() {
      Route::get('/', ['as' => 'PurchaseApprovalorder', 'uses' => 'ApprovalOrder@index']);
      Route::any('modify/{id}', ['as' => 'PurchaseApprovalorderModify', 'uses' => 'ApprovalOrder@modify']);
      Route::any('change-status/{id}/{status}', ['as' => 'PurchaseApprovalorderChange', 'uses' => 'ApprovalOrder@changeStatus']);
    });

    // Purchase Order
    Route::group(['prefix' => 'purchase-order'], function() {
      Route::get('/', ['as' => 'PurchasePurchaseorder', 'uses' => 'PurchaseOrder@index']);
      Route::any('create', ['as' => 'PurchasePurchaseorderCreate', 'uses' => 'PurchaseOrder@create']);
      Route::any('modify/{id}', ['as' => 'PurchasePurchaseorderModify', 'uses' => 'PurchaseOrder@modify']);
      Route::any('delete', ['as' => 'PurchasePurchaseorderDelete', 'uses' => 'PurchaseOrder@delete']);
      Route::any('change-status/{id}/{status}', ['as' => 'PurchaseRequestorderChange', 'uses' => 'PurchaseOrder@changeStatus']);
      Route::any('print/{id}', ['as' => 'PurchasePurchaseorderPrint', 'uses' => 'PurchaseOrder@printOut']);
    });

    // Purchase Order LBS
    Route::group(['prefix' => 'purchase-lbs'], function() {
      Route::get('/', ['as' => 'PurchasePurchaselbs', 'uses' => 'PurchaseLbs@index']);
      Route::any('create', ['as' => 'PurchasePurchaselbsCreate', 'uses' => 'PurchaseLbs@create']);
      Route::any('modify/{id}', ['as' => 'PurchasePurchaselbsModify', 'uses' => 'PurchaseLbs@modify']);
      Route::any('delete', ['as' => 'PurchasePurchaselbsDelete', 'uses' => 'PurchaseLbs@delete']);
      Route::any('change-status/{id}/{status}', ['as' => 'PurchaseRequestorderChange', 'uses' => 'PurchaseLbs@changeStatus']);
      Route::any('print/{id}', ['as' => 'PurchasePurchaselbsPrint', 'uses' => 'PurchaseLbs@printOut']);
    });

    // Request Note
    Route::group(['prefix' => 'request-note'], function() {
      Route::get('/', ['as' => 'PurchaseRequestnote', 'uses' => 'RequestNote@index']);
      Route::any('create', ['as' => 'PurchaseRequestnoteCreate', 'uses' => 'RequestNote@create']);
      Route::any('modify/{id}', ['as' => 'PurchaseRequestnoteModify', 'uses' => 'RequestNote@modify']);
      Route::any('delete', ['as' => 'PurchaseRequestnoteDelete', 'uses' => 'RequestNote@delete']);
      Route::any('change-status/{id}/{status}', ['as' => 'PurchaseRequestnoteChange', 'uses' => 'RequestNote@changeStatus']);
      Route::any('print/{id}', ['as' => 'PurchaseRequestnotePrint', 'uses' => 'RequestNote@printOut']);
    });


  });
  //--- End Purchase

  // Cashier
  Route::group(['namespace' => 'Cashier', 'prefix' => 'cashier'], function(){

    // Cashier Bank
    Route::group(['prefix' => 'bank'], function() {
      Route::get('/', ['as' => 'CashierBank', 'uses' => 'Bank@index']);
      Route::any('create', ['as' => 'CashierBankCreate', 'uses' => 'Bank@create']);
      Route::any('modify/{id}', ['as' => 'CashierBankModify', 'uses' => 'Bank@modify']);
      Route::any('delete', ['as' => 'CashierBankDelete', 'uses' => 'Bank@delete']);
    });

    // Cashier EDC
    Route::group(['prefix' => 'edc'], function() {
      Route::get('/', ['as' => 'CashierEdc', 'uses' => 'Edc@index']);
      Route::any('create', ['as' => 'CashierEdcCreate', 'uses' => 'Edc@create']);
      Route::any('modify/{id}', ['as' => 'CashierEdcModify', 'uses' => 'Edc@modify']);
      Route::any('delete', ['as' => 'CashierEdcDelete', 'uses' => 'Edc@delete']);
    });

    // Cashier Surcharge
    Route::group(['prefix' => 'surcharge'], function() {
      Route::get('/', ['as' => 'CashierSurcharge', 'uses' => 'Surcharge@index']);
      Route::any('create', ['as' => 'CashierSurchargeCreate', 'uses' => 'Surcharge@create']);
      Route::any('modify/{id}', ['as' => 'CashierSurchargeModify', 'uses' => 'Surcharge@modify']);
      Route::any('delete', ['as' => 'CashierSurchargeDelete', 'uses' => 'Surcharge@delete']);
    });

    // Session
    Route::group(['prefix' => 'session'], function() {
      Route::get('/', ['as' => 'CashierSession', 'uses' => 'Session@index']);
      Route::any('create', ['as' => 'CashierSessionCreate', 'uses' => 'Session@create']);
      Route::any('modify/{id}', ['as' => 'CashierSessionModify', 'uses' => 'Session@modify']);
      Route::any('delete', ['as' => 'CashierSessionDelete', 'uses' => 'Session@delete']);
      Route::any('close/{id}', ['as' => 'CashierSessionClose', 'uses' => 'Session@close']);
    });
    //-- End Session

    // Order
    Route::group(['prefix' => 'cashier-order'], function() {
      Route::get('/', ['as' => 'CashierCashierorder', 'uses' => 'CashierOrder@index']);
      Route::any('create', ['as' => 'CashierCashierorderCreate', 'uses' => 'CashierOrder@create']);
      Route::any('modify/{id}', ['as' => 'CashierCashierorderModify', 'uses' => 'CashierOrder@modify']);
      Route::any('delete', ['as' => 'CashierCashierorderDelete', 'uses' => 'CashierOrder@delete']);
    });
    //-- End Order

  });
  //--- End Cashier

  // Sales
  Route::group(['namespace' => 'Sales', 'prefix' => 'sales'], function(){
    // Customer
    Route::group(['prefix' => 'customer'], function() {
      Route::get('/', ['as' => 'SalesCustomer', 'uses' => 'Customer@index']);
      Route::any('create', ['as' => 'SalesCustomerCreate', 'uses' => 'Customer@create']);
      Route::any('modify/{id}', ['as' => 'SalesCustomerModify', 'uses' => 'Customer@modify']);
      Route::any('delete', ['as' => 'SalesCustomerDelete', 'uses' => 'Customer@delete']);
    });
    //-- End Customer

    // Sales Order
    Route::group(['prefix' => 'sales-order'], function() {
      Route::get('/', ['as' => 'SalesSalesorder', 'uses' => 'SalesOrder@index']);
      Route::any('create', ['as' => 'SalesSalesorderCreate', 'uses' => 'SalesOrder@create']);
      Route::any('modify/{id}', ['as' => 'SalesSalesorderModify', 'uses' => 'SalesOrder@modify']);
      Route::any('delete', ['as' => 'SalesSalesorderDelete', 'uses' => 'SalesOrder@delete']);
      Route::any('change-status/{id}/{status}', ['as' => 'SalesSalesorderChange', 'uses' => 'SalesOrder@changeStatus']);
    });
  });
  //--- End Sales

  Route::group(['namespace' => 'Manufacture', 'prefix' => 'manufacture'], function(){
    // Work Center
    Route::group(['prefix' => 'work-center'], function() {
      Route::get('/', ['as' => 'ManufactureWorkcenter', 'uses' => 'WorkCenter@index']);
      Route::any('create', ['as' => 'ManufactureWorkcenterCreate', 'uses' => 'WorkCenter@create']);
      Route::any('modify/{id}', ['as' => 'ManufactureWorkcenterModify', 'uses' => 'WorkCenter@modify']);
      Route::any('delete', ['as' => 'ManufactureWorkcenterDelete', 'uses' => 'WorkCenter@delete']);
    });

    // Routing
    Route::group(['prefix' => 'routing'], function() {
      Route::get('/', ['as' => 'ManufactureRouting', 'uses' => 'Routing@index']);
      Route::any('create', ['as' => 'ManufactureRoutingCreate', 'uses' => 'Routing@create']);
      Route::any('modify/{id}', ['as' => 'ManufactureRoutingModify', 'uses' => 'Routing@modify']);
      Route::any('delete', ['as' => 'ManufactureRoutingDelete', 'uses' => 'Routing@delete']);
    });

    
    // Manufacture Order
    Route::group(['prefix' => 'manufacture-order'], function() {
      Route::get('/', ['as' => 'ManufactureManufactureorder', 'uses' => 'ManufactureOrder@index']);
      Route::any('create', ['as' => 'ManufactureManufactureorderCreate', 'uses' => 'ManufactureOrder@create']);
      Route::any('modify/{id}', ['as' => 'ManufactureManufactureorderModify', 'uses' => 'ManufactureOrder@modify']);
      Route::any('bom-data/{product_master}', ['as' => 'ManufactureManufactureorderBomdata', 'uses' => 'ManufactureOrder@bomData']);
      Route::any('routing-operation-data/{product_master}', ['as' => 'ManufactureManufactureorderRoutingoperationdata', 'uses' => 'ManufactureOrder@routingOperationData']);
      Route::any('change-status/{id}/{status}', ['as' => 'ManufactureManufactureorderChange', 'uses' => 'ManufactureOrder@changeStatus']);
      Route::any('delete', ['as' => 'ManufactureManufactureorderDelete', 'uses' => 'ManufactureOrder@delete']);
    });
    
    // Work Order
    Route::group(['prefix' => 'work-order'], function() {
      Route::get('/', ['as' => 'ManufactureWorkorder', 'uses' => 'WorkOrder@index']);
      Route::any('change-status/{id}/{status}', ['as' => 'ManufactureWorkorderChange', 'uses' => 'WorkOrder@changeStatus']);
      Route::any('bom-data/{id}', ['as' => 'ManufactureWorkorderBomdata', 'uses' => 'WorkOrder@bomData']);
      Route::any('detail/{id}', ['as' => 'ManufactureWorkorderDetail', 'uses' => 'WorkOrder@detail']);
    });
  });

  // Report
  Route::group(['namespace' => 'Report', 'prefix' => 'report'], function(){
    // Report Stock
    Route::group(['prefix' => 'stock-daily'], function() {
      Route::get('/', ['as' => 'ReportStockdaily', 'uses' => 'StockDaily@index']);
    });
    //-- End Session

    // Stock Card Report
    Route::group(['prefix' => 'stock-card'], function() {
      Route::get('/', ['as' => 'ReportStockcard', 'uses' => 'ReportStockCard@index']);
    });
  });
  // End Report


});

// User Finder
Route::get('user-finder/{employee?}', ['as' => 'UserFinder', 'uses' => 'UserFinder@index']);

// Product Finder
Route::get('product-finder/{product?}', ['as' => 'ProductFinder', 'uses' => 'Product\ProductFinder@index']);

// Stock Card
Route::get('stock-card/{id}/{warehouse?}/{warehouse_location?}', ['as' => 'InventoryStockcard', 'uses' => 'Inventory\StockCard@index']);

// Stock Abnormal
Route::get('stock-abnormal/{id}/{warehouse?}/{warehouse_location?}', ['as' => 'InventoryStockabnormal', 'uses' => 'Inventory\StockCard@getAbnormal2']);

// Options
Route::group(['prefix' => 'options'], function(){
  Route::get('supplier', ['as' => 'OptionsSuppier', 'uses' => 'Globals@getOptionSupplier']);
  Route::get('customer', ['as' => 'OptionsCustomer', 'uses' => 'Globals@getOptionCustomer']);
  Route::get('product', ['as' => 'OptionsProduct', 'uses' => 'Globals@getOptionProduct']);
  Route::get('product-uom/{id}', ['as' => 'OptionsProductuom', 'uses' => 'Globals@getOptionProductUom']);
});

// Options
Route::group(['prefix' => 'general'], function(){
  Route::get('ispkp/{supplier}', ['as' => 'GeneralIspkp', 'uses' => 'Globals@isPkp']);
  Route::get('count-alert', ['as' => 'GeneralCountalert', 'uses' => 'Globals@countAlert']);
  Route::any('stockmovement-comment/{id}', ['as' => 'GeneralStockmovementcomment', 'uses' => 'Globals@addStockMovementComment']);
  Route::post('available-stock', ['as' => 'GeneralAvailablestock', 'uses' => 'Globals@availableStock']);
  Route::get('option-uom', ['as' => 'GeneralOptionuom', 'uses' => 'Globals@optionUom']);
});

// Prototype
Route::get('prototype', ['as' => 'Prototype', 'uses' => 'Prototype@index']);

// Export Excell
Route::get('export-excell', ['as' => 'ExportExcell', 'uses' => 'ExportExcell@index']);

// Upload
Route::any('upload', function(){
  if (Input::hasFile('file')) {
    $moveTo = Input::get('moveTo');
    if($moveTo == ''){
      $moveTo = 'uploads';
    }

    $file = Input::file('file');
    $originalName = $file->getClientOriginalName();
    $extension = $file->getClientOriginalExtension();
    $destinationPath = base_path().'/public/'.$moveTo; // upload path
    $fileName = uniqid().substr(md5(mt_rand()), 0, 5).'.'.$extension; // renameing image

    $file->move($destinationPath, $fileName);
    $realPath = $file->getRealPath();

    return response()->json([
      'originalName' => $originalName,
      'fileName' => $fileName,
      'path' => $moveTo.'/'.$fileName
    ]);
  }

});
Route::any('remove-upload', function(){
  $file = Input::get('file');
  if(!empty($file) && file_exists($file)){
    unlink($file);
    return response()->json([
      'status' => true
    ]);
  }else{
    return response()->json([
      'status' => false,
      'message' => 'File Not Exists!'
    ]);
  }
});

Route::get('change-company/{id}', function($id) {
    $listCompany = Auth::user()->listCompany();
    if(count($listCompany) > 0 && in_array($id, $listCompany)){
      $company = App\Models\SettingCompany::find($id);
      if ($company != null) {
          session(['company' => $id]);
      }
    }

    return response()->json([
      'id' => $company->id,
      'name' => $company->name
    ]);
});


Route::get('/closing', function()
{
    $exitCode = Artisan::call('stock:update');

    //
});

Route::get('/favorite', function()
{
    $exitCode = Artisan::call('favorite:closing');

    //
});

// Template
Route::group(['prefix' => 'template', 'middleware' => 'optimize.html'], function () {
  Route::get('header', function (){

    if(Auth::user()) {
      $listCompany = Auth::user()->listCompany();
    } else {
      $listCompany = [];
    }

    $queryCompany = App\Models\SettingCompany::whereIn('id', $listCompany);
    $company = App\Models\SettingCompany::getOption($queryCompany);
    $currentCompany = Auth::user()->authCompany();

    return view('header', compact('company', 'currentCompany'));
  });
  Route::get('footer', function (){
    return view('footer');
  });
  Route::get('sidebar', function (){
    if(Auth::user()) {
      Auth::user()->buildAccess();
    }
    return view('sidebar');
  });
  Route::get('quick-sidebar', function (){
    return view('quick-sidebar');
  });
  Route::get('blank', function (){
    return view('pages.blank');
  });
  Route::get('no-access', function(){
    return view('pages.no-access');
  });
  Route::get('not-found', function(){
    return view('pages.not-found');
  });
  Route::get('error', function(){
    return view('pages.error');
  });
  Route::get('datagrid', function(){
    return view('pages.datagrid');
  });
  Route::get('form-profile', function (){
    return view('pages.form-profile');
  });
  Route::get('form-manufacture-order', function (){
    return view('pages.form-manufacture-order');
  });
  
  Route::get('datagrid-user-finder', function(){
    return view('pages.datagrid-user-finder');
  });
  
  Route::get('datagrid-product', function(){
    return view('pages.datagrid-product');
  });
  Route::get('datagrid-request-order', function(){
    return view('pages.datagrid-request-order');
  });

  Route::get('datagrid-approval-order', function(){
    return view('pages.datagrid-approval-order');
  });

  Route::get('datagrid-purchase-order', function(){
    return view('pages.datagrid-purchase-order');
  });

  Route::get('datagrid-purchase-lbs', function(){
    return view('pages.datagrid-purchase-lbs');
  });

  Route::get('datagrid-receipt-order', function(){
    return view('pages.datagrid-receipt-order');
  });

  Route::get('datagrid-delivery-order', function(){
    return view('pages.datagrid-delivery-order');
  });

  Route::get('datagrid-sales-order', function(){
    return view('pages.datagrid-sales-order');
  });
  
  Route::get('datagrid-manufacture-order', function(){
    return view('pages.datagrid-manufacture-order');
  });
  
  Route::get('datagrid-work-order', function(){
    return view('pages.datagrid-work-order');
  });

  Route::get('datagrid-incoming', function(){
    return view('pages.datagrid-incoming');
  });

  Route::get('datagrid-incoming-popup', function(){
    return view('pages.datagrid-incoming-popup');
  });

  Route::get('datagrid-outgoing', function(){
    return view('pages.datagrid-outgoing');
  });

  Route::get('datagrid-outgoing-popup', function(){
    return view('pages.datagrid-outgoing-popup');
  });

  Route::get('datagrid-stock', function(){
    return view('pages.datagrid-stock');
  });

  Route::get('datagrid-adjustment', function(){
    return view('pages.datagrid-adjustment');
  });

  Route::get('datagrid-external-transfer', function(){
    return view('pages.datagrid-external-transfer');
  });

  Route::get('datagrid-internal-transfer', function(){
    return view('pages.datagrid-internal-transfer');
  });

  Route::get('datagrid-cashier-session', function(){
    return view('pages.datagrid-cashier-session');
  });

  Route::get('datagrid-cashier-order', function(){
    return view('pages.datagrid-cashier-order');
  });

  Route::get('datagrid-return-order', function(){
    return view('pages.datagrid-return-order');
  });

  Route::get('datagrid-stock-daily', function(){
    return view('pages.datagrid-stock-daily');
  });

  Route::get('datagrid-request-note', function(){
    return view('pages.datagrid-request-note');
  });

  Route::get('datagrid-incoming-request', function(){
    return view('pages.datagrid-incoming-request');
  });

  Route::get('datagrid-incoming-request-popup', function(){
    return view('pages.datagrid-incoming-request-popup');
  });

  Route::get('form', function(){
    return view('pages.form');
  });
  // Pages
  Route::get('form-user', function(){
    return view('pages.form-user');
  });
  Route::get('form-application', function(){
    return view('pages.form-application');
  });
  Route::get('form-group', function(){
    return view('pages.form-group');
  });
  Route::get('form-company', function(){
    return view('pages.form-company');
  });
  Route::get('form-contact', function(){
    return view('pages.form-contact');
  });
  Route::get('form-period', function(){
    return view('pages.form-period');
  });
  Route::get('form-update-period', function(){
    return view('pages.form-update-period');
  });
  Route::get('form-workflow', function(){
    return view('pages.form-workflow');
  });
  Route::get('form-warehouse', function(){
    return view('pages.form-warehouse');
  });

  Route::get('form-request-order', function(){
    return view('pages.form-request-order');
  });

  Route::get('form-approval-order', function(){
    return view('pages.form-approval-order');
  });

  Route::get('form-purchase-order', function(){
    return view('pages.form-purchase-order');
  });

  Route::get('form-purchase-lbs', function(){
    return view('pages.form-purchase-lbs');
  });

  Route::get('form-receipt-order', function(){
    return view('pages.form-receipt-order');
  });

  Route::get('form-delivery-order', function(){
    return view('pages.form-delivery-order');
  });

  Route::get('form-sales-order', function(){
    return view('pages.form-sales-order');
  });

  Route::get('form-adjustment', function(){
    return view('pages.form-adjustment');
  });
  Route::get('form-work-order', function(){
    return view('pages.form-work-order');
  });

  Route::get('form-internal-transfer', function(){
    return view('pages.form-internal-transfer');
  });

  Route::get('form-external-transfer', function(){
    return view('pages.form-external-transfer');
  });

  Route::get('form-cashier-order', function(){
    return view('pages.form-cashier-order');
  });

  Route::get('form-return-order', function(){
    return view('pages.form-return-order');
  });

  Route::get('form-request-note', function(){
    return view('pages.form-request-note');
  });

  Route::get('form-routing', function(){
    return view('pages.form-routing');
  });

  // Product
  Route::get('form-uom', function(){
    return view('pages.form-uom');
  });
  Route::get('form-attribute', function(){
    return view('pages.form-attribute');
  });
  Route::get('form-product', function(){
    return view('pages.form-product');
  });

  // Product Finder

  Route::get('datagrid-product-finder', function(){
    return view('pages.datagrid-product-finder');
  });
  
  Route::get('datagrid-product-finder-single', function(){
    return view('pages.datagrid-product-finder-single');
  });

  Route::get('stock-card', function(){
    return view('pages.stock-card');
  });

  // Stock Card Report
  Route::get('report-stock-card', function(){
    return view('pages.datagrid-report-stock-card');
  });
   
  Route::get('stock-abnormal', function(){
    return view('pages.stock-abnormal');
  });
});
