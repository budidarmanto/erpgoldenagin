<?php

namespace Devplus\FormBuilder;
use Input;
use Request;
use Helper;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class FormBuilder{

  public $fields = [];
  public $action = 'create';
  public $upload = false;
  public $title = '';
  public $model;
  public $back = '';
  public $validatorMessages = [];
  public $errorMessage;
  public $saved = false;
  public $hasRequest = false;
  public $role;

  public static function source($model){
    $ins = new static();
    $ins->model = clone $model;
    $ins->role = Auth::user()->getAccess();

    if(Request::isMethod('post')){
        $ins->hasRequest = true;
        $ins->dataPost = Input::all();
    }

    if($ins->model->exists){
      $ins->action = 'modify';
      if(!in_array('modify', $ins->role)){
        $ins->action = 'view';
      }
    }else if(!in_array('create', $ins->role)){
      $ins->action = '';
    }

    return $ins;
  }

  // before save
  // $form->pre(function($dataPost){ return $dataPost });
  public function pre($fn){

    if (isset($this->dataPost) && is_a($fn, '\Closure')) {
      $this->dataPostModified = call_user_func_array($fn, [$this->dataPost]);
    }

    return $this;
  }

  public function title($title){
    $this->title = $title;
  }

  public function back($back){
    $this->back = $back;
  }

  public function add($source, $label, $type){
    $field = new Fields($source, $label, $type);
    if($type == 'file'){
      $this->upload = true;
    }

    $this->fields[] = $field;
    return $field;
  }

  public function isValid(){
    if(!isset($this->dataPost)){
      return false;
    }

    $inputAll = $this->dataPost;
    foreach ($this->fields as $field) {
        if (isset($field->rule)) {
            $rules[$field->source] = $field->rule;
            $attributes[$field->source] = $field->label;
            if($field->type == 'number'){
                $inputAll[$field->source] = floatval(str_replace(',', '', $inputAll[$field->source]));
            }
        }
    }
    if (isset($this->validator)) {
        return !$this->validator->fails();
    }
    if (isset($rules)) {
        $this->validator = Validator::make($inputAll, $rules, $this->validatorMessages, $attributes);
        $errors = $this->validator->errors();
        $this->validatorMessages = $errors->all();
        $this->compileErrorMsg();
        return !$this->validator->fails();
    } else {
        return true;
    }
  }

  protected function compileErrorMsg() {
      $msg = '';
      if (count($this->validatorMessages) > 1) {
          $msg = '<ul>';
          foreach ($this->validatorMessages as $val) {
              $msg .= '<li>' . $val . '</li>';
          }
          $msg .= '</ul>';
      } else if (count($this->validatorMessages) == 1) {
          $msg = array_first($this->validatorMessages);
      }

      $this->errorMessage = $msg;
  }

  public function saved(){
    $this->model->save();
    $this->saved = true;
    return $this->saved;
  }

  public function hasRequest(){
    return $this->hasRequest;
  }

  public function build(){
    if(isset($this->dataPost)){
      if($this->isValid()){

        if(isset($this->dataPostModified)){
          $this->dataPost = $this->dataPostModified;
        }
        $this->model->fill($this->dataPost);
      }
    }

    if($this->back == ''){
      $this->back = Request::segment(1).'/'.Request::segment(2);
    }
    foreach($this->fields as $key => $val){
      $this->fields[$key] = $val->render();
    }



    return [
      'fields' => $this->fields,
      'upload' => $this->upload,
      'action' => $this->action,
      'title' => $this->title,
      'back' => $this->back,
      'link' => Request::path(),
      'data' => $this->model->getAttributes(),
      'role' => $this->role,
      'id' => uniqid().substr(md5(mt_rand()), 0, 3)
    ];

  }

}
