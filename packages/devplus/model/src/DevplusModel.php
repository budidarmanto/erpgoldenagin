<?php

namespace Devplus\Model;

use Illuminate\Database\Eloquent\Model as BaseModel;
use Auth;

abstract class DevplusModel extends BaseModel {
  public $incrementing = false;

  protected static function boot() {
    parent::boot();

    static::creating(function($table) {
        $table->id = uniqid().substr(md5(mt_rand()), 0, 5);
        if(empty($table->created_by)){
            $table->created_by = (Auth::user()) ? Auth::user()->username : 'SYSTEM'; //(Auth::check()) ? Auth::user()->username : 'SYSTEM';
        }
    });

    static::updating(function($table) {
        $table->updated_by = (Auth::user()) ? Auth::user()->username : 'SYSTEM'; //(Auth::check()) ? Auth::user()->username : 'SYSTEM';
    });
  }
}
