<?php

namespace Devplus\Params;

use App\Models\AppConfig;
use Illuminate\Support\Facades\Auth;
use Helper;

class Params{

    public static $data = "";

    public static function init(){

        $dataConfig = AppConfig::all();
        $data = [];
        foreach($dataConfig as $val){
            $data[$val->code] = $val->value;
        }

        self::$data = $data;
    }

    public static function get($key = ''){
        $dataConfig = AppConfig::all();
        $data = [];
        foreach($dataConfig as $val){
            $data[$val->code] = $val->value;
        }

        self::$data = $data;
        
        if(isset(self::$data[$key])){
            return self::$data[$key];
        }
        return '';
    }

    public static function getJson($key = ''){
      if(isset(self::$data[$key])){
          return json_decode(self::$data[$key], true);
      }
      return [];
    }

    public static function getOption($key = ''){
      $data = self::getJson($key);
      $options = [];
      if(!empty($data)){
        foreach($data as $key => $val){
          $options[] = [
            'id' => $key,
            'text' => $val
          ];
        }
      }

      return $options;
    }


}
