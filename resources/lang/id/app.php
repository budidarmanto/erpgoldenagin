<?php

return [

  'simhu' => [
    'project' => [
      'title' => 'Proyek',
      'name' => 'Nama Proyek',
      'company' => 'Kantor/Cabang',
      'singkatan' => 'Nama Proyek (Singkat)',
      'spk_intern_no' => 'No. SPK Intern',
      'spk_intern_tanggal' => 'Tanggal SPK Intern',
      'spk_extern_no' => 'No. SPK Extern',
      'spk_extern_tanggal' => 'Tanggal SPK Extern',
      'kontrak_no' => 'No. Kontrak',
      'kontrak_tanggal' => 'Tanggal Kontrak',
      'kontrak_harga' => 'Nilai Kontrak',
      'nilai_usd' => 'Nilai USD',
      'jenis_pekerjaan' => 'Jenis Pekerjaan',
      'pemberi_kerja' => 'Pemberi Kerja',
      'sumber_dana' => 'Sumber Dana',
      'kontrak_mulai' => 'Tanggal Mulai Kontrak',
      'kontrak_selesai' => 'Tanggal Selesai Kontrak',
      'kontrak_perpanjangan' => 'Masa Perpanjangan',
      'kontrak_pemeliharaan' => 'Masa Pemeliharaan',
      'catatan' => 'Catatan',
      'status' => 'Status',
      'jenis_proyek' => 'Jenis Proyek',
      'manager_proyek' => 'Manager Proyek',
      'jenis_jo' => 'Jenis JO',
      'nama_partner' => 'Nama Partner',
      'porsi_jo' => 'Porsi JO',
      'assign' => 'Di Tugaskan',
    ],
    'jenis_pekerjaan' => [
      'title' => 'Jenis Pekerjaan',
      'nama' => 'Nama Pekerjaan'
    ],
    'report' => [
      'kategori' => [
        'title' => 'Kategori Laporan',
        'name' => 'Nama',
        'code' => 'Kode'
      ],
      'header' => [
        'title' => 'Header',
        'nama' => 'Nama',
        'code' => 'Kode',
        'parent' => 'Induk',
        'kategori' => 'Kategori'
      ],
      'uraian' => [
        'title' => 'Uraian',
        'nama' => 'Nama',
        'code' => 'Kode',
        'parent' => 'Induk',
        'kategori' => 'Kategori'
      ],
    ],
  ],
  'app' => [
    'application' => [
      'title' => 'Aplikasi',
      'code' => 'Kode',
      'name' => 'Nama Aplikasi',
    ],
    'module' => [
      'title' => 'Modul',
      'code' => 'Kode',
      'name' => 'Nama Modul',
      'role' => 'Role',
    ],
    'config' => [
      'title' => 'Konfigurasi Parameter',
      'code' => 'Kode',
      'value' => 'Nilai',
      'description' => 'Deskripsi'
    ],
    'group' => [
      'title' => 'Grup',
      'name' => 'Nama Grup',
    ],
    'role' => [
      'title' => 'Role',
      'code' => 'Kode',
      'name' => 'Nama Role',
    ],
  ],
  'crm' => [
    'contact' => [
      'title' => 'Kontak',
      'picture' => 'Foto',
      'name' => 'Nama Kontak',
      'company' => 'Kantor/Cabang',
      'type' => 'Tipe',
      'address' => 'Alamat',
      'city' => 'Kota',
      'postal_code' => 'Kode Pos',
      'state' => 'Provinsi',
      'country' => 'Negara',
      'website' => 'Situs Web',
      'phone' => 'Telepon',
      'mobile' => 'No. HP',
      'fax' => 'Fax',
      'email' => 'Email',
      'tax' => 'NPWP',
      'active' => 'Aktif',
    ]
  ],
  'setting' => [
    'company' => [
      'title' => 'Kantor/Cabang',
      'name' => 'Nama Kantor/Cabang',
      'picture' => 'Foto',
      'parent' => 'Induk',
      'address' => 'Alamat',
      'city' => 'Kota',
      'postal_code' => 'Kode Pos',
      'state' => 'Provinsi',
      'country' => 'Negara',
      'website' => 'Situs Web',
      'phone' => 'Telepon',
      'fax' => 'Fax',
      'email' => 'Email',
      'active' => 'Aktif'
    ],
    'period' => [
      'title' => 'Masa Pajak',
      'year' => 'Tahun',
      'month' => 'Masa',
      'status' => 'Status'
    ],
    'user' => [
      'title' => 'Pengguna',
      'username' => 'Username',
      'fullname' => 'Nama Lengkap',
      'nickname' => 'Nama Panggilan',
      'last_activity' => 'Aktivitas terakhir',
      'password' => 'Kata Sandi',
      'retypePassword' => 'Ulangi Kata Sandi',
      'group' => 'Grup',
      'company' => 'Kantor/Cabang',
      'email' => 'Email',
      'picture' => 'Foto',
      'active' => 'Aktif'
     ],
     'workflow' => [
       'title' => 'Alur Proses',
       'code' => 'Kode Kategori',
       'name' => 'Nama Kategori',
       'workflow' => 'Tahapan',
       'next' => 'Tahapan Selanjutnya',
       'description' => 'Deskripsi',
       'color' => 'Warna',
       'default' => 'Tahapan Utama',
     ]
  ]

];
