<?php

return [

  'datagrid' => [
    'filter' => [
      'search' => 'Cari',
    ],
    'header' => [
      'create' => 'Tambah',
    ],
    'massButton' => [
      'withSelected' => 'Tindakan Pilihan',
      'delete' => 'Hapus',
    ],
    'status' => [
      'showing' => 'Menampilkan',
      'to' => 'sampai',
      'from' => 'dari',
      'entries' => 'baris',
      'perpage' => 'baris per halaman'
    ],
    'table' => [
      'action' => 'Tindakan',
      'modify' => 'Ubah',
      'delete' => 'Hapus',
      'no_record' => 'Tidak ada data'
    ]
  ],
  'form' => [
    'action' => [
      'discard' => 'Batalkan',
      'back' => 'Kembali',
      'delete' => 'Hapus',
      'savereturn' => 'Simpan dan Kembali',
      'save' => 'Simpan'
    ]
  ],
  'sidebar' => [
    'app' => [
      'name' => 'Konfigurasi Sistem',
      'group' => 'Autorisasi Grup',
      'application' => 'Aplikasi',
      'role' => 'Role',
      'config' => 'Konfigurasi Parameter'
    ],
    'crm' => [
      'name' => 'CRM',
      'contact' => 'Kontak'
    ],
    'simhu' => [
      'name' => 'SIMHU',
      'master' => [
        'name' => 'Master',
        'uraian' => 'Uraian',
        'category' => 'Kategori',
        'header' => 'Header',
        'jenis_pekerjaan' => 'Jenis Pekerjaan',
        'project' => 'Proyek',
      ],
      'report' => 'Laporan',
    ],
    'setting' => [
      'name' => 'Pengaturan',
      'user' => 'Pengguna',
      'company' => 'Kantor/Cabang',
      'period' =>'Masa Pajak',
      'workflow' =>'Alur Proses'
    ]
  ],
  'error' => [
    '500' => [
      'title' => '500',
      'oops' => 'Oops! Suatu kesalahan telah terjadi',
      'comeback' => 'Kita akan memperbaiki. Mohon menunggu beberapa saat.'
    ],
    'no_access' => [
      'oops' => 'Oops! Tidak ada akses',
      'permission' => "Anda tidak memiliki akses untuk halaman ini!",
      'return' => 'Kembali ke beranda'
    ],
    '404' => [
      'oops' => "Oops! Kau tersesat.",
      'find' => "Tidak dapat menemukan halaman yang anda minta.",
      'return' => 'Kembali ke beranda'
    ]
  ]



];
