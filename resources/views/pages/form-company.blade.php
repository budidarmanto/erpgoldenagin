<!-- BEGIN MAIN CONTENT -->
<div class="row">
  <div class="col-md-12">
      <!-- BEGIN: ACCORDION DEMO -->
    <form role="form" id="@{{ response.id }}">
      <div class="portlet">
        <div class="portlet-title">
          @include('pages.partials.form.form-title')
          @include('pages.partials.form.form-action')
        </div>
        <div class="portlet-body">
          <div class="row margin-bottom-5 margin-top-20">
            <div class="col-md-4 text-center">
              <form-builder field="picture"></form-builder>
              <form-builder field="active"></form-builder>
            </div>
            <div class="col-md-8">
              <form-builder field="code"></form-builder>
              <form-builder field="name"></form-builder>
              <form-builder field="email"></form-builder>
              <form-builder field="parent"></form-builder>
              <div class="row">
                <div class="col-md-6">
                  <form-builder field="city"></form-builder>
                </div>
                <div class="col-md-6">
                  <form-builder field="postal_code"></form-builder>
                </div>
              </div>
              <form-builder field="address"></form-builder>
              <div class="row">
                <div class="col-md-6">
                  <form-builder field="state"></form-builder>
                </div>
                <div class="col-md-6">
                  <form-builder field="country"></form-builder>
                </div>
              </div>
              <form-builder field="website"></form-builder>
              <div class="row">
                <div class="col-md-6">
                  <form-builder field="phone"></form-builder>
                </div>
                <div class="col-md-6">
                  <form-builder field="fax"></form-builder>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
<!-- END MAIN CONTENT -->
