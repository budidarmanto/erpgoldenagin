<!-- BEGIN MAIN CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN: ACCORDION DEMO -->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption font-red">
                    <!-- <i class="icon-settings font-green-sharp"></i> -->
                    <span class="caption-subject bold uppercase"> @{{ response.title }} </span>
                    <span class="caption-helper hide"></span>
                </div>

                <div class="actions">
                    <a target="_blank" href="@{{ response.link+( (queryString == '') ? '?export=1' : queryString+'&export=1' ) }}" class="btn btn-sm default"><i class="fa fa-download"></i> Export</a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-6 col-md-offset-6 text-right">
                        @include('pages.partials.datagrid.datagrid-status')
                    </div>
                </div>
                <div class="row margin-bottom-10">
                    <div class="col-md-12">
                        <div class="table-filter row">
                            <div class="col-md-3" style="padding-right: 0;">
                                <div class="input-icon input-icon-sm right">
                                    <i class="fa fa-search"></i>
                                    <input type="text" ng-model="keyword" class="form-control input-sm" placeholder="@lang('system.datagrid.filter.search')" ng-change="keywordChanged()">
                                </div>
                            </div>
                            <div class="col-md-9" style="padding-right: 0; padding-left: 2;">
                                <select style="display: inline-block; width: auto;" ng-model="workflow" class="form-control input-sm" ng-options="item.id as item.label for item in response.optionWorkflow" ng-change="workflowChanged()">
                                    <option value="">-- Semua Status --</option>
                                </select>
                                <div id="date-range" class="tooltips btn btn-fit-height green" >
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Start Table -->
                <div class="table-responsive">
                    <table class="table table-hover table-light dataTable" border="0" cellspacing="0" cellpadding="0">
                        <thead>
                            <tr>
                                <th ng-repeat="column in response.columns">
                                    <div ng-click="setParams('sort', (column.sorting == 'asc' ? '-' : '')+column.source)" ng-class="getSortingStatus(column)" class="th-inner">
                                    @{{ column.label }}
                                    </div>
                                </th>
                                <th style="width: 120px;" class="text-center">@lang('system.datagrid.table.action')</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="dt in response.data" ng-dblclick="handleDoubleClick(dt['id'])">
                                <td>@{{ dt.ro }}</td>
                                <td>@{{ dt.date_start }}</td>
                                <td>@{{ dt.wc }}</td>
                                <td>@{{ dt.mo }}</td>
                                <td>@{{ dt.product }}</td>
                                <td>@{{ dt.qty }}</td>
                                <td>
                                    <div class="btn-group dropdown-status">
                                        <div ng-if="dt.status == '6FF'">
                                            <span class="btn-status" style="background-color: #324070;">Done</span>
                                        </div>
                                        <div ng-if="dt.status != '6FF'">
                                            <button class="btn-status" type="button" data-toggle="dropdown" style="background-color: @{{ getStatus(dt.status).color }}">@{{ getStatus(dt.status).label }}
                                                <i class="fa fa-angle-down" ng-hide="getStatus(dt.status).next.length <= 0 || response.role.indexOf('change') == -1"></i>
                                            </button>
                                            <ul class="dropdown-menu" role="menu" style="right: 0; left: auto;" ng-hide="response.role.indexOf('change') == -1">
                                                <li ng-repeat="next in getStatus(dt.status).next">
                                                    <a href="javascript:;" ng-click="changeStatus(dt, next.id)"> @{{ next.text }} </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <a href="#/@{{ response.link+'/detail/'+dt['id'] }}" class="btn btn-icon-only tooltips" data-original-title="View" data-placement="left">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <p class="text-center margin-top-10 margin-bottom-10 caption10" ng-if="response.data.length <= 0">@lang('system.datagrid.table.no_record')</p>
                <!-- End Table -->
                <div class="row margin-top-10">
                    <div class="col-md-6">
                        <div ng-if="response.massAction.length > 0">
                            <!-- @include('pages.partials.datagrid.datagrid-mass-button') -->
                        </div>
                    </div>
                    <div class="col-md-6">
                        @include('pages.partials.datagrid.datagrid-pagination')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MAIN CONTENT -->


<!-- EDIT BOM POPUP TEMPLATE -->
<script type="text/ng-template" id="modalWODone.html">
    <form>
        <div class="modal-header">
            <h3 class="modal-title" id="modal-title">Work Order Done</h3>
        </div>
        <div class="modal-body" id="modal-body">
            <div class="row">
                <div class="col-md-6" ng-repeat="field in response.fields">
                    <div ng-if="field.name !== 'scrap'">
                        <div class="form-group form-md-line-input ng-scope">
                            <label>@{{field.label}}</label>
                            <div ng-if="field.unit_name">
                                <div style="width:60%;float:left;">
                                    <input type="number" name="@{{field.name}}" ng-model="data[field.name]" required="" decimals="" class="input-sm form-control" value="0">
                                </div>
                                <div style="width:40%;float:left;padding:10px 0px 10px 10px;">
                                    @{{field.unit_name}}
                                </div>
                            </div>
                            <div ng-if="!field.unit_name">
                                <input type="number" name="@{{field.name}}" ng-model="data[field.name]" required="" decimals="" class="input-sm form-control" value="0">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group form-md-line-input ng-scope">
                        <label>Description</label>
                        <input type="text" name="description" ng-model="data['description']" class="input-sm form-control" value="0">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-sm btn-default" type="button" ng-click="cancel()">Batal&nbsp;&nbsp;<i class="fa fa-times"></i></button>
            <button class="btn btn-sm green-haze" type="submit" ng-click="save(data, $event)">Simpan&nbsp;&nbsp;<i class="fa fa-floppy-o"></i></button>
        </div>
    </form>
</script>
<!-- END EDIT VARIANT POPUP TEMPLATE -->