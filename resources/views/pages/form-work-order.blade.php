<!-- BEGIN MAIN CONTENT -->
<div class="row">
    <div class="col-md-12">
      <!-- BEGIN: ACCORDION DEMO -->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption font-green-sharp">
                    <span class="caption-subject bold uppercase ng-binding">Work Order</span> 
                    &nbsp;&nbsp; 
                    <span class="btn-status" style="background: @{{ getStatus(data.status).color }}; color: #fff; padding: 3px 6px;" ng-hide="response.action == 'create'">@{{ getStatus(data.status).label }}</span>
                </div>
                <div class="actions">
                    <a href="#/manufacture/work-order" class="btn btn-sm btn-default"><i class="fa fa-angle-left"></i>&nbsp;&nbsp;Kembali</a>
                </div>
                <div class="actions" ng-hide="getStatus(data.status).text.length <= 0 || response.action == 'create' || response.role.indexOf('change') == -1" style="margin-left: 40px;float: left;">
                    <a href="#" onclick="return false;" class="btn blue btn-outline mt-ladda-btn ladda-button" ng-repeat="next in getStatus(data.status).next" data-style="zoom-out"  ng-click="changeStatus(next.id, $event)" style="margin-right: 5px;" >@{{ next.text }}</a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row margin-bottom-5 margin-top-20">
                    <div class="col-md-6">
                        <div class="form-group form-md-line-input form-md-floating-label" style="padding: 0 0 2px !important;">
                            <label>Work Order</label><br />
                            <input type="text" disabled ng-value="data.ro" class="form-control" />
                        </div>
                        <div class="form-group form-md-line-input form-md-floating-label" style="padding: 0 0 2px !important;">
                            <label>Work Center</label><br />
                            <input type="text" disabled ng-value="data.wc" class="form-control" />
                        </div>
                        <div class="form-group form-md-line-input form-md-floating-label" style="padding: 0 0 2px !important;">
                            <label>Product</label><br />
                            <input type="text" disabled ng-value="data.product" class="form-control" />
                        </div>
                        <div class="form-group form-md-line-input form-md-floating-label" ng-if="data.hasOwnProperty('date_start') && data.date_start !== null" style="padding: 0 0 2px !important;">
                            <label>Start</label><br />
                            <input type="text" disabled ng-value="data.date_start" class="form-control" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group form-md-line-input form-md-floating-label" style="padding: 0 0 2px !important;">
                            <label>Manufacture Order</label><br />
                            <input type="text" disabled ng-value="data.mo" class="form-control" />
                        </div>
                        <div class="form-group form-md-line-input form-md-floating-label" style="padding: 0 0 2px !important;">
                            <label>Duration</label><br />
                            <input type="text" disabled ng-value="getWoDuration()" class="form-control" />
                        </div>
                        <div class="form-group form-md-line-input form-md-floating-label" style="padding: 0 0 2px !important;">
                            <label>Original Production Quantity</label><br />
                            <input type="text" disabled ng-value="data.qty + ' ' + data.uom" class="form-control" />
                        </div>
                        <div class="form-group form-md-line-input form-md-floating-label" ng-if="data.hasOwnProperty('date_end') && data.date_end !== null" style="padding: 0 0 2px !important;">
                            <label>Finish</label><br />
                            <input type="text" disabled ng-value="data.date_end" class="form-control" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
      
        <div class="portlet" ng-if="dataWoPending.length > 0">
            <div class="portlet-title">
                <span class="caption">Pending Time</span>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-hover table-light dataTable" border="0" cellspacing="0" cellpadding="0">
                        <thead>
                            <tr>
                                <th>Pending Start</th>
                                <th>Resume</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="woPending in dataWoPending">
                                <td>@{{ woPending.pending_start }}</td>
                                <td>@{{ woPending.pending_end }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MAIN CONTENT -->

<!-- EDIT BOM POPUP TEMPLATE -->
<script type="text/ng-template" id="modalWODone.html">
    <form>
        <div class="modal-header">
            <h3 class="modal-title" id="modal-title">Work Order Done</h3>
        </div>
        <div class="modal-body" id="modal-body">
            <div class="row">
                <div class="col-md-6" ng-repeat="field in response.fields">
                    <div ng-if="field.name !== 'scrap'">
                        <div class="form-group form-md-line-input ng-scope">
                            <label>@{{field.label}}</label>
                            <div ng-if="field.unit_name">
                                <div style="width:60%;float:left;">
                                    <input type="number" name="@{{field.name}}" ng-model="data[field.name]" required="" decimals="" class="input-sm form-control" value="0">
                                </div>
                                <div style="width:40%;float:left;padding:10px 0px 10px 10px;">
                                    @{{field.unit_name}}
                                </div>
                            </div>
                            <div ng-if="!field.unit_name">
                                <input type="number" name="@{{field.name}}" ng-model="data[field.name]" required="" decimals="" class="input-sm form-control" value="0">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group form-md-line-input ng-scope">
                        <label>Description</label>
                        <input type="text" name="description" ng-model="data['description']" class="input-sm form-control" value="0">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-sm btn-default" type="button" ng-click="cancel()">Batal&nbsp;&nbsp;<i class="fa fa-times"></i></button>
            <button class="btn btn-sm green-haze" type="submit" ng-click="save(data, $event)">Simpan&nbsp;&nbsp;<i class="fa fa-floppy-o"></i></button>
        </div>
    </form>
</script>
<!-- END EDIT VARIANT POPUP TEMPLATE -->