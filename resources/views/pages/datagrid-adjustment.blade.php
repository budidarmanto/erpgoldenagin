<!-- BEGIN MAIN CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN: ACCORDION DEMO -->
        <div class="portlet">
            
			<div class="portlet-title">
				<div class="caption font-red">
					<!-- <i class="icon-settings font-green-sharp"></i> -->
					<span class="caption-subject bold uppercase"> @{{ response.title }} </span>
					<span class="caption-helper hide"></span>
				</div>
				<div class="actions">
				  <!--
				  <a href="javascript:;" ng-click="printPage('inventory/adjustment/print-all)" class="btn btn-sm btn-default ng-scope" data-original-title="Print" data-placement="left" target="__blank" ng-if="response.role.indexOf('print') != -1">
                     <i class="fa fa-print"></i>
                  </a>
				  <a href="javascript:;" ng-click="printPage('inventory/adjustment/print/'+dt['id'])" class="btn btn-icon-only tooltips" data-original-title="Print" data-placement="left" target="__blank" ng-if="response.role.indexOf('print') != -1 && dt['status'] == '3AA'">
                          <i class="fa fa-print"></i>
                        </a>
				  -->
				  
				  <a href="javascript:;" ng-click="printPage('inventory/adjustment?&export=1')" class="btn btn-sm default" target="__blank" ><i class="fa fa-print"></i> Print</a>
				  <a href="#/@{{ response.create }}" class="btn btn-sm btn-primary" ng-if="response.role.indexOf('create') != -1"> @lang('system.datagrid.header.create') <i class="fa fa-plus"></i></a>
				</div>
			</div>
            <div class="portlet-body">
              <div class="row margin-bottom-10">
                <div class="col-md-12">
                  <div class="table-filter row">
                    <div class="col-md-3" style="padding-right: 0;">
                      <div class="input-icon input-icon-sm right">
                        <i class="fa fa-search"></i>
                        <input type="text" ng-model="keyword" class="form-control input-sm" placeholder="@lang('system.datagrid.filter.search')" ng-change="keywordChanged()">
                      </div>
                    </div>
                    <!-- <div class="col-md-2" style="padding-right: 0; padding-left: 2;">
                      <div class="input-icon input-icon-sm left">
                        <i class="fa fa-calendar right"></i>
                        <input type="text" ng-model="date" class="form-control input-sm date-picker" placeholder="Tanggal" ng-change="dateChanged($event)">
                      </div>
                    </div> -->
                    <div class="col-md-2" style="padding-right: 0; padding-left: 2;">
                      <select ng-model="source" class="form-control input-sm" ng-change="sourceChanged()">
                        <option value="">-- Semua Asal --</option>
                        <option ng-repeat="item in response.optionWarehouseLocation" ng-value="item.id" ng-class="item.text.toLowerCase().indexOf('tps') !== -1 ? 'font-red' : ''">@{{item.text}}</option>
                      </select>
                    </div>
                    <div class="col-md-2" style="padding-right: 0; padding-left: 2;">
                      <select ng-model="workflow" class="form-control input-sm" ng-options="item.id as item.label for item in response.optionWorkflow" ng-change="workflowChanged()">
                        <option value="">-- Semua Status --</option>
                      </select>
                    </div>
                  </div>

                </div>
              </div>
              <div class="row margin-bottom-10">
                <div class="col-md-2" style="padding-right: 0; padding-left: 2;">
                  <div class="input-icon input-icon-sm left">
                    <i class="fa fa-calendar right"></i>
                    <input type="text" ng-model="datestart" class="form-control input-sm date-picker" placeholder="Dari Tanggal" ng-change="dateStartChanged($event)" id="datestart">
                  </div>
                </div>
                <div class="col-md-2" style="padding-right: 0; padding-left: 2;">
                  <div class="input-icon input-icon-sm left">
                    <i class="fa fa-calendar right"></i>
                    <input type="text" ng-model="dateend" class="form-control input-sm date-picker" placeholder="Sampai Tanggal" ng-change="dateEndChanged($event)" id="dateend">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 col-md-offset-6 text-right">
                  @include('pages.partials.datagrid.datagrid-status')
                </div>
              </div>
              <!-- Start Table -->
              <div class="table-responsive">
                <table class="table table-hover table-light dataTable" border="0" cellspacing="0" cellpadding="0">
                  <thead>
                    <tr>
                      <!-- <th class="table-select" ng-if="response.massAction.length > 0">
                        <label class="mt-checkbox mt-checkbox-outline">
                            <input type="checkbox" ng-model="toggle" ng-change="allChecked()" />
                            <span></span>
                        </label>
                      </th> -->
                      <th ng-repeat="column in response.columns">
                        <div ng-click="setParams('sort', (column.sorting == 'asc' ? '-' : '')+column.source)" ng-class="getSortingStatus(column)" class="th-inner">
                        @{{ column.label }}
                        </div>
                      </th>
                      <th style="width: 120px;" class="text-center">@lang('system.datagrid.table.action')</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr ng-repeat="dt in response.data" ng-dblclick="handleDoubleClick(dt['id'])">
                      <!-- <td class="table-select" ng-if="response.massAction.length > 0">
                        <label class="mt-checkbox mt-checkbox-outline" ng-init="initSelect(dt)">
                            <input type="checkbox" name="check" ng-model="selected[$index].isChecked"/>
                            <span></span>
                        </label>
                      </td> -->
                      <!-- <td ng-repeat="column in response.columns" compile-template ng-bind-html="dt[column.source]"></td> -->
                      <td>@{{ dt.code }}</td>
                      <td>@{{ dt.date }}</td>
                      <td>@{{ dt.source }}</td>
                      <td>@{{ dt.description }}</td>
                      <td>
                        <div class="btn-group dropdown-status">
                          <button class="btn-status" type="button" data-toggle="dropdown" style="background-color: @{{ getStatus(dt.status).color }}">@{{ getStatus(dt.status).label }}
                              <i class="fa fa-angle-down" ng-hide="getStatus(dt.status).next.length <= 0 || response.role.indexOf('change') == -1"></i>
                          </button>
                          <ul class="dropdown-menu" role="menu" style="right: 0; left: auto;" ng-hide="response.role.indexOf('change') == -1">
                              <li ng-repeat="next in getStatus(dt.status).next">
                                  <a href="javascript:;" ng-click="changeStatus(dt.id, next.id)"> @{{ next.text }} </a>
                              </li>
                          </ul>
                        </div>
                      </td>
                      <td>@{{ dt.created_user }}</td>
                      <td>@{{ dt.changed_user }}</td>
                      <td class="text-center">
                        <a href="#/@{{ response.modify+'/'+dt['id'] }}" class="btn btn-icon-only tooltips" data-original-title="@lang('system.datagrid.table.modify')" data-placement="left" ng-if="response.role.indexOf('modify') != -1 && dt['status'] == '1AC'">
                          <i class="fa fa-edit"></i>
                        </a>
                        <a href="#/@{{ response.modify+'/'+dt['id'] }}" class="btn btn-icon-only tooltips" data-original-title="@lang('system.datagrid.table.modify')" data-placement="left" ng-if="response.role.indexOf('modify') == -1 || dt['status'] != '1AC'">
                          <i class="fa fa-eye"></i>
                        </a>
                        <a href="javascript:;" class="btn btn-icon-only tooltips" data-original-title="@lang('system.datagrid.table.delete')" data-placement="left" delete-confirm-ajax="@{{ dt['id'] }}" ng-if="response.role.indexOf('delete') != -1 && dt['status'] == '1AC'">
                          <i class="fa fa-trash"></i>
                        </a>
                        <a href="javascript:;" ng-click="printPage('inventory/adjustment/print/'+dt['id'])" class="btn btn-icon-only tooltips" data-original-title="Print" data-placement="left" target="__blank" ng-if="response.role.indexOf('view') != -1 && dt['status'] == '3AA'">
                          <i class="fa fa-print"></i>
                        </a>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <p class="text-center margin-top-10 margin-bottom-10 caption10" ng-if="response.data.length <= 0">@lang('system.datagrid.table.no_record')</p>
              <!-- End Table -->
              <div class="row margin-top-10">
                <div class="col-md-6">
                  <div ng-if="response.massAction.length > 0">
                    <!-- @include('pages.partials.datagrid.datagrid-mass-button') -->
                  </div>
                </div>
                <div class="col-md-6">
                  @include('pages.partials.datagrid.datagrid-pagination')
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
<!-- END MAIN CONTENT -->
