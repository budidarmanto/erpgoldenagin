<!-- BEGIN MAIN CONTENT -->
<div class="row">
  <div class="col-md-12">
      <!-- BEGIN: ACCORDION DEMO -->
    <form role="form" >
      <div class="portlet">
        <div class="portlet-title">
          @include('pages.partials.form.form-title')
          @include('pages.partials.form.form-action')
        </div>
        <div class="portlet-body">
          <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <form-builder field="year"></form-builder>
            </div>
          </div>
        </div>
        <div class="portlet-body">
          <div class="portlet">
            <div class="portlet-title">
            </div>
            <div class="portlet-body">
              <div class="table-responsive">
                <table class="table table-hover table-light dataTable" border="0" cellspacing="0" cellpadding="0">
                  <thead>
                    <tr>
                      <th>Month</th>
                      <th>Status</th>
                      <th ng-if="response.data.id != null">Tindakan</th>
                    </tr>
                  </thead>
                  <tbody ng-init="lock = false">
                      <tr ng-repeat="month in dataMonth">
                          <td>@{{ getMonth(month) }}</td>
                          <td>@{{ (month.status == 0) ? 'Open' : 'Close' }}</td>
                          <td ng-if="response.data.id != null">
                            <div ng-if="month.statusLive">
                              <a class="btn-icon-only tooltips" data-original-title="Close" data-placement="left" ng-click="changeStatus(month)" style="font-size: 16px; color: #888;">
                                <i class="fa fa-lock"></i>
                              </a>
                            </div>
                            <div ng-else>
                            </div>
                          </td>
                      </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
