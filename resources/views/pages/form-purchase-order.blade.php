<!-- BEGIN MAIN CONTENT -->
<div class="row">
  <div class="col-md-12">
      <!-- BEGIN: ACCORDION DEMO -->
      <form role="form" id="@{{ response.id }}" id="#formPurchaseOrder">
        <div class="portlet">
          <div class="portlet-title">
            <div class="caption font-green-sharp">
                <!-- <i class="icon-settings font-green-sharp"></i> -->
                <span class="caption-subject bold uppercase">@{{ response.title }}</span>
                &nbsp;&nbsp;
                <span class="btn-status" style="background: @{{ getStatus(data.status).color }}; color: #fff; padding: 3px 6px;" ng-hide="response.action == 'create'">@{{ getStatus(data.status).label }}</span>
            </div>

            @include('pages.partials.form.form-action')
            <div class="actions" style="margin-right: 40px;">
              <a href="javascript:;" ng-click="printPage('purchase/purchase-order/print/'+data.id)" class="btn btn-sm default tooltips" data-original-title="Print" data-placement="left" target="__blank" ng-if="response.role.indexOf('print') != -1 && data.status == '3PO' && response.action != 'create'">
                Print
                <i class="fa fa-print"></i>
              </a>
            </div>
            <div class="actions" ng-hide="getStatus(data.status).text.length <= 0 || response.action == 'create' || response.role.indexOf('change') == -1" style="margin-left: 40px;float: left;">
              <a href="#" onclick="return false;" class="btn blue btn-outline mt-ladda-btn ladda-button" ng-repeat="next in getStatus(data.status).next" data-style="zoom-out"  ng-click="changeStatus(next.id, $event)" style="margin-right: 5px;" >@{{ next.text }}</a>
            </div>
          </div>
          <div class="portlet-body">
            <div class="row margin-bottom-5 margin-top-20">
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-6">
                    <form-builder field="code"></form-builder>
                    <div ng-if="hasReference">
                      <form-builder field="reference"></form-builder>
                    </div>
                    <form-builder field="date"></form-builder>
                    <form-builder field="category"></form-builder>
                    <!-- <form-builder field="diajukan"></form-builder> -->
                  </div>
                  <div class="col-md-6">
                    <form-builder field="contact"></form-builder>
                    <form-builder field="destination"></form-builder>
                    <form-builder field="description"></form-builder>
                    <form-builder field="currency"></form-builder>
                    <div ng-hide="data.currency == response.defaultCurrency">
                      <form-builder field="currency_rate"></form-builder>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
      <div class="portlet">
        <div class="portlet-title">
          <div class="caption">Barang/Jasa</div>
          <div class="actions">
            <a href="#" class="btn btn-sm btn-success" ng-click="showPopupProduct()"  ng-if="response.action!='view'"> Tambah <i class="fa fa-plus"></i></a>
          </div>
        </div>
        <div class="portlet-body">
            <table class="table table-hover table-light dataTable margin-bottom-20" border="0" cellspacing="0" cellpadding="0">
              <thead>
                <tr>
                  <th style="width: 25%;">Barang/Jasa</th>
                  <th class="text-right" style="width: 100px;">Jumlah</th>
                  <th class="text-right" style="width: 100px;">Jumlah Diterima</th>
                  <th class="text-center">Satuan Ukuran</th>
                  <th>Estimasi Kedatangan</th>
                  <th class="text-right">Harga</th>
                  <th class="text-center">Potongan</th>
                  <th class="text-number">Total Potongan</th>
                  <th class="text-center">Pajak</th>
                  <th class="text-right">Subtotal</th>
                  <th class="text-center" ng-if="response.action!='view'" ng-hide="response.action == 'view'"></th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="product in dataProduct | orderBy:'name'">
                  <td>@{{ product.name }}</td>
                  <td class="text-right">
                    <a href="#" editable-text="product.qty" buttons="no" onbeforesave="validateNumber($data)" blur="submit" ng-hide="response.action == 'view'" e-class="number" e-style="width: 100px;">
                      @{{ product.qty | number:2 || 0 }}
                    </a>
                    <span ng-hide="response.action != 'view'">
                      @{{ product.qty | number:2 || 0 }}
                    </span>
                  </td>
                  <td class="text-right">
                    @{{ product.rcp_qty | number:2 || 0 }}
                  </td>
                  <td class="text-center">
                    <!-- <a href="#" editable-select="product.uom" buttons="no" e-ng-options="item.id as item.text for item in product.optionUom" ng-hide="response.action == 'view'">
                      @{{ (product.optionUom | filter:{id:product.uom})[0].text || 'Pilih Satuan' }}
                    </a> -->
                    <span>
                      @{{ (product.optionUom | filter:{id:product.uom})[0].text || '-' }}
                    </span>
                  </td>
                  <td style="position: relative;">
                    <a href="#" e-name="expected_date" editable-text="product.expected_date" buttons="no"  e-class="date-picker" ng-click="handleDatePickers($event)" blur="submit" onbeforesave="saveExpectedDate($data, product)" ng-hide="response.action == 'view'">
                      @{{ product.expected_date || 'Pilih Tanggal' }}
                    </a>
                    <span ng-hide="response.action != 'view'">
                      @{{ product.expected_date || '-' }}
                    </span>
                  </td>
                  <td class="text-right">
                    <a href="#" editable-text="product.price" buttons="no" onbeforesave="validateNumber($data)" blur="submit" ng-hide="response.action == 'view'" e-class="number">
                      @{{ product.price | number:2 || 0 }}
                    </a>
                    <span ng-hide="response.action != 'view'">
                      @{{ product.price | number:2 || 0 }}
                    </span>
                  </td>
                  <td class="text-center">
                    <a href="#" onclick="return false;" ng-click="showPopupDiscount(product)" style="text-decoration: none;" ng-hide="response.action == 'view'">
                      <span ng-if="product.discount.length > 0">@{{ showDiscount(product.discount) }}</span>
                      <span ng-if="product.discount.length <= 0"><i class="fa fa-plus"></i></span>
                    </a>
                    <span ng-hide="response.action != 'view'">
                      @{{ showDiscount(product.discount) }}
                    </span>
                  </td>
                  <td class="text-right">
                      @{{ showTotalDiscount(product) | number:2 }}
                  </td>
                  <td class="text-center">
                    <a href="#" editable-default-select="product.tax" buttons="no" blur="submit" e-ng-options="item.id as item.text for item in optionTax" onbeforesave="taxChanged($data, product)" ng-hide="response.action == 'view'">
                      <span ng-if="product.tax != '' && product.tax != null">@{{ (optionTax | filter:{id:product.tax})[0].text || 'Pajak' }}</span>
                      <span ng-if="product.tax == '' || product.tax == null"><i class="fa fa-plus" style="color:#337ab7;"></i></span>
                    </a>
                    <span ng-hide="response.action != 'view'">
                      <span ng-if="product.tax != '' && product.tax != null">@{{ (optionTax | filter:{id:product.tax})[0].text || 'Pajak' }}</span>
                      <span ng-if="product.tax == '' || product.tax == null">-</span>
                    </span>
                  </td>
                  <td class="text-right">@{{ getSubTotal(product) || 0 | number:2 }}</td>
                  <td ng-hide="response.action == 'view'" class="text-right">
                    <a href="javascript:;" class="btn btn-icon-only tooltips" data-original-title="Delete" data-placement="left" delete-confirm on-delete="deleteProduct(product)">
                      <i class="fa fa-trash"></i>
                    </a>
                  </td>
                </tr>
              </tbody>
            </table>
            <div class="row">
              <div class="col-md-1 col-md-offset-9">Harga</div>
              <div class="col-md-2 text-right">
                @{{ getTotal().price | number:2 }}
              </div>
            </div>
            <div class="row">
              <div class="col-md-1 col-md-offset-9">Potongan</div>
              <div class="col-md-2 text-right">
                @{{ getTotal().discount | number:2 }}
              </div>
            </div>
            <div class="row">
              <div class="col-md-1 col-md-offset-9">Pajak</div>
              <div class="col-md-2 text-right">
                @{{ getTotal().tax | number:2 }}
              </div>
            </div>
            <div class="row">
              <div class="col-md-3 col-md-offset-9">
                <div class="separator" style="border-top: 1px solid #ccc;margin: 5px 0; "></div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-1 col-md-offset-9">Total</div>
              <div class="col-md-2 text-right">
                @{{ getTotal().total | number:2 }}
              </div>
            </div>
        </div>
      </div>
      <!-- Timeline Log & Comments -->
      <div class="row" ng-hide="response.action == 'create'">
        <div class="col-md-12">
          <div style="border-top: 1px solid #e3e3e3; margin-top: 20px; margin-bottom: 20px;"></div>
        </div>
        <div class="col-md-12">
          @include('pages.partials.inventory-stockmovement-comment')
        </div>
      </div>
      <!-- End Timeline Log & Comments -->
  </div>
</div>
<!-- END MAIN CONTENT -->


<!-- EDIT SUPPLIER POPUP TEMPLATE -->
<script type="text/ng-template" id="modalRequestSupplier.html">
  <div class="modal-header">
      <h3 class="modal-title" id="modal-title">Pilih Pemasok</h3>
  </div>
  <div class="modal-body" id="modal-body">
    <p style="color: #666; margin: 0 0 10px 0px;">
    Daftar pemasok untuk produk &ldquo; @{{ data.name }} &rdquo;</p>
    <table class="table table-hover table-light dataTable" border="0" cellspacing="0" cellpadding="0">
      <tbody>
        <tr ng-repeat="supplier in optionSupplier" style="cursor: pointer;" ng-click="save(supplier)">
          <td>@{{ supplier.text }}</td>
          <td class="text-right">@{{ supplier.price | number:2 }}</td>
        </tr>
      </tbody>
    </table>
  </div>
</script>
<!-- END EDIT SUPPLIER POPUP TEMPLATE -->

<!-- EDIT DISCOUNT POPUP TEMPLATE -->
<script type="text/ng-template" id="modalRequestDiscount.html">
  <div class="modal-header">
      <h3 class="modal-title" id="modal-title">Potongan</h3>
  </div>
  <div class="modal-body" id="modal-body">
    <p style="color: #666; margin: 0 0 10px 0px;">
    &ldquo;@{{ data.name }}&rdquo;</p>
    <div class="row margin-bottom-5" ng-repeat="discount in dataDiscount">
      <div class="col-md-8">
        <div class="form-group form-md-line-input form-md-floating-label" style="margin: 0; padding: 0;">
            <div class="input-group right-addon input-group-sm">
                <input type="text" class="form-control input-sm" ng-model="discount.value">
                <!-- <label for="form_control_1">Input Group</label> -->
                <span class="input-group-addon">
                    <i class="">%</i>
                </span>
            </div>
        </div>
      </div>
      <div class="col-md-2">
        <a href="#" onclick="return false;" ng-click="deleteDiscount($index)" class="text-danger" style="margin-top: 12px;display: inline-block;">
          <i class="fa fa-times"></i>
        </a>
      </div>
    </div>
    <div class="margin-top-20">
      <button class="btn btn-xs btn-primary" ng-click="addDiscount()">Tambah Diskon <i class="fa fa-plus"></i></button>
    </div>
  </div>
  <div class="modal-footer" style="text-align: center;">
      <button class="btn btn-sm green-haze" type="submit" ng-click="save()">Simpan&nbsp;&nbsp;<i class="fa fa-floppy-o"></i></button>
  </div>
</script>
<!-- END EDIT DISCOUNT POPUP TEMPLATE -->
