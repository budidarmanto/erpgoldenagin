<!-- BEGIN MAIN CONTENT -->
<div class="row">
  <div class="col-md-12">
      <!-- BEGIN: ACCORDION DEMO -->
    <form role="form" id="@{{ response.id }}">
      <div class="portlet">
        <div class="portlet-title">
          @include('pages.partials.form.form-title')
          @include('pages.partials.form.form-action')
        </div>
        <div class="portlet-body">
          <div class="row margin-bottom-5 margin-top-20">
            <div class="col-md-8 col-md-offset-2">
              <div class="row">
                <div class="col-md-6">
                  <form-builder field="period_month"></form-builder>
                </div>
                <div class="col-md-6">
                  <form-builder field="period_year"></form-builder>
                </div>
              </div>

              <div class="form-group form-md-line-input form-md-floating-label">
                <div class="input-group">
                  <div class="input-group-control">
                    <form-builder field="npwp" form-group="false"></form-builder>
                  </div>
                  <span class="input-group-btn btn-right">
                      <button class="btn btn-default btn-sm" type="button" ng-click="showPopupPegawai()"><i class="icon-magnifier"></i></button>
                  </span>
                </div>
              </div>
              <form-builder field="pegawai"></form-builder>
              <div class="row">
                <div class="col-md-6">
                  <form-builder field="nip"></form-builder>
                </div>
                <div class="col-md-6">
                  <form-builder field="nama"></form-builder>
                </div>
              </div>
              <form-builder field="pembetulan"></form-builder>
              <form-builder field="objek"></form-builder>
              <form-builder field="bruto"></form-builder>
              <form-builder field="pph"></form-builder>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
<!-- END MAIN CONTENT -->

<!-- EDIT MODULE POPUP TEMPLATE -->
<script type="text/ng-template" id="modalPajakPegawai.html">
  <div class="modal-header">
      <h3 class="modal-title" id="modal-title">Pegawai</h3>
  </div>
  <div class="modal-body" id="modal-body">
    @include('pages.datagrid-view')
  </div>
</script>
<!-- END EDIT MODULE POPUP TEMPLATE -->
