<!-- BEGIN MAIN CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN: ACCORDION DEMO -->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption font-red">
                    <!-- <i class="icon-settings font-green-sharp"></i> -->
                    <span class="caption-subject bold uppercase"> @{{ response.title }} </span>
                    <span class="caption-helper hide"></span>
                </div>

                <div class="actions">
                    <select ng-model="export_format" style="width:100px;display:inline-block;margin-right:10px;" class="form-control input-sm">
                      <option selected="selected" value="excel">Excel</option>
                      <option value="pdf">PDF</option>
                    </select>
                  <a target="_blank" href="@{{ response.link+( (queryString == '') ? '?export=1&format='+export_format : queryString+'&export=1&format='+export_format ) }}" class="btn btn-sm default"><i class="fa fa-download"></i> Export</a>
                </div>
                
            </div>

            <div class="portlet-body">
              <div class="row margin-bottom-20">
                <div class="col-md-6" style="padding-left: 2;">
                    <select ng-model="warehouse" class="input-sm" ng-options="item.id as item.text for item in response.optionWarehouse" ng-change="warehouseChanged()" style="width: 200px;">
                      <option value="">-- Semua Gudang --</option>
                    </select>

                    <select 
                        ng-model="warehouse_location" 
                        class="input-sm" 
                        ng-options="item.id as item.text for item in optionWarehouseLocation" style="width: 200px;" ng-change="warehouseLocationChanged()"
                        options-class="{'font-red': text.indexOf('TPS') !== -1}">
                      <option value="">-- Semua Lokasi --</option>
                    </select>
                </div>
                <div class="col-md-6 text-right">
                  @include('pages.partials.datagrid.datagrid-status')
                </div>
              </div>
              <div class="row margin-bottom-10">
                <div class="col-md-12">
                  <div class="table-filter row">
                    <div class="col-md-3" style="padding-right: 0;">
                      <div class="input-icon input-icon-sm right">
                        <i class="fa fa-search"></i>
                        <input type="text" ng-model="keyword" class="form-control input-sm" placeholder="@lang('system.datagrid.filter.search')" ng-change="keywordChanged()">
                      </div>
                    </div>
                    <div class="col-md-3" style="padding-right: 0; padding-left: 2;">
                        <select ng-model="category" class="input-sm" ng-options="item.id as item.text for item in response.optionCategory" ng-change="categoryChanged()">
                        <option value="">-- Semua Kategori --</option>
                    </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row margin-bottom-20">
                <div class="col-md-12">
                    <span style="width:60px; display:inline-block">Range : </span>
                    <div class="input-icon input-icon-sm left" style="width: 200px; display:inline-block">
                        <i class="fa fa-calendar right"></i>
                        <input type="text" ng-model="datestart" class="form-control input-sm date-picker" placeholder="Tanggal Mulai" ng-change="dateStartChanged($event)" id="datestart">
                    </div>
                    <div class="input-icon input-icon-sm left" style="width: 200px;display:inline-block">
                        <i class="fa fa-calendar right"></i>
                        <input type="text" ng-model="dateend" class="form-control input-sm date-picker" placeholder="Tanggal Selesai" ng-change="dateEndChanged($event)" id="dateend">
                    </div>
                  </div>
                </div>
              </div>
              <!-- Begin Table -->
              <div class="table-responsive" style="padding-top:20px;">
                <div ng-repeat="data in response.data" style="margin-bottom:30px;">
                    <div>
                        <p style="margin:0px 0px 5px 0px">Kode : @{{data.code}}</p>
                        <p style="margin:0px 0px 5px 0px">Nama Barang : @{{data.name}}</p>
                        <table class="table table-hover table-light dataTable" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed; width: 100%">
                            <thead>
                                <tr>
                                <th>Date</th>
                                <th>No</th>
                                <th>Description</th>
                                <th>Source</th>
                                <th>Destination</th>
                                <th>In</th>
                                <th>Out</th>
                                <th>Stock</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                  <td colspan="7" style="font-weight: bold;">Stok Awal</td>
                                  <td style="font-weight: bold;">@{{ data.last_stock['begin'] | number:2 }}</td>
                                  <td></td>
                                </tr>
                                <tr ng-repeat="dt in data.stock_card">
                                <td compile-template ng-bind-html="dt['date']"></td>
                                <td compile-template ng-bind-html="dt['code']"></td>
                                <td compile-template ng-bind-html="dt['description']"></td>
                                <td compile-template ng-bind-html="dt['source_name']"></td>
                                <td compile-template ng-bind-html="dt['destination_name']"></td>
                                <td compile-template ng-bind-html="dt['qty_in']"></td>
                                <td compile-template ng-bind-html="dt['qty_out']"></td>
                                <td>
                                  @{{dt['stock_akhir'] | number:2 }}
                                </td>
                                </tr>
                                <tr>
                                  <td colspan="7" style="font-weight: bold;">Stok Akhir</td>
                                  <td style="font-weight: bold;">@{{ data.last_stock['end'] | number:2 }}</td>
                                  <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
              </div>
              <div class="row margin-top-10">
                <div class="col-md-6">
                  <div ng-if="response.massAction.length > 0">
                    @include('pages.partials.datagrid.datagrid-mass-button')
                  </div>
                </div>
                <div class="col-md-6">
                  @include('pages.partials.datagrid.datagrid-pagination')
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
<!-- END MAIN CONTENT -->

<style>
td.text-stock:hover{
  color: #408da7 !important;
  font-weight: bold;

}
</style>
