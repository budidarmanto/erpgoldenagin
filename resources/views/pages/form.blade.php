<!-- BEGIN MAIN CONTENT -->
<div class="row">
  <div class="col-md-12">
      <!-- BEGIN: ACCORDION DEMO -->
    <form role="form" id="@{{ response.id }}">
      <div class="portlet">
        <div class="portlet-title">
          @include('pages.partials.form.form-title')
          @include('pages.partials.form.form-action')
        </div>
        <div class="portlet-body">
          @include('pages.partials.form.form-content')
        </div>
      </div>
    </form>
  </div>
</div>
<!-- END MAIN CONTENT -->
