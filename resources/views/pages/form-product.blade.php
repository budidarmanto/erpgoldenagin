<!-- BEGIN MAIN CONTENT -->
<div class="row">
  <div class="col-md-12">
      <!-- BEGIN: ACCORDION DEMO -->
    <form role="form" id="@{{ response.id }}">
      <div class="portlet">
        <div class="portlet-title">
          @include('pages.partials.form.form-title')
          @include('pages.partials.form.form-action')
        </div>
        <div class="portlet-body">
          <div class="margin-bottom-25 margin-top-20">
            <div class="col-md-4">
              <div class="text-center margin-bottom-20">
                <form-builder field="picture"></form-builder>
              </div>
              <div style="padding-left: 60px;">
                <form-builder field="active"></form-builder>
                <form-builder field="pos"></form-builder>
                <form-builder field="favorite"></form-builder>
              </div>
            </div>
            <div class="col-md-8">
              <form-builder field="code"></form-builder>
              <form-builder field="name"></form-builder>
              <form-builder field="barcode"></form-builder>
                <form-builder field="type"></form-builder>
              <form-builder field="category"></form-builder>
              <div class="row">
                <div class="col-md-6">
                  <form-builder field="uom_category"></form-builder>
                </div>
                <div class="col-md-6">
                  <form-builder field="uom"></form-builder>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <form-builder field="sale_price"></form-builder>
                </div>
                <div class="col-md-6">
                  <form-builder field="tax"></form-builder>
                </div>
              </div>
                <form-builder field="weight"></form-builder>
                <form-builder field="routing"></form-builder>
              <form-builder field="description"></form-builder>

        </div>
        <ul class="nav nav-tabs">
          <li class="ng-hide">
              <a href="#supplier" data-toggle="tab" onclick="return false;"><i class="fa fa-cubes"></i> Pemasok </a>
          </li>
          <li  class="ng-hide"><!-- ng-hide="!useVariant" -->
              <a href="#variant" data-toggle="tab" onclick="return false;"><i class="fa fa-sitemap"></i> Varian </a>
          </li>
          <li class="active">
              <a href="#bom" data-toggle="tab" onclick="return false;"><i class="fa fa-puzzle-piece"></i> Bahan Baku </a>
          </li>
          <li>
              <a href="#alternative" data-toggle="tab" onclick="return false;"><i class="fa fa-exchange"></i> Produk Alternatif/Pengganti </a>
          </li>
        </ul>
        <div class="tab-content" style="margin-bottom: 100px; min-height: 300px;">
          <!-- Supplier -->
          <div class="tab-pane fade hide" id="supplier">
            <div class="portlet">
              <div class="portlet-title">
                <div class="caption">Pemasok</div>
                <div class="actions">
                  <a href="#" class="btn btn-sm btn-success" ng-click="showPopupSupplier()"  ng-if="response.action!='view'"> Tambah <i class="fa fa-plus"></i></a>
                </div>
              </div>
              <div class="portlet-body">
                <div class="table-responsive">
                  <table class="table table-hover table-light dataTable" border="0" cellspacing="0" cellpadding="0">
                    <thead>
                      <tr>
                        <th>Pemasok</th>
                        <th>Harga</th>
                        <th>Diskon</th>
                        <th>Pajak</th>
                        <th style="width: 120px;" class="text-center" ng-if="response.action!='view'">Tindakan</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="supplier in dataSupplier" ng-dblclick="showPopupSupplier(supplier)">
                        <td>@{{ supplier.name }}</td>
                        <td>@{{ supplier.price | number:2 }}</td>
                        <td>@{{ (supplier.discount != null) ? (supplier.discount | number:2)+'%' : '' }}</td>
                        <td>@{{ (supplier.tax != null) ? (response.optionTax | filter:{id:supplier.tax})[0].text : '' }}</td>
                        <td class="text-center" ng-if="response.action!='view'">
                          <a href="javascript:;" class="btn btn-icon-only tooltips" data-original-title="Modify" data-placement="left" ng-click="showPopupSupplier(supplier)">
                            <i class="fa fa-edit"></i>
                          </a>
                          <a href="javascript:;" class="btn btn-icon-only tooltips" data-original-title="Delete" data-placement="left" delete-confirm on-delete="deleteSupplier(supplier)">
                            <i class="fa fa-trash"></i>
                          </a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <!-- End Supplier -->

          <!-- Variant -->
          <div class="tab-pane fade hide" id="variant" ng-hide="!useVariant">
            <div class="portlet">
              <div class="portlet-title">
                <div class="caption">Varian</div>
                <div class="actions">
                  <a href="#" class="btn btn-sm btn-success" ng-click="showPopupVariant()"  ng-if="response.action!='view'"> Tambah <i class="fa fa-plus"></i></a>
                </div>
              </div>
              <div class="portlet-body">
                <div class="table-responsive">
                  <table class="table table-hover table-light dataTable" border="0" cellspacing="0" cellpadding="0">
                    <thead>
                      <tr>
                        <th>Varian</th>
                        <th>Nilai</th>
                        <th style="width: 120px;" class="text-center" ng-if="response.action!='view'">Tindakan</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="variant in dataVariant" ng-dblclick="showPopupVariant(variant)">
                        <td>@{{ (response.optionAttribute | filter:{id:variant.attribute})[0].text }}</td>
                        <td>
                          <span ng-repeat="attributeValue in variant.attribute_value">
                            @{{ ((response.optionAttributeValue | filter:{id:attributeValue})[0].text) + ((($index + 1) ==  variant.attribute_value.length) ? '' : ', ') }}
                          </span>
                        </td>
                        <td class="text-center" ng-if="response.action!='view'">
                          <a href="javascript:;" class="btn btn-icon-only tooltips" data-original-title="Modify" data-placement="left" ng-click="showPopupVariant(variant)">
                            <i class="fa fa-edit"></i>
                          </a>
                          <a href="javascript:;" class="btn btn-icon-only tooltips" data-original-title="Delete" data-placement="left" delete-confirm on-delete="deleteVariant(variant)">
                            <i class="fa fa-trash"></i>
                          </a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <!-- End Variant -->

          <!-- BOM -->
          <div class="tab-pane fade active in" id="bom">
            <div class="portlet">
              <div class="portlet-title">
                <div class="caption">Bahan Baku</div>
                <div class="actions">
                  <a href="#" class="btn btn-sm btn-success" ng-click="showPopupBom()"  ng-if="response.action!='view'"> Tambah <i class="fa fa-plus"></i></a>
                </div>
              </div>
              <div class="portlet-body">
                <div class="table-responsive">
                  <table class="table table-hover table-light dataTable" border="0" cellspacing="0" cellpadding="0">
                    <thead>
                      <tr>
                        <th>Kode</th>
                        <th>Bahan Baku</th>
                        <th>Jumlah</th>
                        <th>Satuan Ukuran</th>
                        <th style="width: 120px;" class="text-center" ng-if="response.action!='view'">Tindakan</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="bom in dataBom" ng-dblclick="showPopupBom(bom)">
                        <td>@{{ bom.product_code }}</td>
                        <td>@{{ bom.product_name }}</td>
                        <td>
                          @{{ bom.qty | number:2 }}
                        </td>
                        <td>
                          @{{ bom.uom_name }}
                        </td>
                        <td class="text-center" ng-if="response.action!='view'">
                          <a href="javascript:;" class="btn btn-icon-only tooltips" data-original-title="Modify" data-placement="left" ng-click="showPopupBom(bom)">
                            <i class="fa fa-edit"></i>
                          </a>
                          <a href="javascript:;" class="btn btn-icon-only tooltips" data-original-title="Delete" data-placement="left" delete-confirm on-delete="deleteBom(bom)">
                            <i class="fa fa-trash"></i>
                          </a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <!-- End BOM -->

          <!-- Replacement -->
          <div class="tab-pane fade" id="alternative">
            <div class="portlet">
              <div class="portlet-title">
                <div class="caption">Produk Alternatif/Pengganti</div>
                <div class="actions">
                  <a href="#" class="btn btn-sm btn-success" ng-click="showPopupAlternative()"  ng-if="response.action!='view'"> Tambah <i class="fa fa-plus"></i></a>
                </div>
              </div>
              <div class="portlet-body">
                <div class="table-responsive">
                  <table class="table table-hover table-light dataTable" border="0" cellspacing="0" cellpadding="0">
                    <thead>
                      <tr>
                        <th>Barang/Jasa</th>
                        <th style="width: 120px;" class="text-center" ng-if="response.action!='view'">Tindakan</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="alternative in dataAlternative" ng-dblclick="showPopupAlternative(alternative)">
                        <td>@{{ alternative.product_name }}</td>
                        <td class="text-center" ng-if="response.action!='view'">
                          <a href="javascript:;" class="btn btn-icon-only tooltips" data-original-title="Modify" data-placement="left" ng-click="showPopupAlternative(alternative)">
                            <i class="fa fa-edit"></i>
                          </a>
                          <a href="javascript:;" class="btn btn-icon-only tooltips" data-original-title="Delete" data-placement="left" delete-confirm on-delete="deleteAlternative(alternative)">
                            <i class="fa fa-trash"></i>
                          </a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <!-- End Replacement -->
        </div>
      </div>
    </form>
  </div>
</div>
<!-- END MAIN CONTENT -->


<!-- EDIT SUPPLIER POPUP TEMPLATE -->
<script type="text/ng-template" id="modalSupplier.html">
  <div class="modal-header">
      <h3 class="modal-title" id="modal-title">Pemasok</h3>
  </div>
  <form>
  <div class="modal-body" id="modal-body">

    <!-- <div ng-repeat="field in response.fields">
      <form-builder field="@{{ field.name }}"></form-builder>
    </div> -->
    <form-builder field="supplier"></form-builder>
    <form-builder field="price"></form-builder>
    <div class="row">
      <div class="col-md-6">
        <form-builder field="tax"></form-builder>
      </div>
      <div class="col-md-6">
        <div class="form-group form-md-line-input form-md-floating-label">
          <div class="input-group">
            <div class="input-group-control">
              <form-builder field="discount" form-group="false"></form-builder>
            </div>
            <span class="input-group-btn btn-right">
              <button class="btn btn-icon-only default btn-sm btn-search">%</button>
            </span>
          </div>
        </div>
      </div>
    </div>

  </div>
  <div class="modal-footer">
      <button class="btn btn-sm btn-default" type="button" ng-click="cancel()">Batal&nbsp;&nbsp;<i class="fa fa-times"></i></button>
      <button class="btn btn-sm green-haze" type="submit" ng-click="save(data, $event)">Simpan&nbsp;&nbsp;<i class="fa fa-floppy-o"></i></button>
  </div>
  </form>
</script>
<!-- END EDIT MODULE POPUP TEMPLATE -->

<!-- EDIT VARIANT POPUP TEMPLATE -->
<script type="text/ng-template" id="modalVariant.html">
  <div class="modal-header">
      <h3 class="modal-title" id="modal-title">Variant</h3>
  </div>
  <div class="modal-body" id="modal-body">
    <div ng-repeat="field in response.fields">
      <form-builder field="@{{ field.name }}"></form-builder>
    </div>
  </div>
  <div class="modal-footer">
      <button class="btn btn-sm btn-default" type="button" ng-click="cancel()">Batal&nbsp;&nbsp;<i class="fa fa-times"></i></button>
      <button class="btn btn-sm green-haze" type="submit" ng-click="save(data, $event)">Simpan&nbsp;&nbsp;<i class="fa fa-floppy-o"></i></button>
  </div>
</script>
<!-- END EDIT VARIANT POPUP TEMPLATE -->

<!-- EDIT BOM POPUP TEMPLATE -->
<script type="text/ng-template" id="modalBom.html">
  <div class="modal-header">
      <h3 class="modal-title" id="modal-title">Bahan Baku</h3>
  </div>
  <div class="modal-body" id="modal-body">
    <div class="row">
        <div class="col-md-6">
            <form-builder field="product"></form-builder>
        </div>
        <div class="col-md-6">
            <form-builder field="bom_type"></form-builder>
        </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <form-builder field="qty"></form-builder>
      </div>
      <div class="col-md-6">
        <form-builder field="uom"></form-builder>
      </div>
    </div>
  </div>
  <div class="modal-footer">
      <button class="btn btn-sm btn-default" type="button" ng-click="cancel()">Batal&nbsp;&nbsp;<i class="fa fa-times"></i></button>
      <button class="btn btn-sm green-haze" type="submit" ng-click="save(data, $event)">Simpan&nbsp;&nbsp;<i class="fa fa-floppy-o"></i></button>
  </div>
</script>
<!-- END EDIT VARIANT POPUP TEMPLATE -->

<!-- EDIT ALTERNATIVE POPUP TEMPLATE -->
<script type="text/ng-template" id="modalAlternative.html">
  <div class="modal-header">
      <h3 class="modal-title" id="modal-title">Produk Alternatif/Pengganti</h3>
  </div>
  <div class="modal-body" id="modal-body">
    <div ng-repeat="field in response.fields">
      <form-builder field="@{{ field.name }}"></form-builder>
    </div>
  </div>
  <div class="modal-footer">
      <button class="btn btn-sm btn-default" type="button" ng-click="cancel()">Batal&nbsp;&nbsp;<i class="fa fa-times"></i></button>
      <button class="btn btn-sm green-haze" type="submit" ng-click="save(data, $event)">Simpan&nbsp;&nbsp;<i class="fa fa-floppy-o"></i></button>
  </div>
</script>
<!-- END EDIT ALTERNATIVE POPUP TEMPLATE -->
