<!-- BEGIN MAIN CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN: ACCORDION DEMO -->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption font-green-sharp">
                    <!-- <i class="icon-settings font-green-sharp"></i> -->
                    <span class="caption-subject bold uppercase"> @{{ response.title }} </span>
                    <span class="caption-helper hide"></span>
                </div>

                <div class="actions">
                  <a href="/pajak/pajak/export" class="btn btn-sm btn-default"><i class="fa fa-download"></i> Download CSV</a>&nbsp;&nbsp;&nbsp;&nbsp;
                  <a href="#/@{{ response.create }}" class="btn btn-sm btn-success" ng-if="response.role.indexOf('create') != -1"> @lang('system.datagrid.header.create') <i class="fa fa-plus"></i></a>
                </div>
            </div>
            <div class="portlet-body">
              <div class="row margin-bottom-10">
                <div class="col-md-6">
                  @include('pages.partials.datagrid.datagrid-filter')
                </div>
                <div class="col-md-6 text-right">
                  @include('pages.partials.datagrid.datagrid-status')
                </div>
              </div>
                <table class="table table-hover table-light dataTable" border="0" cellspacing="0" cellpadding="0">
                  <thead>
                    <tr>
                      <th class="table-select" ng-if="response.massAction.length > 0">
                        <label class="mt-checkbox mt-checkbox-outline">
                            <input type="checkbox" ng-checked="allChecked()" ng-click="master = !master" />
                            <span></span>
                        </label>
                      </th>
                      <th ng-repeat="column in response.columns">
                        <div ng-click="setParams('sort', (column.sorting == 'asc' ? '-' : '')+column.source)" ng-class="getSortingStatus(column)" class="th-inner">
                        @{{ column.label }}
                        </div>
                      </th>
                      <th>Status</th>
                      <th style="width: 120px;" class="text-center">@lang('system.datagrid.table.action')</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr ng-repeat="dt in response.data">
                      <td class="table-select" ng-if="response.massAction.length > 0">
                        <label class="mt-checkbox mt-checkbox-outline" ng-init="initSelect(dt)">
                            <input type="checkbox" name="check" ng-model="selected[$index].isChecked"/>
                            <span></span>
                        </label>
                      </td>
                      <!-- <td ng-repeat="column in response.columns" compile-template ng-bind-html="dt[column.source]"></td> -->
                      <td compile-template ng-bind-html="dt['npwp']"></td>
                      <td compile-template ng-bind-html="dt['nama']"></td>
                      <td compile-template ng-bind-html="dt['period_month']"></td>
                      <td compile-template ng-bind-html="dt['period_year']"></td>
                      <td compile-template ng-bind-html="dt['objek_name']"></td>
                      <td compile-template ng-bind-html="dt['bruto']" align="right"></td>
                      <td compile-template ng-bind-html="dt['pph']" align="right"></td>
                      <td class="text-right">
                        <div class="dropdown">
                          <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:;" style="text-decoration: none; color: @{{ wk.color }};">@{{ getCurrentStatus(dt.status) }}
                              <i class="fa fa-angle-down" ng-hide="getNextWorkflow(dt.status).length == 0"></i>
                          </a>
                          <ul class="dropdown-menu pull-right">
                            <li ng-repeat="wk in getNextWorkflow(dt.status)">
                                <a ng-click="changeStatus(dt, wk.code)" style="color: @{{ wk.color }}">@{{ wk.description }}</a>
                            </li>
                          </ul>
                        </div>
                      </td>
                      <td class="text-center">
                        <a href="#/@{{ response.modify+'/'+dt['id'] }}" class="btn btn-icon-only tooltips" data-original-title="@lang('system.datagrid.table.modify')" data-placement="left" ng-if="response.role.indexOf('modify') != -1">
                          <i class="fa fa-edit"></i>
                        </a>
                        <a href="#/@{{ response.modify+'/'+dt['id'] }}" class="btn btn-icon-only tooltips" data-original-title="@lang('system.datagrid.table.modify')" data-placement="left" ng-if="response.role.indexOf('modify') == -1">
                          <i class="fa fa-eye"></i>
                        </a>
                        <a href="javascript:;" class="btn btn-icon-only tooltips" data-original-title="@lang('system.datagrid.table.delete')" data-placement="left" delete-confirm-ajax="@{{ dt['id'] }}" ng-if="response.role.indexOf('delete') != -1">
                          <i class="fa fa-trash"></i>
                        </a>
                      </td>
                    </tr>
                  </tbody>
                </table>
              <p class="text-center margin-top-10 margin-bottom-10 caption10" ng-if="response.data.length <= 0">@lang('system.datagrid.table.no_record')</p>

              <div class="row margin-top-10">
                <div class="col-md-6">
                  <div ng-if="response.massAction.length > 0">
                    @include('pages.partials.datagrid.datagrid-mass-button')
                  </div>
                </div>
                <div class="col-md-6">
                  @include('pages.partials.datagrid.datagrid-pagination')
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
<!-- END MAIN CONTENT -->
