<div class="modal-header">
  <button type="button" class="close" aria-hidden="true" ng-click="cancel()"></button>
    <h3 class="modal-title" id="modal-title">Barang Masuk &ldquo;@{{ po }}&rdquo;</h3>
</div>
<div class="modal-body" id="modal-body">
  <div class="portlet">
      <div class="portlet-body">
        <div class="row">
          <div class="col-md-6 col-md-offset-6 text-right">
            @include('pages.partials.datagrid.datagrid-status')
          </div>
        </div>
        <div class="row margin-bottom-10">
          <div class="col-md-12">
            <div class="table-filter row">
              <div class="col-md-3" style="padding-right: 0;">
                <div class="input-icon input-icon-sm right">
                  <i class="fa fa-search"></i>
                  <input type="text" ng-model="keyword" class="form-control input-sm" placeholder="@lang('system.datagrid.filter.search')" ng-change="keywordChanged()">
                </div>
              </div>
              <div class="col-md-2" style="padding-right: 0; padding-left: 2;">
                <div class="input-icon input-icon-sm left">
                  <i class="fa fa-calendar right"></i>
                  <input type="text" ng-model="date" class="form-control input-sm date-picker" placeholder="Tanggal" ng-change="dateChanged($event)">
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Start Table -->
        <div class="table-responsive">
          <table class="table table-hover table-light dataTable" border="0" cellspacing="0" cellpadding="0">
            <thead>
              <tr>
                <th class="table-select" ng-hide="isEditing">
                  <label class="mt-checkbox mt-checkbox-outline">
                      <input type="checkbox" ng-model="toggle" ng-change="allChecked()" />
                      <span></span>
                  </label>
                </th>
                <th ng-repeat="column in response.columns" ng-hide="column.source=='code' || column.source=='supplier' || column.source=='date' || column.source=='warehouse_name'">
                  <div ng-click="setParams('sort', (column.sorting == 'asc' ? '-' : '')+column.source)" ng-class="getSortingStatus(column)" class="th-inner">
                  @{{ column.label }}
                  </div>
                </th>
                <th style="width: 60px;" class="text-center"></th>
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat="dt in response.data | orderBy:'product_name'">
                <td class="table-select" ng-hide="isEditing">
                  <label class="mt-checkbox mt-checkbox-outline">
                      <input type="checkbox" name="check" ng-model="dt.selected" ng-checked="dt.selected" ng-change="addSelection(dt)"/>
                      <span></span>
                  </label>
                </td>
                <td ng-repeat="column in response.columns" compile-template ng-bind-html="dt[column.source]" ng-hide="column.source=='code' || column.source=='supplier' || column.source=='date' || column.source=='warehouse_name'"></td>
                <td class="text-right">
                  <button class="btn btn-xs btn-success tooltips" ng-click="addReceipt(dt)" data-original-title="Terima" data-placement="left"><i class="fa fa-check"></i></button>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <p class="text-center margin-top-10 margin-bottom-10 caption10" ng-if="response.data.length <= 0">@lang('system.datagrid.table.no_record')</p>
        <!-- End Table -->
        <div class="row margin-top-10">
          <div class="col-md-6">
            <div>

              <!-- @include('pages.partials.datagrid.datagrid-mass-button') -->
            </div>
          </div>
          <div class="col-md-6">
            <div ng-hide="selectionOnly == true">
              @include('pages.partials.datagrid.datagrid-pagination')
            </div>
          </div>
        </div>
        <div class="row" ng-hide="selected.length <= 0">
          <div class="col-md-12">
            <button class="btn btn-sm btn-success" ng-click="addSelectedReceipt()"><i class="fa fa-plus"></i> Terima</button>
            <span class="text-grey" style="color: #777;margin-left: 10px;">@{{ selected.length }} Produk dipilih</span>
            <a href="#" onclick="return false;" style="margin-left: 30px;" ng-click="onlySelected()" ng-hide="selectionOnly == true">Tampilkan produk yang dipilih</a>
            <a href="#" onclick="return false;" style="margin-left: 30px;" ng-click="allData()" ng-hide="selectionOnly == false">Tampilkan semua produk</a>
            <a href="#" onclick="return false;" style="margin-left: 30px;" ng-click="clearSelection()"><span class="text-success">Bersihkan Pilihan</span></a>
          </div>
        </div>
      </div>
  </div>
</div>
