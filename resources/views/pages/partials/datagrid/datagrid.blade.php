<!-- BEGIN MAIN CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN: ACCORDION DEMO -->
        <div class="portlet light bordered">
            @include('pages.partials.datagrid.datagrid-header')
            <div class="portlet-body">
              <div class="row margin-bottom-10">
                <div class="col-md-6">
                  @include('pages.partials.datagrid.datagrid-filter')
                </div>
                <div class="col-md-6 text-right">
                  @include('pages.partials.datagrid.datagrid-status')
                </div>
              </div>
              @include('pages.partials.datagrid.datagrid-table')
              <div class="row margin-top-10">
                <div class="col-md-6">
                  @include('pages.partials.datagrid.datagrid-mass-button')
                </div>
                <div class="col-md-6">
                  @include('pages.partials.datagrid.datagrid-pagination')
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
<!-- END MAIN CONTENT -->
