<link href="plugins/bootstrap-colorpicker/css/colorpicker.css" rel="stylesheet" type="text/css" />
<link href="plugins/jquery-minicolors/jquery.minicolors.css" rel="stylesheet" type="text/css" />
<!-- BEGIN MAIN CONTENT -->
<div class="row">
  <div class="col-md-12">
      <!-- BEGIN: ACCORDION DEMO -->
    <form role="form" id="@{{ response.id }}">
      <div class="portlet">
        <div class="portlet-title">
          @include('pages.partials.form.form-title')
          @include('pages.partials.form.form-action')
        </div>
        <div class="portlet-body">
          @include('pages.partials.form.form-content')
          <div class="portlet">
            <div class="portlet-title">
              <div class="caption">@lang('app.setting.workflow.title')</div>
              <div class="actions">
                <a href="#" class="btn btn-sm btn-success" ng-click="showPopupWorkflow()"  ng-if="response.action!='view'"> Tambah <i class="fa fa-plus"></i></a>
              </div>
            </div>
            <div class="portlet-body">
              <div class="table-responsive">
                <table class="table table-hover table-light dataTable" border="0" cellspacing="0" cellpadding="0">
                  <thead>
                    <tr>
                      <th>@lang('app.setting.workflow.workflow')</th>
                      <th>@lang('app.setting.workflow.description')</th>
                      <th>@lang('app.setting.workflow.next')</th>
                      <th>Status Label</th>
                      <th>@lang('app.setting.workflow.color')</th>
                      <th class="text-center">@lang('app.setting.workflow.default')</th>
                      <th ng-if="response.action!='view'">@lang('system.datagrid.table.action')</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr ng-repeat="workflow in dataWorkflow" ng-dblclick="showPopupWorkflow(workflow)">
                      <td>
                        @{{ workflow.code }}
                      </td>
                      <td>
                        @{{ workflow.description }}
                      </td>
                      <td>
                        @{{ getWorkflowNext(workflow) }}
                      </td>
                      <td>
                        @{{ workflow.label }}
                      </td>
                      <td>
                        <div style="background:@{{ workflow.color }};width: 40px;height: 18px;"></div>
                      </td>
                      <td class="text-center">
                        @{{ (workflow.default) ? 'Utama' : '' }}
                      </td>
                      <td class="text-center" ng-if="response.action!='view'">
                        <a href="javascript:;" class="btn btn-icon-only tooltips" data-original-title="Modify" data-placement="left" ng-click="showPopupWorkflow(workflow)">
                          <i class="fa fa-edit"></i>
                        </a>
                        <a href="javascript:;" class="btn btn-icon-only tooltips" data-original-title="Delete" data-placement="left" delete-confirm on-delete="deleteWorkflow(workflow)">
                          <i class="fa fa-trash"></i>
                        </a>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
<!-- END MAIN CONTENT -->

<!-- EDIT MODULE POPUP TEMPLATE -->
<script type="text/ng-template" id="modalWorkflow.html">
  <div class="modal-header">
      <h3 class="modal-title" id="modal-title">@lang('app.setting.workflow.workflow')</h3>
  </div>
  <div class="modal-body" id="modal-body">
    <div ng-repeat="field in response.fields">
      <form-builder field="@{{ field.name }}"></form-builder>
    </div>
  </div>
  <div class="modal-footer">
      <button class="btn btn-sm btn-default" type="button" ng-click="cancel()">Batal&nbsp;&nbsp;<i class="fa fa-times"></i></button>
      <button class="btn btn-sm green-haze" type="submit" ng-click="save(data, $event)">Simpan&nbsp;&nbsp;<i class="fa fa-floppy-o"></i></button>
  </div>
</script>
<!-- END EDIT MODULE POPUP TEMPLATE -->
