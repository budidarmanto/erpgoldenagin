<div class="modal-header">
  <button type="button" class="close" aria-hidden="true" ng-click="cancel()"></button>
  <h3 class="modal-title" id="modal-title">Stock Abnormal</h3>
</div>
<div class="modal-body" id="modal-body">
  <div class="table-responsive">
    <table class="table table-hover table-light" border="0" cellspacing="0" cellpadding="0">
      <thead>
        <tr>
          <th rowspan=2>No</th>
          <th rowspan=2>Date</th>
          <th colspan=4 class="text-center">Actual</th>
          <th colspan=2 class="text-center" style="color:red">Current</th>
        </tr>
        <tr>
          <th>Begin</th>
          <th style="padding-left:10px;padding-right:10px;">In</th>
          <th style="padding-left:10px;padding-right:10px;">Out</th>
          <th style="padding-left:10px;padding-right:10px;">End</th>
          <th style="padding-left:10px;padding-right:10px; color:red;">In</th>
          <th style="padding-left:10px;padding-right:10px; color:red;">Out</th>
        </tr>
      </thead>
      <tbody>
        <tr ng-repeat="dt in response.data">
          <td>@{{ $index + 1 }}</td>
          <td>@{{ dt['date'] }}</td>
          <td style="padding-left:10px;padding-right:10px;">@{{ dt['actual_begin_qty'] | number:2 }}</td>
          <td style="padding-left:10px;padding-right:10px;">@{{ dt['actual_plus_qty'] | number:2 }}</td>
          <td style="padding-left:10px;padding-right:10px;">@{{ dt['actual_min_qty'] | number:2 }}</td>
          <td style="padding-left:10px;padding-right:10px;">@{{ dt['actual_end_qty'] | number:2 }}</td>
          <td style="padding-left:10px;padding-right:10px;">@{{ dt['plus_qty'] | number:2 }}</td>
          <td style="padding-left:10px;padding-right:10px;">@{{ dt['min_qty'] | number:2 }}</td>
        </tr>
      </tbody>
    </table>
  </div>
</div>