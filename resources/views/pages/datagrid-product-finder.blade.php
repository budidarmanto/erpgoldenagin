<div class="modal-header">
  <button type="button" class="close" aria-hidden="true" ng-click="cancel()"></button>
    <h3 class="modal-title" id="modal-title">Barang/Jasa</h3>
</div>
<div class="modal-body" id="modal-body">
  <div class="portlet">
      <div class="portlet-body">
        <div class="row margin-bottom-10">
          <div class="col-md-7">
            Gudang : <select ng-model="warehouse" class="input-sm" ng-options="item.id as item.text for item in response.optionWarehouse" ng-change="warehouseChanged()" ng-init="warehouse = response.optionWarehouse[0].id" style="width: 200px;">
              <!-- <option value="">-- Semua Tipe --</option> -->
            </select>
            <select 
                ng-model="warehouse_location" 
                class="input-sm" 
                ng-options="item.id as item.text for item in optionWarehouseLocation" 
                style="width: 200px;" ng-change="warehouseLocationChanged()"
                options-class="{'font-red': text.indexOf('TPS') !== -1}">

            </select>
          </div>
          <div class="col-md-5 text-right">
            @include('pages.partials.datagrid.datagrid-status')
          </div>
        </div>
        <div class="row margin-bottom-10">
          <div class="col-md-12">
            <div class="table-filter row">
              <div class="col-md-3" style="padding-right: 0;">
                <div class="input-icon input-icon-sm right">
                  <i class="fa fa-search"></i>
                  <input type="text" ng-model="keyword" class="form-control input-sm" placeholder="@lang('system.datagrid.filter.search')" ng-change="keywordChanged()">
                </div>
              </div>
              <div class="col-md-3" style="padding-right: 0; padding-left: 2;">
                  <select ng-model="category" class="form-control input-sm" ng-options="item.id as item.text for item in response.optionCategory" ng-change="categoryChanged()">
                    <option value="">-- Semua Kategori --</option>
                  </select>
              </div>
              <div class="col-md-2" style="padding-left: 2;">
                  <select ng-model="type" class="form-control input-sm" ng-options="item.id as item.text for item in response.optionType" ng-change="typeChanged()">
                    <option value="">-- Semua Tipe --</option>
                  </select>
              </div>
            </div>
          </div>
        </div>

<!-- Begin Table -->
<div class="table-responsive">
  <table class="table table-hover table-light dataTable" border="0" cellspacing="0" cellpadding="0">
    <thead>
      <tr>
        <th class="table-select" ng-hide="isEditing">
          <label class="mt-checkbox mt-checkbox-outline">
              <input type="checkbox" ng-model="toggle" ng-change="allChecked()" />
              <span></span>
          </label>
        </th>
        <th></th>
        <th ng-repeat="column in response.columns">
          <div ng-click="setParams('sort', (column.sorting == 'asc' ? '-' : '')+column.source)" ng-class="getSortingStatus(column)" class="th-inner">
          @{{ column.label }}
          </div>
        </th>
        <th>Satuan Ukuran</th>
        <!-- <th class="text-right">Kedatangan</th> -->
        <th class="text-right">Stok</th>
        <!-- <th class="text-right">Pengiriman</th> -->
        <th style="width: 120px;" class="text-center">@lang('system.datagrid.table.action')</th>
      </tr>
    </thead>
    <tbody>
      <tr ng-repeat="dt in response.data">
        <td class="table-select" ng-hide="isEditing">
          <label class="mt-checkbox mt-checkbox-outline">
              <input type="checkbox" name="check" ng-model="dt.selected" ng-checked="dt.selected" ng-change="addSelection(dt)"/>
              <span></span>
          </label>
        </td>
        <td compile-template ng-bind-html="dt.picture"></td>
        <td ng-repeat="column in response.columns" compile-template ng-bind-html="dt[column.source]"></td>
        <td>@{{ dt.uom_name_base }}</td>
        <td class="text-right">@{{ dt.stock_akhir | number:2 }}</td>
        <td class="text-center">
          <button class="btn btn-xs btn-success tooltips" ng-click="addProduct(dt)" data-original-title="Tambah" data-placement="left"><i class="fa fa-plus"></i></button>
        </td>
      </tr>
    </tbody>
  </table>
</div>
<p class="text-center margin-top-10 margin-bottom-10 caption10" ng-if="response.data.length <= 0">@lang('system.datagrid.table.no_record')</p>

<!-- End Table -->
        <div class="row margin-top-10">
          <div class="col-md-6">
            <div>

              <!-- @include('pages.partials.datagrid.datagrid-mass-button') -->
            </div>
          </div>
          <div class="col-md-6">
            <div ng-hide="selectionOnly == true">
              @include('pages.partials.datagrid.datagrid-pagination')
            </div>
          </div>
        </div>
        <div class="row" ng-hide="selected.length <= 0">
          <div class="col-md-12">
            <button class="btn btn-sm btn-success" ng-click="addSelectedProduct()"><i class="fa fa-plus"></i> Tambah</button>
            <span class="text-grey" style="color: #777;margin-left: 10px;">@{{ selected.length }} Produk dipilih</span>
            <a href="#" onclick="return false;" style="margin-left: 30px;" ng-click="onlySelected()" ng-hide="selectionOnly == true">Tampilkan produk yang dipilih</a>
            <a href="#" onclick="return false;" style="margin-left: 30px;" ng-click="allData()" ng-hide="selectionOnly == false">Tampilkan semua produk</a>
            <a href="#" onclick="return false;" style="margin-left: 30px;" ng-click="clearSelection()"><span class="text-success">Bersihkan Pilihan</span></a>
          </div>
        </div>
      </div>
  </div>
</div>
<!-- <div class="modal-footer">
    <button class="btn btn-sm btn-default" type="button" ng-click="cancel()">Batal&nbsp;&nbsp;<i class="fa fa-times"></i></button>
    <button class="btn btn-sm green-haze" type="submit" ng-click="save(data, $event)">Simpan&nbsp;&nbsp;<i class="fa fa-floppy-o"></i></button>
</div> -->
