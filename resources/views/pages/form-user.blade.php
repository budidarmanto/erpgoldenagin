<!-- BEGIN MAIN CONTENT -->
<div class="row">
  <div class="col-md-12">
      <!-- BEGIN: ACCORDION DEMO -->
    <form role="form">
      <div class="portlet">
        <div class="portlet-title">
          @include('pages.partials.form.form-title')
          @include('pages.partials.form.form-action')
        </div>
        <div class="portlet-body">
          <div class="row margin-bottom-5 margin-top-20">
            <div class="col-md-4 text-center">
              <form-builder field="picture"></form-builder>
            </div>
            <div class="col-md-8">
              <form-builder field="fullname"></form-builder>
              <form-builder field="nickname"></form-builder>
              <form-builder field="username"></form-builder>
              <form-builder field="email"></form-builder>
              <form-builder field="password"></form-builder>
              <form-builder field="retypePassword"></form-builder>
              <form-builder field="group"></form-builder>
              <form-builder field="company"></form-builder>
              <form-builder field="warehouse_location"></form-builder>
              <form-builder field="workcenter"></form-builder>
              <form-builder field="active"></form-builder>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
<!-- END MAIN CONTENT -->
