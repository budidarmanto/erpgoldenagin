<!-- BEGIN MAIN CONTENT -->
<div class="row">
  <div class="col-md-12">
      <!-- BEGIN: ACCORDION DEMO -->
    <form role="form" id="@{{ response.id }}">
      <div class="portlet">
        <div class="portlet-title">
          @include('pages.partials.form.form-title')
          @include('pages.partials.form.form-action')
        </div>
        <div class="portlet-body">
          <div class="row margin-bottom-5 margin-top-20">
            <div class="col-md-6">
              <form-builder field="npwp"></form-builder>
              <form-builder field="nip"></form-builder>
              <form-builder field="nama"></form-builder>
              <div class="form-group form-md-radios">
                <label>@{{ response.fields[4].label }}</label>
                <div class="md-radio-inline">
                    <div class="md-radio" ng-repeat="gender in response.optionGender">
                        <input type="radio" id="@{{ gender.id }}" name="gender" class="md-radiobtn" ng-model="data.jenis_kelamin" ng-value="gender.id">
                        <label for="@{{ gender.id }}">
                            <span></span>
                            <span class="check"></span>
                            <span class="box"></span>
                        @{{ gender.text }}
                        </label>
                    </div>
                </div>
              </div>
              <form-builder field="alamat"></form-builder>
              <form-builder field="status_ptkp"></form-builder>
            </div>
            <div class="col-md-6">

              <form-builder field="tanggungan"></form-builder>
              <form-builder field="pangkat"></form-builder>
              <form-builder field="golongan"></form-builder>
              <form-builder field="jabatan"></form-builder>
              <div style="margin-top: 20px;">
                <form-builder field="luar_negeri"></form-builder>
                <div ng-hide="selectCountry == false">
                  <form-builder field="negara"></form-builder>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
<!-- END MAIN CONTENT -->
