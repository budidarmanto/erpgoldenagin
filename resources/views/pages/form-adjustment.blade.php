<!-- BEGIN MAIN CONTENT -->
<div class="row">
  <div class="col-md-12">
      <!-- BEGIN: ACCORDION DEMO -->
      <form role="form" id="@{{ response.id }}" id="#formAdjustment">
        <div class="portlet">
          <div class="portlet-title">
            <div class="caption font-green-sharp">
                <!-- <i class="icon-settings font-green-sharp"></i> -->
                <span class="caption-subject bold uppercase">@{{ response.title }}</span>
                &nbsp;&nbsp;
                <span class="btn-status" style="background: @{{ getStatus(data.status).color }}; color: #fff; padding: 3px 6px;" ng-hide="response.action == 'create'">@{{ getStatus(data.status).label }}</span>
            </div>

            @include('pages.partials.form.form-action')
            <div class="actions" ng-hide="getStatus(data.status).text.length <= 0 || response.action == 'create' || response.role.indexOf('change') == -1" style="margin-left: 40px;float: left;">
              <a href="#" onclick="return false;" class="btn blue btn-outline mt-ladda-btn ladda-button" ng-repeat="next in getStatus(data.status).next" data-style="zoom-out"  ng-click="changeStatus(next.id, $event)" style="margin-right: 5px;" >@{{ next.text }}</a>
            </div>
          </div>
          <div class="portlet-body">
            <div class="row margin-bottom-5 margin-top-20">
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-6">
                    <form-builder field="code"></form-builder>
                    <form-builder field="date"></form-builder>
                    <!-- <form-builder field="diajukan"></form-builder> -->
                  </div>
                  <div class="col-md-6">
                    <form-builder field="source"></form-builder>
                    <form-builder field="description"></form-builder>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
      <div class="portlet">
        <div class="portlet-title">
          <div class="caption">Barang/Jasa</div>
          <div class="actions">
            <a href="#" class="btn btn-sm btn-success" ng-click="showPopupProduct()"  ng-if="response.action!='view'"> Tambah <i class="fa fa-plus"></i></a>
          </div>
        </div>
        <div class="portlet-body">
            <table class="table table-hover table-light dataTable margin-bottom-20" border="0" cellspacing="0" cellpadding="0">
              <thead>
                <tr>
                  <th style="width: 40px;">No.</th>
                  <th style="width: 25%;">Barang/Jasa</th>
                  <th class="text-right" style="width: 100px;">Jumlah</th>
                  <th class="text-center">Satuan Ukuran</th>
                  <th>Deskripsi</th>
                  <th class="text-center" ng-if="response.action!='view'" ng-hide="response.action == 'view'"></th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="product in dataProduct | orderBy:'name'">
                  <td>@{{ $index+1 }}</td>
                  <td>@{{ product.code+' - '+product.name }}</td>
                  <td class="text-right">
                    <a href="#" editable-text="product.qty" buttons="no" onbeforesave="validateNumber($data)" blur="submit" ng-hide="response.action == 'view'" e-class="number" e-style="width: 100px;">
                      @{{ product.qty | number:2 || 0 }}
                    </a>
                    <span ng-hide="response.action != 'view'">
                      @{{ product.qty | number:2 || 0 }}
                    </span>
                  </td>
                  <td class="text-center">
                    <a href="#" editable-select="product.uom" buttons="no" e-ng-options="item.id as item.text for item in product.optionUom" ng-hide="response.action == 'view'">
                      @{{ (product.optionUom | filter:{id:product.uom})[0].text || 'Pilih Satuan' }}
                    </a>
                    <span ng-hide="response.action != 'view'">
                      @{{ (product.optionUom | filter:{id:product.uom})[0].text || '-' }}
                    </span>
                  </td>
                  <td>
                    <a href="#" editable-textarea="product.description" buttons="no" blur="submit" ng-hide="response.action == 'view'">
                      @{{ product.description || 'Deskripsi' }}
                    </a>
                    <span ng-hide="response.action != 'view'">
                      @{{ product.description || 'Deskripsi' }}
                    </span>
                  </td>
                  <td ng-hide="response.action == 'view'" class="text-right">
                    <a href="javascript:;" class="btn btn-icon-only tooltips" data-original-title="Delete" data-placement="left" delete-confirm on-delete="deleteProduct(product)">
                      <i class="fa fa-trash"></i>
                    </a>
                  </td>
                </tr>
              </tbody>
            </table>
        </div>
      </div>
      <!-- Timeline Log & Comments -->
      <div class="row" ng-hide="response.action == 'create'">
        <div class="col-md-12">
          <div style="border-top: 1px solid #e3e3e3; margin-top: 20px; margin-bottom: 20px;"></div>
        </div>
        <div class="col-md-12">
          @include('pages.partials.inventory-stockmovement-comment')
        </div>
      </div>
      <!-- End Timeline Log & Comments -->
  </div>
</div>
<!-- END MAIN CONTENT -->
