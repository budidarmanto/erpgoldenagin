<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style>
    .page-break {
        page-break-after: always;
    }
    body, table {
      font-size: 12px;
    }
    table h1 {
      font-size: 14px;
    }
    table th {
      font-weight: normal;
    }
    @media print {
      table {page-break-inside: avoid;}
    }
    @page {
      margin: 1.5cm;    
    }
    </style>
  </head>
  <body>
    <table style="width:100%">
      <tr>
        <td style="text-align:center">
          <h1 style="margin: 0; padding: 0;">Laporan Persediaan</h1>
        </td>
      </tr>
      <tr>
        <td><span>Gudang: {{ $warehouseName }} / {{ $warehouseLocationName }}</span></td>
      </tr>
      <tr>
        <td><span>Tanggal: {{ date('d/m/Y', strtotime($today)) }}</span></td>
      </tr>
      @if($category)
        <tr>
            <td><span>Kategori: {{ $category->name }}</span></td>
        </tr>
      @endif
      @if($type)
        <tr>
            <td><span>Tipe: {{ $type }}</span></td>
        </tr>
      @endif
    </table>
    <br>
    <table class="table-detail" style="width:100%">
      <tbody>
        <tr>
            <th>No</th>
            <th>Kode</th>
            <th>Nama</th>
            <th>Kategori</th>
            <th>Satuan Ukuran</th>
            <th>Stok</th>
        </tr>
        <?php 
        $no = 1;
        ?>
        @foreach($data as $dt)
        <tr>
            <td>{{ $no++ }}</td>
            <td>{{ $dt['code'] }}</td>
            <td>{{ $dt['name'] }}</td>
            <td>{{ $dt['category_name'] }}</td>
            <td>{{ $dt['uom_name'] }}</td>
            <td>{{ number_format($dt['stock_akhir'],2) }}</td>
        </tr>
        @endforeach
        
      </tbody>
    </table>
  </body>
</html>