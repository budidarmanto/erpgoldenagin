<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  </head>
  <body>
    <table>
      <tr>
        <td colspan="4">
          <h3 style="margin: 0; padding: 0;">Stock Card</h3>
        </td>
        <td colspan="3"><span>Barang: {{ $productName }}</span></td>
      </tr>
      <tr>
        <td colspan="4"></td>
        <td colspan="3"><span>Tanggal: {{ date('d/m/Y', strtotime($fromDate)) }} - {{ date('d/m/Y', strtotime($excelToDate)) }}</span></td>
      </tr>
    </table>
    <table border="1">
      <thead>
        <tr>
          <th>Tanggal</th>
          <th>Kode</th>
          <th>Tipe</th>
          <th>Masuk</th>
          <th>Keluar</th>
          <th>Stok</th>
          <th>Satuan Ukuran</th>
        </tr>
      </thead>
      <tbody>
        @foreach($data as $val)
        <tr>
          <td>{{ $val['date'] }}</td>
          <td>{{ $val['code'] }}</td>
          <td>{{ $val['type_name'] }}</td>
          <td align="center">{{ $val['qty_in'] }}</td>
          <td align="center">{{ $val['qty_out'] }}</td>
          <td align="center">{{ $val['count_stock'] }}</td>
          <td align="center">{{ $val['uom_name'] }}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </body>
</html>
