<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  </head>
  <body>
    <table>
      <tr>
        <td colspan="3">
          <h3 style="margin: 0; padding: 0;">Barang/Jasa</h3>
        </td>
      </tr>
      <tr>
        <td colspan="3"></td>
        <td><span>Tanggal: {{ date('d/m/Y') }}</span></td>
      </tr>
    </table>
    <table border="1">
      <thead>
        <tr>
          <th>Kode Barang/Jasa</th>
          <th>Nama</th>
          <th>Satuan Ukuran</th>
          <th>Kategori</th>
          <th>Status</th>
          <th>Barcode</th>
          <th>Harga Jual</th>
        </tr>
      </thead>
      <tbody>
        @foreach($data as $val)
        <tr>
          <td>{{ $val['code'] }}</td>
          <td>{{ $val['name'] }}</td>
          <td>{{ $val['uom_name'] }}</td>
          <td>{{ $val['category_name'] }}</td>
          <td>{{ $val['active'] }}</td>
          <td>{{ $val['barcode'] }}</td>
          <td>{{ $val['sale_price'] }}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </body>
</html>
