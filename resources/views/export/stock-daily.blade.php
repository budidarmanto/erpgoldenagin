<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  </head>
  <body>
    <table>
      <tr>
        <td colspan="5">
          <h3 style="margin: 0; padding: 0;">Laporan Perubahan Stok</h3>
        </td>
        <td><span>Gudang: {{ $warehouse->name }}</span></td>
      </tr>
      <tr>
        <td colspan="5"></td>
        <td><span>Tanggal: {{ $date }}</span></td>
      </tr>
    </table>
    <table border="1">
      <thead>
        <tr>
          <th style="vertical-align: middle;">Kode Produk</th>
          <th style="vertical-align: middle;">Nama Produk</th>
          <th style="vertical-align: middle;">Stok Awal</th>
          <th style="">Penerimaan</th>
          <th style="">Penyesuaian</th>
          <th style="">Pengiriman</th>
          <th style="">Penjualan Kasir</th>
          <th style="vertical-align: middle;">Stok Akhir</th>
          <th style="vertical-align: middle;">Satuan Ukuran</th>
        </tr>
      </thead>
      <tbody>
        @foreach($data as $val)
        <tr>
          <td>{{ $val['code'] }}</td>
          <td>{{ $val['name'] }}</td>
          <td style="text-align: right;">{{ $val['begin_qty'] }}</td>
          <td style="text-align: right;">{{ $val['total_receipt'] }}</td>
          <td style="text-align: right;">{{ $val['total_adjustment'] }}</td>
          <td style="text-align: right;">{{ $val['total_delivery'] }}</td>
          <td style="text-align: right;">{{ $val['total_chasier'] }}</td>
          <td style="text-align: right;">{{ $val['end_qty'] }}</td>
          <td>{{ $val['uom_name'] }}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </body>
</html>
