<html><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style>
    .page-break {
        page-break-after: always;
    }
    body, table {
      font-size: 12px;
    }
    table h1 {
      font-size: 14px;
    }
    table th {
      font-weight: normal;
    }
    @media print {
      table {page-break-inside: avoid;}
    }
    @page {
      margin: 1.5cm;    
    }
    </style>
</head><body>
    <table style="width:100%">
      <tr>
        <td style="text-align:center">
          <span style="float:right">F-0308010017</span>
          <h1 style="margin: 0; padding: 0;">Laporan Stock Card</h1>
        </td>
      </tr>
      <tr>
        <td><span>Gudang: {{ $warehouseName }} / {{ $warehouseLocationName }}</span></td>
      </tr>
      <tr>
        <td><span>Tanggal: {{ date('d/m/Y', strtotime($fromDate)) }} - {{ date('d/m/Y', strtotime($toDate)) }}</span></td>
      </tr>
      @if($category)
        <tr>
            <td><span>Kategori: {{ $category->name }}</span></td>
        </tr>
      @endif
    </table>
    <br>
    @foreach($data as $val)
    <table class="table-detail" style="width:100%">
      <tbody>
        <tr>
            <td colspan="9">Kode: {{ $val['code'] }}</td>
        </tr>
        <tr>
            <td colspan="9">Nama Barang: {{ $val['name'] }}</td>
        </tr>
        <tr>
            <th>Tanggal Transaksi</th>
            <th>Nomor Penyesuaian</th>
            <th>Keterangan</th>
            <th>Gudang Asal</th>
            <th>Gudang Tujuan</th>
            <th>Masuk</th>
            <th>Keluar</th>
            <th>Saldo Akhir</th>
        </tr>
        @foreach($val['stock_card'] as $dt)
        <tr>
            <td>{{ $dt['date'] }}</td>
            <td>{{ $dt['code'] }}</td>
            <td>{{ $dt['description'] }}</td>
            <td>{{ $dt['source_name'] }}</td>
            <td>{{ $dt['destination_name'] }}</td>
            <td>{{ $dt['qty_in'] }}</td>
            <td>{{ $dt['qty_out'] }}</td>
            <td>{{ $dt['stock_akhir'] }}</td>
        </tr>
        @endforeach
        <tr>
            <td colspan="9"><br></td>
        </tr>
      </tbody>
    </table>
    @endforeach
</body></html>