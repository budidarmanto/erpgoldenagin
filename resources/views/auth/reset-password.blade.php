@include('auth.login-header')
<!-- BEGIN FORGOT PASSWORD FORM -->
<h1>Ubah Password</h1>
<p> Masukan password baru!, password minimal 6 karakter. </p>
<form class="reset-form" action="" method="post">
    {{ csrf_field() }}
    @include('alerts')
    <div class="form-group">
        <input class="form-control form-control-solid placeholder-no-fix form-group" type="password" autocomplete="off" placeholder="Password baru" name="new_password" /> </div>
    <div class="form-group">
        <input class="form-control form-control-solid placeholder-no-fix form-group" type="password" autocomplete="off" placeholder="Ketik ulang password" name="new_password_confirm" /> </div>
    <div class="form-actions forgot">
        <button type="submit" class="btn green-haze uppercase pull-right">Reset</button>
    </div>
</form>
<!-- END FORGOT PASSWORD FORM -->
@include('auth.login-footer')
