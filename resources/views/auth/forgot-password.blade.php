@include('auth.login-header')
<!-- BEGIN FORGOT PASSWORD FORM -->
<h1>Lupa Password?</h1>
<p> Silahkan masukan alamat email Anda. Kami akan mengirimkan tautan untuk memperbaharui kata sandi Anda. </p>
<form action="forgot" method="post">
    {{ csrf_field() }}
    @include('alerts')
    <div class="form-group">
        <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Email" name="email" /> </div>
    <div class="form-actions forgot">
        <button type="submit" class="btn green-haze btn-block uppercase" >Submit</button>
    </div>
    <div class="form-actions">
      <div class="pull-right forget-password-block">
          <a href="login" id="forget-password" class="forget-password"><i class="fa fa-angle-left"></i> Kembali</a>
      </div>
    </div>
</form>
<!-- END FORGOT PASSWORD FORM -->
@include('auth.login-footer')
