@include('print.partials.header')


<div class="content" style="padding: 10px;">

  <div class="row" style="text-align: right;">
    F-0908010017
  </div>
  <div class="row" style="border-bottom: 2px solid #333;"></div>
  <div class="row">
    <h1 style="font-weight: 300;margin: 2px 0; display: inline;">SURAT JALAN</h1>
    <h1 style="font-weight: 300;margin: 2px 0; display: inline; float: right;">&nbsp;&nbsp;PT. GOLDEN AGIN NUSA</h1>
    <img src="../../../img/golden-agin-bw.png" style="margin: 4px 0 0 0; width: 50px; float: right;"/>
  </div>
  <div class="row" style="border-bottom: 2px solid #333;"></div>


    <table style="width: 100%;" style="">
        <tr>
            <td valign="top">
                <table style="width: 100%;">
                    <tr>
                        <td valign="top" style="width: 100px;">
                            Nomor
                        </td>
                        <td style="width: 1px;" valign="top">:</td>
                        <td valign="top">
                          {!! $stockMovement->code; !!}
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="width: 100px;">
                            Tanggal
                        </td>
                        <td style="width: 1px;" valign="top">:</td>
                        <td valign="top">
                          <?php
                          $date = $stockMovement->date;
                          echo date('d F Y', strtotime($date))
                          ?>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="width: 100px;">
                            Pengiriman Barang
                        </td>
                        <td style="width: 1px;" valign="top">:</td>
                        <td valign="top">
                        <?php
                          $date = $stockMovement->expected_date;
                          echo date('d F Y', strtotime($date))
                        ?>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 200px;" valign="top">
                <table style="width: 100%;">
                    <tr>
                        <td valign="top">Gudang Asal</td>
                        <td valign="top">:</td>
                        <td align="right">{!! $warehouseSource->name.'/'.$warehouseLocationSource->name; !!}</td>
                    </tr>
                    <tr>
                        <td valign="top">Gudang Tujuan</td>
                        <td valign="top">:</td>
                        <td align="right">{!! $warehouseDestination->name.'/'.$warehouseLocationDestination->name; !!}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br/>
                <table class="table table-bordered">
                    <thead>
                        <tr class="heading">
                            <th align="center" style="width: 25px;">Kode Barang</th>
                            <th align="center">Nama Barang</th>
                            <th align="center">Jumlah</th>
                            <th align="center">Satuan</th>
                            <th align="center" style="width: 200px;">Catatan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        if (!empty($dataProduct)) {
                            foreach ($dataProduct as $val) {

                                ?>
                                <tr>
                                    <td align="center"><?php echo $val['code']; ?></td>
                                    <td><?php echo $val['name']; ?></td>
                                    <td align="center"><?php echo number_format((($val['qty']) * 1), 2); ?></td>
                                    <td align="center"><?php echo $val['uom_name']; ?></td>
                                    <td><?php echo $val['description']; ?></td>
                                </tr>
                        <?php
                            }
                          }
                        ?>
                    </tbody>
                </table>
<br/>

    <div class="row">
      <table class="table">
          <tr>
              <td style="width: 300px;" valign="top">Catatan:<br/>
                <?php
                echo $stockMovement->description; ?>
              </td>
              <td style="text-align: center;">
                Tanda Terima<br/><br/><br/><br/><br/><br/>
                <div style="border-bottom: 1px solid #333;"></div>
                Dept. antar Bag.
              </td>
              <td style="text-align: center;">
                <br/><br/><br/><br/><br/><br/>
                <div style="border-bottom: 1px solid #333;"></div>
                PPIC / QC
              </td>
              <td style="text-align: center;">
                Hormat Kami
                <br/><br/><br/><br/><br/><br/>
                <div style="border-bottom: 1px solid #333;"></div>
                (Admin)
              </td>
          </tr>
      </table>
    </div>

    <br/><br/><br/><br/>

</div>


@include('print.partials.footer')

<script type="text/javascript">
window.print();
</script>
