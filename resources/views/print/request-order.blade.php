@include('print.partials.header')

<div class="content">
    @if($stockMovement->printed > 1)
    <h1 class="title" style="text-align: right; margin-bottom: 0px;">PURCHASE REQUEST</h1>
    <h4 style="text-align: right; margin-bottom: 40px; margin-top: 0;">{!! 'Copy #'.$stockMovement->printed; !!}</h4>
    @else
    <h1 class="title" style="text-align: right; margin-bottom: 40px;">REQUEST ORDER</h1>
    @endif
    <table style="width: 100%;" style="">
        <tr>
            <td valign="top">
                <table style="width: 100%;">
                    <tr>
                        <td valign="top" style="width: 100px;">
                            Kantor/Cabang :
                        </td>
                        <td style="width: 1px;" valign="top">:</td>
                        <td valign="top">
                          {!! $company->name !!}<br/>
                          {!! $company->address !!}<br/>
                          {!! $company->city.' '.$company->postal_code !!}<br/>
                          {!! $company->phone !!}<br/>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 200px;" valign="top">
                <table style="width: 100%;">
                    <tr>
                        <td valign="top">Tanggal</td>
                        <td>:</td>
                        <td align="right" valign="top"><?php
                            $date = $stockMovement->date;
                            echo date('d F Y', strtotime($date))
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>No. Pengajuan</td>
                        <td>:</td>
                        <td align="right">{!! $stockMovement->code; !!}</td>
                    </tr>
                    <tr>
                        <td valign="top">Tujuan</td>
                        <td valign="top">:</td>
                        <td align="right">{!! $warehouse->name; !!}<br/>
                        {!! ($warehouse->address != '') ? $warehouse->address.'<br/>' : ''; !!}
                        {!! ($warehouse->city != '') ? $warehouse->city . ' ' . $warehouse->postal_code.'<br/>' : ''; !!}
                        {!! $warehouse->phone; !!}<br/></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br/>
    <table class="table" style="width: 100%;">

        <tr>
            <td colspan="2">
                <table class="table table-bordered">
                    <thead>
                        <tr class="heading">
                            <th align="center" style="width: 25px;">No.</th>
                            <th align="center">Nama Barang</th>
                            <th align="center">Qty</th>
                            <th align="center">UOM</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        if (!empty($dataProduct)) {
                            foreach ($dataProduct as $val) {
                              if($val['status'] != 2){
                                ?>
                                <tr>
                                    <td align="center"><?php echo $no++; ?></td>
                                    <td><?php echo $val['name']; ?></td>
                                    <td align="center"><?php echo number_format((($val['qty']) * 1), 2); ?></td>
                                    <td align="center"><?php echo $val['uom_name']; ?></td>
                                </tr>
                        <?php
                              }
                            }
                          }
                        ?>
                    </tbody>
                </table>
                <table class="table">
                    <tr>
                        <td style="width: 110px;" valign="top">Keterangan</td>
                        <td style="width: 1px;" valign="top">:</td>
                        <td style="white-space: pre-wrap;" valign="top"><?php
                        echo $stockMovement->description; ?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br/><br/><br/><br/>
    <table class="table table-bordered">
        <thead>
            <tr class="heading">
                <th align="center" style="width: 25%">Dibuat Oleh</th>
                <th align="center" colspan="2" style="width: 50%">Mengetahui</th>
                <th align="center" style="width: 25%">Disetujui Oleh</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="width: 25%"><br/><br/><br/><br/><br/></td>
                <td style="width: 25%"></td>
                <td style="width: 25%"></td>
                <td style="width: 25%"></td>
            </tr>
            <tr>
                <td align="center">Buyer</td>
                <td align="center">Finance</td>
                <td align="center">Operation Manager</td>
                <td align="center">General Manager</td>
            </tr>
        </tbody>
    </table>
</div>


@include('print.partials.footer')

<script type="text/javascript">
window.print();
</script>
