@include('print.partials.header')
<div class="content">
    @if($stockMovement->printed > 1)
    <h1 class="title" style="text-align: right; margin-bottom: 0px;">PURCHASE ORDER</h1>
    <h4 style="text-align: right; margin-bottom: 40px; margin-top: 0;">{!! 'Copy #'.$stockMovement->printed; !!}</h4>
    @else
    <h1 class="title" style="text-align: right; margin-bottom: 40px;">PURCHASE ORDER</h1>
    @endif
    <table style="width: 100%;" style="">
        <tr>
            <td valign="top">
                <table style="width: 100%;">
                  <tr>
                      <td valign="top">Pemasok</td>
                      <td>:</td>
                      <td valign="top">
                        {!! $supplier->name !!}
                      </td>
                  </tr>
                  <tr>
                      <td valign="top" style="width: 100px;">
                          Kantor/Cabang :
                      </td>
                      <td style="width: 1px;" valign="top">:</td>
                      <td valign="top">
                        {!! $company->name !!}<br/>
                        {!! $company->address !!}<br/>
                        {!! $company->city.' '.$company->postal_code !!}<br/>
                        {!! $company->phone !!}<br/>
                      </td>
                  </tr>
                </table>
            </td>
            <td style="width: 200px;" valign="top">
                <table style="width: 100%;">
                    <tr>
                        <td valign="top">Tanggal</td>
                        <td>:</td>
                        <td align="right" valign="top"><?php
                            $date = $stockMovement->date;
                            echo date('d F Y', strtotime($date))
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>No. Pembelian</td>
                        <td>:</td>
                        <td align="right">{!! $stockMovement->code; !!}</td>
                    </tr>
                    <tr>
                        <td valign="top">Tujuan</td>
                        <td valign="top">:</td>
                        <td align="right">{!! $warehouse->name; !!}<br/>
                        {!! ($warehouse->address != '') ? $warehouse->address.'<br/>' : ''; !!}
                        {!! ($warehouse->city != '') ? $warehouse->city . ' ' . $warehouse->postal_code.'<br/>' : ''; !!}
                        {!! $warehouse->phone; !!}<br/></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br/>
    <table class="table-detail" style="border: 1px solid #eee; width: 100%;">

        <tr>
            <td colspan="2">
                <table class="table table-bordered">
                    <thead>
                        <tr class="heading">
                            <th align="center" style="width: 25px;">No.</th>
                            <th align="center">Nama Barang</th>
                            <th align="center">Qty</th>
                            <th align="center">UOM</th>
                            <th align="center">Harga</th>
                            <th align="center" style="width: 100px;">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $totalPrice = 0;
                        $totalDiscount = 0;
                        $totalTax = 0;
                        $totalAll = 0;

                        $no = 1;
                        if (!empty($dataProduct)) {
                            foreach ($dataProduct as $val) {
                              if($val['status'] != 2){
                                $price = $val['price'] * $val['qty'];

                                $lastDisc = $price;
                                $discount = 0;
                                $dataDiscount = $val['discount'];
                                if(count($dataDiscount) > 0){
                                  foreach($dataDiscount as $vdisc){
                                    $currDisc = $lastDisc * (floatval($vdisc) / 100);
                                    $discount += $currDisc;
                                    $lastDisc -= $currDisc;
                                  }
                                }

                                $priceAfterDiscount = $price - $discount;
                                $tax = $priceAfterDiscount * ($val['tax_amount'] / 100);
                                $subtotal = $priceAfterDiscount + $tax;

                                $totalPrice += $price;
                                $totalDiscount += $discount;
                                $totalTax += $tax;
                                $totalAll += $subtotal;

//
                                ?>
                                <tr>
                                    <td align="center"><?php echo $no++; ?></td>
                                    <td><?php echo $val['name']; ?></td>
                                    <td align="center"><?php echo number_format((($val['qty']) * 1), 2); ?></td>
                                    <td align="center"><?php echo $val['uom_name']; ?></td>
                                    <td>
                                        <table class="table-row" style="width: 100%;">
                                            <tr>
                                                <td>Rp.</td>
                                                <td align="right"><?php echo number_format($val['price'], 2); ?></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <table class="table-row" style="width: 100%;">
                                            <tr>
                                                <td>Rp.</td>
                                                <td align="right"><?php echo number_format($val['qty'] * $val['price'], 2); ?></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                        <?php
                              }
                            }
                          }
                        ?>
                    </tbody>
                </table>
                <table class="table">
                    <tr>
                        <td style="width: 110px;">Tanggal Pengiriman</td>
                        <td style="width: 1px;">:</td>
                        <td><?php echo date('d F Y', strtotime($stockMovement->expected_date)); ?></td>
                        <td colspan="3"></td>
                        <td style="width: 100px;"><strong>Subtotal</strong></td>
                        <td style="width: 100px;" align="right">
                            <table class="table-row">
                                <tr>
                                    <td>Rp.</td>
                                    <td align="right"><?php echo number_format($totalPrice, 2); ?></td>
                                </tr>
                            </table>
                    </tr>
                    <tr>
                        <td style="width: 110px;" valign="top">Keterangan</td>
                        <td style="width: 1px;" valign="top">:</td>
                        <td style="white-space: pre-wrap;" valign="top"><?php
                        echo $stockMovement->description; ?></td>
                        <td colspan="3"></td>
                        <td style="width: 100px;"><strong>Discount</strong></td>
                        <td style="width: 100px;" align="right">
                            <table class="table-row">
                                <tr>
                                    <td>Rp.</td>
                                    <td align="right"><?php echo number_format($totalDiscount, 2); ?></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6"></td>
                        <td style="width: 100px;"><strong>PPN</strong></td>
                        <td style="width: 100px;">
                            <table class="table-row">
                                <tr>
                                    <td>Rp.</td>
                                    <td align="right"><?php echo number_format($totalTax, 2); ?></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6"></td>
                        <td style="width: 100px;"><strong>Total</strong></td>
                        <td style="width: 100px;" align="right">
                            <table class="table-row">
                                <tr>
                                    <td>Rp.</td>
                                    <td align="right"><?php echo number_format($totalAll, 2); ?></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br/><br/><br/><br/>
    <table class="table table-bordered">
        <thead>
            <tr class="heading">
                <th align="center" style="width: 25%">Dibuat Oleh</th>
                <th align="center" colspan="2" style="width: 50%">Mengetahui</th>
                <th align="center" style="width: 25%">Disetujui Oleh</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="width: 25%"><br/><br/><br/><br/><br/></td>
                <td style="width: 25%"></td>
                <td style="width: 25%"></td>
                <td style="width: 25%"></td>
            </tr>
            <tr>
                <td align="center">Buyer</td>
                <td align="center">Finance</td>
                <td align="center">Operation Manager</td>
                <td align="center">General Manager</td>
            </tr>
        </tbody>
    </table>
</div>


@include('print.partials.footer')

<script type="text/javascript">
window.print();
</script>
