@include('print.partials.header')

<div class="content">
    
    <h1 class="title" style="text-align: right; margin-bottom: 0px;">PENYESUAIAN PERSEDIAAN</h1>
   
	<table class="table table-bordered">
		<thead>
			<tr class="heading">
				<th align="center" style="width: 25px;">No.</th>
				<th align="center">No Penyesuaian</th>
				<th align="center">Tanggal</th>
				<th align="center">Asal</th>
				<th align="center">Description></th>
				<th align="center">Status></th>
				<th align="center">Dibuat Oleh></th>
				<th align="center">Diubah Oleh></th>
			</tr>
		</thead>
		<tbody>
			<?php
			$no = 1;
			if (!empty($datagrid)) {
				foreach ($datagrid as $val) {
					?>
					<tr>
						<td align="center"><?php echo $no++; ?></td>
						<td><?php echo $val['code']; ?></td>
						<td align="center"><?php echo $val['date']; ?></td>
						<td align="center"><?php echo $val['source']; ?></td>
						<td align="center"><?php echo $val['description']; ?></td>
						<td align="center"><?php echo $val['status']; ?></td>
						<td align="center"><?php echo $val['created_user']; ?></td>
						<td align="center"><?php echo $val['changed_user']; ?></td>
					</tr>
			<?php
				}
			  }
			?>
		</tbody>
	</table>

</div>


@include('print.partials.footer')

<script type="text/javascript">
window.print();
</script>
