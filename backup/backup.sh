#!/bin/sh

#0 2 * * * /backup/backup.sh >/dev/null 2>&1
dbnames=('gmrp' 'gsms');
#host='localhost'
#user=''
#password=''
backupdir='/home/erpgoldenagin/backup/db'
oldbackupdir='/home/erpgoldenagin/backup/db_backup'

#basename of script name
me=`basename "$0"`

#temporary directory
#tmp=`mktemp -dt "$($me).XXXXX"`;
tmp=`mktemp -dt "$(basename $0).XXXXXX"`

cd "$tmp"
#echo $tmp

for dbname in ${dbnames[@]}; do

suffix=$(date +%Y%m%d%H%M%S)
pg_dump -Fp $dbname > "$dbname.$suffix.sql" 2> "$dbname.$suffix.log"
find . -iname "$dbname.$suffix*" | tar -czf "$backupdir/$dbname.$suffix.tgz" --files-from -

done

#remove temporary directory
rm -rf "$tmp"

#find backup files older than 10 days and move to another backup dir
find $backupdir -type f -iname "*.tgz" -mtime +10 -exec mv {} $oldbackupdir \;

#keep 30 days
find $oldbackudir -type f -iname "*.tgz" -mtime +30 -delete
